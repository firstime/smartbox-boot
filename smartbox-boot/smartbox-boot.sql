/*
Navicat MySQL Data Transfer

Source Server         : 39.100.149.97
Source Server Version : 50645
Source Host           : 39.100.149.97:3306
Source Database       : smartbox-boot

Target Server Type    : MYSQL
Target Server Version : 50645
File Encoding         : 65001

Date: 2019-12-07 02:16:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `demo`
-- ----------------------------
DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `key_word` varchar(255) DEFAULT NULL COMMENT '关键词',
  `punch_time` datetime DEFAULT NULL COMMENT '打卡时间',
  `salary_money` decimal(10,3) DEFAULT NULL COMMENT '工资',
  `bonus_money` double(10,2) DEFAULT NULL COMMENT '奖金',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别 {男:1,女:2}',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `content` varchar(1000) DEFAULT NULL COMMENT '个人简介',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of demo
-- ----------------------------
INSERT INTO `demo` VALUES ('08375a2dff80e821d5a158dd98302b23', '导入小虎', null, null, null, null, '2', '28', null, null, null, 'jeecg-boot', '2019-04-10 11:42:57', null, null, null);
INSERT INTO `demo` VALUES ('1182261206883254273', '', '', null, '0.000', '0.00', '', '0', null, '', '', 'test', '2019-10-10 11:46:51', '', null, 'A01');
INSERT INTO `demo` VALUES ('1182261229989675009', '', '', null, '0.000', '0.00', '', '0', null, '', '', 'test', '2019-10-10 11:46:57', '', null, 'A01');
INSERT INTO `demo` VALUES ('1c2ba51b29a42d9de02bbd708ea8121a', '777777', '777', '2018-12-07 19:43:17', null, null, null, '7', '2018-12-07', null, null, null, null, 'admin', '2019-02-21 18:26:04', null);
INSERT INTO `demo` VALUES ('1dc29e80be14d1400f165b5c6b30c707', 'zhang daihao', null, null, null, null, '2', null, null, 'zhangdaiscott@163.com', null, null, null, null, null, null);
INSERT INTO `demo` VALUES ('304e651dc769d5c9b6e08fb30457a602', '小白兔', null, null, null, null, '2', '28', null, null, null, 'scott', '2019-01-19 13:12:53', 'qinfeng', '2019-01-19 13:13:12', null);
INSERT INTO `demo` VALUES ('4', 'Sandy', '开源，很好', '2018-12-15 00:00:00', null, null, '2', '21', '2018-12-15', 'test4@baomidou.com', '聪明00', null, null, 'admin', '2019-02-25 16:29:27', null);
INSERT INTO `demo` VALUES ('42c08b1a2e5b2a96ffa4cc88383d4b11', '秦50090', null, '2019-01-05 20:33:31', null, null, null, '28', '2019-01-05', null, null, 'admin', '2019-01-19 20:33:54', 'admin', '2019-01-19 20:34:29', null);
INSERT INTO `demo` VALUES ('4436302a0de50bb83025286bc414d6a9', 'zhang daihao', null, null, null, null, null, null, null, 'zhangdaiscott@163.com', null, 'admin', '2019-01-19 15:39:04', null, null, null);
INSERT INTO `demo` VALUES ('4981637bf71b0c1ed1365241dfcfa0ea', '小虎', null, null, null, null, '2', '28', null, null, null, 'scott', '2019-01-19 13:12:53', 'qinfeng', '2019-01-19 13:13:12', null);
INSERT INTO `demo` VALUES ('5c16e6a5c31296bcd3f1053d5d118815', '导入zhangdaiscott', null, null, null, null, '1', null, '2019-01-03', null, null, 'jeecg-boot', '2019-04-10 11:42:57', 'admin', '2019-05-19 18:35:51', null);
INSERT INTO `demo` VALUES ('7', 'zhangdaiscott', null, null, null, null, '1', null, '2019-01-03', null, null, null, null, null, null, null);
INSERT INTO `demo` VALUES ('73bc58611012617ca446d8999379e4ac', '郭靖11a', '777', '2018-12-07 00:00:00', null, null, null, null, null, null, null, 'jeecg-boot', '2019-03-28 18:16:39', null, null, null);
INSERT INTO `demo` VALUES ('917e240eaa0b1b2d198ae869b64a81c3', 'zhang daihao', null, null, null, null, '2', '0', '2018-11-29', 'zhangdaiscott@163.com', null, null, null, null, null, null);
INSERT INTO `demo` VALUES ('94420c5d8fc4420dde1e7196154b3a24', '秦111', null, null, null, null, null, null, null, null, null, 'scott', '2019-01-19 12:54:58', 'qinfeng', '2019-01-19 13:12:10', null);
INSERT INTO `demo` VALUES ('95740656751c5f22e5932ab0ae33b1e4', '杨康22a', '奸臣', null, null, null, null, null, null, null, null, 'jeecg-boot', '2019-03-28 18:16:39', null, null, null);
INSERT INTO `demo` VALUES ('b86897900c770503771c7bb88e5d1e9b', 'scott1', '开源、很好、hello', null, null, null, '1', null, null, 'zhangdaiscott@163.com', null, 'scott', '2019-01-19 12:22:34', null, null, null);
INSERT INTO `demo` VALUES ('c0b7c3de7c62a295ab715943de8a315d', '秦风555', null, null, null, null, null, null, null, null, null, 'admin', '2019-01-19 13:18:30', 'admin', '2019-01-19 13:18:50', null);
INSERT INTO `demo` VALUES ('c28fa8391ef81d6fabd8bd894a7615aa', '小麦', null, null, null, null, '2', null, null, 'zhangdaiscott@163.com', null, 'jeecg-boot', '2019-04-04 17:18:09', null, null, null);
INSERT INTO `demo` VALUES ('c2c0d49e3c01913067cf8d1fb3c971d2', 'zhang daihao', null, null, null, null, '2', null, null, 'zhangdaiscott@163.com', null, 'admin', '2019-01-19 23:37:18', 'admin', '2019-01-21 16:49:06', null);
INSERT INTO `demo` VALUES ('c96279c666b4b82e3ef1e4e2978701ce', '报名时间', null, null, null, null, null, null, null, null, null, 'jeecg-boot', '2019-03-28 18:00:52', null, null, null);
INSERT INTO `demo` VALUES ('d24668721446e8478eeeafe4db66dcff', 'zhang daihao999', null, null, null, null, '1', null, null, 'zhangdaiscott@163.com', null, null, null, null, null, null);
INSERT INTO `demo` VALUES ('eaa6c1116b41dc10a94eae34cf990133', 'zhang daihao', null, null, null, null, null, null, null, 'zhangdaiscott@163.com', null, null, null, null, null, null);
INSERT INTO `demo` VALUES ('ffa9da1ad40632dfcabac51d766865bd', '秦999', null, null, null, null, null, null, null, null, null, 'admin', '2019-01-19 23:36:34', 'admin', '2019-02-14 17:30:43', null);

-- ----------------------------
-- Table structure for `dev_basic_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_basic_info`;
CREATE TABLE `dev_basic_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '序号',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `file_type` varchar(2) DEFAULT NULL COMMENT '文件类型',
  `site_id` varchar(32) DEFAULT NULL COMMENT '局点ID',
  `file_name` varchar(32) DEFAULT NULL COMMENT '文件名称',
  `file_path` varchar(60) DEFAULT NULL COMMENT '文件路径',
  `file_size` double(32,0) DEFAULT NULL COMMENT '文件大小',
  `collect_time` datetime DEFAULT NULL COMMENT '收集时间',
  `device_start_time` varchar(32) DEFAULT NULL COMMENT '设备启动时间',
  `device_run_time` varchar(500) DEFAULT NULL COMMENT '设备运行时间',
  `device_type` varchar(16) DEFAULT NULL COMMENT '设备类型',
  `device_name` varchar(200) DEFAULT NULL COMMENT '设备名称',
  `device_verison` varchar(200) DEFAULT NULL COMMENT '设备版本',
  `device_patch` varchar(32) DEFAULT NULL COMMENT '设备补丁',
  `device_status` int(1) DEFAULT '0' COMMENT '设备状态0异常1正常',
  `ip_address` varchar(32) DEFAULT NULL COMMENT 'IP地址',
  `isbkp` varchar(6) DEFAULT NULL COMMENT '是否备板',
  `bkp_board_type` varchar(32) DEFAULT NULL COMMENT '备板类型',
  `bkp_pcb_version` varchar(32) DEFAULT NULL COMMENT '备板编码',
  `description` varchar(255) DEFAULT NULL COMMENT '设备描述信息',
  `license_state` varchar(4) DEFAULT NULL COMMENT 'license状态',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'license名称',
  `product_version` varchar(255) DEFAULT NULL COMMENT '产品版本',
  `product_serial_no` varchar(32) DEFAULT NULL COMMENT '产品序列号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `expired_time` datetime DEFAULT NULL COMMENT '过期时间',
  `box_no` varchar(100) DEFAULT NULL COMMENT '盒子编号mac',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='设备基本信息表：网元基本信息，包括设备名称、设备版本、补丁等信息';

-- ----------------------------
-- Records of dev_basic_info
-- ----------------------------
INSERT INTO `dev_basic_info` VALUES ('008a2f44090911eabe2400163e043dd1', '662d318c3f74b1714d8af5e11f270bb3', null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quidway  S9306  Terabit  Routing  Switch', 'V200R013C00SPC500', null, '0', '192.168.1.101', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('009a9d27090911eabe2400163e043dd1', '662d318c3f74b1714d8af5e11f270bb3', null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quidway  S9306  Terabit  Routing  Switch', 'V200R013C00SPC500', null, '0', '192.168.1.102', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('1', 'e9ca23d68d884d4ebb19d07889727dae', '1', '1', 'test.json', 'xxx', '1024', '2019-09-06 00:00:00', '2019-09-07 18:37:39', '2019-09-07 18:37:44', 'S9300', 'xxxx', 'V200R013C00SPC500', 'null', '1', '192.168.1.100', 'true', 'LE0KS9306', 'E02BAKI VER.E', 'Quidway S9306 Terabit Routing Switch ', '生效', '华为商业授权', 'V1', 'XXXX', '2019-09-08 02:22:35', '2019-09-08 02:22:39', 'smbox1');
INSERT INTO `dev_basic_info` VALUES ('10', '10', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('12', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2', 'test.json', 'xxx', '1024', '2019-09-06 00:00:00', '2019-09-07 18:37:39', '2019-09-07 18:37:44', 'S9300', 'xxxx', 'V200R013C00SPC500', 'null', '0', '192.168.1.101', 'true', 'LE0KS9306', 'E02BAKI VER.E', 'Quidway S9306 Terabit Routing Switch ', '生效', '华为商业授权', 'V1', 'XXXX', '2019-09-08 02:22:35', '2019-09-08 02:22:39', 'smbox1');
INSERT INTO `dev_basic_info` VALUES ('13', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('14', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('15', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('16', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('17', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('18', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('19', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('2', '1', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('20', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('21', null, null, null, null, null, null, null, null, '14  weeks,  6  days,  16  hours,  10  minutes', null, 'Quid', 'V200R013C00SPC500', null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('3', '3', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('4', '4', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('5', '5', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('6', '6', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('7', '7', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('8', '8', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_basic_info` VALUES ('9', '9', null, null, null, null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `dev_board_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_board_info`;
CREATE TABLE `dev_board_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '序号',
  `basic_info_id` varchar(55) DEFAULT NULL COMMENT '基本信息表id',
  `chassis_id` varchar(55) DEFAULT NULL COMMENT '机框id',
  `board_type` varchar(4) DEFAULT NULL COMMENT '单板类型',
  `slot` varchar(4) DEFAULT NULL COMMENT '槽位号',
  `slot_status` int(1) DEFAULT NULL COMMENT '槽位状态',
  `board_name` varchar(32) DEFAULT NULL COMMENT '单板名称',
  `board_pcb_version` varchar(32) DEFAULT NULL COMMENT '单板版本',
  `cpu_usage_rate` double(32,2) DEFAULT NULL COMMENT 'CPU使用率',
  `mem_usage_rate` double(32,2) DEFAULT NULL COMMENT '内存使用率',
  `mem_used` varchar(32) DEFAULT NULL COMMENT '内存使用大小',
  `mem_total` varchar(32) DEFAULT NULL COMMENT '内存总大小',
  `sdram` varchar(32) DEFAULT NULL COMMENT 'SDRAM大小',
  `flash` varchar(32) DEFAULT NULL COMMENT 'flash大小',
  `nvram` varchar(32) DEFAULT NULL COMMENT 'nvram大小',
  `cfcard_mem` varchar(32) DEFAULT NULL COMMENT 'cfcard内存大小',
  `board_up_time` varchar(64) DEFAULT NULL COMMENT '设备启动时间',
  `user_count` varchar(32) DEFAULT NULL COMMENT '用户数',
  `ipv4_user_count` varchar(32) DEFAULT NULL COMMENT 'IPV4用户',
  `ipv6_user_count` varchar(32) DEFAULT NULL COMMENT 'IPV6用户',
  `mac_in_used` varchar(32) DEFAULT NULL COMMENT 'MAC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='设备单板基本信息表';

-- ----------------------------
-- Records of dev_board_info
-- ----------------------------
INSERT INTO `dev_board_info` VALUES ('', 'cc7de2a9eb4611e9be2400163e043dd1', 'cc8931f2eb4611e9be2400163e043dd1', null, null, null, 'MPU  7(Slave)', 'SWC02MFUD000', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0092c5d6090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'MPU  7(Slave)', 'SWC02MFUD000', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0096bb25090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'MPU  8(Master)', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0097d431090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'LPU  1', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('00989867090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'LPU  2', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('00991dd7090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'LPU  5', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0099a4b8090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', '0091fa96090911eabe2400163e043dd1', null, null, null, 'CMU  9(Master)', '', null, null, null, null, '', null, null, null, '4  weeks,  6  days,  16  hours,  8  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('009bab46090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'MPU  7(Slave)', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('009ce39a090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'MPU  8(Master)', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('009e19b1090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'LPU  1', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('009fba94090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'LPU  2', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('00a052ea090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'LPU  5', 'SWC02MFUD000  VER.B', null, null, null, null, '4096  M  bytes', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('00a13d1d090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', '009b3ee6090911eabe2400163e043dd1', null, null, null, 'CMU 9(Master)', '', null, null, null, null, '', null, null, null, '4 weeks, 6 days, 16 hours, 8 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c4230e9-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'MPU  7(Slave)', 'SWC02MFUD000', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c4d2d0b-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'MPU  8(Master)', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c587694-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  1', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c5d6475-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  2', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c635e64-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  5', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c68e56c-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'CMU  9(Master)', '', null, null, null, null, 'null', null, null, null, '4  weeks,  6  days,  16  hours,  8  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c78a6d2-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'MPU  7(Slave)', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c840d32-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'MPU  8(Master)', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c8dccda-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  1', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c92bbb1-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  2', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c97c93d-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'LPU  5', 'SWC02MFUD000  VER.B', null, null, null, null, 'null', null, null, null, '14  weeks,  6  days,  3  hours,  32  minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('0c9c7ecd-eb45-11e9-be24-00163e043dd1', '0', '0', null, null, null, 'CMU 9(Master)', '', null, null, null, null, 'null', null, null, null, '4 weeks, 6 days, 16 hours, 8 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('1', '1', '1', 'MPU', '7', '1', 'MPU 7(Slave)', 'SWC02MFUD000 VER.B', null, null, null, null, '4096', '64', '512', null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('10', '1', '2', 'LPU', '2', '0', 'LPU2', 'SWC02X32SX2S VER.A', null, null, null, null, '1024', '64', null, null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('11', '1', '2', 'LPU', '5', '0', 'LPU5', 'SWC02X32SX2S VER.A', null, null, null, null, '2048', '64', null, null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('12', '12', '2', 'CMU', '9', '1', 'CMU 9(Master)', 'LE02CMUA VER.B', null, null, null, null, null, null, null, null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('13', '3', '2', null, null, '0', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('2', '1', '1', 'MPU', '8', '0', 'MPU 8(Master)', 'SWC02MFUD000 VER.B', null, null, null, null, '4096', '64', '512', null, '4 weeks, 6 days, 16 hours, 9 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('3', '1', '1', 'LPU', '1', '0', 'LPU 1', 'SWC02X32SX2S VER.A', null, null, null, null, '1024', '64', null, null, '14 weeks, 6 days, 15 hours, 59 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('4', '1', '1', 'LPU', '2', '0', 'LPU 2', 'SWC02X32SX2S VER.A', null, null, null, null, '1024', '64', null, null, '14 weeks, 6 days, 15 hours, 59 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('5', '1', '1', 'LPU', '5', '0', 'LPU 5', 'SWC02G48TXF0 VER.A', null, null, null, null, '2048', '64', null, null, '14 weeks, 6 days, 16 hours, 2 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('6', '1', '1', 'CMU', '9', '0', 'CMU 9(Master)', 'LE02CMUA VER.B', null, null, null, null, null, null, null, null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('7', '1', '2', 'MPU', '7', '0', 'MPU 7(Slave)', 'SWC02MFUD000 VER.B', null, null, null, null, '4096', '64', '512', null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('8', '1', '2', 'MPU', '8', '0', 'MPU 8(Master)', 'SWC02MFUD000 VER.B', null, null, null, null, '4096', '64', '512', null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);
INSERT INTO `dev_board_info` VALUES ('9', '1', '2', 'LPU', '1', '0', 'LPU 1', 'SWC02X32SX2S VER.A', null, null, null, null, '1024', '64', null, null, '14 weeks, 6 days, 3 hours, 32 minutes', null, null, null, null);

-- ----------------------------
-- Table structure for `dev_box_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_box_info`;
CREATE TABLE `dev_box_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '盒子ID',
  `box_no` varchar(100) DEFAULT NULL COMMENT '盒子编号MAC',
  `box_status` int(1) DEFAULT NULL COMMENT '盒子状态',
  `box_detail` varchar(100) DEFAULT NULL COMMENT '盒子描述',
  `box_type` varchar(50) DEFAULT NULL COMMENT '设备ID',
  `user_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='盒子基本信息表-白名单';

-- ----------------------------
-- Records of dev_box_info
-- ----------------------------
INSERT INTO `dev_box_info` VALUES ('1', 'XXXX', '1', '北京方正电子', 'v1版本', '662d318c3f74b1714d8af5e11f270bb3');
INSERT INTO `dev_box_info` VALUES ('2', 'GGGG', '1', '联想', 'v1.1版本', 'e9ca23d68d884d4ebb19d07889727dae');
INSERT INTO `dev_box_info` VALUES ('3', 'smbox1', '1', '北京方正电子', 'v1版本', '662d318c3f74b1714d8af5e11f270bb3');

-- ----------------------------
-- Table structure for `dev_chassis_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_chassis_info`;
CREATE TABLE `dev_chassis_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '序号',
  `basicinfo_id` varchar(55) DEFAULT NULL COMMENT '设备基本信息表id',
  `board_id` varchar(55) DEFAULT NULL COMMENT '机框id',
  `chassis_type` varchar(12) DEFAULT NULL COMMENT '机框类型1-master;2-slave',
  `chassis_name` varchar(32) DEFAULT NULL COMMENT '机框名称',
  `device_type` varchar(32) DEFAULT NULL COMMENT '设备类型',
  `total_power` varchar(32) DEFAULT NULL COMMENT '电源大小',
  `need_power` varchar(32) DEFAULT NULL COMMENT '需要电源大小',
  `remain_power` varchar(32) DEFAULT NULL COMMENT '当前电源大小',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='机框基本信息表';

-- ----------------------------
-- Records of dev_chassis_info
-- ----------------------------
INSERT INTO `dev_chassis_info` VALUES ('0091fa96090911eabe2400163e043dd1', '008a2f44090911eabe2400163e043dd1', null, null, 'Chassis  1  (Master  Switch)', 'S9300', null, null, null);
INSERT INTO `dev_chassis_info` VALUES ('009b3ee6090911eabe2400163e043dd1', '009a9d27090911eabe2400163e043dd1', null, null, 'Chassis  2  (Master  Switch)', 'S9300', null, null, null);
INSERT INTO `dev_chassis_info` VALUES ('2', '11', '2', 'Standby', 'Chassis 2 (Standby Switch)', 'S9300', null, null, null);

-- ----------------------------
-- Table structure for `dev_devices`
-- ----------------------------
DROP TABLE IF EXISTS `dev_devices`;
CREATE TABLE `dev_devices` (
  `id` varchar(32) NOT NULL,
  `dev_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `dev_type` varchar(100) DEFAULT NULL COMMENT '字典编码',
  `dev_mac` varchar(100) DEFAULT NULL,
  `dev_box` varchar(100) DEFAULT NULL,
  `dev_status` int(1) DEFAULT NULL,
  `dev_updown` int(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除状态',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `has_child` varchar(3) DEFAULT NULL,
  `PRODUCT_FAMILY` varchar(100) DEFAULT NULL,
  `PRODUCT_NAME` varchar(100) DEFAULT NULL,
  `PRODUCT` varchar(100) DEFAULT NULL,
  `TRANSMIT_CAPACITY` varchar(100) DEFAULT NULL,
  `IP` varchar(100) DEFAULT NULL,
  `VERISION` varchar(100) DEFAULT NULL,
  `VERSION_START_TTIME` varchar(100) DEFAULT NULL,
  `PATCH` varchar(100) DEFAULT NULL,
  `PATCH_START_TIME` varchar(100) DEFAULT NULL,
  `REGION` varchar(100) DEFAULT NULL,
  `COUNTRY` varchar(100) DEFAULT NULL,
  `SITE_NAME` varchar(100) DEFAULT NULL,
  `GROUP_NAME` varchar(100) DEFAULT NULL,
  `NETWORK_TYPE` varchar(100) DEFAULT NULL,
  `NETWORK_NAME` varchar(100) DEFAULT NULL,
  `LOCATION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `indextable_dict_code` (`dev_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of dev_devices
-- ----------------------------
INSERT INTO `dev_devices` VALUES ('0b5d19e1fce4b2e6647e6b4a17760c14', '通告类型', 'msg_category', null, null, '1', '1', '消息类型1:通知公告2:系统消息', '0', 'admin', '2019-04-22 18:01:35', null, null, '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('236e8a4baff0db8c62c00dd95632834f', '同步工作流引擎', 'activiti_sync', null, null, null, null, '同步工作流引擎', '0', 'admin', '2019-05-15 15:27:33', null, null, '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('2e02df51611a4b9632828ab7e5338f00', '权限策略', 'perms_type', null, null, '2', '1', '权限策略', '0', 'admin', '2019-04-26 18:26:55', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('2f0320997ade5dd147c90130f7218c3e', '推送类别', 'msg_type', null, null, '23', null, '', '0', 'admin', '2019-03-17 21:21:32', 'admin', '2019-03-26 19:57:45', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('3486f32803bb953e7155dab3513dc68b', '删除状态', 'del_flag', null, null, null, null, null, '0', 'admin', '2019-01-18 21:46:26', 'admin', '2019-03-30 11:17:11', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('3d9a351be3436fbefb1307d4cfb49bf2', '性别', 'sex', null, null, null, null, null, '0', null, '2019-01-04 14:56:32', 'admin', '2019-03-30 11:28:27', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('404a04a15f371566c658ee9ef9fc392a', 'cehis2', '22', null, null, null, null, null, '1', 'admin', '2019-01-30 11:17:21', 'admin', '2019-03-30 11:18:12', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4274efc2292239b6f000b153f50823ff', '全局权限策略', 'global_perms_type', null, null, null, null, '全局权限策略', '0', 'admin', '2019-05-10 17:54:05', null, null, '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4c03fca6bf1f0299c381213961566349', 'Online图表展示模板', 'online_graph_display_template', null, null, null, null, 'Online图表展示模板', '0', 'admin', '2019-04-12 17:28:50', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4c753b5293304e7a445fd2741b46529d', '字典状态', 'dict_item_status', null, null, null, null, null, '0', 'admin', '2020-06-18 23:18:42', 'admin', '2019-03-30 19:33:52', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4d7fec1a7799a436d26d02325eff295e', '优先级', 'priority', null, null, '4', null, '优先级', '0', 'admin', '2019-03-16 17:03:34', 'admin', '2019-04-16 17:39:23', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4e4602b3e3686f0911384e188dc7efb4', '条件规则', 'rule_conditions', null, null, null, null, '', '0', 'admin', '2019-04-01 10:15:03', 'admin', '2019-04-01 10:30:47', '11', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('4f69be5f507accea8d5df5f11346181a', '发送消息类型', 'msgType', null, null, null, null, null, '0', 'admin', '2019-04-11 14:27:09', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('68168534ff5065a152bfab275c2136f8', '有效无效状态', 'valid_status', null, null, null, null, '有效无效状态', '0', 'admin', '2020-09-26 19:21:14', 'admin', '2019-04-26 19:21:23', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('6b78e3f59faec1a4750acff08030a79b', '用户类型', 'user_type', null, null, null, null, null, '1', null, '2019-01-04 14:59:01', 'admin', '2019-03-18 23:28:18', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('72cce0989df68887546746d8f09811aa', 'Online表单类型', 'cgform_table_type', null, null, null, null, '', '0', 'admin', '2019-01-27 10:13:02', 'admin', '2019-03-30 11:37:36', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('78bda155fe380b1b3f175f1e88c284c6', '流程状态', 'bpm_status', null, null, null, null, '流程状态', '0', 'admin', '2019-05-09 16:31:52', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('83bfb33147013cc81640d5fd9eda030c', '日志类型', 'log_type', null, null, null, null, null, '0', 'admin', '2019-03-18 23:22:19', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('845da5006c97754728bf48b6a10f79cc', '状态', 'status', null, null, null, null, null, '1', 'admin', '2019-03-18 21:45:25', 'admin', '2019-03-18 21:58:25', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('880a895c98afeca9d9ac39f29e67c13e', '操作类型', 'operate_type', null, null, null, null, '操作类型', '0', 'admin', '2019-07-22 10:54:29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('8dfe32e2d29ea9430a988b3b558bf233', '发布状态', 'send_status', null, null, null, null, '发布状态', '0', 'admin', '2019-04-16 17:40:42', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('a7adbcd86c37f7dbc9b66945c82ef9e6', '1是0否', 'yn', null, null, null, null, '', '1', 'admin', '2019-05-22 19:29:29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('a9d9942bd0eccb6e89de92d130ec4c4a', '消息发送状态', 'msgSendStatus', null, null, null, null, null, '0', 'admin', '2019-04-12 18:18:17', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('ac2f7c0c5c5775fcea7e2387bcb22f01', '菜单类型', 'menu_type', null, null, null, null, null, '0', 'admin', '2020-12-18 23:24:32', 'admin', '2019-04-01 15:27:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('ad7c65ba97c20a6805d5dcdf13cdaf36', 'onlineT类型', 'ceshi_online', null, null, null, null, null, '1', 'admin', '2019-03-22 16:31:49', 'admin', '2019-03-22 16:34:16', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('bd1b8bc28e65d6feefefb6f3c79f42fd', 'Online图表数据类型', 'online_graph_data_type', null, null, null, null, 'Online图表数据类型', '0', 'admin', '2019-04-12 17:24:24', 'admin', '2019-04-12 17:24:57', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('c36169beb12de8a71c8683ee7c28a503', '部门状态', 'depart_status', null, null, null, null, null, '0', 'admin', '2019-03-18 21:59:51', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('c5a14c75172783d72cbee6ee7f5df5d1', 'Online图表类型', 'online_graph_type', null, null, null, null, 'Online图表类型', '0', 'admin', '2019-04-12 17:04:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('d6e1152968b02d69ff358c75b48a6ee1', '流程类型', 'bpm_process_type', null, null, null, null, null, '0', 'admin', '2021-02-22 19:26:54', 'admin', '2019-03-30 18:14:44', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `dev_devices` VALUES ('fc6cd58fde2e8481db10d3a1e68ce70c', '用户状态', 'user_status', null, null, null, null, null, '0', 'admin', '2019-03-18 21:57:25', 'admin', '2019-03-18 23:11:58', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `dev_devices_item`
-- ----------------------------
DROP TABLE IF EXISTS `dev_devices_item`;
CREATE TABLE `dev_devices_item` (
  `id` varchar(32) NOT NULL,
  `dev_id` varchar(32) DEFAULT NULL COMMENT '字典id',
  `item_text` varchar(100) DEFAULT NULL COMMENT '字典项文本',
  `item_value` varchar(100) DEFAULT NULL COMMENT '字典项值',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `sort_order` int(10) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态（1启用 0不启用）',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `CHASIS_TYPE` varchar(100) DEFAULT NULL COMMENT '框分类',
  `SLOT` varchar(100) DEFAULT NULL COMMENT '单板槽位号',
  `BOARD_TYPE` varchar(100) DEFAULT NULL COMMENT '单板类型',
  `MEM__TOTAL` varchar(100) DEFAULT NULL COMMENT '单板内存（MB）',
  `CF_CARD_MEM` varchar(100) DEFAULT NULL COMMENT 'cf卡容量（MB）',
  `CF_CARD_AVAIL` varchar(100) DEFAULT NULL COMMENT 'cf卡剩余空间',
  `CPU_USAGE` varchar(100) DEFAULT NULL COMMENT 'CPU使用率',
  `MEM_USAGE` varchar(100) DEFAULT NULL COMMENT '单板内存利用率',
  `MEM_USED` varchar(100) DEFAULT NULL COMMENT '内存使用量',
  `BOARD_NAME` varchar(100) DEFAULT NULL COMMENT '单板名称',
  `REAL_TRANSMIT_CAP` varchar(100) DEFAULT NULL COMMENT '实际转发能力',
  `BOARD_CODE` varchar(100) DEFAULT NULL COMMENT '单板编码',
  `SLOT_AVAIL` varchar(100) DEFAULT NULL COMMENT '剩余槽位数',
  `BOARD_USER_COUNT` varchar(100) DEFAULT NULL COMMENT '单板用户数',
  `DEV_USER_COUNT` varchar(100) DEFAULT NULL COMMENT '整机用户数',
  `LICENSE_USER_COUNT` varchar(100) DEFAULT NULL COMMENT 'License用户数',
  `SDRAM` varchar(100) DEFAULT NULL COMMENT 'SDRAM大小',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_dict_id` (`dev_id`) USING BTREE,
  KEY `index_table_sort_order` (`sort_order`) USING BTREE,
  KEY `index_table_dict_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of dev_devices_item
-- ----------------------------

-- ----------------------------
-- Table structure for `dev_interface_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_interface_info`;
CREATE TABLE `dev_interface_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '序号',
  `basicinfo_id` varchar(55) DEFAULT NULL COMMENT '设备ID',
  `interface_name` varchar(32) DEFAULT NULL COMMENT '接口名称',
  `master_inter_name` varchar(55) DEFAULT NULL COMMENT '上层接口名称',
  `interface_type` varchar(8) DEFAULT NULL COMMENT '接口类型',
  `interface_slot` varchar(32) DEFAULT NULL COMMENT '接口槽位号',
  `in_uti` varchar(32) DEFAULT NULL COMMENT '入口利用率',
  `out_uti` varchar(32) DEFAULT NULL COMMENT '出口利用率',
  `phy_status` varchar(4) DEFAULT NULL COMMENT '物理口状态',
  `protocal_status` varchar(32) DEFAULT NULL COMMENT '协议口状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='接口基本信息';

-- ----------------------------
-- Records of dev_interface_info
-- ----------------------------
INSERT INTO `dev_interface_info` VALUES ('1', '1', 'Eth-Trunk1', null, 'NA', null, '0%', '0%', 'up', 'up');
INSERT INTO `dev_interface_info` VALUES ('10', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('11', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('12', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('13', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('14', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('15', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('16', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('17', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('18', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('19', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('2', '1', ' XGigabitEthernet1/2/0/8 ', null, '10G', '1/2/0/8 ', '0%', '0%', 'up', 'up');
INSERT INTO `dev_interface_info` VALUES ('20', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('21', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('22', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('24', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('25', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('3', '1', null, null, null, null, null, null, 'up', 'up');
INSERT INTO `dev_interface_info` VALUES ('4', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('5', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('6', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('7', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('8', '1', null, null, null, null, null, null, null, null);
INSERT INTO `dev_interface_info` VALUES ('9', '1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `dev_subboard_info`
-- ----------------------------
DROP TABLE IF EXISTS `dev_subboard_info`;
CREATE TABLE `dev_subboard_info` (
  `id` varchar(55) NOT NULL DEFAULT '' COMMENT '序号',
  `board_id` varchar(55) DEFAULT NULL COMMENT '单板编号',
  `picName` varchar(32) DEFAULT NULL COMMENT '子卡名称',
  `pic_slot` varchar(4) DEFAULT NULL COMMENT '子卡槽位号',
  `pic_version` varchar(32) DEFAULT NULL COMMENT '子卡版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dev_subboard_info
-- ----------------------------
INSERT INTO `dev_subboard_info` VALUES ('', 'cc8e3359eb4611e9be2400163e043dd1', null, null, 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('0095e7d5090911eabe2400163e043dd1', '0092c5d6090911eabe2400163e043dd1', 'LE1D2VS04000', '1', 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('00973d62090911eabe2400163e043dd1', '0096bb25090911eabe2400163e043dd1', 'LE1D2VS04000', '1', 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('009c1d97090911eabe2400163e043dd1', '009bab46090911eabe2400163e043dd1', 'LE1D2VS04000', '1', 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('009db582090911eabe2400163e043dd1', '009ce39a090911eabe2400163e043dd1', 'LE1D2VS04000', '1', 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('0c48603e-eb45-11e9-be24-00163e043dd1', '0', null, null, 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('0c52db07-eb45-11e9-be24-00163e043dd1', '0', null, null, 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('0c7eddf3-eb45-11e9-be24-00163e043dd1', '0', null, null, 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('0c88d4eb-eb45-11e9-be24-00163e043dd1', '0', null, null, 'SWC02VS04000  VER.A');
INSERT INTO `dev_subboard_info` VALUES ('1', '1', 'LE1D2VS04000', '1', 'SWC02VS04000 VER.A');
INSERT INTO `dev_subboard_info` VALUES ('10', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('11', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('12', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('2', '2', 'LE1D2VS04000', '1', 'SWC02VS04000 VER.A');
INSERT INTO `dev_subboard_info` VALUES ('3', '7', 'LE1D2VS04000', '1', 'SWC02VS04000');
INSERT INTO `dev_subboard_info` VALUES ('4', '8', 'LE1D2VS04000', '1', 'SWC02VS04000');
INSERT INTO `dev_subboard_info` VALUES ('5', '1', null, '2', null);
INSERT INTO `dev_subboard_info` VALUES ('6', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('7', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('8', '1', null, null, null);
INSERT INTO `dev_subboard_info` VALUES ('9', '1', null, null, null);

-- ----------------------------
-- Table structure for `jeecg_monthly_growth_analysis`
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_monthly_growth_analysis`;
CREATE TABLE `jeecg_monthly_growth_analysis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL COMMENT '月份',
  `main_income` decimal(18,2) DEFAULT '0.00' COMMENT '佣金/主营收入',
  `other_income` decimal(18,2) DEFAULT '0.00' COMMENT '其他收入',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jeecg_monthly_growth_analysis
-- ----------------------------
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('1', '2018', '1月', '114758.90', '4426054.19');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('2', '2018', '2月', '8970734.12', '1230188.67');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('3', '2018', '3月', '26755421.23', '2048836.84');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('4', '2018', '4月', '2404990.63', '374171.44');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('5', '2018', '5月', '5450793.02', '502306.10');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('6', '2018', '6月', '17186212.11', '1375154.97');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('7', '2018', '7月', '579975.67', '461483.99');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('8', '2018', '8月', '1393590.06', '330403.76');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('9', '2018', '9月', '735761.21', '1647474.92');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('10', '2018', '10月', '1670442.44', '3423368.33');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('11', '2018', '11月', '2993130.34', '3552024.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('12', '2018', '12月', '4206227.26', '3645614.92');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('13', '2019', '1月', '483834.66', '418046.77');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('14', '2019', '2月', '11666578.65', '731352.20');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('15', '2019', '3月', '27080982.08', '1878538.81');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('16', '2019', '4月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('17', '2019', '5月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('18', '2019', '6月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('19', '2019', '7月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('20', '2019', '8月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('21', '2019', '9月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('22', '2019', '10月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('23', '2019', '11月', '0.00', '0.00');
INSERT INTO `jeecg_monthly_growth_analysis` VALUES ('24', '2019', '12月', '0.00', '0.00');

-- ----------------------------
-- Table structure for `jeecg_order_customer`
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_customer`;
CREATE TABLE `jeecg_order_customer` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '客户名',
  `sex` varchar(4) DEFAULT NULL COMMENT '性别',
  `idcard` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `idcard_pic` varchar(500) DEFAULT NULL COMMENT '身份证扫描件',
  `telphone` varchar(32) DEFAULT NULL COMMENT '电话1',
  `order_id` varchar(32) NOT NULL COMMENT '外键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jeecg_order_customer
-- ----------------------------
INSERT INTO `jeecg_order_customer` VALUES ('15538561502720', '3333', '1', '', null, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15538561512681', '3332333', '2', '', null, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', 'admin', '2019-03-29 18:43:12');
INSERT INTO `jeecg_order_customer` VALUES ('15538561550142', '4442', '2', '', null, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15541168497342', '444', '', '', '', '', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15541168499553', '5555', '', '', '', '', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15541169272690', '小王1', '1', '', '', '18611788525', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15541169288141', '效力1', '1', '', '', '18611788525', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15541169441372', '小红1', '1', '', '', '18611788525', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15543695362380', '1111', '', '', '', '', '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15543695397221', '222', '', '', '', '', '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('15543695398992', '333', '', '', '', '', '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('18dc5eb1068ccdfe90e358951ca1a3d6', 'dr2', '', '', '', '', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('195d280490fe88ca1475512ddcaf2af9', '12', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('217a2bf83709775d2cd85bf598392327', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('22bc052ae53ed09913b946abba93fa89', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('23bafeae88126c3bf3322a29a04f0d5e', 'x秦风', null, null, null, null, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('25c4a552c6843f36fad6303bfa99a382', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('2d32144e2bee63264f3f16215c258381', '33333', '2', null, null, null, 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('2d43170d6327f941bd1a017999495e25', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('2e5f62a8b6e0a0ce19b52a6feae23d48', '3', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('313abf99558ac5f13ecca3b87e562ad1', 'scott', '2', null, null, null, 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2019-02-25 16:29:48', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('34a1c5cf6cee360ed610ed0bed70e0f9', '导入秦风', null, null, null, null, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('3c87400f8109b4cf43c5598f0d40e34d', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('40964bcbbecb38e5ac15e6d08cf3cd43', '233', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('41e3dee0b0b6e6530eccb7fbb22fd7a3', '4555', '1', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('4808ae8344c7679a4a2f461db5dc3a70', '44', '1', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('4b6cef12f195fad94d57279b2241770d', 'dr12', '', '', '', '', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('524e695283f8e8c256cc24f39d6d8542', '小王', '2', '370285198604033222', null, '18611788674', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('57c2a8367db34016114cbc9fa368dba0', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('5df36a1608b8c7ac99ad9bc408fe54bf', '4', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('6b694e9ba54bb289ae9cc499e40031e7', 'x秦风', '1', null, null, null, 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2019-02-25 16:29:48', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('6c6fd2716c2dcd044ed03c2c95d261f8', '李四', '2', '370285198602058833', '', '18611788676', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('742d008214dee0afff2145555692973e', '秦风', '1', '370285198602058822', null, '18611788676', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('7469c3e5d371767ff90a739d297689b5', '导入秦风', '2', null, null, null, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:02');
INSERT INTO `jeecg_order_customer` VALUES ('7a96e2c7b24847d4a29940dbc0eda6e5', 'drscott', null, null, null, null, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('7f5a40818e225ee18bda6da7932ac5f9', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('8011575abfd7c8085e71ff66df1124b9', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('8404f31d7196221a573c9bd6c8f15003', '小张', '1', '370285198602058211', null, '18611788676', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('859020e10a2f721f201cdbff78cf7b9f', 'scott', null, null, null, null, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('8cc3c4d26e3060975df3a2adb781eeb4', 'dr33', null, null, null, null, 'b2feb454e43c46b2038768899061e464', 'jeecg-boot', '2019-04-04 17:23:09', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('8d1725c23a6a50685ff0dedfd437030d', '4', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('933cae3a79f60a93922d59aace5346ce', '小王', null, '370285198604033222', null, '18611788674', '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('9bdb5400b709ba4eaf3444de475880d7', 'dr22', null, null, null, null, '22c17790dcd04b296c4a2a089f71895f', 'jeecg-boot', '2019-04-04 17:23:09', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('9f87677f70e5f864679314389443a3eb', '33', '2', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('a2c2b7101f75c02deb328ba777137897', '44', '2', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('ab4d002dc552c326147e318c87d3bed4', 'ddddd', '1', '370285198604033222', null, '18611755848', '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2019-04-04 17:30:47', 'admin', '2019-04-04 17:31:17');
INSERT INTO `jeecg_order_customer` VALUES ('ad116f722a438e5f23095a0b5fcc8e89', 'dr秦风', null, null, null, null, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('b1ba147b75f5eaa48212586097fc3fd1', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('b43bf432c251f0e6b206e403b8ec29bc', 'lisi', null, null, null, null, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('bcdd300a7d44c45a66bdaac14903c801', '33', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('beb983293e47e2dc1a9b3d649aa3eb34', 'ddd3', null, null, null, null, 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('c219808196406f1b8c7f1062589de4b5', '44', '1', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('c8ed061d4b27c0c7a64e100f2b1c8ab5', '张经理', '2', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('cc5de4af7f06cd6d250965ebe92a0395', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('cf8817bd703bf7c7c77a2118edc26cc7', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('d72b26fae42e71270fce2097a88da58a', '导入scott', null, 'www', null, null, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:05');
INSERT INTO `jeecg_order_customer` VALUES ('dbdc60a6ac1a8c43f24afee384039b68', 'xiaowang', null, null, null, null, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('dc5883b50466de94d900919ed96d97af', '33', '1', '370285198602058823', null, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('deeb73e553ad8dc0a0b3cfd5a338de8e', '3333', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('e2570278bf189ac05df3673231326f47', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('e39cb23bb950b2bdedfc284686c6128a', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('e46fe9111a9100844af582a18a2aa402', '1', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('ee7af0acb9beb9bf8d8b3819a8a7fdc3', '2', null, null, null, null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('f5d2605e844192d9e548f9bd240ac908', '小张', null, '370285198602058211', null, '18611788676', '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_customer` VALUES ('f6db6547382126613a3e46e7cd58a5f2', '导入scott', null, null, null, null, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', null, null);

-- ----------------------------
-- Table structure for `jeecg_order_main`
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_main`;
CREATE TABLE `jeecg_order_main` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `order_code` varchar(50) DEFAULT NULL COMMENT '订单号',
  `ctype` varchar(500) DEFAULT NULL COMMENT '订单类型',
  `order_date` datetime DEFAULT NULL COMMENT '订单日期',
  `order_money` double(10,3) DEFAULT NULL COMMENT '订单金额',
  `content` varchar(500) DEFAULT NULL COMMENT '订单备注',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jeecg_order_main
-- ----------------------------
INSERT INTO `jeecg_order_main` VALUES ('1169255488991289346', '432432', null, null, null, null, 'admin', '2019-09-04 14:26:47', null, null);
INSERT INTO `jeecg_order_main` VALUES ('1169255599192432642', '42343252532', null, null, null, null, 'admin', '2019-09-04 14:27:13', null, null);
INSERT INTO `jeecg_order_main` VALUES ('1169255656679563266', '56666666', null, null, null, null, 'admin', '2019-09-04 14:27:27', null, null);
INSERT INTO `jeecg_order_main` VALUES ('1169255686865969154', '77777', null, null, null, null, 'admin', '2019-09-04 14:27:34', null, null);
INSERT INTO `jeecg_order_main` VALUES ('163e2efcbc6d7d54eb3f8a137da8a75a', 'B100', null, null, '3000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_main` VALUES ('3a867ebf2cebce9bae3f79676d8d86f3', '导入B100', '2222', null, '3000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:13');
INSERT INTO `jeecg_order_main` VALUES ('4bca3ea6881d39dbf67ef1e42c649766', '1212', null, null, null, null, 'admin', '2019-04-03 10:55:43', null, null);
INSERT INTO `jeecg_order_main` VALUES ('4cba137333127e8e31df7ad168cc3732', '青岛订单A0001', '2', '2019-04-03 10:56:07', null, null, 'admin', '2019-04-03 10:56:11', null, null);
INSERT INTO `jeecg_order_main` VALUES ('54e739bef5b67569c963c38da52581ec', 'NC911', '1', '2019-02-18 09:58:51', '40.000', null, 'admin', '2019-02-18 09:58:47', 'admin', '2019-02-18 09:58:59');
INSERT INTO `jeecg_order_main` VALUES ('5d6e2b9e44037526270b6206196f6689', 'N333', null, '2019-04-04 17:19:11', null, '聪明00', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_main` VALUES ('6a719071a29927a14f19482f8693d69a', 'c100', null, null, '5000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_main` VALUES ('8ab1186410a65118c4d746eb085d3bed', '导入400', '1', '2019-02-18 09:58:51', '40.000', null, 'admin', '2019-02-18 09:58:47', 'admin', '2019-02-18 09:58:59');
INSERT INTO `jeecg_order_main` VALUES ('9a57c850e4f68cf94ef7d8585dbaf7e6', 'halou100dd', null, '2019-04-04 17:30:32', null, null, 'admin', '2019-04-04 17:30:41', 'admin', '2019-04-04 17:31:08');
INSERT INTO `jeecg_order_main` VALUES ('a2cce75872cc8fcc47f78de9ffd378c2', '导入B100', null, null, '3000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_main` VALUES ('b190737bd04cca8360e6f87c9ef9ec4e', 'B0018888', '1', null, null, null, 'admin', '2019-02-15 18:39:29', 'admin', '2019-02-15 18:39:37');
INSERT INTO `jeecg_order_main` VALUES ('d908bfee3377e946e59220c4a4eb414a', 'SSSS001', null, null, '599.000', null, 'admin', '2019-04-01 15:43:03', 'admin', '2019-04-01 16:26:52');
INSERT INTO `jeecg_order_main` VALUES ('e73434dad84ebdce2d4e0c2a2f06d8ea', '导入200', null, null, '3000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_main` VALUES ('eb13ab35d2946a2b0cfe3452bca1e73f', 'BJ9980', '1', null, '90.000', null, 'admin', '2019-02-16 17:36:42', 'admin', '2019-02-16 17:46:16');
INSERT INTO `jeecg_order_main` VALUES ('f618a85b17e2c4dd58d268220c8dd9a1', 'N001', null, '2019-04-01 19:09:02', '2222.000', null, 'admin', '2019-04-01 19:09:47', 'admin', '2019-04-01 19:10:00');
INSERT INTO `jeecg_order_main` VALUES ('f71f7f8930b5b6b1703d9948d189982b', 'BY911', null, '2019-04-06 19:08:39', null, null, 'admin', '2019-04-01 16:36:02', 'admin', '2019-04-01 16:36:08');
INSERT INTO `jeecg_order_main` VALUES ('f8889aaef6d1bccffd98d2889c0aafb5', 'A100', null, '2018-10-10 00:00:00', '6000.000', null, 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_main` VALUES ('fe81ee5d19bbf9eef2066d4f29dfbe0f', 'uuuu', null, null, null, null, 'jeecg-boot', '2019-04-03 11:00:39', null, null);

-- ----------------------------
-- Table structure for `jeecg_order_ticket`
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_ticket`;
CREATE TABLE `jeecg_order_ticket` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `ticket_code` varchar(100) NOT NULL COMMENT '航班号',
  `tickect_date` datetime DEFAULT NULL COMMENT '航班时间',
  `order_id` varchar(32) NOT NULL COMMENT '外键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jeecg_order_ticket
-- ----------------------------
INSERT INTO `jeecg_order_ticket` VALUES ('0f0e3a40a215958f807eea08a6e1ac0a', '88', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('0fa3bd0bbcf53650c0bb3c0cac6d8cb7', 'ffff', '2019-02-21 00:00:00', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('14221afb4f5f749c1deef26ac56fdac3', '33', '2019-03-09 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15538561502730', '222', null, '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15538561526461', '2244', '2019-03-29 00:00:00', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', 'admin', '2019-03-29 18:43:26');
INSERT INTO `jeecg_order_ticket` VALUES ('15541168478913', 'hhhhh', null, 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15541169272810', '22211', '2019-04-01 19:09:40', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15541169302331', '333311', '2019-04-01 19:09:40', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15541169713092', '333311', '2019-04-01 19:09:47', 'f618a85b17e2c4dd58d268220c8dd9a1', 'admin', '2019-04-01 19:10:07', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15542604293170', 'c', null, 'fe81ee5d19bbf9eef2066d4f29dfbe0f', 'jeecg-boot', '2019-04-03 11:00:39', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15542604374431', 'd', null, 'fe81ee5d19bbf9eef2066d4f29dfbe0f', 'jeecg-boot', '2019-04-03 11:00:39', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15543695362380', 'ccc2', null, '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15543695381291', 'cccc1', null, '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('15543695740352', 'dddd', null, '5d6e2b9e44037526270b6206196f6689', 'admin', '2019-04-04 17:19:40', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('18905bc89ee3851805aab38ed3b505ec', '44', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('1f809cbd26f4e574697e1c10de575d72', 'A100', null, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('21051adb51529bdaa8798b5a3dd7f7f7', 'C10029', '2019-02-20 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('269576e766b917f8b6509a2bb0c4d4bd', 'A100', null, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('2d473ffc79e5b38a17919e15f8b7078e', '66', '2019-03-29 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('3655b66fca5fef9c6aac6d70182ffda2', 'AA123', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('365d5919155473ade45840fd626c51a9', 'dddd', '2019-04-04 17:25:29', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('4889a782e78706ab4306a925cfb163a5', 'C34', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:35:00', 'admin', '2019-04-01 16:35:07');
INSERT INTO `jeecg_order_ticket` VALUES ('48d385796382cf87fa4bdf13b42d9a28', '导入A100', null, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('541faed56efbeb4be9df581bd8264d3a', '88', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('57a27a7dfd6a48e7d981f300c181b355', '6', '2019-03-30 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('5ce4dc439c874266e42e6c0ff8dc8b5c', '导入A100', null, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('5f16e6a64ab22a161bd94cc205f2c662', '222', '2019-02-23 00:00:00', 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2019-02-25 16:29:48', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('645a06152998a576c051474157625c41', '88', '2019-04-04 17:25:31', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('6e3562f2571ea9e96b2d24497b5f5eec', '55', '2019-03-23 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('8fd2b389151568738b1cc4d8e27a6110', '导入A100', null, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('93f1a84053e546f59137432ff5564cac', '55', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('969ddc5d2e198d50903686917f996470', 'A10029', '2019-04-01 00:00:00', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('96e7303a8d22a5c384e08d7bcf7ac2bf', 'A100', null, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('9e8a3336f6c63f558f2b68ce2e1e666e', 'dddd', null, '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2019-04-04 17:30:55', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('a28db02c810c65660015095cb81ed434', 'A100', null, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('b217bb0e4ec6a45b6cbf6db880060c0f', 'A100', null, '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('ba708df70bb2652ed1051a394cfa0bb3', '333', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('beabbfcb195d39bedeeafe8318794562', 'A1345', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:04', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('bf450223cb505f89078a311ef7b6ed16', '777', '2019-03-30 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c06165b6603e3e1335db187b3c841eef', 'fff', null, '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2019-04-04 17:30:58', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c113136abc26ace3a6da4e41d7dc1c7e', '44', '2019-03-15 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c1abdc2e30aeb25de13ad6ee3488ac24', '77', '2019-03-22 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c23751a7deb44f553ce50a94948c042a', '33', '2019-03-09 00:00:00', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c64547666b634b3d6a0feedcf05f25ce', 'C10019', '2019-04-01 00:00:00', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('c8b8d3217f37da78dddf711a1f7da485', 'A100', null, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('cab691c1c1ff7a6dfd7248421917fd3c', 'A100', null, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('cca10a9a850b456d9b72be87da7b0883', '77', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('d2fbba11f4814d9b1d3cb1a3f342234a', 'C10019', '2019-02-18 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('d746c1ed956a562e97eef9c6faf94efa', '111', '2019-02-01 00:00:00', 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2019-02-25 16:29:48', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('dbdb07a16826808e4276e84b2aa4731a', '导入A100', null, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('e7075639c37513afc0bbc4bf7b5d98b9', '88', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('fa759dc104d0371f8aa28665b323dab6', '888', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);
INSERT INTO `jeecg_order_ticket` VALUES ('ff197da84a9a3af53878eddc91afbb2e', '33', null, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', null, null);

-- ----------------------------
-- Table structure for `jeecg_project_nature_income`
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_project_nature_income`;
CREATE TABLE `jeecg_project_nature_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nature` varchar(50) NOT NULL COMMENT '项目性质',
  `insurance_fee` decimal(18,2) DEFAULT '0.00' COMMENT '保险经纪佣金费',
  `risk_consulting_fee` decimal(18,2) DEFAULT '0.00' COMMENT '风险咨询费',
  `evaluation_fee` decimal(18,2) DEFAULT '0.00' COMMENT '承保公估评估费',
  `insurance_evaluation_fee` decimal(18,2) DEFAULT '0.00' COMMENT '保险公估费',
  `bidding_consulting_fee` decimal(18,2) DEFAULT '0.00' COMMENT '投标咨询费',
  `interol_consulting_fee` decimal(18,2) DEFAULT '0.00' COMMENT '内控咨询费',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jeecg_project_nature_income
-- ----------------------------
INSERT INTO `jeecg_project_nature_income` VALUES ('1', '市场化-电商业务', '4865.41', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('2', '统筹型', '35767081.88', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('3', '市场化-非股东', '1487045.35', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('4', '市场化-参控股', '382690.56', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('5', '市场化-员工福利', '256684.91', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('6', '市场化-再保险', '563451.03', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('7', '市场化-海外业务', '760576.25', '770458.75', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `jeecg_project_nature_income` VALUES ('8', '市场化-风险咨询', '910183.93', '0.00', '0.00', '0.00', '0.00', '226415.09');

-- ----------------------------
-- Table structure for `joa_demo`
-- ----------------------------
DROP TABLE IF EXISTS `joa_demo`;
CREATE TABLE `joa_demo` (
  `id` varchar(32) DEFAULT NULL COMMENT 'ID',
  `name` varchar(100) DEFAULT NULL COMMENT '请假人',
  `days` int(11) DEFAULT NULL COMMENT '请假天数',
  `begin_date` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime DEFAULT NULL COMMENT '请假结束时间',
  `reason` varchar(500) DEFAULT NULL COMMENT '请假原因',
  `bpm_status` varchar(50) DEFAULT '1' COMMENT '流程状态',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='流程测试';

-- ----------------------------
-- Records of joa_demo
-- ----------------------------

-- ----------------------------
-- Table structure for `onl_cgform_button`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_button`;
CREATE TABLE `onl_cgform_button` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `BUTTON_CODE` varchar(50) DEFAULT NULL COMMENT '按钮编码',
  `BUTTON_ICON` varchar(20) DEFAULT NULL COMMENT '按钮图标',
  `BUTTON_NAME` varchar(50) DEFAULT NULL COMMENT '按钮名称',
  `BUTTON_STATUS` varchar(2) DEFAULT NULL COMMENT '按钮状态',
  `BUTTON_STYLE` varchar(20) DEFAULT NULL COMMENT '按钮样式',
  `EXP` varchar(255) DEFAULT NULL COMMENT '表达式',
  `CGFORM_HEAD_ID` varchar(32) DEFAULT NULL COMMENT '表单ID',
  `OPT_TYPE` varchar(20) DEFAULT NULL COMMENT '按钮类型',
  `ORDER_NUM` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `index_formid` (`CGFORM_HEAD_ID`) USING BTREE,
  KEY `index_button_code` (`BUTTON_CODE`) USING BTREE,
  KEY `index_button_status` (`BUTTON_STATUS`) USING BTREE,
  KEY `index_button_order` (`ORDER_NUM`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_button
-- ----------------------------
INSERT INTO `onl_cgform_button` VALUES ('a45bc1c6fba96be6b0c91ffcdd6b54aa', 'genereate_person_config', 'icon-edit', '生成配置', '1', 'link', null, 'e2faf977fdaf4b25a524f58c2441a51c', 'js', null);

-- ----------------------------
-- Table structure for `onl_cgform_enhance_java`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_java`;
CREATE TABLE `onl_cgform_enhance_java` (
  `ID` varchar(36) NOT NULL,
  `BUTTON_CODE` varchar(32) DEFAULT NULL COMMENT '按钮编码',
  `CG_JAVA_TYPE` varchar(32) NOT NULL COMMENT '类型',
  `CG_JAVA_VALUE` varchar(200) NOT NULL COMMENT '数值',
  `CGFORM_HEAD_ID` varchar(32) NOT NULL COMMENT '表单ID',
  `ACTIVE_STATUS` varchar(2) DEFAULT '1' COMMENT '生效状态',
  `EVENT` varchar(10) NOT NULL DEFAULT 'end' COMMENT '事件状态(end:结束，start:开始)',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `index_fmid` (`CGFORM_HEAD_ID`) USING BTREE,
  KEY `index_buttoncode` (`BUTTON_CODE`) USING BTREE,
  KEY `index_status` (`ACTIVE_STATUS`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_enhance_java
-- ----------------------------

-- ----------------------------
-- Table structure for `onl_cgform_enhance_js`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_js`;
CREATE TABLE `onl_cgform_enhance_js` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `CG_JS` longtext COMMENT 'JS增强内容',
  `CG_JS_TYPE` varchar(20) DEFAULT NULL COMMENT '类型',
  `CONTENT` varchar(1000) DEFAULT NULL COMMENT '备注',
  `CGFORM_HEAD_ID` varchar(32) DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `index_fmid` (`CGFORM_HEAD_ID`) USING BTREE,
  KEY `index_jstype` (`CG_JS_TYPE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_enhance_js
-- ----------------------------
INSERT INTO `onl_cgform_enhance_js` VALUES ('274b5d741a0262d3411958f0c465c5f0', 'genereate_person_config(row){\nconsole.log(\'选择\',row)\nalert(row.name + \'，个人积分配置生成成功！\');\n}', 'list', null, 'e2faf977fdaf4b25a524f58c2441a51c');
INSERT INTO `onl_cgform_enhance_js` VALUES ('2cbaf25f1edb620bea2d8de07f8233a1', 'air_china_post_materiel_item_onlChange(){\n    return {\n        wl_name(){\n           \n            let id = event.row.id\n            let cnum = event.row.num\n            let value = event.value\n            let targrt = event.target\n            let columnKey = event.column.key\n           let nval = 200*cnum\n           console.log(\'row\',event.row);\n           console.log(\'cnum\',cnum);\n           let otherValues = {\'jifen\': nval}\n              \n                that.triggleChangeValues(targrt,id,otherValues)\n\n        }\n    }\n}', 'form', null, 'e67d26b610dd414c884c4dbb24e71ce3');
INSERT INTO `onl_cgform_enhance_js` VALUES ('35d4ef464e5e8c87c9aa82ea89215fc1', '', 'list', null, 'e67d26b610dd414c884c4dbb24e71ce3');

-- ----------------------------
-- Table structure for `onl_cgform_enhance_sql`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_sql`;
CREATE TABLE `onl_cgform_enhance_sql` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `BUTTON_CODE` varchar(50) DEFAULT NULL COMMENT '按钮编码',
  `CGB_SQL` longtext COMMENT 'SQL内容',
  `CGB_SQL_NAME` varchar(50) DEFAULT NULL COMMENT 'Sql名称',
  `CONTENT` varchar(1000) DEFAULT NULL COMMENT '备注',
  `CGFORM_HEAD_ID` varchar(32) DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `index_formid` (`CGFORM_HEAD_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_enhance_sql
-- ----------------------------

-- ----------------------------
-- Table structure for `onl_cgform_field`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_field`;
CREATE TABLE `onl_cgform_field` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `cgform_head_id` varchar(32) NOT NULL COMMENT '表ID',
  `db_field_name` varchar(32) NOT NULL COMMENT '字段名字',
  `db_field_txt` varchar(200) DEFAULT NULL COMMENT '字段备注',
  `db_field_name_old` varchar(32) DEFAULT NULL COMMENT '原字段名',
  `db_is_key` tinyint(1) DEFAULT NULL COMMENT '是否主键 0否 1是',
  `db_is_null` tinyint(1) DEFAULT NULL COMMENT '是否允许为空0否 1是',
  `db_type` varchar(32) NOT NULL COMMENT '数据库字段类型',
  `db_length` int(11) NOT NULL COMMENT '数据库字段长度',
  `db_point_length` int(11) DEFAULT NULL COMMENT '小数点',
  `db_default_val` varchar(20) DEFAULT NULL COMMENT '表字段默认值',
  `dict_field` varchar(100) DEFAULT NULL COMMENT '字典code',
  `dict_table` varchar(100) DEFAULT NULL COMMENT '字典表',
  `dict_text` varchar(100) DEFAULT NULL COMMENT '字典Text',
  `field_show_type` varchar(10) DEFAULT NULL COMMENT '表单控件类型',
  `field_href` varchar(200) DEFAULT NULL COMMENT '跳转URL',
  `field_length` int(11) DEFAULT NULL COMMENT '表单控件长度',
  `field_valid_type` varchar(300) DEFAULT NULL COMMENT '表单字段校验规则',
  `field_must_input` varchar(2) DEFAULT NULL COMMENT '字段是否必填',
  `field_extend_json` varchar(500) DEFAULT NULL COMMENT '扩展参数JSON',
  `field_value_rule_code` varchar(500) DEFAULT NULL COMMENT '填值规则code',
  `is_query` tinyint(1) DEFAULT NULL COMMENT '是否查询条件0否 1是',
  `is_show_form` tinyint(1) DEFAULT NULL COMMENT '表单是否显示0否 1是',
  `is_show_list` tinyint(1) DEFAULT NULL COMMENT '列表是否显示0否 1是',
  `query_mode` varchar(10) DEFAULT NULL COMMENT '查询模式',
  `main_table` varchar(100) DEFAULT NULL COMMENT '外键主表名',
  `main_field` varchar(100) DEFAULT NULL COMMENT '外键主键字段',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `inex_table_id` (`cgform_head_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_field
-- ----------------------------
INSERT INTO `onl_cgform_field` VALUES ('0021c969dc23a9150d6f70a13b52e73e', '402860816aa5921f016aa5921f480000', 'begin_date', '开始时间', 'begin_date', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('020c1622c3df0aef30185f57874f6959', '79091e8277c744158530321513119c68', 'bpm_status', '流程状态', null, '0', '1', 'String', '32', '0', '1', 'bpm_status', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '1', 'single', '', '', '8', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:29:26', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('02b20e692456403e2fed1a89a06833b4', '402860816bff91c0016bff91d2810005', 'phone', '联系方式', 'phone', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('03668009f0ad92b20abb1a377197ee47', 'deea5a8ec619460c9245ba85dbc59e80', 'order_fk_id', '订单外键ID', null, '0', '0', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', 'test_order_main', 'id', '10', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:42:53', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('03709092184fdf4a66b0cdb4dd10a159', '402860816bff91c0016bffa220a9000b', 'bpm_status', '流程状态', null, '0', '1', 'String', '32', '0', '1', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '46', 'admin', '2019-07-22 16:15:32', '2019-07-19 15:34:44', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('03c105d2706c8286416833684de67406', '79091e8277c744158530321513119c68', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('03fd5ab69f331ff760c3f7d86b4a93f8', '4028318169e81b970169e81b97650000', 'log_content', '日志内容', 'log_content', '0', '1', 'string', '1000', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '3', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('045eb432c418b2b103b1e1b8e8a8a75d', 'fb7125a344a649b990c12949945cb6c1', 'age', '年龄', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', null, null, null, '2019-03-26 19:24:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('04e4185a503e6aaaa31c243829ff4ac7', 'd35109c3632c4952a19ecc094943dd71', 'birthday', '生日', null, '0', '1', 'Date', '32', '0', '', '', '', '', 'date', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('04ff134cb4aae845059e10b3b85f1451', '7ea60a25fa27470e9080d6a921aabbd1', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('051dd70c504c97a028daab2af261ea35', '1acb6f81a1d9439da6cc4e868617b565', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('052dcc6f34976b66754fd99415bd22ce', '79091e8277c744158530321513119c68', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('054db05394e83b318f097a60bc044134', '402860816bff91c0016bffa220a9000b', 'residence_address', '户籍地址', 'residence_address', '0', '1', 'string', '200', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '28', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0604945c206e867644e9a44b4c9b20c6', 'fb19fb067cd841f9ae93d4eb3b883dc0', '2', '4', null, '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '5', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('06a1badae6119abf4ec48858a3e94e1c', '402860816bff91c0016bffa220a9000b', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '43', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('06f1cfff58395ff62526b894f6182641', 'e67d26b610dd414c884c4dbb24e71ce3', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('07a307972076a392ffc61b11437f89dd', '402860816bff91c0016bff91c0cb0000', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('07f4776fd641389a8c98a85713990dce', '402860816bff91c0016bff91c0cb0000', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '14', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('09450359eb90b40d224ec43588a62f9e', '402860816bff91c0016bff91c0cb0000', 'user_id', '用户ID', 'user_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0a4cdcb7e54f614ab952024f6c72bb6d', 'beee191324fd40c1afec4fda18bd9d47', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0adc06d9b497684bcbb5a781e044517c', '1acb6f81a1d9439da6cc4e868617b565', 'supplier', '供应商', null, '0', '1', 'String', '32', '0', '', 'air_china_ supplier', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '15', 'admin', '2019-06-10 14:47:14', '2019-04-24 16:52:00', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0ba1bf74e2a6a94a7a63010ec7230706', '402860816bff91c0016bffa220a9000b', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '42', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0cba94f0497d4d3d829fc573f58eff9f', '402860816bff91c0016bffa220a9000b', 'graduation_time', '毕业时间', 'graduation_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '16', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0d00c51a4ddad2598a587fadc968a8b2', '402860816bff91c0016bff91cfea0004', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0ddd0c0afc967a9ab6050401ca62a4be', 'e67d26b610dd414c884c4dbb24e71ce3', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('0fb6fa76c5c78a1e957dbb411e110738', '402860816bff91c0016bff91d8830007', 'politically_status', '政治面貌', 'politically_status', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('105c8e44ad13026b641f0363601f30f3', 'e5464aa8fa7b47c580e91593cf9b46dc', 'num', '循环数量', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1130f1e252533529bb1167b896dffe32', 'deea5a8ec619460c9245ba85dbc59e80', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('117fc4ba649d6690a3ac482ad5e4ad38', '56870166aba54ebfacb20ba6c770bd73', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('13246645b7650491b70205d99703ca06', '402860816aa5921f016aa5dedcb90009', 'bpm_status', '流程状态', 'bpm_status', '0', '1', 'string', '32', '0', '1', 'bpm_status', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '1', 'group', '', '', '8', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('135dd0ee50712722db65b8762bd487ea', '8994f2817b5a45d9890aa04497a317c5', 'update_time', '更新日期', null, '0', '1', 'date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '4', null, null, '2019-03-23 11:39:16', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('14ec4c83c29966ab42b6b718c5a3e774', '7ea60a25fa27470e9080d6a921aabbd1', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('16363d0bc125125e395772278d0cf22e', '4b556f0168f64976a3d20bfb932bc798', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('179c290e383009810fb738f07bd5af8d', '402860816bff91c0016bff91d2810005', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('17cbda69da9dd3632625a0647c259070', '73162c3b8161413e8ecdca7eb288d0c9', 'wl_name', '物料名字', null, '0', '1', 'String', '200', '0', '', '', '', '', 'text', '', '120', null, '1', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('18fefb2257901f05508f8ec13ada78a3', 'e5464aa8fa7b47c580e91593cf9b46dc', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-04-24 17:09:48', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1973ef1d3728fbff2db6a352e001f5f7', 'fb7125a344a649b990c12949945cb6c1', 'name', '用户名', null, '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '5', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1ab5be1f937f393b3e5cc214ef1b855c', '7ea60a25fa27470e9080d6a921aabbd1', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1b6c7b95028bed9ff656d65557dd2bdf', '402860816bff91c0016bffa220a9000b', 'user_id', '用户id', 'user_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1c2f307e315bac77a6d3f02e88387a43', 'deea5a8ec619460c9245ba85dbc59e80', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1c3b2ad0a52ecb47fa7fd53f25875beb', 'deea5a8ec619460c9245ba85dbc59e80', 'price', '价格', null, '0', '1', 'double', '32', '0', '', '', '', '', 'text', '', '120', 'n', '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1c4d25a12388c80d397bb4f4664fe4e6', '4b556f0168f64976a3d20bfb932bc798', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1cfe967bb457cbaa6e041e45d019b583', '402860816bff91c0016bff91c7010001', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1e3d8cfbf12155559666a23ee2c6c5ca', 'e5464aa8fa7b47c580e91593cf9b46dc', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1ed46fdeb289bd7805c9b83332ccd3b4', '402860816bff91c0016bff91d2810005', 'relation', '关系', 'relation', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('1fa5f07b3e70d4925b69b2bf51309421', '56870166aba54ebfacb20ba6c770bd73', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('20ff34fb0466089cb633d73d5a6f08d6', 'd35109c3632c4952a19ecc094943dd71', 'update_time', '更新日期', null, '0', '1', 'date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2113a4ec7b88b4820dcbbdf96e46bbb7', 'fbc35f067da94a70adb622ddba259352', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2150e48b2cb6072d2d8ecd79a7daf7cc', '402860816bff91c0016bff91ca7e0002', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2323239efb5a40b73034411868dfc41d', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'update_by', '更新人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '3', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('23f42061ed218bdbc1262913c071e1cd', 'e5464aa8fa7b47c580e91593cf9b46dc', 'iz_valid', '启动状态', null, '0', '1', 'int', '2', '0', '', 'air_china_valid', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '12', 'admin', '2019-04-24 17:09:49', '2019-04-24 14:09:06', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('242cc59b23965a92161eca69ffdbf018', 'd35109c3632c4952a19ecc094943dd71', 'age', '年龄', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('265702edb8872e322fe72d3640e34ac5', '402860816bff91c0016bff91cfea0004', 'from_time', '开始日期', 'from_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('284864d99fddfdcb00e188e3a512cb28', '1acb6f81a1d9439da6cc4e868617b565', 'no', '预算表序号', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '10', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2889d3cef706f91e092d76a56b8055be', '402860816bff91c0016bff91cda80003', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('29e4abea55d9fa7dbbd0c8dbbb2b3756', '402860816bff91c0016bff91cda80003', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2d53a66f0b72d820b86ff445e2181d76', 'beee191324fd40c1afec4fda18bd9d47', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2dfc4c81926f678c5f8d5ffd27858201', 'e2faf977fdaf4b25a524f58c2441a51c', 'account', '用户编码', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2e5275b6407e1b4265af8519077fa4a5', 'd3ae1c692b9640e0a091f8c46e17bb01', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2e66b9db37648389e0846e2204111732', '73162c3b8161413e8ecdca7eb288d0c9', 'has_child', '是否有子节点', null, '0', '1', 'string', '3', '0', '', 'valid_status', '', '', 'list', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '10', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2ee58d8e4844dfe1fa6b1b841ae0b312', '402860816bff91c0016bff91d2810005', 'politically_status', '政治面貌', 'politically_status', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('2f111722eb3a994450e67e3211fd69a8', '402860816bff91c0016bff91ca7e0002', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('30143cc3de69c413828f9fba20662026', '402860816bff91c0016bffa220a9000b', 'healthy', '健康状况', 'healthy', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('31fd90306c3942f09cb79deabbf2f541', '402860816bff91c0016bff91d2810005', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('336495117e3a31351fed6963b950dddb', '402860816bff91c0016bffa220a9000b', 'inside_transfer', '内部工作调动情况', 'inside_transfer', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '37', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('345c8b48e1e128e77c4c6e2b36512804', '402860816aa5921f016aa5dedcb90009', 'create_by', '创建人', 'create_by', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '2', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('351faaeb2dd8105e9c66f678211c9d4f', 'dbf4675875e14676a3f9a8b2b8941140', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('354b2ce39c5e8ec3f0bbb01bf8ff0fb7', '32f75e4043ef4070919dbd4337186a3d', 'content', '描述', null, '0', '1', 'String', '300', '0', '', '', '', '', 'textarea', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-04-11 10:15:31', '2019-03-28 15:24:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('35ca1c8aa1501bc8a79c880928841f18', '402860816aa5921f016aa5921f480000', 'update_by', '修改人id', 'update_by', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '11', 'admin', '2019-05-11 15:31:55', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3635793767371c6db9f76b4b79f9d321', '402860816bff91c0016bff91d8830007', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('370a6eebc2d732eaf121fe0830d853a6', 'e5464aa8fa7b47c580e91593cf9b46dc', 'wl_code', '物料编码', null, '0', '1', 'String', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '7', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('37e2548874f09ef7d08642a30bc918fa', 'fbc35f067da94a70adb622ddba259352', 'group_name', '小组名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('391e7cbd9f29743b11bb555c50547b1f', '32f75e4043ef4070919dbd4337186a3d', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3b439859f98e30e34d25e983eb22e408', '402860816bff91c0016bff91c7010001', 'award_time', '获奖时间', 'award_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3bf44e68de518f3ddf72b87671d0ff90', '8994f2817b5a45d9890aa04497a317c5', 'update_by', '更新人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '3', null, null, '2019-03-23 11:39:16', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3cd2061ea15ce9eeb4b7cf2e544ccb6b', 'd35109c3632c4952a19ecc094943dd71', 'file_kk', '附件', null, '0', '1', 'String', '500', '0', '', '', '', '', 'file', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '12', 'admin', '2019-08-23 23:45:15', '2019-06-10 20:06:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3cfd4d60c7d8409ae716a579bcb0910d', '402860816bff91c0016bff91c0cb0000', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '16', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3e32f6c30c9028872388f70743c5d6a5', '402860816bff91c0016bff91c0cb0000', 'reason', '申请理由', 'reason', '0', '1', 'string', '200', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3e70d1c516c3533c6698300665c669e1', '402860816bff91c0016bff91c0cb0000', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:31', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('3f2ace8f968a0e5b91d1340ee2957cda', '402860816bff91c0016bff91d8830007', 'real_name', '姓名', 'real_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('40471eb4560bf0bbd2ffef17d48a269d', 'dbf4675875e14676a3f9a8b2b8941140', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('404b516d4f2229f292783db595b02ba1', '402860816bff91c0016bff91d8830007', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('405de5ea82e54138a0613dd41b006dfb', '56870166aba54ebfacb20ba6c770bd73', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4164314d6a51d100169a29872b7504d8', '402860816bff91c0016bff91ca7e0002', 'cert_time', '发证时间', 'cert_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('41d4215c01b0d26871f2cb83d3e532ae', '402860816bff91c0016bff91c0cb0000', 'bpm_status', '流程状态', null, '0', '1', 'String', '32', '0', '1', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '17', 'admin', '2019-07-19 18:09:01', '2019-07-19 15:35:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('422a44a15fa39fd57c3c23eb601f7c03', '56870166aba54ebfacb20ba6c770bd73', 'descc', '描述', null, '0', '1', 'String', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('42cccfa014c9e131a0a1b23f563d3688', '402860816bff91c0016bffa220a9000b', 'sex', '性别', 'sex', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4312f618c83e07db82e468b81a1eaa45', '402860816bff91c0016bffa220a9000b', 'photo', '照片', 'photo', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '20', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('44bdc595f1e565fc053e01134b92bb47', 'd3ae1c692b9640e0a091f8c46e17bb01', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('44e81e24d2384b0f187e8f69eda55390', '402860816bff91c0016bff91cda80003', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('45c0a1a89a1e2a72533b9af894be1011', '27fc5f91274344afa7673a732b279939', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('45d59eb647257fcbcb9d143ff1ba2080', 'deea5a8ec619460c9245ba85dbc59e80', 'pro_type', '产品类型', null, '0', '1', 'String', '32', '0', '', 'sex', '', '', 'radio', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-06-10 16:07:16', '2019-04-23 20:54:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('469b250595f15dfebe69991d72e4bfb2', 'e9faf717024b4aae95cff224ae9b6d97', 'name', '员工姓名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('46be01bef342519e268902d0d36a7473', 'deea5a8ec619460c9245ba85dbc59e80', 'descc', '描述', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '11', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('46f1a875f86a4f48d0540ad0d5e667d7', '56870166aba54ebfacb20ba6c770bd73', 'order_date', '下单时间', null, '0', '1', 'Date', '32', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('47c21a6b45e59a6b70bb9c0cc4510a68', '1acb6f81a1d9439da6cc4e868617b565', 'integral_val', '积分值', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '13', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('47fa05530f3537a1be8f9e7a9e98be82', 'd35109c3632c4952a19ecc094943dd71', 'sex', '性别', null, '0', '1', 'string', '32', '0', '', 'sex', '', '', 'list', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4851697fdf63709d2bc7451b7415f4af', '32f75e4043ef4070919dbd4337186a3d', 'sex', '性别', null, '0', '1', 'String', '32', '0', '1', 'sex', '', '', 'list', '', '120', null, '0', '', '', '1', '1', '1', 'single', '', '', '6', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('485a8ddce2c033f88af674ec98b68e32', '402860816bff91c0016bffa220a9000b', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '40', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('492a462b212fc34b0ee70e872684ed7e', '7ea60a25fa27470e9080d6a921aabbd1', 'name', '用户名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('499a5dac033a01ce58009e4c5b786697', 'e9faf717024b4aae95cff224ae9b6d97', 'age', '员工年龄', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('49cd3dbd4f7f7cf0d19b1ee1045cfa69', 'e67d26b610dd414c884c4dbb24e71ce3', 'post_code', '岗位编码', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4ba7c553ca4babcec75273c531cd65e1', '402860816bff91c0016bff91cfea0004', 'workplace', '工作单位', 'workplace', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4c2cba9fc950333421c4193576b8384d', '32f75e4043ef4070919dbd4337186a3d', 'salary', '工资', null, '0', '1', 'double', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '10', 'admin', '2019-04-11 10:15:32', '2019-03-28 15:24:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4c570c5cf05590348e12621ca62773cf', '402860816aa5921f016aa5921f480000', 'name', '请假人', 'name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '2', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4cacfa054e96791ab938b5c8f8e02cd1', '27fc5f91274344afa7673a732b279939', 'bpm_status', '流程状态', null, '0', '1', 'String', '2', '0', '', 'bpm_status', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', null, null, '2019-07-01 16:28:20', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4f718d95ad9de33eac18fd0663e4c1f1', '32f75e4043ef4070919dbd4337186a3d', 'birthday', '生日', null, '0', '1', 'Date', '32', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '1', '1', '1', 'single', '', '', '8', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('4f7cba71de7afe6efbd024f5f9935521', '402860816bff91c0016bff91cda80003', 'to_time', '截止时间', 'to_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('506c9b0b2331a24e5c284274d28fe569', '27fc5f91274344afa7673a732b279939', 'kkk', '描述', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('508eb28e1409a2a9501cdf6fd7eb24c7', 'dbf4675875e14676a3f9a8b2b8941140', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('509a4f63f02e784bc04499a6a9be8528', 'd35109c3632c4952a19ecc094943dd71', 'update_by', '更新人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('519f68557b953fc2d38400182b187366', '402860816bff91c0016bffa220a9000b', 'residence_type', '户籍类别', 'residence_type', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('52ee861bc1b62cd8e4f10632b3d9d1b2', '79091e8277c744158530321513119c68', 'name', '顺序会签标题', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('56a7800e4e476812c74217c2aad781aa', '32feeb502544416c9bf41329c10a88f4', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('56e247f12d62b49cd9bd537e3efecf16', '402860816bff91c0016bff91c0cb0000', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('588400f6ebcdd0bc9bb560dd36636af9', 'e2faf977fdaf4b25a524f58c2441a51c', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('58a96f945912d33b64ebf5dee98156dc', '402860816bff91c0016bffa220a9000b', 'mobile', '手机号', 'mobile', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '19', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('58eea85add4788b83c893092434bc413', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'update_time', '更新日期', null, '0', '1', 'date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '4', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('59ae1e853668c676e85329aa029c68a6', '402860816bff91c0016bff91c0cb0000', 'status', '状态（1：申请中 2：通过）', 'status', '0', '1', 'string', '2', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5a1ab458d88bb766f92c3d791495cdcd', '402860816bff91c0016bff91d2810005', 'age', '年龄', 'age', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5a4ef083dd572114aeb581b6828de545', '402860816bff91c0016bff91c7010001', 'award_name', '获奖名称', 'award_name', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5a621f27aa443fe9eccc73717e4fa172', '4028318169e81b970169e81b97650000', 'method', '请求java方法', 'method', '0', '1', 'string', '500', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '8', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5a655b208d6318ed02f236f15a319b5f', 'fbc35f067da94a70adb622ddba259352', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5a6f88710c49bbe8e2e0ca58e149abad', '402860816bff91c0016bff91cda80003', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5b17ba693745c258f6b66380ac851e5f', 'd35109c3632c4952a19ecc094943dd71', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '0', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5c76f5ecc774d7339eb0c2199c0052bc', '402860816bff91c0016bff91c0cb0000', 'biz_no', '编号', 'biz_no', '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '2', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5c8c8d573e01e4f40b5a7c451515e1d2', '32feeb502544416c9bf41329c10a88f4', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5dfbea516ee2390d712eace5405c5219', '402860816bff91c0016bff91ca7e0002', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5e4484b7348dc3e59a0c58bdc3828cc0', '27fc5f91274344afa7673a732b279939', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('5e4ac29ac2007ceabf93368330290a42', '402860816bff91c0016bff91d8830007', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('61c7a0058c264dd746eb35e6f50fc15b', '402860816aa5921f016aa5dedcb90009', 'update_time', '更新日期', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '5', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6232ade7e2a0c1e97e2c0945b32e61b6', '402860816bff91c0016bffa220a9000b', 'paying_social_insurance', '是否上社保', 'paying_social_insurance', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '32', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6490a98dccb6df218feaeb4ce11bc03b', '402860816aa5921f016aa5921f480000', 'update_time', '修改时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '10', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('654362725195829005036b3db47ec826', '402860816bff91c0016bffa220a9000b', 'post', '职务', 'post', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('66a7ef842bc34e105a90186e48167ef2', 'dbf4675875e14676a3f9a8b2b8941140', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('68769fa7e4696e3a28f4cecf63076b7b', '402860816bff91c0016bff91ca7e0002', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('69666f21896136af16a6303aff440156', '402860816bff91c0016bffa220a9000b', 'nation', '民族', 'nation', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('69d11490788fecfc9fb7d74bf449ba86', '32f75e4043ef4070919dbd4337186a3d', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6a30c2e6f01ddd24349da55a37025cc0', 'd35109c3632c4952a19ecc094943dd71', 'top_pic', '头像', null, '0', '1', 'String', '500', '0', '', '', '', '', 'image', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '11', 'admin', '2019-08-23 23:45:15', '2019-06-10 20:06:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6b6f3aa00b8e73fb785154e795189739', '402860816aa5921f016aa5dedcb90009', 'start_time', '会签发起时间', 'start_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6cfb5acbbb69782bf0c7043b53f595b2', '402860816bff91c0016bff91cda80003', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('6f73e96a659c200c083006b6fce1f043', '402860816bff91c0016bff91ca7e0002', 'cert_name', '证书名称', 'cert_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7154c75d754a5f88bef2b68829baf576', '4028318169e81b970169e81b97650000', 'operate_type', '操作类型', 'operate_type', '0', '1', 'string', '10', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '4', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('71afb00a1971125ecfa13b4dfa49665e', '402860816bff91c0016bff91cfea0004', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('71d5b0675df5aba71688c9d7d75cccee', '4028318169e81b970169e81b97650000', 'log_type', '日志类型（1登录日志，2操作日志）', 'log_type', '0', '1', 'string', '10', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '2', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('71e9ab74dae687837365e50eed090591', '1acb6f81a1d9439da6cc4e868617b565', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7280c56a210e6a47794fda855d0c6abb', 'fbc35f067da94a70adb622ddba259352', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7365f05f551092716223d5d449efd8c7', 'beee191324fd40c1afec4fda18bd9d47', 'name', 'ss', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('742329ccdb185cf5d3e0b5b0c05dcffa', '402860816bff91c0016bffa220a9000b', 'interest', '兴趣爱好', 'interest', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '34', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('744444a7ada3bbb05c6b114b5ba0d477', '402860816aa5921f016aa5dedcb90009', 'id', 'id', 'id', '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('74af99545de724a4abd2022581a36026', 'fb7125a344a649b990c12949945cb6c1', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('756b07656386dbd91245f7ffda32ae61', '402860816bff91c0016bff91d8830007', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('75841fa7c75ebdc94655bd5e44fbc9f6', '402860816bff91c0016bffa220a9000b', 'native_place', '籍贯', 'native_place', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('75ba781c67711bed71bba1c3e3c68e11', '8994f2817b5a45d9890aa04497a317c5', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '0', null, null, '2019-03-23 11:39:16', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7639c1bc4327f1f674ffeab2ca261134', '32f75e4043ef4070919dbd4337186a3d', 'update_by', '更新人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('78a40344207c791b8d7ac7de721ce1c4', '79091e8277c744158530321513119c68', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('78eb7e3b77cd49f9acb9b024cfe834e1', '402860816aa5921f016aa5dedcb90009', 'create_time', '创建日期', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '3', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('78fd804d93dc716fd8c2ccc45f788565', 'fb7125a344a649b990c12949945cb6c1', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('790c9f3dba773ed9a6ea3ad627393f57', '402860816bff91c0016bffa220a9000b', 'archives_location', '档案所在地', 'archives_location', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '36', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7a665ed90ef64b4d65632c941e5795b2', '4b556f0168f64976a3d20bfb932bc798', 'sex', '性别', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', null, null, '2019-04-12 23:38:29', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7b642d983ac06bfef91edde2c932dbe7', '1acb6f81a1d9439da6cc4e868617b565', 'xg_shangxian', '选购上限', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '14', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7b849e57debfb889caea5e0fef09062b', 'beee191324fd40c1afec4fda18bd9d47', 'sex2', 'dd', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7beb639aa9ffda07edb5ce1e49c2287f', '402860816bff91c0016bff91d2810005', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7c6aecc377816c69147f1193b17dfcc5', 'e9faf717024b4aae95cff224ae9b6d97', 'sex', '员工性别', null, '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7cadf4e0be0b173bb5b8d39613e25190', '402860816bff91c0016bffa220a9000b', 'residence_postcode', '户籍邮编', 'residence_postcode', '0', '1', 'string', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '29', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7d107728408c21ece332406a21f2d692', '402860816bff91c0016bff91cfea0004', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7e066f60680158d47b328ef519d80e49', 'beee191324fd40c1afec4fda18bd9d47', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('7f10901c6ade3aa9d9ff46ed7039c70f', '1acb6f81a1d9439da6cc4e868617b565', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('81ed9556c9fda1bbb46d94a53a6c90c7', '402860816bff91c0016bff91c0cb0000', 'depart_name', '部门名称', 'depart', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8422485e1cbf4455f9ded7d0af59379c', '402860816bff91c0016bff91cfea0004', 'to_time', '截止时间', 'to_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('845c70568d44a074f067d6d277950525', '402860816bff91c0016bffa220a9000b', 'entrytime', '入职时间', 'entrytime', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '23', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8529ddaed8d5f3d9084e873203d55cac', '402860816bff91c0016bffa220a9000b', 'marital_status', '婚姻状况', 'marital_status', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '24', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('857a0daa9cd8a058f2f15fc7c5fb3571', '402860816bff91c0016bffa220a9000b', 'email', '邮箱', 'email', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '17', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8652ca69a947fd4c961a3ac7c0fa252a', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'create_by', '创建人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '1', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('86bbafef5683674a736cf7241c458d44', '27fc5f91274344afa7673a732b279939', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('86e0f3a8f31c60698157f139ed993954', '402860816bff91c0016bffa220a9000b', 'having_reserve_funds', '是否有公积金', 'having_reserve_funds', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '33', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('873e2bb041b17bff77d3aca72900ea1b', '56870166aba54ebfacb20ba6c770bd73', 'order_code', '订单编码', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('877391ae770a4ce2c95181ef410982ce', '4028318169e81b970169e81b97650000', 'request_param', '请求参数', 'request_param', '0', '1', 'string', '255', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '10', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('87e82e3c723a6abb020122babdac6bd1', '8994f2817b5a45d9890aa04497a317c5', 'create_by', '创建人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '1', null, null, '2019-03-23 11:39:16', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('87f7a2703c5850f0b063dd866d0e2917', '402860816bff91c0016bffa220a9000b', 'birthday', '出生日期', 'birthday', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('87fafe1a4a8a626e3875697574c19f15', '402860816bff91c0016bff91d2810005', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '14', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('88a12570e14c9f6f442e731ae5ad0eb1', 'beee191324fd40c1afec4fda18bd9d47', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('89ab9eedbac6141e7a0df6d37a3655d0', 'e67d26b610dd414c884c4dbb24e71ce3', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8a24fb45e2af120c253c8b61c0085f7a', '402860816bff91c0016bff91cda80003', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8ac8a0c0087469a4e7579229ff17f273', 'e5464aa8fa7b47c580e91593cf9b46dc', 'jifen', '合计积分', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '10', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8bd4deadc9e96c1a6d7abd77033105f6', 'e67d26b610dd414c884c4dbb24e71ce3', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8ca56210938fbe649f840e505eb9fd41', '56870166aba54ebfacb20ba6c770bd73', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8ea43fd1e4ce82becee61b2f1e2e843f', '32feeb502544416c9bf41329c10a88f4', 'sex', '性别', null, '0', '1', 'String', '32', '0', '', 'sex', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8f1d302868640b72cef52171a023a203', 'e9faf717024b4aae95cff224ae9b6d97', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8f3e6fb68179c690f748f3c541fb50f1', '7ea60a25fa27470e9080d6a921aabbd1', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('8fc0be84bed1216635c69af918e097ff', '402860816aa5921f016aa5dedcb90009', 'name', '并行会签标题', 'name', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('90a822b8a63bbbc1e9575c9f4e21e021', 'd35109c3632c4952a19ecc094943dd71', 'descc', '描述', null, '0', '1', 'string', '500', '0', '', '', '', '', 'textarea', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('90f39a6e29dae2e1fbb59d7d605f7c09', '1acb6f81a1d9439da6cc4e868617b565', 'iz_valid', '启用状态', null, '0', '1', 'String', '2', '0', '', 'air_china_valid', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '11', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9370c9304af30b8d29defe0a5ada6e5b', '62e29cdb81ac44d1a2d8ff89851b853d', 'DC_DDSA', 'DD', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9371f61d39c5d57ddb0a2db96b2e2412', '402860816bff91c0016bffa220a9000b', 'speciality', '专业', 'speciality', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '15', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('947174892512ea97fafde899d427ea7e', '402860816bff91c0016bff91c0cb0000', 'real_name', '姓名', 'real_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('94b8bf435175cc545366e11992280757', '32f75e4043ef4070919dbd4337186a3d', 'age', '年龄', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '1', '1', '1', 'group', '', '', '7', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('94e682cb802777fe4205536888f69353', '402860816bff91c0016bff91d2810005', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('957386b500be42a200d6a56d54345392', 'deea5a8ec619460c9245ba85dbc59e80', 'num', '数量', null, '0', '1', 'int', '32', '0', '', '', '', '', 'text', '', '120', 'n', '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('960d2847922b61dadeb3518ef55fb0c1', '1acb6f81a1d9439da6cc4e868617b565', 'wl_name', '物料名称', null, '0', '1', 'String', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9665f02764774fdd77c19923d3ff3c3e', '4028318169e81b970169e81b97650000', 'cost_time', '耗时', 'cost_time', '0', '1', 'string', '19', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '12', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('96c585a4f71e5c38ed25b9741366365b', '402860816bff91c0016bff91c7010001', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9765efa2cafde6d0ede2215848c9e80b', '32f75e4043ef4070919dbd4337186a3d', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '0', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('98e82cb1595609a3b42fa75c60ac1229', '402860816bff91c0016bff91d2810005', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9914a0c84805e72c4b6075e36edb13f9', '402860816aa5921f016aa5921f480000', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '9', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9920ecec9c9109fc6b93e86f8fdfa03b', '402860816bff91c0016bffa220a9000b', 'depart_name', '所在部门', 'depart_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '1', '1', '1', 'group', '', '', '2', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('99b43bbb23237815ebb74b12b4d7ea2f', '62e29cdb81ac44d1a2d8ff89851b853d', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9a579c506f75f75baf88352a5eb2c249', '1acb6f81a1d9439da6cc4e868617b565', 'bpm_status', '流程状态', null, '0', '1', 'String', '2', '0', '1', 'bpm_status', '', '', 'list', '', '120', null, '0', '', '', '0', '0', '1', 'single', '', '', '16', 'admin', '2019-06-10 14:47:14', '2019-05-07 16:54:43', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9c40fb4db8afed3c682c6b8a732fd69d', 'e2faf977fdaf4b25a524f58c2441a51c', 'post', '用户岗位', null, '0', '1', 'String', '32', '0', '', 'post_code', 'air_china_post_materiel_main', 'post_name', 'sel_search', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9d85bafa399f28a40e1de1eeef747223', '4028318169e81b970169e81b97650000', 'ip', 'IP', 'ip', '0', '1', 'string', '100', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '7', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9d89ff1a019f41d80307652041490944', '32feeb502544416c9bf41329c10a88f4', 'name', '请假人', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('9e50680eb4e79b3af352a5933d239dff', 'dbf4675875e14676a3f9a8b2b8941140', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a01a7fe5660206e6f407ed98b6c732d6', '402860816bff91c0016bff91cfea0004', 'phone', '联系方式', 'phone', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a1f5daba36f536e7acf6a939826183b0', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '0', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a45eba33810c485b9d8e6f70818a1dfa', '402860816aa5921f016aa5921f480000', 'bpm_status', '流程状态', 'bpm_status', '0', '1', 'string', '50', '0', '1', 'bpm_status', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '1', 'group', '', '', '7', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a6471d4fb3dbffef01dab1f7d452bb30', '27fc5f91274344afa7673a732b279939', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a6722b498602d7d7b5177b16789d8cc1', 'e5464aa8fa7b47c580e91593cf9b46dc', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-04-24 17:09:48', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a76f561057ac9e43a8ca09e478a1eab8', '402860816bff91c0016bff91ca7e0002', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a7822f6e4cffb37fc0729cbd4cfd8655', '32f75e4043ef4070919dbd4337186a3d', 'name', '用户名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '1', '1', '1', 'single', '', '', '5', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a82ca42a76e9d2b8dae6d57dbb5edb54', 'deea5a8ec619460c9245ba85dbc59e80', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a940adc4585fa3b5bd2114ea9abe8491', '402860816bff91c0016bff91ca7e0002', 'cert_level', '证书级别', 'cert_level', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('a94f1d7da64f3aa35c32155ea00ccb2f', '402860816bff91c0016bffa220a9000b', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('aa07931514727913413880b7a2b76dcb', 'd3ae1c692b9640e0a091f8c46e17bb01', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('aa4780601419c21dabb6c42fc511e71c', '402860816bff91c0016bffa220a9000b', 'have_children', '有无子女', 'have_children', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '25', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ab10e0aa029ded2f4420a33420de225d', '1acb6f81a1d9439da6cc4e868617b565', 'wl_code', '物料编码', null, '0', '1', 'String', '60', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ab1f880ba593f3757dac70e003945aa2', '402860816bff91c0016bff91c0cb0000', 'depart_id', '部门ID', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-07-19 18:09:01', '2019-07-17 19:38:45', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ab58f43f853fd1f65f83c22966883afb', 'beee191324fd40c1afec4fda18bd9d47', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ab8e6f1cca421c5ce395a2c1fdfd2100', '32feeb502544416c9bf41329c10a88f4', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('abe61a8ddf966a979457b763329a537b', 'e5464aa8fa7b47c580e91593cf9b46dc', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ac91565da5fb8fe43a4da3dec660b25f', '402860816bff91c0016bff91c7010001', 'award_place', '获奖地点', 'award_place', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('acff5c8aef3b6288b87fd91215012206', 'e5464aa8fa7b47c580e91593cf9b46dc', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ad061417d5b53c67975eb83657505218', '73162c3b8161413e8ecdca7eb288d0c9', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ad93762c6c4a1dd8331e5fa11215b568', 'e2faf977fdaf4b25a524f58c2441a51c', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ae31da96f38fc2941cb93d1bb1ab9431', 'deea5a8ec619460c9245ba85dbc59e80', 'product_name', '产品名字', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ae77bb317366622698c8ab9bf2325833', 'deea5a8ec619460c9245ba85dbc59e80', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('af0fe0df8b626129de62e22212732517', '402860816bff91c0016bff91cda80003', 'speciality', '专业', 'speciality', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('af6c582b902e2f2bf9930eba61ae7938', '73162c3b8161413e8ecdca7eb288d0c9', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('afd3ef1d494a9b69d2c7a3cdde937f6f', '402860816bff91c0016bffa220a9000b', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '39', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b01304904babd7479de2acfe8a77157f', '402860816aa5921f016aa5921f480000', 'id', 'ID', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b05b4cbb74f389a7376f51ed9fd97030', '402860816bff91c0016bff91d8830007', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b0a06bdbefd304d81a1838d8d94deda9', '4b556f0168f64976a3d20bfb932bc798', 'name', '用户名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b0b1cf271dd6b221a902da2d2f8f889a', 'e9faf717024b4aae95cff224ae9b6d97', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b1fc6e2ca671b19e57b08a4f57fc2454', 'fb7125a344a649b990c12949945cb6c1', 'update_time', '更新日期', null, '0', '1', 'date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b22694cf34ffb967b8717647816ad5df', 'e5464aa8fa7b47c580e91593cf9b46dc', 'fk_id', '外键', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', 'air_china_post_materiel_main', 'id', '15', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b276facab025f9750b0aff391693cc4b', '402860816bff91c0016bff91c7010001', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b3542d3e7908ed885ecc4ba9e7300705', '4b556f0168f64976a3d20bfb932bc798', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b47af4d937e55c6208939bac5378bfad', '62e29cdb81ac44d1a2d8ff89851b853d', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b53203fc52d067bb4730dbcb7e496bd3', '56870166aba54ebfacb20ba6c770bd73', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-06-10 16:01:35', '2019-04-20 11:38:39', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b5cfd3c9691a884430f3d9cd5ecb211f', 'e2faf977fdaf4b25a524f58c2441a51c', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b6874a05734cad8bd96ffd2f31f1ebca', '402860816bff91c0016bff91c7010001', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b733fa73519603b22d401fabbf9e9781', '402860816bff91c0016bff91c0cb0000', 'hiredate', '入职时间', 'hiredate', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b7938e4518f9062ce62702cf45986e06', 'e2faf977fdaf4b25a524f58c2441a51c', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b7a1243aaa712e2c152c0c7a46f88683', '402860816bff91c0016bff91d8830007', 'age', '年龄', 'age', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b91258e3dc15b28c2e3f0d934e6e27e8', 'fb7125a344a649b990c12949945cb6c1', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '0', null, null, '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b92572ae142f8dd5f2ac02fb45e6b2c1', 'e2faf977fdaf4b25a524f58c2441a51c', 'name', '用户名', null, '0', '1', 'String', '32', '0', '', 'realname,username', 'report_user', 'name,account', 'sel_search', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('b9fbace24688c9c9a8c9be72c1d014e7', '402860816bff91c0016bffa220a9000b', 'phone', '电话', 'phone', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '18', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ba5f4b2affa94f36eda7f6f133db7ae3', '402860816bff91c0016bff91d2810005', 'workplace', '工作单位', 'workplace', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bad02e68ea37bf387337516af84a1ddb', '73162c3b8161413e8ecdca7eb288d0c9', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bb44475e1d1738a19745bf9f3ebf9e40', '402860816bff91c0016bff91cfea0004', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bbbb38347b1a5340a1d293e455c632ce', 'fb19fb067cd841f9ae93d4eb3b883dc0', '3', '4', null, '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '6', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bc648624ad14c826bbc6e9b23a2b9858', '402860816bff91c0016bff91ca7e0002', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bc793fdbef9f6474425456c4eb9d197a', '402860816bff91c0016bff91cfea0004', 'witness', '证明人', 'references', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bc7df6f3cf49f670c36a3de25e25e715', '402860816bff91c0016bff91d2810005', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bd39cb237049ac60218b3f4dd844f30c', '402860816bff91c0016bffa220a9000b', 'current_address', '现居住地', 'current_address', '0', '1', 'string', '200', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '30', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bde81809057b1a4c974fa0f090501fdd', '402860816aa5921f016aa5dedcb90009', 'update_by', '更新人', 'update_by', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '4', 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('be3f8c157d8a1b40e6f7b836552a8095', '8994f2817b5a45d9890aa04497a317c5', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '2', null, null, '2019-03-23 11:39:16', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('be5eeda7d51dace73d3818bd8467b53b', '402860816bff91c0016bff91c0cb0000', 'update_time', '更新时间', 'update_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '15', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('be868eed386da3cfcf49ea9afcdadf11', 'd35109c3632c4952a19ecc094943dd71', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bec3082fc5f0f194be5cd72cc2866ff4', 'e5464aa8fa7b47c580e91593cf9b46dc', 'wl_name', '物料名字', null, '0', '1', 'String', '200', '0', '', 'wl_code', 'air_china_materiel', 'wl_name', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bed0bc67f570613eaa6a1bd8bcaaddcc', '4b556f0168f64976a3d20bfb932bc798', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bf61aa04c5ca77ad54c764f8f8b2bdec', '402860816bff91c0016bff91d8830007', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '12', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('bfc2f19fae367f885adb8bd82a344391', '4028318169e81b970169e81b97650000', 'userid', '操作用户账号', 'userid', '0', '1', 'string', '32', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '5', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c0d66c95773774e7ac1f2a88df307e7a', '402860816aa5921f016aa5921f480000', 'reason', '请假原因', 'reason', '0', '1', 'string', '500', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c29216d975fee50af175bca8c664a475', 'e67d26b610dd414c884c4dbb24e71ce3', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:56', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c43d87b6340c29c0c354aa9c579f387f', '32feeb502544416c9bf41329c10a88f4', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c5dd2fc34ae825ebfced2ec74948654c', '402860816aa5921f016aa5921f480000', 'end_date', '请假结束时间', 'end_date', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c5f6ea01a6523a60df153cc61dc92f4d', 'fbc35f067da94a70adb622ddba259352', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c6730e00df5efd77fedf181df29102de', '402860816bff91c0016bff91c7010001', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c75a7cb0a21958aa7ca5442f66019669', 'e9faf717024b4aae95cff224ae9b6d97', 'depart', '所属部门', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c772ed9cbe2d1dc69e9ffa73d3487021', '4b556f0168f64976a3d20bfb932bc798', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-04-12 23:38:28', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c91b697b1bcd2be943fc746e2660bc9e', '402860816bff91c0016bff91d2810005', 'real_name', '姓名', 'real_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('c9b698d3f75aa780ee1eb67ef090b15b', '73162c3b8161413e8ecdca7eb288d0c9', 'wl_code', '物料编码', null, '0', '1', 'String', '200', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('caf5a071f2299c0f9ff2f3038d6d0fc6', '402860816bff91c0016bff91ca7e0002', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cb33a708b95e19085f8c9001d2d5c64c', 'e9faf717024b4aae95cff224ae9b6d97', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cb7da49a981a1b0acc5f7e8a0130bdcd', 'd35109c3632c4952a19ecc094943dd71', 'user_code', '用户编码', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '0', 'single', '', '', '10', 'admin', '2019-08-23 23:45:15', '2019-05-11 16:26:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cb871284e845e26e88242a96fac9c576', '402860816bff91c0016bff91c7010001', 'order_no', '序号', 'order_no', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cc1ddc1304d3eb5d9a189da0a509ccd0', '32f75e4043ef4070919dbd4337186a3d', 'create_by', '创建人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cee3c1dbf67b4a7d9626b8032897a4c7', '402860816bff91c0016bff91d8830007', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cf4c5a4c06ae6bac701edfeedfcd16aa', 'd3ae1c692b9640e0a091f8c46e17bb01', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('cfeb6491427aec2b4db9694af867da23', 'e9faf717024b4aae95cff224ae9b6d97', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d2551b70dc96a45a73b304bf755a996f', '402860816bff91c0016bff91d8830007', 'workplace', '工作单位', 'workplace', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d29dcdba14ea61808391fff2d927efea', '402860816bff91c0016bff91c0cb0000', 'work_summary', '工作总结', 'work_summary', '0', '1', 'Text', '65535', '0', '', '', '', '', 'textarea', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d3a701472d27ca8435d6a781a597038d', 'deea5a8ec619460c9245ba85dbc59e80', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-06-10 16:07:16', '2019-04-20 11:41:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d48bfd2a840f9b1d00bd3b5599dca0f0', '402860816bff91c0016bff91cda80003', 'post', '职务', 'post', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d4d8cae3cd9ea93e378fc14303eee105', 'd35109c3632c4952a19ecc094943dd71', 'create_by', '创建人登录名称', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d4dea775487aef5a7aea41791d3a65db', 'e5464aa8fa7b47c580e91593cf9b46dc', 'cycle_time', '发放周期(年)', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '11', 'admin', '2019-04-24 17:09:49', '2019-04-24 14:09:06', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d52c79620e21128fb69b4e8628cf25cc', 'dbf4675875e14676a3f9a8b2b8941140', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', null, null, '2019-05-27 18:02:07', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d530ab1bc3c51e8249a506a25d1003c7', '79091e8277c744158530321513119c68', 'start_time', '会签发起时间', null, '0', '1', 'Date', '32', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d53e70411c206efecb8dcd00174e907c', '62e29cdb81ac44d1a2d8ff89851b853d', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d5df0a35352ee960053686e959e9084b', '1acb6f81a1d9439da6cc4e868617b565', 'wl_unit', '计量单位', null, '0', '1', 'String', '100', '0', '', 'air_china_unit', '', '', 'list_multi', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d6fad89f4f26d733291863c2dfbc5945', '27fc5f91274344afa7673a732b279939', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d766ea5809e2ec9ff2cdbcb18f610ab3', '7ea60a25fa27470e9080d6a921aabbd1', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d7c3b107f004cbc99dfe1fe6c79894d8', '402860816bff91c0016bffa220a9000b', 'social_insurance_type', '参加社保类型', 'social_insurance_type', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '35', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d8234b56acea1a752271a6c911dd91a0', '7ea60a25fa27470e9080d6a921aabbd1', 'age', '年龄', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', null, null, '2019-04-17 00:22:21', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d86909d794b01ad7fbb5e61d28b6603b', '73162c3b8161413e8ecdca7eb288d0c9', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d949d9157831c2fb7ba9f175081fe036', '402860816bff91c0016bff91cda80003', 'school', '学校', 'school', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d9d308feb95968dbb322c9bff0c18452', '32feeb502544416c9bf41329c10a88f4', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d9dde2f59bb148c6b7e95256acad8972', 'e67d26b610dd414c884c4dbb24e71ce3', 'post_name', '岗位名字', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('d9f9ae6986cb85019a3a4030f62f4d1a', '402860816bff91c0016bff91cfea0004', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('db1fb3e12385cb967b63420cfe97cde6', '402860816bff91c0016bff91cda80003', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('dd3e1e13d7429807b83a00a090e060b7', '402860816bff91c0016bffa220a9000b', 'join_party_info', '入党（团）时间地点', 'join_party_info', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '26', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ddc302f84c75a5f056855c664b82202a', '402860816aa5921f016aa5921f480000', 'days', '请假天数', 'days', '0', '1', 'int', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ddcc14a2105588982b4ae657f2893d81', '32feeb502544416c9bf41329c10a88f4', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('dedb920a5e876e27eb144464209ebe1e', '27fc5f91274344afa7673a732b279939', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('df489194c6008f3bd21b2c1c11fde337', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '2', null, null, '2019-03-23 11:39:48', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e0c5d6e483897d5c4e7894dc66dd1aff', '32feeb502544416c9bf41329c10a88f4', 'bpm_status', '流程状态', null, '0', '1', 'String', '2', '0', '', 'bpm_status', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:58', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e24de426223dc0271a55eccc1d5457d0', '73162c3b8161413e8ecdca7eb288d0c9', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e2d73ccda7f10f5a1ccce3c48b1e699e', '402860816bff91c0016bffa220a9000b', 'residence_street', '户口所在街道', 'residence_street', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '27', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e4914fdff68ac72486ada105e6e9fa36', 'e9faf717024b4aae95cff224ae9b6d97', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e4a4c1d50b7b46678bc14fd5b90ee082', '73162c3b8161413e8ecdca7eb288d0c9', 'create_time', '创建日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e50b4398731e06572c247993a0dcc38d', 'd35109c3632c4952a19ecc094943dd71', 'name', '用户名', null, '0', '1', 'string', '200', '0', '', '', '', '', 'text', '', '120', '*', '0', '', '', '0', '1', '1', 'single', '', '', '5', 'admin', '2019-08-23 23:45:15', '2019-03-15 14:24:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e53f53766d1f7718b3ee5eabe105b969', '402860816bff91c0016bffa220a9000b', 'social_insurance_time', '五险一金日期', 'social_insurance_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '38', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e672d5974a06d5c37b3b4c94a6f29f96', '4028318169e81b970169e81b97650000', 'request_url', '请求路径', 'request_url', '0', '1', 'string', '255', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '9', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e7aade12ca722b59c1ec681d14247ff8', '402860816bff91c0016bff91d8830007', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '14', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e845925368919482df6dac58e6ed708d', '402860816bff91c0016bff91d8830007', 'phone', '联系方式', 'phone', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e88d328af34dd8a17f51437c52b68a2d', '402860816bff91c0016bff91cfea0004', 'create_by', '创建人', 'create_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('e99cc08f4d88dd8f788399db8d448ee8', '62e29cdb81ac44d1a2d8ff89851b853d', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ea644c4c208739640933ba6e568045c1', 'e2faf977fdaf4b25a524f58c2441a51c', 'ruz_date', '入职时间', null, '0', '1', 'Date', '32', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ea96d1c33f0f0d7245045e70a5793257', '402860816bff91c0016bffa220a9000b', 'current_postcode', '现居住地邮编', 'current_postcode', '0', '1', 'string', '10', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '31', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ebc41362681919cc680fcc58bf87fdcb', '1acb6f81a1d9439da6cc4e868617b565', 'price', '单价', null, '0', '1', 'double', '10', '2', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '12', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ec20e66d5eb9b8b7f58de9edc0f7630b', '1acb6f81a1d9439da6cc4e868617b565', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ec5e9cb5809b2f8ce1446df4a27693f0', '27fc5f91274344afa7673a732b279939', 'name', '用户名', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '7', 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ed16f23d08e7bcda11a1383fda68057e', '402860816bff91c0016bff91c7010001', 'employee_id', '员工ID', 'employee_id', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', 'oa_employee_info', 'id', '2', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('edda30c64e1dccee510d40b77a8ca094', 'fb7125a344a649b990c12949945cb6c1', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '0', '0', 'single', '', '', '3', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ee09e0e21fa350b9346b70292dcfca00', '79091e8277c744158530321513119c68', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ee4ffe04a25fcf556e78183f1f521546', '402860816aa5921f016aa5921f480000', 'create_by', '创建人id', 'create_by', '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '8', 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ee5803611f63643059b6375166d71567', '402860816bff91c0016bff91c7010001', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ef81373c5fd7130d7e23859d90c9eb3e', '402860816bff91c0016bff91cda80003', 'from_time', '开始日期', 'from_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '3', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('efd1b955a75b5046e9857e00fe94ae2c', 'fbc35f067da94a70adb622ddba259352', 'id', '主键', null, '1', '0', 'string', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '1', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f015cc2ffdcc2c4be1e9b3622eb69b52', 'fbc35f067da94a70adb622ddba259352', 'sys_org_code', '所属部门', null, '0', '1', 'string', '64', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '6', null, null, '2019-07-03 19:44:23', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f06b2bb01ea1fae487b7e3c3eb521d5b', 'd3ae1c692b9640e0a091f8c46e17bb01', 'create_by', '创建人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '2', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f0967fd139b440f79f21248bf4e4a209', 'd3ae1c692b9640e0a091f8c46e17bb01', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', null, null, '2019-07-24 14:47:30', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f0a453930aa16ca32f2e3be860bfe542', '402860816bff91c0016bffa220a9000b', 'education', '学历', 'education', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '14', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f12753b4a3815697a72017a7436fe733', 'e2faf977fdaf4b25a524f58c2441a51c', 'update_time', '更新日期', null, '0', '1', 'Date', '20', '0', '', '', '', '', 'datetime', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '5', 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f1ab7d3b55ade57eeac6c55b32ce813a', '1acb6f81a1d9439da6cc4e868617b565', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f1c7fde21f26c7ed64a0ef1095900c52', '4028318169e81b970169e81b97650000', 'request_type', '请求类型', 'request_type', '0', '1', 'string', '10', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '11', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f4647a91a4ac5d6d32bb0692b800bffe', '402860816bff91c0016bff91c0cb0000', 'probation_post', '试用期职位', 'probation_post', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f4b0bc7f3d6562e28d7c5e2d56510ecd', 'e5464aa8fa7b47c580e91593cf9b46dc', 'first_num', '首次数量', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '8', 'admin', '2019-04-24 17:09:49', '2019-04-24 14:31:31', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f558359b06aea79a992c102ce3563a4d', '4028318169e81b970169e81b97650000', 'username', '操作用户名称', 'username', '0', '1', 'string', '100', '0', null, null, null, null, 'text', null, '120', null, '0', null, null, '0', '1', '1', 'group', null, null, '6', null, null, '2019-04-04 19:28:36', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f6afcb7d8ea81879593ff737b55ddcc0', '402860816bff91c0016bff91cda80003', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f6f8aed87ec73994f6a12abbc079dbb1', '402860816bff91c0016bffa220a9000b', 'update_by', '更新人', 'update_by', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '41', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f7332af7586c83c87f7b9ea144a5292d', '62e29cdb81ac44d1a2d8ff89851b853d', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', null, null, '2019-05-11 14:01:14', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f76719783433487f4710232e2ae0e521', '402860816bff91c0016bff91cfea0004', 'id', 'id', 'id', '1', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'group', '', '', '1', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f7e7eb84ddc34d7e09d10af213ac6667', '402860816bff91c0016bff91d2810005', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '11', 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f81d7103c0c857e5c744cda2bc4c000a', '402860816bff91c0016bff91ca7e0002', 'cert_organizations', '发证机关', 'cert_organizations', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '6', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f8a0fd20a1173270afdfed1129d5c669', '402860816bff91c0016bffa220a9000b', 'depart_id', '所在部门id', null, '0', '1', 'String', '32', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '44', 'admin', '2019-07-22 16:15:32', '2019-07-19 15:33:44', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f8cc6be747fec10802eb625ac529c16f', '402860816bff91c0016bff91cfea0004', 'create_time', '创建时间', 'create_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '10', 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f94db83e41c69f407d3c9a81c5892269', '402860816bff91c0016bffa220a9000b', 'first_job_time', '首次工作时间', 'first_job_time', '0', '1', 'Date', '0', '0', '', '', '', '', 'date', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '22', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('f95d2cbefd25444909c83aaf8c4f72fb', '402860816bff91c0016bff91ca7e0002', 'memo', '备注', 'memo', '0', '1', 'string', '255', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '7', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fa3a12d7abf72b23afe425f8dbd57f86', '1acb6f81a1d9439da6cc4e868617b565', 'size_type', '尺码类型', null, '0', '1', 'String', '2', '0', '', 'air_china_size', '', '', 'list', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fa8f5a0ba673e0208934567462844eab', '402860816bff91c0016bff91ca7e0002', 'sys_org_code', '组织机构编码', 'sys_org_code', '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '13', 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fc55d467102c2c782286f546d7820c3d', '73162c3b8161413e8ecdca7eb288d0c9', 'pid', '父物料', null, '0', '1', 'String', '36', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'single', '', '', '9', 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fc76a3832d232829852cae6c66e44f67', '402860816bff91c0016bffa220a9000b', 'identity_no', '身份证号', 'identity_no', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '21', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fcd519058d68fa4dab192335602b5d24', '402860816bff91c0016bffa220a9000b', 'real_name', '姓名', 'real_name', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '5', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('fde00160a5d664effaa4b5552e814e74', 'fb7125a344a649b990c12949945cb6c1', 'sex', '性别', null, '0', '1', 'string', '32', '0', '', '', '', '', 'text', '', '120', '', '0', '', '', '0', '1', '1', 'single', '', '', '6', 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ff49b468e54e137032f7e4d976b83b5a', '402860816bff91c0016bffa220a9000b', 'politically_status', '政治面貌', 'politically_status', '0', '1', 'string', '20', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '8', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ff601f75d0e7ced226748eb8fba2c896', '402860816bff91c0016bff91d8830007', 'relation', '关系', 'relation', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '4', 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ffacafee9fa46eb297ca3252f95acef9', '402860816bff91c0016bffa220a9000b', 'school', '毕业学校', 'school', '0', '1', 'string', '100', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '1', '1', 'group', '', '', '9', 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin');
INSERT INTO `onl_cgform_field` VALUES ('ffcbf379fffabbd13aa2c22ce565ec12', '79091e8277c744158530321513119c68', 'update_by', '更新人', null, '0', '1', 'string', '50', '0', '', '', '', '', 'text', '', '120', null, '0', '', '', '0', '0', '0', 'single', '', '', '4', 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin');

-- ----------------------------
-- Table structure for `onl_cgform_head`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_head`;
CREATE TABLE `onl_cgform_head` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `table_name` varchar(50) NOT NULL COMMENT '表名',
  `table_type` int(11) NOT NULL COMMENT '表类型: 0单表、1主表、2附表',
  `table_version` int(11) DEFAULT '1' COMMENT '表版本',
  `table_txt` varchar(200) NOT NULL COMMENT '表说明',
  `is_checkbox` varchar(5) NOT NULL COMMENT '是否带checkbox',
  `is_db_synch` varchar(20) NOT NULL DEFAULT 'N' COMMENT '同步数据库状态',
  `is_page` varchar(5) NOT NULL COMMENT '是否分页',
  `is_tree` varchar(5) NOT NULL COMMENT '是否是树',
  `id_sequence` varchar(200) DEFAULT NULL COMMENT '主键生成序列',
  `id_type` varchar(100) DEFAULT NULL COMMENT '主键类型',
  `query_mode` varchar(10) NOT NULL COMMENT '查询模式',
  `relation_type` int(11) DEFAULT NULL COMMENT '映射关系 0一对多  1一对一',
  `sub_table_str` varchar(1000) DEFAULT NULL COMMENT '子表',
  `tab_order_num` int(11) DEFAULT NULL COMMENT '附表排序序号',
  `tree_parent_id_field` varchar(50) DEFAULT NULL COMMENT '树形表单父id',
  `tree_id_field` varchar(50) DEFAULT NULL COMMENT '树表主键字段',
  `tree_fieldname` varchar(50) DEFAULT NULL COMMENT '树开表单列字段',
  `form_category` varchar(50) NOT NULL DEFAULT 'bdfl_ptbd' COMMENT '表单分类',
  `form_template` varchar(50) DEFAULT NULL COMMENT 'PC表单模板',
  `form_template_mobile` varchar(50) DEFAULT NULL COMMENT '表单模板样式(移动端)',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `index_onlineform_table_name` (`table_name`) USING BTREE,
  KEY `index_form_templdate` (`form_template`) USING BTREE,
  KEY `index_templdate_mobile` (`form_template_mobile`) USING BTREE,
  KEY `index_onlineform_table_version` (`table_version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_head
-- ----------------------------
INSERT INTO `onl_cgform_head` VALUES ('56870166aba54ebfacb20ba6c770bd73', 'test_order_main', '2', '4', '测试订单主表', 'N', 'Y', 'Y', 'N', null, 'UUID', 'single', null, 'test_order_product', null, null, null, null, 'bdfl_include', '2', null, 'admin', '2019-04-20 11:39:54', 'admin', '2019-04-20 11:38:39');
INSERT INTO `onl_cgform_head` VALUES ('d35109c3632c4952a19ecc094943dd71', 'test_demo', '1', '14', '测试用户表', 'N', 'Y', 'Y', 'N', null, 'UUID', 'single', null, null, null, null, null, null, 'bdfl_include', '1', null, 'admin', '2019-03-22 21:51:35', 'admin', '2019-03-15 14:24:35');
INSERT INTO `onl_cgform_head` VALUES ('deea5a8ec619460c9245ba85dbc59e80', 'test_order_product', '3', '7', '订单产品明细', 'N', 'Y', 'Y', 'N', null, 'UUID', 'single', '0', null, null, null, null, null, 'bdfl_include', '1', null, 'admin', '2019-04-20 11:42:53', 'admin', '2019-04-20 11:41:19');

-- ----------------------------
-- Table structure for `onl_cgform_index`
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_index`;
CREATE TABLE `onl_cgform_index` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `cgform_head_id` varchar(32) DEFAULT NULL COMMENT '主表id',
  `index_name` varchar(100) DEFAULT NULL COMMENT '索引名称',
  `index_field` varchar(500) DEFAULT NULL COMMENT '索引栏位',
  `index_type` varchar(32) DEFAULT NULL COMMENT '索引类型',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_id` (`cgform_head_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of onl_cgform_index
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('quartzScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `JOB_NAME` `JOB_GROUP`';

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_announcement`
-- ----------------------------
DROP TABLE IF EXISTS `sys_announcement`;
CREATE TABLE `sys_announcement` (
  `id` varchar(32) NOT NULL,
  `titile` varchar(100) DEFAULT NULL COMMENT '标题',
  `msg_content` text COMMENT '内容',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `sender` varchar(100) DEFAULT NULL COMMENT '发布人',
  `priority` varchar(255) DEFAULT NULL COMMENT '优先级（L低，M中，H高）',
  `msg_category` varchar(10) NOT NULL DEFAULT '2' COMMENT '消息类型1:通知公告2:系统消息',
  `msg_type` varchar(10) DEFAULT NULL COMMENT '通告对象类型（USER:指定用户，ALL:全体用户）',
  `send_status` varchar(10) DEFAULT NULL COMMENT '发布状态（0未发布，1已发布，2已撤销）',
  `send_time` datetime DEFAULT NULL COMMENT '发布时间',
  `cancel_time` datetime DEFAULT NULL COMMENT '撤销时间',
  `del_flag` varchar(1) DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `user_ids` text COMMENT '指定用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统通告表';

-- ----------------------------
-- Records of sys_announcement
-- ----------------------------
INSERT INTO `sys_announcement` VALUES ('026c4b10e31d1bc90ee78d3d8bb93e2e', '66666666666666666444444', '<p>55</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:45:20', null, '0', 'admin', '2019-09-10 09:45:16', 'admin', '2019-09-10 09:45:20', null);
INSERT INTO `sys_announcement` VALUES ('03547f8fb41df9993fd5e14e66787106', '33344', '<p>6666</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:32:37', null, '0', 'admin', '2019-09-10 09:32:34', 'admin', '2019-09-10 09:32:37', null);
INSERT INTO `sys_announcement` VALUES ('04636df93c4bb53ef4acd3e4f6447ca5', '9999777', '<p>777</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:03:46', null, '0', 'admin', '2019-09-10 09:03:36', 'admin', '2019-09-10 09:03:46', null);
INSERT INTO `sys_announcement` VALUES ('17c6dcba98af95c3e43c6d2061b4864b', '77778888', '', null, null, 'admin', null, '2', 'ALL', '1', '2019-09-10 09:04:24', null, '0', 'admin', '2019-09-10 09:04:19', 'admin', '2019-09-10 09:04:24', null);
INSERT INTO `sys_announcement` VALUES ('1b714f8ebc3cc33f8b4f906103b6a18d', '5467567', null, null, null, 'admin', null, '2', null, '1', '2019-03-30 12:40:38', null, '0', 'admin', '2019-02-26 17:23:26', 'admin', '2019-02-26 17:35:10', null);
INSERT INTO `sys_announcement` VALUES ('1dbf5e5d90e40fde0dec28e83e71f8e0', '哈哈滚滚滚', null, null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 10:03:41', null, '0', 'admin', '2019-09-10 10:03:34', 'admin', '2019-09-10 10:03:41', null);
INSERT INTO `sys_announcement` VALUES ('2d41ee63d9a4a2a4c68a75610f1175ad', '99999999999', '<p>999</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:08:29', null, '0', 'admin', '2019-09-10 09:08:26', 'admin', '2019-09-10 09:08:29', null);
INSERT INTO `sys_announcement` VALUES ('3a878b538d336a847c202621e9c7e3b1', 'jjhj', null, null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 10:17:16', null, '0', 'admin', '2019-09-10 10:17:09', 'admin', '2019-09-10 10:17:16', null);
INSERT INTO `sys_announcement` VALUES ('3d11237ccdf62450d20bb8abdb331178', '111222', null, null, null, null, null, '2', null, '0', null, null, '1', 'admin', '2019-03-29 17:19:47', 'admin', '2019-03-29 17:19:50', null);
INSERT INTO `sys_announcement` VALUES ('3d9ff31c64f660f0884ec6f94e365108', '423432432', '<p>555</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:44:03', null, '0', 'admin', '2019-09-10 09:43:59', 'admin', '2019-09-10 09:44:03', null);
INSERT INTO `sys_announcement` VALUES ('6dc1ec36a03515ed0f3e312b36878d61', '111', null, null, null, null, null, '1', 'ALL', '0', null, null, '0', 'jeecg', '2019-09-07 13:42:38', null, null, null);
INSERT INTO `sys_announcement` VALUES ('72709e3ba330c11d62c3f34301e23e61', '7777884', '<p>7474</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:33:25', null, '0', 'admin', '2019-09-10 09:33:22', 'admin', '2019-09-10 09:33:25', null);
INSERT INTO `sys_announcement` VALUES ('748792387aa779cf3b5fd80139014a1a', '姐姐哈哈哈', null, null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 10:04:13', null, '0', 'admin', '2019-09-10 10:04:07', 'admin', '2019-09-10 10:04:13', null);
INSERT INTO `sys_announcement` VALUES ('7ef04e95f8de030b1d5f7a9144090dc6', '111', null, '2019-02-06 17:28:10', '2019-03-08 17:28:11', null, null, '2', null, '0', null, null, '1', 'admin', '2019-02-26 17:28:17', 'admin', '2019-03-26 19:59:49', null);
INSERT INTO `sys_announcement` VALUES ('93a9060a1c20e4bf98b3f768a02c2ff9', '111', '111', '2019-02-06 17:20:17', '2019-02-21 17:20:20', 'admin', 'M', '2', 'ALL', '1', '2019-02-26 17:24:29', null, '0', 'admin', '2019-02-26 17:16:26', 'admin', '2019-02-26 17:19:35', null);
INSERT INTO `sys_announcement` VALUES ('9d2a74618697caaac269f1a6b28c350a', '柔柔弱弱若若若若若', '<p style=\"padding-left: 40px;\">6666</p>', null, null, 'admin', null, '2', 'ALL', '1', '2019-09-10 09:32:06', null, '0', 'admin', '2019-09-10 09:31:51', 'admin', '2019-09-10 09:32:06', null);
INSERT INTO `sys_announcement` VALUES ('a22acea8d7842b9cb99f6a3c0bbe7eff', 'bbbb', '<p>bbb</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-09 02:49:00', null, '0', 'admin', '2019-09-09 02:48:40', 'admin', '2019-09-09 02:49:00', null);
INSERT INTO `sys_announcement` VALUES ('a79ae25daa297f1fdc3703a86094ad37', '8888999', '<p>999</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:05:20', null, '0', 'admin', '2019-09-10 09:05:14', 'admin', '2019-09-10 09:05:20', null);
INSERT INTO `sys_announcement` VALUES ('b0bb417835b51a8c84293a93669f1e56', '雨叫姐姐', null, null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 10:14:00', null, '0', 'admin', '2019-09-10 10:13:54', 'admin', '2019-09-10 10:14:00', null);
INSERT INTO `sys_announcement` VALUES ('c5052284b0b0ce472e3a9ddf877a3f59', '9999', '<p>999</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 08:37:26', null, '0', 'admin', '2019-09-10 08:33:56', 'admin', '2019-09-10 08:37:26', null);
INSERT INTO `sys_announcement` VALUES ('d09091bc06954094777f1c221df4ff7f', '000000000', '<p>8888</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:06:59', null, '0', 'admin', '2019-09-10 09:06:54', 'admin', '2019-09-10 09:06:59', null);
INSERT INTO `sys_announcement` VALUES ('de1dc57f31037079e1e55c8347fe6ef7', '222', '2222', '2019-02-06 17:28:26', '2019-02-23 17:28:28', 'admin', 'M', '2', 'ALL', '1', '2019-03-29 17:19:56', null, '1', 'admin', '2019-02-26 17:28:36', 'admin', '2019-02-26 17:28:40', null);
INSERT INTO `sys_announcement` VALUES ('e46a3614274d926d010b342c7725597d', '99999999999999997777', '<p>67</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 09:34:55', null, '0', 'admin', '2019-09-10 09:34:50', 'admin', '2019-09-10 09:34:55', null);
INSERT INTO `sys_announcement` VALUES ('e52f3eb6215f139cb2224c52517af3bd', '334', '334', null, null, null, null, '2', null, '0', null, null, '1', 'admin', '2019-03-30 12:40:28', 'admin', '2019-03-30 12:40:32', null);
INSERT INTO `sys_announcement` VALUES ('eea9d1a45a847d98a4c26dd7e77e6cd6', '7756556', '<p>777</p>', null, null, 'admin', null, '2', 'ALL', '1', '2019-09-10 09:33:02', null, '0', 'admin', '2019-09-10 09:33:00', 'admin', '2019-09-10 09:33:02', null);
INSERT INTO `sys_announcement` VALUES ('fb9d9474599e6ca37c102008fcc03b98', '8888', '<p>888</p>', null, null, 'admin', null, '1', 'ALL', '1', '2019-09-10 08:35:19', null, '0', 'admin', '2019-09-10 08:35:34', 'admin', '2019-09-10 08:35:19', null);

-- ----------------------------
-- Table structure for `sys_announcement_send`
-- ----------------------------
DROP TABLE IF EXISTS `sys_announcement_send`;
CREATE TABLE `sys_announcement_send` (
  `id` varchar(32) DEFAULT NULL,
  `annt_id` varchar(32) DEFAULT NULL COMMENT '通告ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `read_flag` varchar(10) DEFAULT NULL COMMENT '阅读状态（0未读，1已读）',
  `read_time` datetime DEFAULT NULL COMMENT '阅读时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户通告阅读标记表';

-- ----------------------------
-- Records of sys_announcement_send
-- ----------------------------
INSERT INTO `sys_announcement_send` VALUES ('646c0c405ec643d4dc4160db2446f8ff', '93a9060a1c20e4bf98b3f768a02c2ff9', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-09 02:47:35', 'admin', '2019-05-17 11:50:56', 'admin', '2019-09-09 02:47:35');
INSERT INTO `sys_announcement_send` VALUES ('0caf244cb14d61c84109290acbfc75ad', '93a9060a1c20e4bf98b3f768a02c2ff9', 'a75d45a015c44384a04449ee80dc3503', '1', '2019-09-04 10:52:17', 'jeecg', '2019-09-04 11:20:11', 'jeecg', '2019-09-04 10:52:17');
INSERT INTO `sys_announcement_send` VALUES ('8a0b25c741c1801951c5738c32759754', '93a9060a1c20e4bf98b3f768a02c2ff9', '662d318c3f74b1714d8af5e11f270bb3', '1', '2019-09-09 05:27:41', 'test', '2019-09-07 19:51:55', 'test', '2019-09-09 05:27:41');
INSERT INTO `sys_announcement_send` VALUES ('8f9b99951a8898958b079b78e70e97bb', '93a9060a1c20e4bf98b3f768a02c2ff9', 'a83197a3d87a4365bdf87726c51ccf47', '0', null, 'abc', '2019-09-08 11:41:01', null, null);
INSERT INTO `sys_announcement_send` VALUES ('3720e11416626294b4ff50fb33f75e05', 'a22acea8d7842b9cb99f6a3c0bbe7eff', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-09 02:49:13', 'admin', '2019-09-09 02:49:01', 'admin', '2019-09-09 02:49:13');
INSERT INTO `sys_announcement_send` VALUES ('e388e3d1af2099f5609806df7bd47003', 'a22acea8d7842b9cb99f6a3c0bbe7eff', '662d318c3f74b1714d8af5e11f270bb3', '1', '2019-09-09 03:57:10', 'test', '2019-09-09 03:26:39', 'test', '2019-09-09 03:57:10');
INSERT INTO `sys_announcement_send` VALUES ('0192800733d914e2f37be46aea324bb5', 'fb9d9474599e6ca37c102008fcc03b98', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 08:37:07', null, null);
INSERT INTO `sys_announcement_send` VALUES ('74f3d1069b152b9f60b079792e358e90', 'c5052284b0b0ce472e3a9ddf877a3f59', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 08:37:26', null, null);
INSERT INTO `sys_announcement_send` VALUES ('05c600a090a4476ee97599019763a112', '04636df93c4bb53ef4acd3e4f6447ca5', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:03:46', null, null);
INSERT INTO `sys_announcement_send` VALUES ('f6bb9afdf1e8e571875950cdf06ec91b', '17c6dcba98af95c3e43c6d2061b4864b', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:04:24', null, null);
INSERT INTO `sys_announcement_send` VALUES ('e81775e4bfaf1f3da1c19e17cfc758c4', 'a79ae25daa297f1fdc3703a86094ad37', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:05:28', 'admin', '2019-09-10 09:05:20', 'admin', '2019-09-10 09:05:28');
INSERT INTO `sys_announcement_send` VALUES ('5d17c3a325f3005b050ebf2007c235ae', 'd09091bc06954094777f1c221df4ff7f', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:06:59', null, null);
INSERT INTO `sys_announcement_send` VALUES ('22c11d4e4a4da5fdbbb61f02b1d035ec', '2d41ee63d9a4a2a4c68a75610f1175ad', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:08:30', null, null);
INSERT INTO `sys_announcement_send` VALUES ('350571d81a3290363757771956b5eec1', '9d2a74618697caaac269f1a6b28c350a', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:32:06', null, null);
INSERT INTO `sys_announcement_send` VALUES ('d3e1450c8b041654309e869f892f02b9', '03547f8fb41df9993fd5e14e66787106', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:32:37', null, null);
INSERT INTO `sys_announcement_send` VALUES ('f1e537eb0e10b8190edc309236594751', '03547f8fb41df9993fd5e14e66787106', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:32:38', null, null);
INSERT INTO `sys_announcement_send` VALUES ('eb88b328a86ae3dded9b2d7b01079e40', 'eea9d1a45a847d98a4c26dd7e77e6cd6', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:33:03', null, null);
INSERT INTO `sys_announcement_send` VALUES ('40e1e513341e8b482642ce512f2b412e', 'eea9d1a45a847d98a4c26dd7e77e6cd6', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:33:03', null, null);
INSERT INTO `sys_announcement_send` VALUES ('304ea7f3d59e7ea23ea562627655d527', '72709e3ba330c11d62c3f34301e23e61', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:33:27', 'admin', '2019-09-10 09:33:25', 'admin', '2019-09-10 09:33:27');
INSERT INTO `sys_announcement_send` VALUES ('3f2c0badf7948018ec0ef485f4e6ae4f', '72709e3ba330c11d62c3f34301e23e61', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:33:27', 'admin', '2019-09-10 09:33:25', 'admin', '2019-09-10 09:33:27');
INSERT INTO `sys_announcement_send` VALUES ('62b1fff2e4dc67c7690861b67b148ab2', 'e46a3614274d926d010b342c7725597d', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:35:02', 'admin', '2019-09-10 09:34:55', 'admin', '2019-09-10 09:35:02');
INSERT INTO `sys_announcement_send` VALUES ('f7a94246e86cb6702f1f72f25783a8c8', 'e46a3614274d926d010b342c7725597d', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:35:02', 'admin', '2019-09-10 09:34:55', 'admin', '2019-09-10 09:35:02');
INSERT INTO `sys_announcement_send` VALUES ('d6875c2b5740dd77d01b065178f36147', '3d9ff31c64f660f0884ec6f94e365108', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:44:04', null, null);
INSERT INTO `sys_announcement_send` VALUES ('855e0af597e2b8606491f44db5f1c3df', '3d9ff31c64f660f0884ec6f94e365108', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 09:44:04', null, null);
INSERT INTO `sys_announcement_send` VALUES ('f277f0b26c39979b8333e9033059e82c', '026c4b10e31d1bc90ee78d3d8bb93e2e', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 09:45:24', 'admin', '2019-09-10 09:45:20', 'admin', '2019-09-10 09:45:24');
INSERT INTO `sys_announcement_send` VALUES ('c680653a1e158e0822f60da1959e734c', '1dbf5e5d90e40fde0dec28e83e71f8e0', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 10:03:41', null, null);
INSERT INTO `sys_announcement_send` VALUES ('fdaeeb73938f7ee1582ebe8deca8983f', '748792387aa779cf3b5fd80139014a1a', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 10:04:13', null, null);
INSERT INTO `sys_announcement_send` VALUES ('b3fe4d8d2316d754702de9f9494f66bd', 'b0bb417835b51a8c84293a93669f1e56', 'e9ca23d68d884d4ebb19d07889727dae', '0', null, 'admin', '2019-09-10 10:14:00', null, null);
INSERT INTO `sys_announcement_send` VALUES ('a00f636b9b957491e81f36cff47de39c', '3a878b538d336a847c202621e9c7e3b1', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-09-10 10:17:20', 'admin', '2019-09-10 10:17:16', 'admin', '2019-09-10 10:17:20');
INSERT INTO `sys_announcement_send` VALUES ('0914e671dbee3603df268cbfc0c747b1', '026c4b10e31d1bc90ee78d3d8bb93e2e', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('d0f25a6afd4bd4465de275502f2f7fbe', '03547f8fb41df9993fd5e14e66787106', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('19b3c210b530a9095fa7d7d10d631529', '04636df93c4bb53ef4acd3e4f6447ca5', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('608212483a95fa827ed9112af204ef13', '17c6dcba98af95c3e43c6d2061b4864b', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('37ed1c436563c21ec6df256675906ce0', '1dbf5e5d90e40fde0dec28e83e71f8e0', '662d318c3f74b1714d8af5e11f270bb3', '1', '2019-09-10 15:06:28', 'test', '2019-09-10 15:05:47', 'test', '2019-09-10 15:06:28');
INSERT INTO `sys_announcement_send` VALUES ('8110c629a3ffe4108f6c1713cc8bf50f', '2d41ee63d9a4a2a4c68a75610f1175ad', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('69867d9c9739d99f0d2bd05a5f36c218', '3a878b538d336a847c202621e9c7e3b1', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('0de9898762c7d1f01abc2f9ecd6e5c9d', '3d9ff31c64f660f0884ec6f94e365108', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('62266ed7991ecaf95894309ce6916497', '72709e3ba330c11d62c3f34301e23e61', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('ceb9f06d4f4a07c56c6e817e06cb0e88', '748792387aa779cf3b5fd80139014a1a', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('e9f8097babf37e87a057cf4a0e626f36', '9d2a74618697caaac269f1a6b28c350a', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('428ca1d1e98f00e008967685f76fd384', 'a79ae25daa297f1fdc3703a86094ad37', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('f9a924a298075576d755fe5a462314f5', 'b0bb417835b51a8c84293a93669f1e56', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('a0891c0d605dad923bb7b4bfa512fe1e', 'c5052284b0b0ce472e3a9ddf877a3f59', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('0fc6e600212f4f84c086449405a5b5cd', 'd09091bc06954094777f1c221df4ff7f', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('d118e7ef8775dc07932fd039a606ccb5', 'e46a3614274d926d010b342c7725597d', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('99147f306976b8c9d9dc12d07222821b', 'eea9d1a45a847d98a4c26dd7e77e6cd6', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);
INSERT INTO `sys_announcement_send` VALUES ('80685df6cba5431b5946b0534b715715', 'fb9d9474599e6ca37c102008fcc03b98', '662d318c3f74b1714d8af5e11f270bb3', '0', null, 'test', '2019-09-10 15:05:47', null, null);

-- ----------------------------
-- Table structure for `sys_category`
-- ----------------------------
DROP TABLE IF EXISTS `sys_category`;
CREATE TABLE `sys_category` (
  `id` varchar(36) NOT NULL,
  `pid` varchar(36) DEFAULT NULL COMMENT '父级节点',
  `name` varchar(100) DEFAULT NULL COMMENT '类型名称',
  `code` varchar(100) DEFAULT NULL COMMENT '类型编码',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `has_child` varchar(3) DEFAULT NULL COMMENT '是否有子节点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_category
-- ----------------------------
INSERT INTO `sys_category` VALUES ('11269338cf80b620350f97688fbdecc7', '59c67023117dbad7a571b2c21582a04e', '222', '222', 'admin', '2019-08-23 18:52:03', null, null, 'A01', null);
INSERT INTO `sys_category` VALUES ('1d4fd1864e90c66ac1de76e68695a808', '0', '11', '111', 'admin', '2019-08-23 18:51:43', 'admin', '2019-08-23 18:51:49', 'A01', '1');
INSERT INTO `sys_category` VALUES ('59c67023117dbad7a571b2c21582a04e', '0', '22', '22', 'admin', '2019-08-23 18:51:57', 'admin', '2019-08-23 18:52:03', 'A01', '1');
INSERT INTO `sys_category` VALUES ('91d66a2458ef8f21b3bd8325b02150b3', '1d4fd1864e90c66ac1de76e68695a808', '111', '111', 'admin', '2019-08-23 18:51:49', null, null, 'A01', null);

-- ----------------------------
-- Table structure for `sys_data_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_data_log`;
CREATE TABLE `sys_data_log` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `data_table` varchar(32) DEFAULT NULL COMMENT '表名',
  `data_id` varchar(32) DEFAULT NULL COMMENT '数据ID',
  `data_content` text COMMENT '数据内容',
  `data_version` int(11) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sindex` (`data_table`,`data_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_data_log
-- ----------------------------
INSERT INTO `sys_data_log` VALUES ('402880f05ab0d198015ab12274bf0006', 'admin', '2017-03-09 11:35:09', null, null, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 23, 2016 12:00:00 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"9001\",\"status\":\"1\",\"content\":\"111\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', '3');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab700bead0009', 'admin', '2017-03-10 14:56:03', null, null, 'jeecg_demo', '402880f05ab6d12b015ab700be8d0008', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 2:56:03 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"111\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab700be8d0008\"}', '1');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab705a23f000d', 'admin', '2017-03-10 15:01:24', null, null, 'jeecg_demo', '402880f05ab6d12b015ab705a233000c', '{\"mobilePhone\":\"\",\"officePhone\":\"11\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:01:24 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"11\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab705a233000c\"}', '1');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab712a6420013', 'admin', '2017-03-10 15:15:37', null, null, 'jeecg_demo', '402880f05ab6d12b015ab712a6360012', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:15:37 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小王\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab712a6360012\"}', '1');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab712d0510015', 'admin', '2017-03-10 15:15:47', null, null, 'jeecg_demo', '402880f05ab6d12b015ab712a6360012', '{\"mobilePhone\":\"18611788525\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:15:37 AM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小王\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab712a6360012\"}', '2');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab71308240018', 'admin', '2017-03-10 15:16:02', null, null, 'jeecg_demo', '8a8ab0b246dc81120146dc81860f016f', '{\"mobilePhone\":\"13111111111\",\"officePhone\":\"66666666\",\"email\":\"demo@jeecg.com\",\"age\":12,\"salary\":10.00,\"birthday\":\"Feb 14, 2014 12:00:00 AM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小明\",\"status\":\"\",\"content\":\"\",\"id\":\"8a8ab0b246dc81120146dc81860f016f\"}', '1');
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab72806c3001b', 'admin', '2017-03-10 15:38:58', null, null, 'jeecg_demo', '8a8ab0b246dc81120146dc81860f016f', '{\"mobilePhone\":\"18611788888\",\"officePhone\":\"66666666\",\"email\":\"demo@jeecg.com\",\"age\":12,\"salary\":10.00,\"birthday\":\"Feb 14, 2014 12:00:00 AM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小明\",\"status\":\"\",\"content\":\"\",\"id\":\"8a8ab0b246dc81120146dc81860f016f\"}', '2');
INSERT INTO `sys_data_log` VALUES ('4028ef815318148a0153181567690001', 'admin', '2016-02-25 18:59:29', null, null, 'jeecg_demo', '4028ef815318148a0153181566270000', '{\"mobilePhone\":\"13423423423\",\"officePhone\":\"1\",\"email\":\"\",\"age\":1,\"salary\":1,\"birthday\":\"Feb 25, 2016 12:00:00 AM\",\"createDate\":\"Feb 25, 2016 6:59:24 PM\",\"depId\":\"402880e447e9a9570147e9b6a3be0005\",\"userName\":\"1\",\"status\":\"0\",\"id\":\"4028ef815318148a0153181566270000\"}', '1');
INSERT INTO `sys_data_log` VALUES ('4028ef815318148a01531815ec5c0003', 'admin', '2016-02-25 19:00:03', null, null, 'jeecg_demo', '4028ef815318148a0153181566270000', '{\"mobilePhone\":\"13426498659\",\"officePhone\":\"1\",\"email\":\"\",\"age\":1,\"salary\":1.00,\"birthday\":\"Feb 25, 2016 12:00:00 AM\",\"createDate\":\"Feb 25, 2016 6:59:24 AM\",\"depId\":\"402880e447e9a9570147e9b6a3be0005\",\"userName\":\"1\",\"status\":\"0\",\"id\":\"4028ef815318148a0153181566270000\"}', '2');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c0502e6b0003', 'admin', '2016-03-29 10:59:53', null, null, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"18455477548\",\"officePhone\":\"123\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', '1');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c0509aa40006', 'admin', '2016-03-29 11:00:21', null, null, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', '1');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c051c4a70008', 'admin', '2016-03-29 11:01:37', null, null, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', '2');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c051d4b5000a', 'admin', '2016-03-29 11:01:41', null, null, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"123\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', '2');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c07033d8000d', 'admin', '2016-03-29 11:34:52', null, null, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"123\",\"email\":\"\",\"age\":23,\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', '3');
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c070492e000f', 'admin', '2016-03-29 11:34:57', null, null, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"age\":22,\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', '3');
INSERT INTO `sys_data_log` VALUES ('4028ef81550c1a7901550c1cd7850002', 'admin', '2016-06-01 21:17:44', null, null, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 1, 2016 9:17:44 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"121221\",\"status\":\"0\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', '1');
INSERT INTO `sys_data_log` VALUES ('4028ef81568c31ec01568c3307080004', 'admin', '2016-08-15 11:16:09', null, null, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 23, 2016 12:00:00 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"9001\",\"status\":\"1\",\"content\":\"111\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', '2');

-- ----------------------------
-- Table structure for `sys_depart`
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父机构ID',
  `depart_name` varchar(100) NOT NULL COMMENT '机构/部门名称',
  `depart_name_en` varchar(500) DEFAULT NULL COMMENT '英文名',
  `depart_name_abbr` varchar(500) DEFAULT NULL COMMENT '缩写',
  `depart_order` int(11) DEFAULT '0' COMMENT '排序',
  `description` text COMMENT '描述',
  `org_type` varchar(10) DEFAULT NULL COMMENT '机构类型 1一级部门 2子部门',
  `org_code` varchar(64) NOT NULL COMMENT '机构编码',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号',
  `fax` varchar(32) DEFAULT NULL COMMENT '传真',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `memo` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` varchar(1) DEFAULT NULL COMMENT '状态（1启用，0不启用）',
  `del_flag` varchar(1) DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_depart_parent_id` (`parent_id`) USING BTREE,
  KEY `index_depart_depart_order` (`depart_order`) USING BTREE,
  KEY `index_depart_org_code` (`org_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='组织机构表';

-- ----------------------------
-- Records of sys_depart
-- ----------------------------
INSERT INTO `sys_depart` VALUES ('4f1765520d6346f9bd9c79e2479e5b12', 'c6d7cb4deeac411cb3384b1b31278596', '市场部', null, null, '0', null, '2', 'A01A03', null, null, null, null, null, '0', 'admin', '2019-02-20 17:15:34', 'admin', '2019-02-26 16:36:18');
INSERT INTO `sys_depart` VALUES ('5159cde220114246b045e574adceafe9', '6d35e179cd814e3299bd588ea7daed3f', '研发部', null, null, '0', null, '2', 'A02A02', null, null, null, null, null, '0', 'admin', '2019-02-26 16:44:38', 'admin', '2019-03-07 09:36:53');
INSERT INTO `sys_depart` VALUES ('57197590443c44f083d42ae24ef26a2c', 'c6d7cb4deeac411cb3384b1b31278596', '研发部', null, null, '0', null, '2', 'A01A05', null, null, null, null, null, '0', 'admin', '2019-02-21 16:14:41', 'admin', '2019-03-27 19:05:49');
INSERT INTO `sys_depart` VALUES ('67fc001af12a4f9b8458005d3f19934a', 'c6d7cb4deeac411cb3384b1b31278596', '财务部', null, null, '0', null, '2', 'A01A04', null, null, null, null, null, '0', 'admin', '2019-02-21 16:14:35', 'admin', '2019-02-25 12:49:41');
INSERT INTO `sys_depart` VALUES ('6d35e179cd814e3299bd588ea7daed3f', '', '卓尔互动公司', null, null, '0', null, '1', 'A02', null, null, null, null, null, '0', 'admin', '2019-02-26 16:36:39', 'admin', '2019-03-22 16:47:25');
INSERT INTO `sys_depart` VALUES ('743ba9dbdc114af8953a11022ef3096a', 'f28c6f53abd841ac87ead43afc483433', '财务部', null, null, '0', null, '2', 'A03A01', null, null, null, null, null, '0', 'admin', '2019-03-22 16:45:43', null, null);
INSERT INTO `sys_depart` VALUES ('a7d7e77e06c84325a40932163adcdaa6', '6d35e179cd814e3299bd588ea7daed3f', '财务部', null, null, '0', null, '2', 'A02A01', null, null, null, null, null, '0', 'admin', '2019-02-26 16:36:47', 'admin', '2019-02-26 16:37:25');
INSERT INTO `sys_depart` VALUES ('c6d7cb4deeac411cb3384b1b31278596', '', '北京国炬公司', null, null, '0', null, '1', 'A01', null, null, null, null, null, '0', 'admin', '2019-02-11 14:21:51', 'admin', '2019-03-22 16:47:19');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` varchar(32) NOT NULL,
  `dict_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `dict_code` varchar(100) DEFAULT NULL COMMENT '字典编码',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除状态',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `type` int(1) unsigned zerofill DEFAULT '0' COMMENT '字典类型0为string,1为number',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `indextable_dict_code` (`dict_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('0b5d19e1fce4b2e6647e6b4a17760c14', '通告类型', 'msg_category', '消息类型1:通知公告2:系统消息', '0', 'admin', '2019-04-22 18:01:35', null, null, '0');
INSERT INTO `sys_dict` VALUES ('236e8a4baff0db8c62c00dd95632834f', '同步工作流引擎', 'activiti_sync', '同步工作流引擎', '0', 'admin', '2019-05-15 15:27:33', null, null, '0');
INSERT INTO `sys_dict` VALUES ('2e02df51611a4b9632828ab7e5338f00', '权限策略', 'perms_type', '权限策略', '0', 'admin', '2019-04-26 18:26:55', null, null, '0');
INSERT INTO `sys_dict` VALUES ('2f0320997ade5dd147c90130f7218c3e', '推送类别', 'msg_type', '', '0', 'admin', '2019-03-17 21:21:32', 'admin', '2019-03-26 19:57:45', '0');
INSERT INTO `sys_dict` VALUES ('3486f32803bb953e7155dab3513dc68b', '删除状态', 'del_flag', null, '0', 'admin', '2019-01-18 21:46:26', 'admin', '2019-03-30 11:17:11', '0');
INSERT INTO `sys_dict` VALUES ('3d9a351be3436fbefb1307d4cfb49bf2', '性别', 'sex', null, '0', null, '2019-01-04 14:56:32', 'admin', '2019-03-30 11:28:27', '1');
INSERT INTO `sys_dict` VALUES ('404a04a15f371566c658ee9ef9fc392a', 'cehis2', '22', null, '1', 'admin', '2019-01-30 11:17:21', 'admin', '2019-03-30 11:18:12', '0');
INSERT INTO `sys_dict` VALUES ('4274efc2292239b6f000b153f50823ff', '全局权限策略', 'global_perms_type', '全局权限策略', '0', 'admin', '2019-05-10 17:54:05', null, null, '0');
INSERT INTO `sys_dict` VALUES ('4c03fca6bf1f0299c381213961566349', 'Online图表展示模板', 'online_graph_display_template', 'Online图表展示模板', '0', 'admin', '2019-04-12 17:28:50', null, null, '0');
INSERT INTO `sys_dict` VALUES ('4c753b5293304e7a445fd2741b46529d', '字典状态', 'dict_item_status', null, '0', 'admin', '2020-06-18 23:18:42', 'admin', '2019-03-30 19:33:52', '1');
INSERT INTO `sys_dict` VALUES ('4d7fec1a7799a436d26d02325eff295e', '优先级', 'priority', '优先级', '0', 'admin', '2019-03-16 17:03:34', 'admin', '2019-04-16 17:39:23', '0');
INSERT INTO `sys_dict` VALUES ('4e4602b3e3686f0911384e188dc7efb4', '条件规则', 'rule_conditions', '', '0', 'admin', '2019-04-01 10:15:03', 'admin', '2019-04-01 10:30:47', '0');
INSERT INTO `sys_dict` VALUES ('4f69be5f507accea8d5df5f11346181a', '发送消息类型', 'msgType', null, '0', 'admin', '2019-04-11 14:27:09', null, null, '0');
INSERT INTO `sys_dict` VALUES ('68168534ff5065a152bfab275c2136f8', '有效无效状态', 'valid_status', '有效无效状态', '0', 'admin', '2020-09-26 19:21:14', 'admin', '2019-04-26 19:21:23', '0');
INSERT INTO `sys_dict` VALUES ('6b78e3f59faec1a4750acff08030a79b', '用户类型', 'user_type', null, '1', null, '2019-01-04 14:59:01', 'admin', '2019-03-18 23:28:18', '0');
INSERT INTO `sys_dict` VALUES ('72cce0989df68887546746d8f09811aa', 'Online表单类型', 'cgform_table_type', '', '0', 'admin', '2019-01-27 10:13:02', 'admin', '2019-03-30 11:37:36', '0');
INSERT INTO `sys_dict` VALUES ('78bda155fe380b1b3f175f1e88c284c6', '流程状态', 'bpm_status', '流程状态', '0', 'admin', '2019-05-09 16:31:52', null, null, '0');
INSERT INTO `sys_dict` VALUES ('83bfb33147013cc81640d5fd9eda030c', '日志类型', 'log_type', null, '0', 'admin', '2019-03-18 23:22:19', null, null, '1');
INSERT INTO `sys_dict` VALUES ('845da5006c97754728bf48b6a10f79cc', '状态', 'status', null, '1', 'admin', '2019-03-18 21:45:25', 'admin', '2019-03-18 21:58:25', '0');
INSERT INTO `sys_dict` VALUES ('880a895c98afeca9d9ac39f29e67c13e', '操作类型', 'operate_type', '操作类型', '0', 'admin', '2019-07-22 10:54:29', null, null, '0');
INSERT INTO `sys_dict` VALUES ('8dfe32e2d29ea9430a988b3b558bf233', '发布状态', 'send_status', '发布状态', '0', 'admin', '2019-04-16 17:40:42', null, null, '0');
INSERT INTO `sys_dict` VALUES ('a7adbcd86c37f7dbc9b66945c82ef9e6', '1是0否', 'yn', '', '1', 'admin', '2019-05-22 19:29:29', null, null, '0');
INSERT INTO `sys_dict` VALUES ('a9d9942bd0eccb6e89de92d130ec4c4a', '消息发送状态', 'msgSendStatus', null, '0', 'admin', '2019-04-12 18:18:17', null, null, '0');
INSERT INTO `sys_dict` VALUES ('ac2f7c0c5c5775fcea7e2387bcb22f01', '菜单类型', 'menu_type', null, '0', 'admin', '2020-12-18 23:24:32', 'admin', '2019-04-01 15:27:06', '1');
INSERT INTO `sys_dict` VALUES ('ad7c65ba97c20a6805d5dcdf13cdaf36', 'onlineT类型', 'ceshi_online', null, '1', 'admin', '2019-03-22 16:31:49', 'admin', '2019-03-22 16:34:16', '0');
INSERT INTO `sys_dict` VALUES ('bd1b8bc28e65d6feefefb6f3c79f42fd', 'Online图表数据类型', 'online_graph_data_type', 'Online图表数据类型', '0', 'admin', '2019-04-12 17:24:24', 'admin', '2019-04-12 17:24:57', '0');
INSERT INTO `sys_dict` VALUES ('c36169beb12de8a71c8683ee7c28a503', '部门状态', 'depart_status', null, '0', 'admin', '2019-03-18 21:59:51', null, null, '0');
INSERT INTO `sys_dict` VALUES ('c5a14c75172783d72cbee6ee7f5df5d1', 'Online图表类型', 'online_graph_type', 'Online图表类型', '0', 'admin', '2019-04-12 17:04:06', null, null, '0');
INSERT INTO `sys_dict` VALUES ('d6e1152968b02d69ff358c75b48a6ee1', '流程类型', 'bpm_process_type', null, '0', 'admin', '2021-02-22 19:26:54', 'admin', '2019-03-30 18:14:44', '0');
INSERT INTO `sys_dict` VALUES ('fc6cd58fde2e8481db10d3a1e68ce70c', '用户状态', 'user_status', null, '0', 'admin', '2019-03-18 21:57:25', 'admin', '2019-03-18 23:11:58', '1');

-- ----------------------------
-- Table structure for `sys_dict_item`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item` (
  `id` varchar(32) NOT NULL,
  `dict_id` varchar(32) DEFAULT NULL COMMENT '字典id',
  `item_text` varchar(100) DEFAULT NULL COMMENT '字典项文本',
  `item_value` varchar(100) DEFAULT NULL COMMENT '字典项值',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `sort_order` int(10) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态（1启用 0不启用）',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_dict_id` (`dict_id`) USING BTREE,
  KEY `index_table_sort_order` (`sort_order`) USING BTREE,
  KEY `index_table_dict_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('0072d115e07c875d76c9b022e2179128', '4d7fec1a7799a436d26d02325eff295e', '低', 'L', '低', '3', '1', 'admin', '2019-04-16 17:04:59', null, null);
INSERT INTO `sys_dict_item` VALUES ('05a2e732ce7b00aa52141ecc3e330b4e', '3486f32803bb953e7155dab3513dc68b', '已删除', '1', null, null, '1', 'admin', '2025-10-18 21:46:56', 'admin', '2019-03-28 22:23:20');
INSERT INTO `sys_dict_item` VALUES ('096c2e758d823def3855f6376bc736fb', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'SQL', 'sql', null, '1', '1', 'admin', '2019-04-12 17:26:26', null, null);
INSERT INTO `sys_dict_item` VALUES ('0c9532916f5cd722017b46bc4d953e41', '2f0320997ade5dd147c90130f7218c3e', '指定用户', 'USER', null, null, '1', 'admin', '2019-03-17 21:22:19', 'admin', '2019-03-17 21:22:28');
INSERT INTO `sys_dict_item` VALUES ('0ca4beba9efc4f9dd54af0911a946d5c', '72cce0989df68887546746d8f09811aa', '附表', '3', null, '3', '1', 'admin', '2019-03-27 10:13:43', null, null);
INSERT INTO `sys_dict_item` VALUES ('1030a2652608f5eac3b49d70458b8532', '2e02df51611a4b9632828ab7e5338f00', '禁用', '2', '禁用', '2', '1', 'admin', '2021-03-26 18:27:28', 'admin', '2019-04-26 18:39:11');
INSERT INTO `sys_dict_item` VALUES ('147c48ff4b51545032a9119d13f3222a', 'd6e1152968b02d69ff358c75b48a6ee1', '测试流程', 'test', null, '1', '1', 'admin', '2019-03-22 19:27:05', null, null);
INSERT INTO `sys_dict_item` VALUES ('1543fe7e5e26fb97cdafe4981bedc0c8', '4c03fca6bf1f0299c381213961566349', '单排布局', 'single', null, '2', '1', 'admin', '2022-07-12 17:43:39', 'admin', '2019-04-12 17:43:57');
INSERT INTO `sys_dict_item` VALUES ('1b8a6341163062dad8cb2fddd34e0c3b', '404a04a15f371566c658ee9ef9fc392a', '22', '222', null, '1', '1', 'admin', '2019-03-30 11:17:48', null, null);
INSERT INTO `sys_dict_item` VALUES ('1ce390c52453891f93514c1bd2795d44', 'ad7c65ba97c20a6805d5dcdf13cdaf36', '000', '00', null, '1', '1', 'admin', '2019-03-22 16:34:34', null, null);
INSERT INTO `sys_dict_item` VALUES ('1db531bcff19649fa82a644c8a939dc4', '4c03fca6bf1f0299c381213961566349', '组合布局', 'combination', '', '4', '1', 'admin', '2019-05-11 16:07:08', null, null);
INSERT INTO `sys_dict_item` VALUES ('222705e11ef0264d4214affff1fb4ff9', '4f69be5f507accea8d5df5f11346181a', '短信', '1', '', '1', '1', 'admin', '2023-02-28 10:50:36', 'admin', '2019-04-28 10:58:11');
INSERT INTO `sys_dict_item` VALUES ('23a5bb76004ed0e39414e928c4cde155', '4e4602b3e3686f0911384e188dc7efb4', '不等于', '!=', '不等于', '3', '1', 'admin', '2019-04-01 16:46:15', 'admin', '2019-04-01 17:48:40');
INSERT INTO `sys_dict_item` VALUES ('25847e9cb661a7c711f9998452dc09e6', '4e4602b3e3686f0911384e188dc7efb4', '小于等于', '<=', '小于等于', '6', '1', 'admin', '2019-04-01 16:44:34', 'admin', '2019-04-01 17:49:10');
INSERT INTO `sys_dict_item` VALUES ('2d51376643f220afdeb6d216a8ac2c01', '68168534ff5065a152bfab275c2136f8', '有效', '1', '有效', '2', '1', 'admin', '2019-04-26 19:22:01', null, null);
INSERT INTO `sys_dict_item` VALUES ('308c8aadf0c37ecdde188b97ca9833f5', '8dfe32e2d29ea9430a988b3b558bf233', '已发布', '1', '已发布', '2', '1', 'admin', '2019-04-16 17:41:24', null, null);
INSERT INTO `sys_dict_item` VALUES ('333e6b2196e01ef9a5f76d74e86a6e33', '8dfe32e2d29ea9430a988b3b558bf233', '未发布', '0', '未发布', '1', '1', 'admin', '2019-04-16 17:41:12', null, null);
INSERT INTO `sys_dict_item` VALUES ('337ea1e401bda7233f6258c284ce4f50', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'JSON', 'json', null, '1', '1', 'admin', '2019-04-12 17:26:33', null, null);
INSERT INTO `sys_dict_item` VALUES ('33bc9d9f753cf7dc40e70461e50fdc54', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送失败', '2', null, '3', '1', 'admin', '2019-04-12 18:20:02', null, null);
INSERT INTO `sys_dict_item` VALUES ('3fbc03d6c994ae06d083751248037c0e', '78bda155fe380b1b3f175f1e88c284c6', '已完成', '3', '已完成', '3', '1', 'admin', '2019-05-09 16:33:25', null, null);
INSERT INTO `sys_dict_item` VALUES ('41d7aaa40c9b61756ffb1f28da5ead8e', '0b5d19e1fce4b2e6647e6b4a17760c14', '通知公告', '1', null, '1', '1', 'admin', '2019-04-22 18:01:57', null, null);
INSERT INTO `sys_dict_item` VALUES ('41fa1e9571505d643aea87aeb83d4d76', '4e4602b3e3686f0911384e188dc7efb4', '等于', '=', '等于', '4', '1', 'admin', '2019-04-01 16:45:24', 'admin', '2019-04-01 17:49:00');
INSERT INTO `sys_dict_item` VALUES ('43d2295b8610adce9510ff196a49c6e9', '845da5006c97754728bf48b6a10f79cc', '正常', '1', null, null, '1', 'admin', '2019-03-18 21:45:51', null, null);
INSERT INTO `sys_dict_item` VALUES ('4f05fb5376f4c61502c5105f52e4dd2b', '83bfb33147013cc81640d5fd9eda030c', '操作日志', '2', null, null, '1', 'admin', '2019-03-18 23:22:49', null, null);
INSERT INTO `sys_dict_item` VALUES ('50223341bfb5ba30bf6319789d8d17fe', 'd6e1152968b02d69ff358c75b48a6ee1', '业务办理', 'business', null, '3', '1', 'admin', '2023-04-22 19:28:05', 'admin', '2019-03-22 23:24:39');
INSERT INTO `sys_dict_item` VALUES ('51222413e5906cdaf160bb5c86fb827c', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '是', '1', '', '1', '1', 'admin', '2019-05-22 19:29:45', null, null);
INSERT INTO `sys_dict_item` VALUES ('538fca35afe004972c5f3947c039e766', '2e02df51611a4b9632828ab7e5338f00', '显示', '1', '显示', '1', '1', 'admin', '2025-03-26 18:27:13', 'admin', '2019-04-26 18:39:07');
INSERT INTO `sys_dict_item` VALUES ('5584c21993bde231bbde2b966f2633ac', '4e4602b3e3686f0911384e188dc7efb4', '自定义SQL表达式', 'USE_SQL_RULES', '自定义SQL表达式', '9', '1', 'admin', '2019-04-01 10:45:24', 'admin', '2019-04-01 17:49:27');
INSERT INTO `sys_dict_item` VALUES ('58b73b344305c99b9d8db0fc056bbc0a', '72cce0989df68887546746d8f09811aa', '主表', '2', null, '2', '1', 'admin', '2019-03-27 10:13:36', null, null);
INSERT INTO `sys_dict_item` VALUES ('5b65a88f076b32e8e69d19bbaadb52d5', '2f0320997ade5dd147c90130f7218c3e', '全体用户', 'ALL', null, null, '1', 'admin', '2020-10-17 21:22:43', 'admin', '2019-03-28 22:17:09');
INSERT INTO `sys_dict_item` VALUES ('5d833f69296f691843ccdd0c91212b6b', '880a895c98afeca9d9ac39f29e67c13e', '修改', '3', '', '3', '1', 'admin', '2019-07-22 10:55:07', 'admin', '2019-07-22 10:55:41');
INSERT INTO `sys_dict_item` VALUES ('5d84a8634c8fdfe96275385075b105c9', '3d9a351be3436fbefb1307d4cfb49bf2', '女', '2', null, '2', '1', null, '2019-01-04 14:56:56', null, '2019-01-04 17:38:12');
INSERT INTO `sys_dict_item` VALUES ('66c952ae2c3701a993e7db58f3baf55e', '4e4602b3e3686f0911384e188dc7efb4', '大于', '>', '大于', '1', '1', 'admin', '2019-04-01 10:45:46', 'admin', '2019-04-01 17:48:29');
INSERT INTO `sys_dict_item` VALUES ('6937c5dde8f92e9a00d4e2ded9198694', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'easyui', '3', null, '1', '1', 'admin', '2019-03-22 16:32:15', null, null);
INSERT INTO `sys_dict_item` VALUES ('69cacf64e244100289ddd4aa9fa3b915', 'a9d9942bd0eccb6e89de92d130ec4c4a', '未发送', '0', null, '1', '1', 'admin', '2019-04-12 18:19:23', null, null);
INSERT INTO `sys_dict_item` VALUES ('6a7a9e1403a7943aba69e54ebeff9762', '4f69be5f507accea8d5df5f11346181a', '邮件', '2', '', '2', '1', 'admin', '2031-02-28 10:50:44', 'admin', '2019-04-28 10:59:03');
INSERT INTO `sys_dict_item` VALUES ('6c682d78ddf1715baf79a1d52d2aa8c2', '72cce0989df68887546746d8f09811aa', '单表', '1', null, '1', '1', 'admin', '2019-03-27 10:13:29', null, null);
INSERT INTO `sys_dict_item` VALUES ('6d404fd2d82311fbc87722cd302a28bc', '4e4602b3e3686f0911384e188dc7efb4', '模糊', 'LIKE', '模糊', '7', '1', 'admin', '2019-04-01 16:46:02', 'admin', '2019-04-01 17:49:20');
INSERT INTO `sys_dict_item` VALUES ('6d4e26e78e1a09699182e08516c49fc4', '4d7fec1a7799a436d26d02325eff295e', '高', 'H', '高', '1', '1', 'admin', '2019-04-16 17:04:24', null, null);
INSERT INTO `sys_dict_item` VALUES ('700e9f030654f3f90e9ba76ab0713551', '6b78e3f59faec1a4750acff08030a79b', '333', '333', null, null, '1', 'admin', '2019-02-21 19:59:47', null, null);
INSERT INTO `sys_dict_item` VALUES ('7050c1522702bac3be40e3b7d2e1dfd8', 'c5a14c75172783d72cbee6ee7f5df5d1', '柱状图', 'bar', null, '1', '1', 'admin', '2019-04-12 17:05:17', null, null);
INSERT INTO `sys_dict_item` VALUES ('71b924faa93805c5c1579f12e001c809', 'd6e1152968b02d69ff358c75b48a6ee1', 'OA办公', 'oa', null, '2', '1', 'admin', '2021-03-22 19:27:17', 'admin', '2019-03-22 23:24:36');
INSERT INTO `sys_dict_item` VALUES ('75b260d7db45a39fc7f21badeabdb0ed', 'c36169beb12de8a71c8683ee7c28a503', '不启用', '0', null, null, '1', 'admin', '2019-03-18 23:29:41', 'admin', '2019-03-18 23:29:54');
INSERT INTO `sys_dict_item` VALUES ('7688469db4a3eba61e6e35578dc7c2e5', 'c36169beb12de8a71c8683ee7c28a503', '启用', '1', null, null, '1', 'admin', '2019-03-18 23:29:28', null, null);
INSERT INTO `sys_dict_item` VALUES ('78ea6cadac457967a4b1c4eb7aaa418c', 'fc6cd58fde2e8481db10d3a1e68ce70c', '正常', '1', null, null, '1', 'admin', '2019-03-18 23:30:28', null, null);
INSERT INTO `sys_dict_item` VALUES ('7ccf7b80c70ee002eceb3116854b75cb', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '按钮权限', '2', null, null, '1', 'admin', '2019-03-18 23:25:40', null, null);
INSERT INTO `sys_dict_item` VALUES ('81fb2bb0e838dc68b43f96cc309f8257', 'fc6cd58fde2e8481db10d3a1e68ce70c', '冻结', '2', null, null, '1', 'admin', '2019-03-18 23:30:37', null, null);
INSERT INTO `sys_dict_item` VALUES ('83250269359855501ec4e9c0b7e21596', '4274efc2292239b6f000b153f50823ff', '显示/访问(授权后显示/可访问)', '1', '', '1', '1', 'admin', '2019-05-10 17:54:51', null, null);
INSERT INTO `sys_dict_item` VALUES ('84778d7e928bc843ad4756db1322301f', '4e4602b3e3686f0911384e188dc7efb4', '大于等于', '>=', '大于等于', '5', '1', 'admin', '2019-04-01 10:46:02', 'admin', '2019-04-01 17:49:05');
INSERT INTO `sys_dict_item` VALUES ('848d4da35ebd93782029c57b103e5b36', 'c5a14c75172783d72cbee6ee7f5df5d1', '饼图', 'pie', null, '3', '1', 'admin', '2019-04-12 17:05:49', null, null);
INSERT INTO `sys_dict_item` VALUES ('84dfc178dd61b95a72900fcdd624c471', '78bda155fe380b1b3f175f1e88c284c6', '处理中', '2', '处理中', '2', '1', 'admin', '2019-05-09 16:33:01', null, null);
INSERT INTO `sys_dict_item` VALUES ('86f19c7e0a73a0bae451021ac05b99dd', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '子菜单', '1', null, null, '1', 'admin', '2019-03-18 23:25:27', null, null);
INSERT INTO `sys_dict_item` VALUES ('8bccb963e1cd9e8d42482c54cc609ca2', '4f69be5f507accea8d5df5f11346181a', '微信', '3', null, '3', '1', 'admin', '2021-05-11 14:29:12', 'admin', '2019-04-11 14:29:31');
INSERT INTO `sys_dict_item` VALUES ('8c618902365ca681ebbbe1e28f11a548', '4c753b5293304e7a445fd2741b46529d', '启用', '1', '', '0', '1', 'admin', '2020-07-18 23:19:27', 'admin', '2019-05-17 14:51:18');
INSERT INTO `sys_dict_item` VALUES ('8cdf08045056671efd10677b8456c999', '4274efc2292239b6f000b153f50823ff', '可编辑(未授权时禁用)', '2', '', '2', '1', 'admin', '2019-05-10 17:55:38', null, null);
INSERT INTO `sys_dict_item` VALUES ('8ff48e657a7c5090d4f2a59b37d1b878', '4d7fec1a7799a436d26d02325eff295e', '中', 'M', '中', '2', '1', 'admin', '2019-04-16 17:04:40', null, null);
INSERT INTO `sys_dict_item` VALUES ('948923658baa330319e59b2213cda97c', '880a895c98afeca9d9ac39f29e67c13e', '添加', '2', '', '2', '1', 'admin', '2019-07-22 10:54:59', 'admin', '2019-07-22 10:55:36');
INSERT INTO `sys_dict_item` VALUES ('9a96c4a4e4c5c9b4e4d0cbf6eb3243cc', '4c753b5293304e7a445fd2741b46529d', '不启用', '0', null, '1', '1', 'admin', '2019-03-18 23:19:53', null, null);
INSERT INTO `sys_dict_item` VALUES ('a1e7d1ca507cff4a480c8caba7c1339e', '880a895c98afeca9d9ac39f29e67c13e', '导出', '6', '', '6', '1', 'admin', '2019-07-22 12:06:50', null, null);
INSERT INTO `sys_dict_item` VALUES ('a2321496db6febc956a6c70fab94cb0c', '404a04a15f371566c658ee9ef9fc392a', '3', '3', null, '1', '1', 'admin', '2019-03-30 11:18:18', null, null);
INSERT INTO `sys_dict_item` VALUES ('a2be752dd4ec980afaec1efd1fb589af', '8dfe32e2d29ea9430a988b3b558bf233', '已撤销', '2', '已撤销', '3', '1', 'admin', '2019-04-16 17:41:39', null, null);
INSERT INTO `sys_dict_item` VALUES ('aa0d8a8042a18715a17f0a888d360aa4', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '一级菜单', '0', null, null, '1', 'admin', '2019-03-18 23:24:52', null, null);
INSERT INTO `sys_dict_item` VALUES ('adcf2a1fe93bb99a84833043f475fe0b', '4e4602b3e3686f0911384e188dc7efb4', '包含', 'IN', '包含', '8', '1', 'admin', '2019-04-01 16:45:47', 'admin', '2019-04-01 17:49:24');
INSERT INTO `sys_dict_item` VALUES ('b029a41a851465332ee4ee69dcf0a4c2', '0b5d19e1fce4b2e6647e6b4a17760c14', '系统消息', '2', null, '1', '1', 'admin', '2019-02-22 18:02:08', 'admin', '2019-04-22 18:02:13');
INSERT INTO `sys_dict_item` VALUES ('b2a8b4bb2c8e66c2c4b1bb086337f393', '3486f32803bb953e7155dab3513dc68b', '正常', '0', null, null, '1', 'admin', '2022-10-18 21:46:48', 'admin', '2019-03-28 22:22:20');
INSERT INTO `sys_dict_item` VALUES ('b57f98b88363188daf38d42f25991956', '6b78e3f59faec1a4750acff08030a79b', '22', '222', null, null, '0', 'admin', '2019-02-21 19:59:43', 'admin', '2019-03-11 21:23:27');
INSERT INTO `sys_dict_item` VALUES ('b5f3bd5f66bb9a83fecd89228c0d93d1', '68168534ff5065a152bfab275c2136f8', '无效', '0', '无效', '1', '1', 'admin', '2019-04-26 19:21:49', null, null);
INSERT INTO `sys_dict_item` VALUES ('b9fbe2a3602d4a27b45c100ac5328484', '78bda155fe380b1b3f175f1e88c284c6', '待提交', '1', '待提交', '1', '1', 'admin', '2019-05-09 16:32:35', null, null);
INSERT INTO `sys_dict_item` VALUES ('ba27737829c6e0e582e334832703d75e', '236e8a4baff0db8c62c00dd95632834f', '同步', '1', '同步', '1', '1', 'admin', '2019-05-15 15:28:15', null, null);
INSERT INTO `sys_dict_item` VALUES ('bcec04526b04307e24a005d6dcd27fd6', '880a895c98afeca9d9ac39f29e67c13e', '导入', '5', '', '5', '1', 'admin', '2019-07-22 12:06:41', null, null);
INSERT INTO `sys_dict_item` VALUES ('c53da022b9912e0aed691bbec3c78473', '880a895c98afeca9d9ac39f29e67c13e', '查询', '1', '', '1', '1', 'admin', '2019-07-22 10:54:51', null, null);
INSERT INTO `sys_dict_item` VALUES ('c5700a71ad08994d18ad1dacc37a71a9', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '否', '0', '', '1', '1', 'admin', '2019-05-22 19:29:55', null, null);
INSERT INTO `sys_dict_item` VALUES ('cbfcc5b88fc3a90975df23ffc8cbe29c', 'c5a14c75172783d72cbee6ee7f5df5d1', '曲线图', 'line', null, '2', '1', 'admin', '2019-05-12 17:05:30', 'admin', '2019-04-12 17:06:06');
INSERT INTO `sys_dict_item` VALUES ('d217592908ea3e00ff986ce97f24fb98', 'c5a14c75172783d72cbee6ee7f5df5d1', '数据列表', 'table', null, '4', '1', 'admin', '2019-04-12 17:05:56', null, null);
INSERT INTO `sys_dict_item` VALUES ('df168368dcef46cade2aadd80100d8aa', '3d9a351be3436fbefb1307d4cfb49bf2', '男', '1', null, '1', '1', null, '2027-08-04 14:56:49', 'admin', '2019-03-23 22:44:44');
INSERT INTO `sys_dict_item` VALUES ('e6329e3a66a003819e2eb830b0ca2ea0', '4e4602b3e3686f0911384e188dc7efb4', '小于', '<', '小于', '2', '1', 'admin', '2019-04-01 16:44:15', 'admin', '2019-04-01 17:48:34');
INSERT INTO `sys_dict_item` VALUES ('e94eb7af89f1dbfa0d823580a7a6e66a', '236e8a4baff0db8c62c00dd95632834f', '不同步', '0', '不同步', '2', '1', 'admin', '2019-05-15 15:28:28', null, null);
INSERT INTO `sys_dict_item` VALUES ('f0162f4cc572c9273f3e26b2b4d8c082', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'booostrap', '1', null, '1', '1', 'admin', '2021-08-22 16:32:04', 'admin', '2019-03-22 16:33:57');
INSERT INTO `sys_dict_item` VALUES ('f16c5706f3ae05c57a53850c64ce7c45', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送成功', '1', null, '2', '1', 'admin', '2019-04-12 18:19:43', null, null);
INSERT INTO `sys_dict_item` VALUES ('f2a7920421f3335afdf6ad2b342f6b5d', '845da5006c97754728bf48b6a10f79cc', '冻结', '2', null, null, '1', 'admin', '2019-03-18 21:46:02', null, null);
INSERT INTO `sys_dict_item` VALUES ('f37f90c496ec9841c4c326b065e00bb2', '83bfb33147013cc81640d5fd9eda030c', '登录日志', '1', null, null, '1', 'admin', '2019-03-18 23:22:37', null, null);
INSERT INTO `sys_dict_item` VALUES ('f753aff60ff3931c0ecb4812d8b5e643', '4c03fca6bf1f0299c381213961566349', '双排布局', 'double', null, '3', '1', 'admin', '2019-04-12 17:43:51', null, null);
INSERT INTO `sys_dict_item` VALUES ('f80a8f6838215753b05e1a5ba3346d22', '880a895c98afeca9d9ac39f29e67c13e', '删除', '4', '', '4', '1', 'admin', '2019-07-22 10:55:14', 'admin', '2019-07-22 10:55:30');
INSERT INTO `sys_dict_item` VALUES ('fcec03570f68a175e1964808dc3f1c91', '4c03fca6bf1f0299c381213961566349', 'Tab风格', 'tab', null, '1', '1', 'admin', '2019-04-12 17:43:31', null, null);
INSERT INTO `sys_dict_item` VALUES ('fe50b23ae5e68434def76f67cef35d2d', '78bda155fe380b1b3f175f1e88c284c6', '已作废', '4', '已作废', '4', '1', 'admin', '2021-09-09 16:33:43', 'admin', '2019-05-09 16:34:40');

-- ----------------------------
-- Table structure for `sys_dict_item_copy`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item_copy`;
CREATE TABLE `sys_dict_item_copy` (
  `id` varchar(32) NOT NULL,
  `dict_id` varchar(32) DEFAULT NULL COMMENT '字典id',
  `item_text` varchar(100) DEFAULT NULL COMMENT '字典项文本',
  `item_value` varchar(100) DEFAULT NULL COMMENT '字典项值',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `sort_order` int(10) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态（1启用 0不启用）',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_dict_id` (`dict_id`) USING BTREE,
  KEY `index_table_sort_order` (`sort_order`) USING BTREE,
  KEY `index_table_dict_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict_item_copy
-- ----------------------------
INSERT INTO `sys_dict_item_copy` VALUES ('0072d115e07c875d76c9b022e2179128', '4d7fec1a7799a436d26d02325eff295e', '低', 'L', '低', '3', '1', 'admin', '2019-04-16 17:04:59', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('05a2e732ce7b00aa52141ecc3e330b4e', '3486f32803bb953e7155dab3513dc68b', '已删除', '1', null, null, '1', 'admin', '2025-10-18 21:46:56', 'admin', '2019-03-28 22:23:20');
INSERT INTO `sys_dict_item_copy` VALUES ('096c2e758d823def3855f6376bc736fb', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'SQL', 'sql', null, '1', '1', 'admin', '2019-04-12 17:26:26', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('0c9532916f5cd722017b46bc4d953e41', '2f0320997ade5dd147c90130f7218c3e', '指定用户', 'USER', null, null, '1', 'admin', '2019-03-17 21:22:19', 'admin', '2019-03-17 21:22:28');
INSERT INTO `sys_dict_item_copy` VALUES ('0ca4beba9efc4f9dd54af0911a946d5c', '72cce0989df68887546746d8f09811aa', '附表', '3', null, '3', '1', 'admin', '2019-03-27 10:13:43', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('1030a2652608f5eac3b49d70458b8532', '2e02df51611a4b9632828ab7e5338f00', '禁用', '2', '禁用', '2', '1', 'admin', '2021-03-26 18:27:28', 'admin', '2019-04-26 18:39:11');
INSERT INTO `sys_dict_item_copy` VALUES ('147c48ff4b51545032a9119d13f3222a', 'd6e1152968b02d69ff358c75b48a6ee1', '测试流程', 'test', null, '1', '1', 'admin', '2019-03-22 19:27:05', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('1543fe7e5e26fb97cdafe4981bedc0c8', '4c03fca6bf1f0299c381213961566349', '单排布局', 'single', null, '2', '1', 'admin', '2022-07-12 17:43:39', 'admin', '2019-04-12 17:43:57');
INSERT INTO `sys_dict_item_copy` VALUES ('1b8a6341163062dad8cb2fddd34e0c3b', '404a04a15f371566c658ee9ef9fc392a', '22', '222', null, '1', '1', 'admin', '2019-03-30 11:17:48', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('1ce390c52453891f93514c1bd2795d44', 'ad7c65ba97c20a6805d5dcdf13cdaf36', '000', '00', null, '1', '1', 'admin', '2019-03-22 16:34:34', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('1db531bcff19649fa82a644c8a939dc4', '4c03fca6bf1f0299c381213961566349', '组合布局', 'combination', '', '4', '1', 'admin', '2019-05-11 16:07:08', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('222705e11ef0264d4214affff1fb4ff9', '4f69be5f507accea8d5df5f11346181a', '短信', '1', '', '1', '1', 'admin', '2023-02-28 10:50:36', 'admin', '2019-04-28 10:58:11');
INSERT INTO `sys_dict_item_copy` VALUES ('23a5bb76004ed0e39414e928c4cde155', '4e4602b3e3686f0911384e188dc7efb4', '不等于', '!=', '不等于', '3', '1', 'admin', '2019-04-01 16:46:15', 'admin', '2019-04-01 17:48:40');
INSERT INTO `sys_dict_item_copy` VALUES ('25847e9cb661a7c711f9998452dc09e6', '4e4602b3e3686f0911384e188dc7efb4', '小于等于', '<=', '小于等于', '6', '1', 'admin', '2019-04-01 16:44:34', 'admin', '2019-04-01 17:49:10');
INSERT INTO `sys_dict_item_copy` VALUES ('2d51376643f220afdeb6d216a8ac2c01', '68168534ff5065a152bfab275c2136f8', '有效', '1', '有效', '2', '1', 'admin', '2019-04-26 19:22:01', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('308c8aadf0c37ecdde188b97ca9833f5', '8dfe32e2d29ea9430a988b3b558bf233', '已发布', '1', '已发布', '2', '1', 'admin', '2019-04-16 17:41:24', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('333e6b2196e01ef9a5f76d74e86a6e33', '8dfe32e2d29ea9430a988b3b558bf233', '未发布', '0', '未发布', '1', '1', 'admin', '2019-04-16 17:41:12', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('337ea1e401bda7233f6258c284ce4f50', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'JSON', 'json', null, '1', '1', 'admin', '2019-04-12 17:26:33', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('33bc9d9f753cf7dc40e70461e50fdc54', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送失败', '2', null, '3', '1', 'admin', '2019-04-12 18:20:02', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('3fbc03d6c994ae06d083751248037c0e', '78bda155fe380b1b3f175f1e88c284c6', '已完成', '3', '已完成', '3', '1', 'admin', '2019-05-09 16:33:25', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('41d7aaa40c9b61756ffb1f28da5ead8e', '0b5d19e1fce4b2e6647e6b4a17760c14', '通知公告', '1', null, '1', '1', 'admin', '2019-04-22 18:01:57', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('41fa1e9571505d643aea87aeb83d4d76', '4e4602b3e3686f0911384e188dc7efb4', '等于', '=', '等于', '4', '1', 'admin', '2019-04-01 16:45:24', 'admin', '2019-04-01 17:49:00');
INSERT INTO `sys_dict_item_copy` VALUES ('43d2295b8610adce9510ff196a49c6e9', '845da5006c97754728bf48b6a10f79cc', '正常', '1', null, null, '1', 'admin', '2019-03-18 21:45:51', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('4f05fb5376f4c61502c5105f52e4dd2b', '83bfb33147013cc81640d5fd9eda030c', '操作日志', '2', null, null, '1', 'admin', '2019-03-18 23:22:49', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('50223341bfb5ba30bf6319789d8d17fe', 'd6e1152968b02d69ff358c75b48a6ee1', '业务办理', 'business', null, '3', '1', 'admin', '2023-04-22 19:28:05', 'admin', '2019-03-22 23:24:39');
INSERT INTO `sys_dict_item_copy` VALUES ('51222413e5906cdaf160bb5c86fb827c', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '是', '1', '', '1', '1', 'admin', '2019-05-22 19:29:45', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('538fca35afe004972c5f3947c039e766', '2e02df51611a4b9632828ab7e5338f00', '显示', '1', '显示', '1', '1', 'admin', '2025-03-26 18:27:13', 'admin', '2019-04-26 18:39:07');
INSERT INTO `sys_dict_item_copy` VALUES ('5584c21993bde231bbde2b966f2633ac', '4e4602b3e3686f0911384e188dc7efb4', '自定义SQL表达式', 'USE_SQL_RULES', '自定义SQL表达式', '9', '1', 'admin', '2019-04-01 10:45:24', 'admin', '2019-04-01 17:49:27');
INSERT INTO `sys_dict_item_copy` VALUES ('58b73b344305c99b9d8db0fc056bbc0a', '72cce0989df68887546746d8f09811aa', '主表', '2', null, '2', '1', 'admin', '2019-03-27 10:13:36', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('5b65a88f076b32e8e69d19bbaadb52d5', '2f0320997ade5dd147c90130f7218c3e', '全体用户', 'ALL', null, null, '1', 'admin', '2020-10-17 21:22:43', 'admin', '2019-03-28 22:17:09');
INSERT INTO `sys_dict_item_copy` VALUES ('5d833f69296f691843ccdd0c91212b6b', '880a895c98afeca9d9ac39f29e67c13e', '修改', '3', '', '3', '1', 'admin', '2019-07-22 10:55:07', 'admin', '2019-07-22 10:55:41');
INSERT INTO `sys_dict_item_copy` VALUES ('5d84a8634c8fdfe96275385075b105c9', '3d9a351be3436fbefb1307d4cfb49bf2', '女', '2', null, '2', '1', null, '2019-01-04 14:56:56', null, '2019-01-04 17:38:12');
INSERT INTO `sys_dict_item_copy` VALUES ('66c952ae2c3701a993e7db58f3baf55e', '4e4602b3e3686f0911384e188dc7efb4', '大于', '>', '大于', '1', '1', 'admin', '2019-04-01 10:45:46', 'admin', '2019-04-01 17:48:29');
INSERT INTO `sys_dict_item_copy` VALUES ('6937c5dde8f92e9a00d4e2ded9198694', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'easyui', '3', null, '1', '1', 'admin', '2019-03-22 16:32:15', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('69cacf64e244100289ddd4aa9fa3b915', 'a9d9942bd0eccb6e89de92d130ec4c4a', '未发送', '0', null, '1', '1', 'admin', '2019-04-12 18:19:23', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('6a7a9e1403a7943aba69e54ebeff9762', '4f69be5f507accea8d5df5f11346181a', '邮件', '2', '', '2', '1', 'admin', '2031-02-28 10:50:44', 'admin', '2019-04-28 10:59:03');
INSERT INTO `sys_dict_item_copy` VALUES ('6c682d78ddf1715baf79a1d52d2aa8c2', '72cce0989df68887546746d8f09811aa', '单表', '1', null, '1', '1', 'admin', '2019-03-27 10:13:29', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('6d404fd2d82311fbc87722cd302a28bc', '4e4602b3e3686f0911384e188dc7efb4', '模糊', 'LIKE', '模糊', '7', '1', 'admin', '2019-04-01 16:46:02', 'admin', '2019-04-01 17:49:20');
INSERT INTO `sys_dict_item_copy` VALUES ('6d4e26e78e1a09699182e08516c49fc4', '4d7fec1a7799a436d26d02325eff295e', '高', 'H', '高', '1', '1', 'admin', '2019-04-16 17:04:24', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('700e9f030654f3f90e9ba76ab0713551', '6b78e3f59faec1a4750acff08030a79b', '333', '333', null, null, '1', 'admin', '2019-02-21 19:59:47', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('7050c1522702bac3be40e3b7d2e1dfd8', 'c5a14c75172783d72cbee6ee7f5df5d1', '柱状图', 'bar', null, '1', '1', 'admin', '2019-04-12 17:05:17', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('71b924faa93805c5c1579f12e001c809', 'd6e1152968b02d69ff358c75b48a6ee1', 'OA办公', 'oa', null, '2', '1', 'admin', '2021-03-22 19:27:17', 'admin', '2019-03-22 23:24:36');
INSERT INTO `sys_dict_item_copy` VALUES ('75b260d7db45a39fc7f21badeabdb0ed', 'c36169beb12de8a71c8683ee7c28a503', '不启用', '0', null, null, '1', 'admin', '2019-03-18 23:29:41', 'admin', '2019-03-18 23:29:54');
INSERT INTO `sys_dict_item_copy` VALUES ('7688469db4a3eba61e6e35578dc7c2e5', 'c36169beb12de8a71c8683ee7c28a503', '启用', '1', null, null, '1', 'admin', '2019-03-18 23:29:28', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('78ea6cadac457967a4b1c4eb7aaa418c', 'fc6cd58fde2e8481db10d3a1e68ce70c', '正常', '1', null, null, '1', 'admin', '2019-03-18 23:30:28', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('7ccf7b80c70ee002eceb3116854b75cb', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '按钮权限', '2', null, null, '1', 'admin', '2019-03-18 23:25:40', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('81fb2bb0e838dc68b43f96cc309f8257', 'fc6cd58fde2e8481db10d3a1e68ce70c', '冻结', '2', null, null, '1', 'admin', '2019-03-18 23:30:37', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('83250269359855501ec4e9c0b7e21596', '4274efc2292239b6f000b153f50823ff', '显示/访问(授权后显示/可访问)', '1', '', '1', '1', 'admin', '2019-05-10 17:54:51', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('84778d7e928bc843ad4756db1322301f', '4e4602b3e3686f0911384e188dc7efb4', '大于等于', '>=', '大于等于', '5', '1', 'admin', '2019-04-01 10:46:02', 'admin', '2019-04-01 17:49:05');
INSERT INTO `sys_dict_item_copy` VALUES ('848d4da35ebd93782029c57b103e5b36', 'c5a14c75172783d72cbee6ee7f5df5d1', '饼图', 'pie', null, '3', '1', 'admin', '2019-04-12 17:05:49', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('84dfc178dd61b95a72900fcdd624c471', '78bda155fe380b1b3f175f1e88c284c6', '处理中', '2', '处理中', '2', '1', 'admin', '2019-05-09 16:33:01', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('86f19c7e0a73a0bae451021ac05b99dd', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '子菜单', '1', null, null, '1', 'admin', '2019-03-18 23:25:27', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('8bccb963e1cd9e8d42482c54cc609ca2', '4f69be5f507accea8d5df5f11346181a', '微信', '3', null, '3', '1', 'admin', '2021-05-11 14:29:12', 'admin', '2019-04-11 14:29:31');
INSERT INTO `sys_dict_item_copy` VALUES ('8c618902365ca681ebbbe1e28f11a548', '4c753b5293304e7a445fd2741b46529d', '启用', '1', '', '0', '1', 'admin', '2020-07-18 23:19:27', 'admin', '2019-05-17 14:51:18');
INSERT INTO `sys_dict_item_copy` VALUES ('8cdf08045056671efd10677b8456c999', '4274efc2292239b6f000b153f50823ff', '可编辑(未授权时禁用)', '2', '', '2', '1', 'admin', '2019-05-10 17:55:38', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('8ff48e657a7c5090d4f2a59b37d1b878', '4d7fec1a7799a436d26d02325eff295e', '中', 'M', '中', '2', '1', 'admin', '2019-04-16 17:04:40', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('948923658baa330319e59b2213cda97c', '880a895c98afeca9d9ac39f29e67c13e', '添加', '2', '', '2', '1', 'admin', '2019-07-22 10:54:59', 'admin', '2019-07-22 10:55:36');
INSERT INTO `sys_dict_item_copy` VALUES ('9a96c4a4e4c5c9b4e4d0cbf6eb3243cc', '4c753b5293304e7a445fd2741b46529d', '不启用', '0', null, '1', '1', 'admin', '2019-03-18 23:19:53', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('a1e7d1ca507cff4a480c8caba7c1339e', '880a895c98afeca9d9ac39f29e67c13e', '导出', '6', '', '6', '1', 'admin', '2019-07-22 12:06:50', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('a2321496db6febc956a6c70fab94cb0c', '404a04a15f371566c658ee9ef9fc392a', '3', '3', null, '1', '1', 'admin', '2019-03-30 11:18:18', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('a2be752dd4ec980afaec1efd1fb589af', '8dfe32e2d29ea9430a988b3b558bf233', '已撤销', '2', '已撤销', '3', '1', 'admin', '2019-04-16 17:41:39', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('aa0d8a8042a18715a17f0a888d360aa4', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '一级菜单', '0', null, null, '1', 'admin', '2019-03-18 23:24:52', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('adcf2a1fe93bb99a84833043f475fe0b', '4e4602b3e3686f0911384e188dc7efb4', '包含', 'IN', '包含', '8', '1', 'admin', '2019-04-01 16:45:47', 'admin', '2019-04-01 17:49:24');
INSERT INTO `sys_dict_item_copy` VALUES ('b029a41a851465332ee4ee69dcf0a4c2', '0b5d19e1fce4b2e6647e6b4a17760c14', '系统消息', '2', null, '1', '1', 'admin', '2019-02-22 18:02:08', 'admin', '2019-04-22 18:02:13');
INSERT INTO `sys_dict_item_copy` VALUES ('b2a8b4bb2c8e66c2c4b1bb086337f393', '3486f32803bb953e7155dab3513dc68b', '正常', '0', null, null, '1', 'admin', '2022-10-18 21:46:48', 'admin', '2019-03-28 22:22:20');
INSERT INTO `sys_dict_item_copy` VALUES ('b57f98b88363188daf38d42f25991956', '6b78e3f59faec1a4750acff08030a79b', '22', '222', null, null, '0', 'admin', '2019-02-21 19:59:43', 'admin', '2019-03-11 21:23:27');
INSERT INTO `sys_dict_item_copy` VALUES ('b5f3bd5f66bb9a83fecd89228c0d93d1', '68168534ff5065a152bfab275c2136f8', '无效', '0', '无效', '1', '1', 'admin', '2019-04-26 19:21:49', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('b9fbe2a3602d4a27b45c100ac5328484', '78bda155fe380b1b3f175f1e88c284c6', '待提交', '1', '待提交', '1', '1', 'admin', '2019-05-09 16:32:35', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('ba27737829c6e0e582e334832703d75e', '236e8a4baff0db8c62c00dd95632834f', '同步', '1', '同步', '1', '1', 'admin', '2019-05-15 15:28:15', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('bcec04526b04307e24a005d6dcd27fd6', '880a895c98afeca9d9ac39f29e67c13e', '导入', '5', '', '5', '1', 'admin', '2019-07-22 12:06:41', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('c53da022b9912e0aed691bbec3c78473', '880a895c98afeca9d9ac39f29e67c13e', '查询', '1', '', '1', '1', 'admin', '2019-07-22 10:54:51', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('c5700a71ad08994d18ad1dacc37a71a9', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '否', '0', '', '1', '1', 'admin', '2019-05-22 19:29:55', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('cbfcc5b88fc3a90975df23ffc8cbe29c', 'c5a14c75172783d72cbee6ee7f5df5d1', '曲线图', 'line', null, '2', '1', 'admin', '2019-05-12 17:05:30', 'admin', '2019-04-12 17:06:06');
INSERT INTO `sys_dict_item_copy` VALUES ('d217592908ea3e00ff986ce97f24fb98', 'c5a14c75172783d72cbee6ee7f5df5d1', '数据列表', 'table', null, '4', '1', 'admin', '2019-04-12 17:05:56', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('df168368dcef46cade2aadd80100d8aa', '3d9a351be3436fbefb1307d4cfb49bf2', '男', '1', null, '1', '1', null, '2027-08-04 14:56:49', 'admin', '2019-03-23 22:44:44');
INSERT INTO `sys_dict_item_copy` VALUES ('e6329e3a66a003819e2eb830b0ca2ea0', '4e4602b3e3686f0911384e188dc7efb4', '小于', '<', '小于', '2', '1', 'admin', '2019-04-01 16:44:15', 'admin', '2019-04-01 17:48:34');
INSERT INTO `sys_dict_item_copy` VALUES ('e94eb7af89f1dbfa0d823580a7a6e66a', '236e8a4baff0db8c62c00dd95632834f', '不同步', '0', '不同步', '2', '1', 'admin', '2019-05-15 15:28:28', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('f0162f4cc572c9273f3e26b2b4d8c082', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'booostrap', '1', null, '1', '1', 'admin', '2021-08-22 16:32:04', 'admin', '2019-03-22 16:33:57');
INSERT INTO `sys_dict_item_copy` VALUES ('f16c5706f3ae05c57a53850c64ce7c45', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送成功', '1', null, '2', '1', 'admin', '2019-04-12 18:19:43', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('f2a7920421f3335afdf6ad2b342f6b5d', '845da5006c97754728bf48b6a10f79cc', '冻结', '2', null, null, '1', 'admin', '2019-03-18 21:46:02', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('f37f90c496ec9841c4c326b065e00bb2', '83bfb33147013cc81640d5fd9eda030c', '登录日志', '1', null, null, '1', 'admin', '2019-03-18 23:22:37', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('f753aff60ff3931c0ecb4812d8b5e643', '4c03fca6bf1f0299c381213961566349', '双排布局', 'double', null, '3', '1', 'admin', '2019-04-12 17:43:51', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('f80a8f6838215753b05e1a5ba3346d22', '880a895c98afeca9d9ac39f29e67c13e', '删除', '4', '', '4', '1', 'admin', '2019-07-22 10:55:14', 'admin', '2019-07-22 10:55:30');
INSERT INTO `sys_dict_item_copy` VALUES ('fcec03570f68a175e1964808dc3f1c91', '4c03fca6bf1f0299c381213961566349', 'Tab风格', 'tab', null, '1', '1', 'admin', '2019-04-12 17:43:31', null, null);
INSERT INTO `sys_dict_item_copy` VALUES ('fe50b23ae5e68434def76f67cef35d2d', '78bda155fe380b1b3f175f1e88c284c6', '已作废', '4', '已作废', '4', '1', 'admin', '2021-09-09 16:33:43', 'admin', '2019-05-09 16:34:40');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL,
  `log_type` int(2) DEFAULT NULL COMMENT '日志类型（1登录日志，2操作日志）',
  `log_content` varchar(1000) DEFAULT NULL COMMENT '日志内容',
  `operate_type` int(2) DEFAULT NULL COMMENT '操作类型',
  `userid` varchar(32) DEFAULT NULL COMMENT '操作用户账号',
  `username` varchar(100) DEFAULT NULL COMMENT '操作用户名称',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP',
  `method` varchar(500) DEFAULT NULL COMMENT '请求java方法',
  `request_url` varchar(255) DEFAULT NULL COMMENT '请求路径',
  `request_param` longtext COMMENT '请求参数',
  `request_type` varchar(10) DEFAULT NULL COMMENT '请求类型',
  `cost_time` bigint(20) DEFAULT NULL COMMENT '耗时',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_userid` (`userid`) USING BTREE,
  KEY `index_logt_ype` (`log_type`) USING BTREE,
  KEY `index_operate_type` (`operate_type`) USING BTREE,
  KEY `index_log_type` (`log_type`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('b09ccd219a1ce5c7270bb659748b8330', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 15:34:38', null, null);
INSERT INTO `sys_log` VALUES ('a22ddd4b5b0b84bd7794edd24b25fc64', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1547883299809,\"email\":\"zhangdaiscott@163.com\",\"id\":\"7eac655877842eb39dc2f0469f3964ec\",\"name\":\"zhang daihao\"}]', null, '25', 'admin', '2019-01-19 15:34:59', null, null);
INSERT INTO `sys_log` VALUES ('07a0b3f8b4140a7a586305c2f40a2310', '2', '删除测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.delete()', null, '[\"7eac655877842eb39dc2f0469f3964ec\"]', null, '14', 'admin', '2019-01-19 15:38:11', null, null);
INSERT INTO `sys_log` VALUES ('d7902eeab2c34611fad046a79bff1c1b', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1547883544104,\"email\":\"zhangdaiscott@163.com\",\"id\":\"4436302a0de50bb83025286bc414d6a9\",\"name\":\"zhang daihao\"}]', null, '1682', 'admin', '2019-01-19 15:39:05', null, null);
INSERT INTO `sys_log` VALUES ('a68160f37cace166fedd299c4ca0be10', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 15:40:00', null, null);
INSERT INTO `sys_log` VALUES ('c6c0316b6989bf1eea0a3803f593bf69', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 15:47:19', null, null);
INSERT INTO `sys_log` VALUES ('4b1341863a8fffeccda8bbe413bd815f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 15:59:52', null, null);
INSERT INTO `sys_log` VALUES ('ed50b1fbc80c3b953f4551081b10335e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 16:19:06', null, null);
INSERT INTO `sys_log` VALUES ('dabdcb8e15ea9215a1af22f7567ff73d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 16:48:13', null, null);
INSERT INTO `sys_log` VALUES ('446724ea6dd41f4a03111c42e00d80cd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 16:56:36', null, null);
INSERT INTO `sys_log` VALUES ('0e41fe3a34d5715bf4c88e220663583a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 17:04:06', null, null);
INSERT INTO `sys_log` VALUES ('9f2db1ffaf89518a25cc6701da0c5858', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 17:05:07', null, null);
INSERT INTO `sys_log` VALUES ('954f1ccb8b230d2d7d4858eec3aba0a4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 17:08:37', null, null);
INSERT INTO `sys_log` VALUES ('7374f3a2ccb20216cf8eecb26037ce0a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 18:08:51', null, null);
INSERT INTO `sys_log` VALUES ('130de55edac71aab730786307cc65936', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 20:22:57', null, null);
INSERT INTO `sys_log` VALUES ('0bc44e2d682c9f28525d203589a90b43', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 20:31:08', null, null);
INSERT INTO `sys_log` VALUES ('122edcafd54dd06e12838f41123d9d5d', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"age\":28,\"birthday\":1546617600000,\"createBy\":\"admin\",\"createTime\":1547901234989,\"id\":\"42c08b1a2e5b2a96ffa4cc88383d4b11\",\"name\":\"秦500\",\"punchTime\":1546691611000}]', null, '21387', 'admin', '2019-01-19 20:34:11', null, null);
INSERT INTO `sys_log` VALUES ('1a570aac0c30ac2955b59e2dc7a6204c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 20:58:26', null, null);
INSERT INTO `sys_log` VALUES ('c18db091677ec01d55e913662b9028a9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:19:07', null, null);
INSERT INTO `sys_log` VALUES ('88d8b4b50bdab58c52fe25fa711fbbef', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:21:39', null, null);
INSERT INTO `sys_log` VALUES ('6b876be6e384337b36ad28a4a5868be8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:22:12', null, null);
INSERT INTO `sys_log` VALUES ('cb6b52fbbdd4c5698c17edaf9960e11e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:22:23', null, null);
INSERT INTO `sys_log` VALUES ('fea8e1e2d229557185be0d9a10ebce17', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:55:55', null, null);
INSERT INTO `sys_log` VALUES ('c1842fc83cdf0b0cc0264bf093e9c55d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 21:56:15', null, null);
INSERT INTO `sys_log` VALUES ('543970eba4d1c522e3cb597b0fd4ad13', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 22:53:18', null, null);
INSERT INTO `sys_log` VALUES ('e9ce2b3f7ac1fa3f5f7fd247207ca5c0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 22:53:35', null, null);
INSERT INTO `sys_log` VALUES ('0e365a21c60e4460813bdc4e3cb320a3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 23:01:34', null, null);
INSERT INTO `sys_log` VALUES ('d3df1a4057b6d7fb4dab073a727ba21f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 23:14:02', null, null);
INSERT INTO `sys_log` VALUES ('8f616500d666a5a67bc98e7ccd73c2e2', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1547912194199,\"id\":\"ffa9da1ad40632dfcabac51d766865bd\",\"name\":\"秦999\"}]', null, '386', 'admin', '2019-01-19 23:36:34', null, null);
INSERT INTO `sys_log` VALUES ('055cf35c8865761b479c7f289dc36616', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1547912238787,\"email\":\"zhangdaiscott@163.com\",\"id\":\"c2c0d49e3c01913067cf8d1fb3c971d2\",\"name\":\"zhang daihao\"}]', null, '16', 'admin', '2019-01-19 23:37:18', null, null);
INSERT INTO `sys_log` VALUES ('69e3164d007be2b9834e4fb398186f39', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-19 23:38:17', null, null);
INSERT INTO `sys_log` VALUES ('92e514fee917a1a459c4ffdb0ca42516', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 10:20:52', null, null);
INSERT INTO `sys_log` VALUES ('d3f08843a9b2b3284711e376fb785beb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 10:58:03', null, null);
INSERT INTO `sys_log` VALUES ('76bea561f662ec0ccf05bc370f1ffe35', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 11:08:55', null, null);
INSERT INTO `sys_log` VALUES ('273081678d85acebaa6615973bff31db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:02:50', null, null);
INSERT INTO `sys_log` VALUES ('b26369680b41d581649cf865e88331e9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:03:07', null, null);
INSERT INTO `sys_log` VALUES ('7313b43ff53015d79a58b4dc7c660721', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:03:07', null, null);
INSERT INTO `sys_log` VALUES ('f99912c5ff252594f14d31b768f8ad15', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:03:10', null, null);
INSERT INTO `sys_log` VALUES ('dcec1957987abbe6658f1f2c96980366', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:05:21', null, null);
INSERT INTO `sys_log` VALUES ('c7b6156c4f42b70c562b507766f4546c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:14:08', null, null);
INSERT INTO `sys_log` VALUES ('52673feae24ea5bc3ca111f19c9a85d4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:16:46', null, null);
INSERT INTO `sys_log` VALUES ('507b55d3b5ddc487fb40ca1f716a1253', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:43:02', null, null);
INSERT INTO `sys_log` VALUES ('7351132f4f5f65e5bf157dd7ad5344a4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:51:36', null, null);
INSERT INTO `sys_log` VALUES ('961992e05772bc7ad2ca927cf7649440', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 15:55:10', null, null);
INSERT INTO `sys_log` VALUES ('3b07fda32423a5696b2097e1c23c00d4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 16:04:32', null, null);
INSERT INTO `sys_log` VALUES ('8447099784da63b3b2cd2fbbc5eabcea', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 16:04:49', null, null);
INSERT INTO `sys_log` VALUES ('b20ff98a10af3c25c1991741fd59ea64', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 16:07:48', null, null);
INSERT INTO `sys_log` VALUES ('9acebd2d37c9078f9568125fb9696976', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 16:07:59', null, null);
INSERT INTO `sys_log` VALUES ('d70c2847d8d0936a2a761f745a84aa48', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 16:39:34', null, null);
INSERT INTO `sys_log` VALUES ('279e519d647f1a4e1f85f9b90ab370b9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 17:01:30', null, null);
INSERT INTO `sys_log` VALUES ('b605a83a9b5f3cdaaa1b3f4f41a5f12d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 17:04:20', null, null);
INSERT INTO `sys_log` VALUES ('0a24b1f04f79a2bcb83c4cd12d077cbc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 17:34:37', null, null);
INSERT INTO `sys_log` VALUES ('661c4792f00b0814e486c3d623d7259f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 18:06:11', null, null);
INSERT INTO `sys_log` VALUES ('d1746c5c937fcb650bd835ff74dabdff', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 18:06:32', null, null);
INSERT INTO `sys_log` VALUES ('8ec3a287a37d155047e80a80769d5226', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 18:37:14', null, null);
INSERT INTO `sys_log` VALUES ('6cbd2a9257fae1cb7ff7bc2eb264b3ab', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 19:08:14', null, null);
INSERT INTO `sys_log` VALUES ('f06e8fa83b408be905b4dc7caeaf9a80', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 19:40:33', null, null);
INSERT INTO `sys_log` VALUES ('f84e86c9a21149134b1f2599a424164b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-21 20:12:27', null, null);
INSERT INTO `sys_log` VALUES ('88bfc5b77b4be0d6d0f7c8661cf24853', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:25:53', null, null);
INSERT INTO `sys_log` VALUES ('b9bf472a12fc25a9d4b500421b08b025', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:53:54', null, null);
INSERT INTO `sys_log` VALUES ('dbbcfb7f59311637a613ec9a6c63f04a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:53:57', null, null);
INSERT INTO `sys_log` VALUES ('69ea2322f72b41bcdc7f235889132703', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:54:43', null, null);
INSERT INTO `sys_log` VALUES ('62d197757e2cb40f9e8cb57fa6a207f7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:54:54', null, null);
INSERT INTO `sys_log` VALUES ('ccad29843623a6c3ca59548b1d533b15', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 10:56:33', null, null);
INSERT INTO `sys_log` VALUES ('4d9299e2daac1f49eac0cec75a90c32e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 11:28:39', null, null);
INSERT INTO `sys_log` VALUES ('43848099c1e70910ba1572868ee40415', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 11:28:48', null, null);
INSERT INTO `sys_log` VALUES ('6fb7db45b11bc22347b234fda07700c8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 12:00:22', null, null);
INSERT INTO `sys_log` VALUES ('e8cde8dcd6253b249d67a05aaf10f968', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 12:30:28', null, null);
INSERT INTO `sys_log` VALUES ('6a4231540c73ad67128d5a24e6a877ff', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 12:54:30', null, null);
INSERT INTO `sys_log` VALUES ('2b3be3da6ba9d1ee49f378d729d69c50', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 13:24:41', null, null);
INSERT INTO `sys_log` VALUES ('78f519b618f82a39adad391fbf6b9c7a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 13:49:58', null, null);
INSERT INTO `sys_log` VALUES ('1487d69ff97888f3a899e2ababb5ae48', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 14:21:17', null, null);
INSERT INTO `sys_log` VALUES ('cc7fa5567e7833a3475b29b7441a2976', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 14:21:31', null, null);
INSERT INTO `sys_log` VALUES ('52e36d72cd04bea2604747e006b038ec', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 19:47:17', null, null);
INSERT INTO `sys_log` VALUES ('523a54948d5edaf421566014b66f9465', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 19:50:27', null, null);
INSERT INTO `sys_log` VALUES ('48e4e10ac7e583050fd85734f0676a7c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 19:58:08', null, null);
INSERT INTO `sys_log` VALUES ('dee4d42c439b51b228ab5db5d0723fc0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-22 20:02:56', null, null);
INSERT INTO `sys_log` VALUES ('965c74ffe09d8a06bb817efa6d62254b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 10:01:35', null, null);
INSERT INTO `sys_log` VALUES ('059bac84373e9dae94363ea18802d70f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 10:06:56', null, null);
INSERT INTO `sys_log` VALUES ('9ef3f1ed07003e3abec3445920b062f1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 11:17:05', null, null);
INSERT INTO `sys_log` VALUES ('0169622dcd4e89b177a0917778ac7f9c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 11:17:18', null, null);
INSERT INTO `sys_log` VALUES ('f8960d64e93606fa52220cc9c4ae35a2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 11:21:02', null, null);
INSERT INTO `sys_log` VALUES ('4261867172d0fd5c04c993638661ac0b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 11:24:47', null, null);
INSERT INTO `sys_log` VALUES ('32464c6f7f772ddda0a963b19ad2fd70', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 11:30:20', null, null);
INSERT INTO `sys_log` VALUES ('d29cf7aae44523bf2f3d187e91356fe8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 12:20:35', null, null);
INSERT INTO `sys_log` VALUES ('0e9c0d0d26ddc652a7277912e0784d11', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 12:27:41', null, null);
INSERT INTO `sys_log` VALUES ('25f8b1b345b1c8a070fe81d715540c85', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-23 15:39:46', null, null);
INSERT INTO `sys_log` VALUES ('8327cced60486bad4009276e14403502', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 09:56:29', null, null);
INSERT INTO `sys_log` VALUES ('515c28df59f07478339b61ca5b1b54a8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 10:34:51', null, null);
INSERT INTO `sys_log` VALUES ('fa0612372b332b6c3ce787d9ca6dd2cc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 11:48:21', null, null);
INSERT INTO `sys_log` VALUES ('8300e85a2c2f16c2358d31e8b364edf7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 11:55:39', null, null);
INSERT INTO `sys_log` VALUES ('3d9874f248a984608ca98c36c21c5a7a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 13:05:56', null, null);
INSERT INTO `sys_log` VALUES ('cc8ab347f332c55570830c5fc39bbf9f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 13:08:38', null, null);
INSERT INTO `sys_log` VALUES ('8742a458bf166fd5f134ac65fa8903f9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-24 13:09:10', null, null);
INSERT INTO `sys_log` VALUES ('bbe2e637bafa0d7f465dc9e1266cff3d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 11:16:50', null, null);
INSERT INTO `sys_log` VALUES ('b3474fc5aad9ec2f36ccbbf7bf864a69', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 11:17:24', null, null);
INSERT INTO `sys_log` VALUES ('260bb025d91b59d0135d635ef85eeb82', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 11:40:13', null, null);
INSERT INTO `sys_log` VALUES ('1a5b71c9458c17f9bcb19a5747fd47dd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 11:56:48', null, null);
INSERT INTO `sys_log` VALUES ('e720278084b0d4316448ec59d4e3399d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 15:52:45', null, null);
INSERT INTO `sys_log` VALUES ('f6646950c8465da1d1219b7a7a209fc2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 19:14:07', null, null);
INSERT INTO `sys_log` VALUES ('36358cacfc5eb3ba7e85cfe156218b71', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 19:14:17', null, null);
INSERT INTO `sys_log` VALUES ('ee2bb63c47c868d59a45503b3d2f34ea', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 19:16:14', null, null);
INSERT INTO `sys_log` VALUES ('b0d11dfec52e02f504c63e2f8224b00d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-25 19:27:44', null, null);
INSERT INTO `sys_log` VALUES ('4acfbc327681d89dab861c77401f8992', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 10:54:36', null, null);
INSERT INTO `sys_log` VALUES ('96ada57ac17c4477f4e4c8d596d4cc1a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 10:54:44', null, null);
INSERT INTO `sys_log` VALUES ('e4e40e21437b23b74324e0402cceb71a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 11:34:40', null, null);
INSERT INTO `sys_log` VALUES ('d92d9e003666c6b020f079eaee721f9f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 12:08:43', null, null);
INSERT INTO `sys_log` VALUES ('68f7394ca53c59438b2b41e7bb9f3094', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 14:09:34', null, null);
INSERT INTO `sys_log` VALUES ('a9b34565c6460dc9cede00ad150393f9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 14:17:53', null, null);
INSERT INTO `sys_log` VALUES ('fa427f74dc6bd9cca3db478b5842f7f7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 14:19:07', null, null);
INSERT INTO `sys_log` VALUES ('8b66ec251e3107765768dbd0590eeb29', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 14:25:48', null, null);
INSERT INTO `sys_log` VALUES ('e42a38382fce916909d6d09f66147006', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-28 14:28:44', null, null);
INSERT INTO `sys_log` VALUES ('ed0bbe9047a7471ae1cdc1c2941eccb1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-29 17:52:38', null, null);
INSERT INTO `sys_log` VALUES ('36fd54ce8bc1ee4aac9e3ea4bfdcd5a8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-29 18:49:34', null, null);
INSERT INTO `sys_log` VALUES ('40b3a9bee45b23548250936310b273f4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 14:42:03', null, null);
INSERT INTO `sys_log` VALUES ('c9be887c9292153e39861c91243b7432', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 15:12:15', null, null);
INSERT INTO `sys_log` VALUES ('e40823376fa8c0e74a4e760de695e824', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 15:36:38', null, null);
INSERT INTO `sys_log` VALUES ('993010965223b8e3a7a784409f7e377e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 15:50:41', null, null);
INSERT INTO `sys_log` VALUES ('aa47c8cf2a4f2de16f415b9d9d3dbf05', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 16:14:36', null, null);
INSERT INTO `sys_log` VALUES ('4a0020835a71fc6dcaefd01968d21f81', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-30 18:46:37', null, null);
INSERT INTO `sys_log` VALUES ('fa9cebbb6af23d2830584b3aacd51e46', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-31 13:59:17', null, null);
INSERT INTO `sys_log` VALUES ('60a975067f02cf05e74fa7b71e8e862a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-31 14:31:58', null, null);
INSERT INTO `sys_log` VALUES ('fbb8834e9736bdd4b6d3baee895c4ca4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-01-31 18:05:03', null, null);
INSERT INTO `sys_log` VALUES ('623e4bc7c098f368abcc368227235caf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-01 09:48:57', null, null);
INSERT INTO `sys_log` VALUES ('9f31eedbe3f3c5c431b490d5fec0094c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-01 09:56:36', null, null);
INSERT INTO `sys_log` VALUES ('b945fe8b63e0fc26d02c85466f36ebd9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-01 09:57:34', null, null);
INSERT INTO `sys_log` VALUES ('968d434c45aae64c9ad0e86d18238065', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-01 10:02:22', null, null);
INSERT INTO `sys_log` VALUES ('732a1015057fde25d81ee12a7fbf66b2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-01 10:05:08', null, null);
INSERT INTO `sys_log` VALUES ('d9a0bb9fe6d2c675aa84f9441c0bd8bb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 10:56:23', null, null);
INSERT INTO `sys_log` VALUES ('9c64406daa2b6e7ad1f6776789d61e43', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 10:56:46', null, null);
INSERT INTO `sys_log` VALUES ('1912a44dd4a6ffa1636d2dde9c2f1ab7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 11:01:03', null, null);
INSERT INTO `sys_log` VALUES ('d19b6e77ab1b6d6aa58996a93918754c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 11:33:06', null, null);
INSERT INTO `sys_log` VALUES ('81f7a606359aff9f97f95c15ce8e7c69', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 11:33:42', null, null);
INSERT INTO `sys_log` VALUES ('7da063020a42db99e0f3bb9500498828', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 13:45:36', null, null);
INSERT INTO `sys_log` VALUES ('b6ee157afd006ceddc8c7558c251192e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-11 14:20:56', null, null);
INSERT INTO `sys_log` VALUES ('65ace1ae98891f48ab4121d9258e4f1e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 10:45:20', null, null);
INSERT INTO `sys_log` VALUES ('e2af7674bb716a7c0b703c7c7e20b906', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 11:38:55', null, null);
INSERT INTO `sys_log` VALUES ('60d4f59974170c67826e64480533d793', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 13:25:40', null, null);
INSERT INTO `sys_log` VALUES ('775e987a2ca37edc4f21e022b265a84a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 13:36:13', null, null);
INSERT INTO `sys_log` VALUES ('dd6fbb9b6224c927c0923c16b9285525', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 13:37:01', null, null);
INSERT INTO `sys_log` VALUES ('f3d371d6f71409ea2fe52405b725db4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 13:38:15', null, null);
INSERT INTO `sys_log` VALUES ('c33b4e0bbf998330e44fad65e9d0029e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 15:54:56', null, null);
INSERT INTO `sys_log` VALUES ('189842bf681338dc99dfa66d366a0e6f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 15:55:01', null, null);
INSERT INTO `sys_log` VALUES ('e14cd21cf5eaad9ea3689730a824a50c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 16:12:32', null, null);
INSERT INTO `sys_log` VALUES ('5cf2431447eab30fd3623e831033eea0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-12 19:17:29', null, null);
INSERT INTO `sys_log` VALUES ('9bfe7312f2951503082a28c2cc966ce4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 10:24:02', null, null);
INSERT INTO `sys_log` VALUES ('da9a15efcf4e1e4f24647db7e2143238', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 11:19:54', null, null);
INSERT INTO `sys_log` VALUES ('8317a81bce60a10afeb44af6ef6c807a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 11:27:58', null, null);
INSERT INTO `sys_log` VALUES ('0a6eb1fa998b749012216542a2447ae7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 11:29:30', null, null);
INSERT INTO `sys_log` VALUES ('e5a9b045449136719d4c19c429c2dd56', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 13:08:05', null, null);
INSERT INTO `sys_log` VALUES ('aaf10eab9c2b6ed6af1d7a9ce844d146', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 13:08:10', null, null);
INSERT INTO `sys_log` VALUES ('b4ccdfc1280e73439eb1ad183076675b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 14:10:45', null, null);
INSERT INTO `sys_log` VALUES ('018fe8d3f049a32fb8b541c893058713', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 15:17:42', null, null);
INSERT INTO `sys_log` VALUES ('f3aab8f9dff7bf705aa29c6dcce49011', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 15:18:27', null, null);
INSERT INTO `sys_log` VALUES ('efa591832b375b4609a5890b0c6f3250', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:00:13', null, null);
INSERT INTO `sys_log` VALUES ('2c6822927334eb0810b71465fd9c4945', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:02:47', null, null);
INSERT INTO `sys_log` VALUES ('7289cf420ac87ea0538bde81435b1aaa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:03:51', null, null);
INSERT INTO `sys_log` VALUES ('db8adca4aa7972fdc283be96d877efe0', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:04:28', null, null);
INSERT INTO `sys_log` VALUES ('c5e541648bab341230c93377b4d4e262', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:05:09', null, null);
INSERT INTO `sys_log` VALUES ('e261674e2640fe6d0a3cd86df631537d', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:05:51', null, null);
INSERT INTO `sys_log` VALUES ('406e79995e3340d052d85a74a5d40d1b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:23:16', null, null);
INSERT INTO `sys_log` VALUES ('4de1ed55165f7086f1a425a26a2f56ec', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 16:26:27', null, null);
INSERT INTO `sys_log` VALUES ('d8eed69045aae6cedbff402b4e35f495', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-13 18:22:52', null, null);
INSERT INTO `sys_log` VALUES ('bbf4fb593d6918cc767bb50c9b6c16c5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 10:44:20', null, null);
INSERT INTO `sys_log` VALUES ('506ce2d73a038b6e491a35a6c74a7343', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 13:44:04', null, null);
INSERT INTO `sys_log` VALUES ('4303dbb3e502f11a3c4078f899bb3070', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 17:28:44', null, null);
INSERT INTO `sys_log` VALUES ('2de252a92b59ebfbf16860cc563e3865', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 22:04:17', null, null);
INSERT INTO `sys_log` VALUES ('e4c330b381e2fbfde49f1d4dd43e68b7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 22:22:01', null, null);
INSERT INTO `sys_log` VALUES ('22735c059b01949a87cb918f5ef3be76', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 22:41:28', null, null);
INSERT INTO `sys_log` VALUES ('c5954beca75d6a0c014e2de3b621275a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-14 22:41:31', null, null);
INSERT INTO `sys_log` VALUES ('db8c89112bf4706fb558664dd741aa46', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 09:33:23', null, null);
INSERT INTO `sys_log` VALUES ('fa0ce422c12a565461eca56006052891', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 10:13:21', null, null);
INSERT INTO `sys_log` VALUES ('a34ed4c6fef2b9f07a20e54ef4501b99', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 10:48:00', null, null);
INSERT INTO `sys_log` VALUES ('b55cc05e8dd4279c0fa145833db19ba8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 11:37:40', null, null);
INSERT INTO `sys_log` VALUES ('5c675eeb69795180eee2c1069efc114b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 12:59:43', null, null);
INSERT INTO `sys_log` VALUES ('5dee273feb8dd12989b40c2c92ce8c4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 13:42:58', null, null);
INSERT INTO `sys_log` VALUES ('994efef0ebca19292e14a39b385b0e21', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 16:22:24', null, null);
INSERT INTO `sys_log` VALUES ('fc22aaf9660e66558689a58dfa443074', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 16:30:55', null, null);
INSERT INTO `sys_log` VALUES ('2c6ede513b83fbc23aaedb89dbfa868a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 18:03:00', null, null);
INSERT INTO `sys_log` VALUES ('13c1e763e9d624a69727a38b85411352', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 18:39:00', null, null);
INSERT INTO `sys_log` VALUES ('fadb32d678346ee4bab02997988ff3bc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-15 22:55:16', null, null);
INSERT INTO `sys_log` VALUES ('0aa792eadeae39a1ed2a98ea5d2f6d27', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-16 09:11:39', null, null);
INSERT INTO `sys_log` VALUES ('0aa9272c0581e1d7f62b1293375b4574', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-16 17:26:36', null, null);
INSERT INTO `sys_log` VALUES ('81c9056ac38e6f881d60f3d41df1845e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-17 11:44:47', null, null);
INSERT INTO `sys_log` VALUES ('eb4536aa50a58985baf0a763a1ce2ebf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-17 19:48:49', null, null);
INSERT INTO `sys_log` VALUES ('f9062582881b42f6b139c313d8ab0463', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-17 20:47:26', null, null);
INSERT INTO `sys_log` VALUES ('22d8a2fbd53eafb21f6f62ae073c0fc1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-17 22:28:52', null, null);
INSERT INTO `sys_log` VALUES ('7bc7b1ff923dbb19fb0ecd800cd690bd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-18 09:34:59', null, null);
INSERT INTO `sys_log` VALUES ('faea0dbfb7f86b571fed0dd270623831', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-18 14:12:14', null, null);
INSERT INTO `sys_log` VALUES ('063baad688535096d2ed906ae6f3a128', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-18 22:09:21', null, null);
INSERT INTO `sys_log` VALUES ('528baecc596a66eaadc8887bff911f55', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 10:08:48', null, null);
INSERT INTO `sys_log` VALUES ('e540ca989819c54baefffbc3d05e8b58', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 10:10:54', null, null);
INSERT INTO `sys_log` VALUES ('0643f3ad4394de9fb3c491080c6a7a03', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 10:18:42', null, null);
INSERT INTO `sys_log` VALUES ('eb0b8a7cdf77df133566d7bd5a5f1fc0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 11:02:55', null, null);
INSERT INTO `sys_log` VALUES ('0913bb0e92715892c470cf538726dfbc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 16:17:09', null, null);
INSERT INTO `sys_log` VALUES ('5034aec34f0b79da510e66008dbf2fcc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 16:18:29', null, null);
INSERT INTO `sys_log` VALUES ('e4afd66ac249dde9c3bd9da50f9c2469', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 17:41:54', null, null);
INSERT INTO `sys_log` VALUES ('07132c1228b1c165f62ea35f4ff1cbe9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 18:15:44', null, null);
INSERT INTO `sys_log` VALUES ('4f7f587bec68ed5bf9f68b0ccd76d62b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-19 21:01:47', null, null);
INSERT INTO `sys_log` VALUES ('12709e62742056aa4a57fa8c2c82d84a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 09:13:10', null, null);
INSERT INTO `sys_log` VALUES ('680b3e6e4768d80d6ea0ce8ba71bdd0e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 09:14:03', null, null);
INSERT INTO `sys_log` VALUES ('a6e323785535592ee208aa7e53554644', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 09:15:27', null, null);
INSERT INTO `sys_log` VALUES ('3a4a0e27d77aa8b624180e5fd5e4004e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 09:51:58', null, null);
INSERT INTO `sys_log` VALUES ('b98b7ac9e890657aa86a900763afbe2a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 11:49:44', null, null);
INSERT INTO `sys_log` VALUES ('d1eb2a8ebed28d34199c5fc4a1579c4c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 12:55:09', null, null);
INSERT INTO `sys_log` VALUES ('85949de2d54078e6b8f3df0a3c79c43d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:08:44', null, null);
INSERT INTO `sys_log` VALUES ('77579d78a903635cc4942882f568e9e5', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:13:33', null, null);
INSERT INTO `sys_log` VALUES ('679e12ba247575749e03aa8f67347ac6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:14:42', null, null);
INSERT INTO `sys_log` VALUES ('5c35117cbeb39428fcc2ddd90ce96a2b', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:18:51', null, null);
INSERT INTO `sys_log` VALUES ('7225200c3cec4789af4f1da2c46b129d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:19:23', null, null);
INSERT INTO `sys_log` VALUES ('22ad9f87788506456c774801389d6a01', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 17:20:10', null, null);
INSERT INTO `sys_log` VALUES ('81c95e1c8805fa191753fc99ba54c3e9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:01:21', null, null);
INSERT INTO `sys_log` VALUES ('7285730e2644f49def0937dc99bfbe3d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:07:01', null, null);
INSERT INTO `sys_log` VALUES ('4922f2f1173a1edc11dfd11cb2a100ae', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:08:27', null, null);
INSERT INTO `sys_log` VALUES ('e37cce529d0c98c47b4977d7ddf963c0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:17:54', null, null);
INSERT INTO `sys_log` VALUES ('66493cd0347eeb6ee2ef5ee923604683', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:29:47', null, null);
INSERT INTO `sys_log` VALUES ('f04910792a74c563d057c4fcb345f963', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:30:00', null, null);
INSERT INTO `sys_log` VALUES ('210a01dcb34302eaed0d1e95820655d0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:30:15', null, null);
INSERT INTO `sys_log` VALUES ('48929ec94226d9ccff9fae4ff48e95e3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:32:55', null, null);
INSERT INTO `sys_log` VALUES ('d2ac19a709ea08f7259286df28efd635', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:35:37', null, null);
INSERT INTO `sys_log` VALUES ('d8fd478e6ceb03a575719e1a54342333', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:43:43', null, null);
INSERT INTO `sys_log` VALUES ('a35a476c303983701045507c9af3fa03', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-20 18:44:58', null, null);
INSERT INTO `sys_log` VALUES ('7e41208e29d412d586fc39375628b0d0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-21 15:34:35', null, null);
INSERT INTO `sys_log` VALUES ('1f33d11e1833ae497e3ef65a3f02dd5b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-21 19:51:12', null, null);
INSERT INTO `sys_log` VALUES ('dae0658783324c81fa6909b6e4a25a65', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 11:46:41', null, null);
INSERT INTO `sys_log` VALUES ('a77d29673cfe97c9e03cfb879b934f62', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 12:41:15', null, null);
INSERT INTO `sys_log` VALUES ('baaf37e5937f938ac92856bc74cc2b86', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 13:48:53', null, null);
INSERT INTO `sys_log` VALUES ('3f47afcdce94596494746ac34eebf13b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 13:59:10', null, null);
INSERT INTO `sys_log` VALUES ('b99fc7c53d4e3edc0c618edc11d3a073', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 15:58:43', null, null);
INSERT INTO `sys_log` VALUES ('024a4c5ba78538d05373dac650b227d1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 15:59:50', null, null);
INSERT INTO `sys_log` VALUES ('873f425879ef9ca7ced982acda19ea58', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 16:35:59', null, null);
INSERT INTO `sys_log` VALUES ('1b05434820cbcb038028da9f5cda31bb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 17:45:16', null, null);
INSERT INTO `sys_log` VALUES ('5f314fc45492d7f90b74d1ca74d1d392', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 17:45:48', null, null);
INSERT INTO `sys_log` VALUES ('20751803c1e5b2d758b981ba22f61fcd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 18:11:05', null, null);
INSERT INTO `sys_log` VALUES ('50e8de3e6b45f8625b8fd5590c9fd834', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 18:23:18', null, null);
INSERT INTO `sys_log` VALUES ('6737424e01b38f2273e9728bf39f3e37', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-22 19:43:37', null, null);
INSERT INTO `sys_log` VALUES ('0473dedf4aa653b253b008dacff2937c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 13:04:44', null, null);
INSERT INTO `sys_log` VALUES ('a95192071de908f37f4998af4c269bcb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 14:26:53', null, null);
INSERT INTO `sys_log` VALUES ('3569ada5c43a4022d3d13ac801aff40e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 14:50:55', null, null);
INSERT INTO `sys_log` VALUES ('562092eb81561ee0f63be5dd9367d298', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:20:59', null, null);
INSERT INTO `sys_log` VALUES ('131ccd390401b6e3894a37e4d1d195d3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:26:52', null, null);
INSERT INTO `sys_log` VALUES ('c12e3d7655a5a8b192bb9964a2a66946', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:35:45', null, null);
INSERT INTO `sys_log` VALUES ('6bc98b7dc91a3924f794202867367aca', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:50:34', null, null);
INSERT INTO `sys_log` VALUES ('002b7112a147edeb6149a891494577d0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:52:15', null, null);
INSERT INTO `sys_log` VALUES ('202344b08b69ad70754e6adaa777eae0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:54:22', null, null);
INSERT INTO `sys_log` VALUES ('eeb1f2e2c1b480e0bb62533848cbb176', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:55:46', null, null);
INSERT INTO `sys_log` VALUES ('94fe4465d779e0438cfe6f0cb1a1aa7e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:57:42', null, null);
INSERT INTO `sys_log` VALUES ('d03aaee882d13b796db860cb95f27724', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 22:59:54', null, null);
INSERT INTO `sys_log` VALUES ('1db82f78233c120c6ec7648ca1177986', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 23:07:39', null, null);
INSERT INTO `sys_log` VALUES ('7dc448f04edf4b9655362ad1a1c58753', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 23:10:17', null, null);
INSERT INTO `sys_log` VALUES ('fce1553149aea9bfd93e089f387199c8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 23:11:35', null, null);
INSERT INTO `sys_log` VALUES ('e713a89e753cbecf1e10247b2112c3f8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-24 23:14:36', null, null);
INSERT INTO `sys_log` VALUES ('0a634ed086442afa7a5fc9aa000b898a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:10:39', null, null);
INSERT INTO `sys_log` VALUES ('bfa0766f53dbd3a0fe4043f57bd9bbee', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:35:33', null, null);
INSERT INTO `sys_log` VALUES ('e3b531fa12e47ac19a2ab0c883dee595', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:40:13', null, null);
INSERT INTO `sys_log` VALUES ('18eafaeec588403245269a41732d1a74', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:45:14', null, null);
INSERT INTO `sys_log` VALUES ('99357d793f2507cfb7b270677b4fe56c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:46:42', null, null);
INSERT INTO `sys_log` VALUES ('b38f42f4e15ee72e494bdf6f6feb0ae7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:49:58', null, null);
INSERT INTO `sys_log` VALUES ('bfe758860662ae07a15598396a12cfaa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:50:00', null, null);
INSERT INTO `sys_log` VALUES ('69a7a5b960d6aedda5c4bd8b877be0a8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:50:48', null, null);
INSERT INTO `sys_log` VALUES ('4084f184160940a96e47d7be1fab4ea3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:11', null, null);
INSERT INTO `sys_log` VALUES ('1241cf8e9fd0e28478a07bf755f528c5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:12', null, null);
INSERT INTO `sys_log` VALUES ('e0da357be27d66de1c9e9b8ecb22f9f9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:13', null, null);
INSERT INTO `sys_log` VALUES ('9f4960f89a10d7fdcf22d1ea46143fff', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:13', null, null);
INSERT INTO `sys_log` VALUES ('ab8a71b7565d356d12e12c6730b0ceb0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:13', null, null);
INSERT INTO `sys_log` VALUES ('35fdedc363d9fe514b44095da40f170b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:51:30', null, null);
INSERT INTO `sys_log` VALUES ('7126b35521cd0dba932e6f04b0dac88f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:52:22', null, null);
INSERT INTO `sys_log` VALUES ('9bd6e11c5a2f0bb70215cfa097a4b29c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 10:57:52', null, null);
INSERT INTO `sys_log` VALUES ('7e2edea80050d2e46aa2e8faef8e29ce', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:01:38', null, null);
INSERT INTO `sys_log` VALUES ('190eb7b4d493eb01b13c5b97916eeb13', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:09:05', null, null);
INSERT INTO `sys_log` VALUES ('ea268ad02db29012b2f1bd3d4aea1419', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:10:22', null, null);
INSERT INTO `sys_log` VALUES ('7dc498b45fbf25c59686d9dda0d3eb66', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:12:38', null, null);
INSERT INTO `sys_log` VALUES ('583d3aa445d408f4ecd19ee0a85514af', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:18:04', null, null);
INSERT INTO `sys_log` VALUES ('9cea908c7a78dc77fdaed975819983bd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:20:15', null, null);
INSERT INTO `sys_log` VALUES ('d70329497664391dabc25effe7406c50', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:25:12', null, null);
INSERT INTO `sys_log` VALUES ('0b9940fc5487026a3f16cade73efead5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:28:28', null, null);
INSERT INTO `sys_log` VALUES ('f21f9f700bf4f5bd9edda7a16ed338f8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:30:05', null, null);
INSERT INTO `sys_log` VALUES ('f5c08b45885d248c422a5d406cd5f223', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:41:54', null, null);
INSERT INTO `sys_log` VALUES ('e9a31bfc128b3f5ae01656916c605ddb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:44:56', null, null);
INSERT INTO `sys_log` VALUES ('6baccd034e970c6f109791cff43bc327', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:46:12', null, null);
INSERT INTO `sys_log` VALUES ('d2b516c5d834bd0fca91cda416fe499e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:46:42', null, null);
INSERT INTO `sys_log` VALUES ('f9abb524e0dc3571571dc6e50ec6db75', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:47:13', null, null);
INSERT INTO `sys_log` VALUES ('d1111594fef195980370c5f91ccf9212', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:48:09', null, null);
INSERT INTO `sys_log` VALUES ('9174fe77fe8ba69243f72d5577b391d3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:48:38', null, null);
INSERT INTO `sys_log` VALUES ('2ab9cf95ac35fdbb8fe976e13c404c41', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:49:05', null, null);
INSERT INTO `sys_log` VALUES ('9be945480d69038865279f02df5cee45', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:49:47', null, null);
INSERT INTO `sys_log` VALUES ('c2bfe3b92e6bfb7016cc82e95419a602', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 11:54:05', null, null);
INSERT INTO `sys_log` VALUES ('7c310b99a84411798a2aaf4074a28e7e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:42:57', null, null);
INSERT INTO `sys_log` VALUES ('b23293288a84ba965509f466ed0e7e2f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:43:11', null, null);
INSERT INTO `sys_log` VALUES ('cf590576a5f6a42b347e6b5bf5ebf5bd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:43:31', null, null);
INSERT INTO `sys_log` VALUES ('b5df1807f08af5db640da11affec24d3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:49:25', null, null);
INSERT INTO `sys_log` VALUES ('2746af3dd0309cdeeff7d27999fbcda1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:52:55', null, null);
INSERT INTO `sys_log` VALUES ('2a383edf5445dc8493f5240144ca72f5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 12:56:05', null, null);
INSERT INTO `sys_log` VALUES ('d2910961a0ff046cc3ef6cf8d33a8094', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 15:38:47', null, null);
INSERT INTO `sys_log` VALUES ('00f763e007e5a6bddf4cb8e562a53005', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 15:41:31', null, null);
INSERT INTO `sys_log` VALUES ('8ab131214232450ca202103ef81f0a2d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 15:46:29', null, null);
INSERT INTO `sys_log` VALUES ('606cb4f81f9bb412e2b2bdaa0f3e5dda', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 16:27:23', null, null);
INSERT INTO `sys_log` VALUES ('7b85fba62bc001773fff1a54e1609aef', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 16:28:20', null, null);
INSERT INTO `sys_log` VALUES ('b3127e34f395e1f1790450da5689a4a1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 16:28:35', null, null);
INSERT INTO `sys_log` VALUES ('fb2871cda1421b766f8e68cb36a22bf3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:35:01', null, null);
INSERT INTO `sys_log` VALUES ('29fb5d4297748af3cd1c7f2611b7a2d6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:38:05', null, null);
INSERT INTO `sys_log` VALUES ('580256f7c7ea658786dba919009451b6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:39:43', null, null);
INSERT INTO `sys_log` VALUES ('8802209912ca66d56f2ea241ffd0cc13', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:52:34', null, null);
INSERT INTO `sys_log` VALUES ('4778fe2992fd5efd65f86cb0e00e338e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:53:11', null, null);
INSERT INTO `sys_log` VALUES ('d68957c067fb27e80a23babebdb1591f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:55:06', null, null);
INSERT INTO `sys_log` VALUES ('472c34745b8f86a46efa28f408465a63', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 17:56:31', null, null);
INSERT INTO `sys_log` VALUES ('26975f09c66025d1c8d87a6894a3c262', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 18:29:08', null, null);
INSERT INTO `sys_log` VALUES ('e4a166fcd0fc4037cb26e35cc1fb87b2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 18:32:41', null, null);
INSERT INTO `sys_log` VALUES ('286af82485388bfcd3bb9821ff1a4727', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 18:34:37', null, null);
INSERT INTO `sys_log` VALUES ('eaf74cd1489b02d39c470eed131fc918', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 18:37:39', null, null);
INSERT INTO `sys_log` VALUES ('e48a6bd82c92a8005c80c5ef36746117', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 19:32:26', null, null);
INSERT INTO `sys_log` VALUES ('10922e0d030960e6b026c67c6179247b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-25 20:07:59', null, null);
INSERT INTO `sys_log` VALUES ('b53c9e8ce1e129a09a3cda8c01fe644c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:38:49', null, null);
INSERT INTO `sys_log` VALUES ('65be8e015c9f2c493bd0a4e405dd8221', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:40:20', null, null);
INSERT INTO `sys_log` VALUES ('8ff27392165c8c707ee10ec0010c7bb8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:44:07', null, null);
INSERT INTO `sys_log` VALUES ('337b647a4085e48f61c7832e6527517d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:45:16', null, null);
INSERT INTO `sys_log` VALUES ('caee69e55ec929f7ba904280cac971e6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:49:15', null, null);
INSERT INTO `sys_log` VALUES ('bdeae62057ae9858b6a546c1bdb6efc7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:49:29', null, null);
INSERT INTO `sys_log` VALUES ('ea66ed22fde49640cee5d73c6ef69718', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:50:04', null, null);
INSERT INTO `sys_log` VALUES ('47c5a5b799e10255c96ccd65286541ef', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:50:35', null, null);
INSERT INTO `sys_log` VALUES ('cfba34db2d7fbb15a2971212f09b59ec', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 11:51:43', null, null);
INSERT INTO `sys_log` VALUES ('faad055dd212ed9506b444f8f1a920b9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:00:38', null, null);
INSERT INTO `sys_log` VALUES ('ef7219725c4b84cc71f56f97a8eab01a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:08:00', null, null);
INSERT INTO `sys_log` VALUES ('2811e224e4e8d70f2946c815988b9b7c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:08:14', null, null);
INSERT INTO `sys_log` VALUES ('7feae2fb5001ca0095c05a8b08270317', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:17:31', null, null);
INSERT INTO `sys_log` VALUES ('b4c3c7af9899b9af3f42f730cfabc9b2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:17:58', null, null);
INSERT INTO `sys_log` VALUES ('28e8a7ed786eaced3182c70f68c7ea78', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:18:33', null, null);
INSERT INTO `sys_log` VALUES ('3a76114e431912ff9a19a4b6eb795112', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:19:41', null, null);
INSERT INTO `sys_log` VALUES ('557b3c346d9bc8f7a83fac9f5b12dc1b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:20:28', null, null);
INSERT INTO `sys_log` VALUES ('e92544c6102243e7908e0cbb217f5198', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 12:48:28', null, null);
INSERT INTO `sys_log` VALUES ('61445cc950f5d04d91339089b18edef9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:13:22', null, null);
INSERT INTO `sys_log` VALUES ('776c2e546c9ab0375d97590b048b8a9d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:13:51', null, null);
INSERT INTO `sys_log` VALUES ('ef7669ac0350730d198f59b8411b19d1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:28:43', null, null);
INSERT INTO `sys_log` VALUES ('b3cceb535fa5577cc21b12502535ad29', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:29:01', null, null);
INSERT INTO `sys_log` VALUES ('18a51a5f04eeaad6530665f6b0883f0c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:34:11', null, null);
INSERT INTO `sys_log` VALUES ('8a13971104d70e35111d10dd99de392e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:34:25', null, null);
INSERT INTO `sys_log` VALUES ('6d93d5667245ef8e5d6eafdbc9113f51', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:34:42', null, null);
INSERT INTO `sys_log` VALUES ('7ba3df5d2612ac3dd724e07a55411386', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:35:03', null, null);
INSERT INTO `sys_log` VALUES ('7148b3d58f121ef04bcbea5dd2e5fe3b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:35:23', null, null);
INSERT INTO `sys_log` VALUES ('709b0f2bf8cb8f785f883509e54ace28', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:37:37', null, null);
INSERT INTO `sys_log` VALUES ('4114145795da30b34545e9e39b7822d9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:39:33', null, null);
INSERT INTO `sys_log` VALUES ('f543c25bdd741055aeb4f77c5b5acf58', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:40:58', null, null);
INSERT INTO `sys_log` VALUES ('bc594b8921a0bcdb26d4a93916316092', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 14:42:58', null, null);
INSERT INTO `sys_log` VALUES ('11f511eeeb2e91af86b9d5e05132fc89', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 15:13:43', null, null);
INSERT INTO `sys_log` VALUES ('810deb9fd39fa2f0a8e30e3db42f7c2b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 18:51:00', null, null);
INSERT INTO `sys_log` VALUES ('d5b9e5d9bfbbd8e6d651087ead76d9f7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 20:17:41', null, null);
INSERT INTO `sys_log` VALUES ('016510fc662d9bb24d0186c5478df268', '1', '用户名: admin,登录成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-02-26 20:27:18', null, null);
INSERT INTO `sys_log` VALUES ('eb6f5196de91dd2e8316696bddd61345', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 22:26:36', null, null);
INSERT INTO `sys_log` VALUES ('d762a1cba3dc23068f352323d98909e0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-02-26 22:26:49', null, null);
INSERT INTO `sys_log` VALUES ('69fc2407b46abad64fa44482c0dca59f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-01 12:04:25', null, null);
INSERT INTO `sys_log` VALUES ('0d6cd835072c83f46d1d2a3cf13225d3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-01 12:04:44', null, null);
INSERT INTO `sys_log` VALUES ('e78f8832d61c1603c17767ee2b78ef07', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-01 19:50:03', null, null);
INSERT INTO `sys_log` VALUES ('0475b4445d5f58f29212258c1644f339', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-01 20:20:23', null, null);
INSERT INTO `sys_log` VALUES ('2b5a76869a7d1900487cd220de378dba', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-05 16:32:00', null, null);
INSERT INTO `sys_log` VALUES ('21b8493a05050584d9bb06cfc2a05a6b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-05 17:29:52', null, null);
INSERT INTO `sys_log` VALUES ('a61d9db83888d42b0d24621de48a880d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-05 22:49:48', null, null);
INSERT INTO `sys_log` VALUES ('097be3e8fdf77a245f5c85884e97b88c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-05 22:52:45', null, null);
INSERT INTO `sys_log` VALUES ('7b2b322a47e1ce131d71c50b46d7d29e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-06 15:55:20', null, null);
INSERT INTO `sys_log` VALUES ('cedf399271592c27dcb6f6ce3312e77d', '2', '删除测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.delete()', null, '[\"7501\"]', null, '24', 'admin', '2019-03-06 16:03:13', null, null);
INSERT INTO `sys_log` VALUES ('efe77038e00cfff98d6931c3e7a4c3d6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-06 16:20:19', null, null);
INSERT INTO `sys_log` VALUES ('ffac84fff3c65bb17aa1bda3a0d2029e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-06 20:10:50', null, null);
INSERT INTO `sys_log` VALUES ('45819fe1b96af820575a12e9f973014e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 09:19:22', null, null);
INSERT INTO `sys_log` VALUES ('87885bc889d23c7c208614da8e021fb0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 10:23:22', null, null);
INSERT INTO `sys_log` VALUES ('54c2bad38dafd9e636ce992aa93b26af', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 11:57:14', null, null);
INSERT INTO `sys_log` VALUES ('d3c4f120d8a23b62ec9e24b431a58496', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 14:17:24', null, null);
INSERT INTO `sys_log` VALUES ('d01d658731dac4b580a879d986b03456', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 15:00:37', null, null);
INSERT INTO `sys_log` VALUES ('ab550d09101a88bc999ea57cbb05aa5a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 17:59:59', null, null);
INSERT INTO `sys_log` VALUES ('aeb738ab880c262772453d35fc98f2f2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 18:50:30', null, null);
INSERT INTO `sys_log` VALUES ('375aadb2833e57a0d5a2ce0546a65ca4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 20:38:52', null, null);
INSERT INTO `sys_log` VALUES ('96d7fe922f46123e0497e22dedf89328', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-07 23:10:48', null, null);
INSERT INTO `sys_log` VALUES ('636d37d423199e15b4030f35c60859fe', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 10:07:21', null, null);
INSERT INTO `sys_log` VALUES ('a7d1f4a774eb8644e2b1d37ca5f93641', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 10:16:03', null, null);
INSERT INTO `sys_log` VALUES ('017e9596f489951f1cc7d978085adc00', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 10:58:00', null, null);
INSERT INTO `sys_log` VALUES ('0b42292a532c796495a34d8d9c633afa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 12:58:03', null, null);
INSERT INTO `sys_log` VALUES ('b428718441be738cf8b5ce92109068c3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 16:21:37', null, null);
INSERT INTO `sys_log` VALUES ('89d2bc84e056f327291c53821d421034', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 16:57:46', null, null);
INSERT INTO `sys_log` VALUES ('e09bb0a74c268a9aaf1f94edcc2eb65a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-08 18:26:14', null, null);
INSERT INTO `sys_log` VALUES ('0dc22e52c9173e4e880728bc7734ff65', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 11:14:47', null, null);
INSERT INTO `sys_log` VALUES ('5358b182eab53a79eec236a9cee1e0fc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 13:01:21', null, null);
INSERT INTO `sys_log` VALUES ('23176e4b29c3d2f3abadd99ebeffa347', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 16:37:50', null, null);
INSERT INTO `sys_log` VALUES ('703fbcb7e198e8e64978ec0518971420', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 17:53:55', null, null);
INSERT INTO `sys_log` VALUES ('9e9d01c430b72703ce3a94589be54bbe', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 18:26:06', null, null);
INSERT INTO `sys_log` VALUES ('ef54197116da89bf091c0ed58321eea4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 19:22:06', null, null);
INSERT INTO `sys_log` VALUES ('111156480d4d18ebf40427083f25830f', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 19:48:58', null, null);
INSERT INTO `sys_log` VALUES ('a9bd713f975bfbff87638432a104b715', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:04:44', null, null);
INSERT INTO `sys_log` VALUES ('06fbb85b34f518cd211b948552de72f8', '1', '登录失败，用户名:null不存在！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:08:38', null, null);
INSERT INTO `sys_log` VALUES ('9b568a868e57f24c5ba146848061613f', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:09:15', null, null);
INSERT INTO `sys_log` VALUES ('02026841bf8a9204db2c500c86a4a9be', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:44:58', null, null);
INSERT INTO `sys_log` VALUES ('c1a68605bee6b3d1264390c1cfe7a9fa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:49:55', null, null);
INSERT INTO `sys_log` VALUES ('cbd720f20fc090c7350a98be0738816a', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 20:55:19', null, null);
INSERT INTO `sys_log` VALUES ('2676be4ffc66f83221fd95e23d494827', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 21:31:28', null, null);
INSERT INTO `sys_log` VALUES ('e9d3202c14f7f2812346fb4c2b781c67', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-09 21:38:36', null, null);
INSERT INTO `sys_log` VALUES ('d9e0150666b69cced93eb4defb19788b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 23:11:12', null, null);
INSERT INTO `sys_log` VALUES ('1f0b36f7e021aa5d059ffb0a74ef6de4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-09 23:11:25', null, null);
INSERT INTO `sys_log` VALUES ('326b2df4ab05a8dbb03a0a0087e82a25', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-10 11:53:20', null, null);
INSERT INTO `sys_log` VALUES ('7ae9cad197aee3d50e93bc3a242d68ec', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-10 13:12:26', null, null);
INSERT INTO `sys_log` VALUES ('78caf9e97aedfb8c7feef0fc8fdb4fb5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-10 17:04:46', null, null);
INSERT INTO `sys_log` VALUES ('d00964eee24c6f9a8609a42eeebef957', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-10 17:04:48', null, null);
INSERT INTO `sys_log` VALUES ('04f97d7f906c1e97384a94f3728606a4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-11 12:08:51', null, null);
INSERT INTO `sys_log` VALUES ('2b433e88db411bef115bc9357ba6a78b', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.105', null, null, null, null, null, 'jeecg-boot', '2019-03-11 12:09:36', null, null);
INSERT INTO `sys_log` VALUES ('63ccf8dda5d9bf825ecdbfb9ff9f456c', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.105', null, null, null, null, null, 'jeecg-boot', '2019-03-11 12:14:08', null, null);
INSERT INTO `sys_log` VALUES ('404d5fb6cce1001c3553a69089a618c8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-11 12:29:12', null, null);
INSERT INTO `sys_log` VALUES ('9ed114408a130e69c0de4c91b2d6bf7e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-11 13:03:40', null, null);
INSERT INTO `sys_log` VALUES ('2eb964935df6f3a4d2f3af6ac5f2ded1', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.200', null, null, null, null, null, 'jeecg-boot', '2019-03-11 13:27:18', null, null);
INSERT INTO `sys_log` VALUES ('e864c0007983211026d6987bd0cd4dc8', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.114', null, null, null, null, null, 'jeecg-boot', '2019-03-11 13:37:08', null, null);
INSERT INTO `sys_log` VALUES ('8b2ad448021fbb5509ea04c9a780b165', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-11 14:35:25', null, null);
INSERT INTO `sys_log` VALUES ('69a9dfb2fb02e4537b86c9c5c05184ae', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.104', null, null, null, null, null, 'jeecg-boot', '2019-03-11 15:22:14', null, null);
INSERT INTO `sys_log` VALUES ('98b7fc431e4654f403e27ec9af845c7b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-11 17:31:38', null, null);
INSERT INTO `sys_log` VALUES ('42bf42af90d4df949ad0a6cd1b39805e', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.200', null, null, null, null, null, 'jeecg-boot', '2019-03-11 17:39:01', null, null);
INSERT INTO `sys_log` VALUES ('e234abc35a52f0dd2512b0ce2ea0e4f2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-11 20:05:26', null, null);
INSERT INTO `sys_log` VALUES ('69baa4f883fe881f401ea063ddfd0079', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-12 20:51:03', null, null);
INSERT INTO `sys_log` VALUES ('a867c282a8d97f7758235f881804bb48', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-13 18:28:20', null, null);
INSERT INTO `sys_log` VALUES ('1aa593c64062f0137c0691eabac07521', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 10:45:10', null, null);
INSERT INTO `sys_log` VALUES ('de978382f59685babf3684d1c090d136', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 12:55:55', null, null);
INSERT INTO `sys_log` VALUES ('75c7fa1a7d3639be1b112e263561e43a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 17:07:59', null, null);
INSERT INTO `sys_log` VALUES ('7a9d307d22fb2301d6a9396094afc82f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 18:45:04', null, null);
INSERT INTO `sys_log` VALUES ('28dbc8d16f98fb4b1f481462fcaba48b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 20:56:57', null, null);
INSERT INTO `sys_log` VALUES ('f1186792c6584729a0f6da4432d951f9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-14 21:45:52', null, null);
INSERT INTO `sys_log` VALUES ('4f31f3ebaf5d1a159d2bb11dd9984909', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-15 11:14:15', null, null);
INSERT INTO `sys_log` VALUES ('9a5c1fbf3543880af6461182e24b75db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-15 13:51:27', null, null);
INSERT INTO `sys_log` VALUES ('b86958d773b2c2bd79baa2e8c3c84050', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-15 16:49:36', null, null);
INSERT INTO `sys_log` VALUES ('a052befb699ee69b3197b139fd9263f0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-15 17:34:58', null, null);
INSERT INTO `sys_log` VALUES ('6836a652dc96246c028577e510695c6f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-15 20:47:02', null, null);
INSERT INTO `sys_log` VALUES ('8fe913a5b037943c6667ee4908f88bea', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-16 11:18:48', null, null);
INSERT INTO `sys_log` VALUES ('9410b7974fbc9df415867095b210e572', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-16 11:18:58', null, null);
INSERT INTO `sys_log` VALUES ('98d4b573769af6d9c10cd5c509bfb7af', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-16 11:21:25', null, null);
INSERT INTO `sys_log` VALUES ('18b16a451fec0fe7bf491ab348c65e30', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-16 11:55:45', null, null);
INSERT INTO `sys_log` VALUES ('0d85728028ed67da696137c0e82ab2f6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-16 12:58:43', null, null);
INSERT INTO `sys_log` VALUES ('4aa770f37a7de0039ba0f720c5246486', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:26:12', null, null);
INSERT INTO `sys_log` VALUES ('2fecb508d344c5b3a40f471d7b110f14', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:36:53', null, null);
INSERT INTO `sys_log` VALUES ('61aac4cfe67ec6437cd901f95fbd6f45', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:40:21', null, null);
INSERT INTO `sys_log` VALUES ('62e208389a400e37250cfa51c204bdc8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:44:49', null, null);
INSERT INTO `sys_log` VALUES ('eb9a522fd947c7a706c5a106ca32b8c9', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:50:17', null, null);
INSERT INTO `sys_log` VALUES ('bd9167a87aee4574a30d67825acaad0a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:51:03', null, null);
INSERT INTO `sys_log` VALUES ('49f1ec54eb16af2001ff6809a089e940', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:59:10', null, null);
INSERT INTO `sys_log` VALUES ('bdfd95b4d4c271d7d8d38f89f4a55da9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:59:12', null, null);
INSERT INTO `sys_log` VALUES ('95063e0bdfa5c9817cc0f66e96baad93', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 17:59:16', null, null);
INSERT INTO `sys_log` VALUES ('30da94dd068a5a57f3cece2ca5ac1a25', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 18:01:43', null, null);
INSERT INTO `sys_log` VALUES ('8fde5f89e8ad30cf3811b8683a9a77b1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 18:02:41', null, null);
INSERT INTO `sys_log` VALUES ('2ebe7f0432f01788d69d39bc6df04a1a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 18:05:09', null, null);
INSERT INTO `sys_log` VALUES ('beb9ef68b586f05bd7cf43058e01ad4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-17 22:29:02', null, null);
INSERT INTO `sys_log` VALUES ('befbcf5a27ef8d2ca8e6234077f9413d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 16:01:33', null, null);
INSERT INTO `sys_log` VALUES ('378b44af9c1042c1438450b11c707fcf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 16:07:39', null, null);
INSERT INTO `sys_log` VALUES ('0571e5730ee624d0dd1b095ad7101738', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 16:10:50', null, null);
INSERT INTO `sys_log` VALUES ('3ec2023daa4a7d6a542bf28b11acf586', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 16:18:20', null, null);
INSERT INTO `sys_log` VALUES ('64c00f27ddc93fda22f91b38d2b828b5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 17:34:45', null, null);
INSERT INTO `sys_log` VALUES ('21bad1470a40da8336294ca7330f443d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 17:35:32', null, null);
INSERT INTO `sys_log` VALUES ('72ee87d0637fb3365fdff9ccbf286c4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 17:36:44', null, null);
INSERT INTO `sys_log` VALUES ('d8c43edd685431ab3ef7b867efc29214', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 17:37:18', null, null);
INSERT INTO `sys_log` VALUES ('0ad51ba59da2c8763a4e6ed6e0a292b2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 17:37:53', null, null);
INSERT INTO `sys_log` VALUES ('d916bd1d956418e569549ee1c7220576', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 19:18:42', null, null);
INSERT INTO `sys_log` VALUES ('917dbb5db85d1a6f142135827e259bbf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 20:21:03', null, null);
INSERT INTO `sys_log` VALUES ('db2b518e7086a0561f936d327a0ab522', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 22:39:23', null, null);
INSERT INTO `sys_log` VALUES ('61d2d2fd3e9e23f67c23b893a1ae1e72', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 22:44:56', null, null);
INSERT INTO `sys_log` VALUES ('671a44fd91bf267549d407e0c2a680ee', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 22:45:16', null, null);
INSERT INTO `sys_log` VALUES ('586e8244eff6d6761077ef15ab9a82d9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-18 23:03:51', null, null);
INSERT INTO `sys_log` VALUES ('2b4d33d9be98e1e4cdd408a55f731050', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 10:32:00', null, null);
INSERT INTO `sys_log` VALUES ('3267222d9387284b864792531b450bfe', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 10:33:23', null, null);
INSERT INTO `sys_log` VALUES ('a28de45f52c027a3348a557efab6f430', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 10:34:26', null, null);
INSERT INTO `sys_log` VALUES ('9db7e7d214dbe9fe8fff5ff20634e282', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 11:19:34', null, null);
INSERT INTO `sys_log` VALUES ('74209dfc97285eb7919868545fc2c649', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 11:23:08', null, null);
INSERT INTO `sys_log` VALUES ('49d48fda33126595f6936a5d64e47af0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 13:17:51', null, null);
INSERT INTO `sys_log` VALUES ('fe0dc06eaef69047131f39052fcce5c4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 13:56:05', null, null);
INSERT INTO `sys_log` VALUES ('f540eff3f6e86c1e0beccd300efd357f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 15:15:26', null, null);
INSERT INTO `sys_log` VALUES ('3fd0d771bbdd34fae8b48690ddd57799', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 17:17:22', null, null);
INSERT INTO `sys_log` VALUES ('27e8812c9a16889f14935eecacf188eb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 18:52:19', null, null);
INSERT INTO `sys_log` VALUES ('88bab180edf685549c7344ec8db7d954', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 19:07:06', null, null);
INSERT INTO `sys_log` VALUES ('ed9b4ffc8afab10732aac2d0f84c567b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-19 19:10:52', null, null);
INSERT INTO `sys_log` VALUES ('ad97829fe7fefcd38c80d1eb1328e40f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 09:28:18', null, null);
INSERT INTO `sys_log` VALUES ('3d25a4cdd75b9c4c137394ce68e67154', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 09:59:31', null, null);
INSERT INTO `sys_log` VALUES ('5c7e834e089ef86555d8c2627b1b29b5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:25:26', null, null);
INSERT INTO `sys_log` VALUES ('b3adf055f54878657611ef430f85803e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:33:57', null, null);
INSERT INTO `sys_log` VALUES ('8d105ea6c89691bc8ee7d4fd568aa690', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:39:33', null, null);
INSERT INTO `sys_log` VALUES ('445436e800d306ec1d7763c0fe28ad38', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:43:00', null, null);
INSERT INTO `sys_log` VALUES ('7f9c3d539030049a39756208670be394', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:44:40', null, null);
INSERT INTO `sys_log` VALUES ('feaf7c377abc5824c1757d280dd3c164', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 11:58:54', null, null);
INSERT INTO `sys_log` VALUES ('c72bb25acd132303788699834ae039b4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:06:06', null, null);
INSERT INTO `sys_log` VALUES ('e4c06405615399d6b1ebea45c8112b4d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:07:26', null, null);
INSERT INTO `sys_log` VALUES ('f95d517f43ba2229c80c14c1883a4ee9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:11:59', null, null);
INSERT INTO `sys_log` VALUES ('d18bff297a5c2fa54d708f25a7d790d6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:13:05', null, null);
INSERT INTO `sys_log` VALUES ('b5f6636c6e24e559ddf1feb3e1a77fd5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:14:05', null, null);
INSERT INTO `sys_log` VALUES ('aeca30df24ce26f008a7e2101f7c513c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:27:53', null, null);
INSERT INTO `sys_log` VALUES ('cd7a7c49e02ca9613b6879fda4e563cf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 12:29:08', null, null);
INSERT INTO `sys_log` VALUES ('a7ee4b4c236bc0e8f56db5fdf1e5ac38', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 13:21:36', null, null);
INSERT INTO `sys_log` VALUES ('6d45672f99bbfd01d6385153e9c3ad91', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 13:49:20', null, null);
INSERT INTO `sys_log` VALUES ('905d2cf4308f70a3a2121a3476e38ed0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 14:00:30', null, null);
INSERT INTO `sys_log` VALUES ('27d23027dc320175d22391d06f50082f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 15:50:06', null, null);
INSERT INTO `sys_log` VALUES ('52fde989fb8bb78d03fb9c14242f5613', '1', '用户名: admin,登录成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-20 17:04:09', null, null);
INSERT INTO `sys_log` VALUES ('952947331f8f3379494c4742be797fc3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-20 18:42:11', null, null);
INSERT INTO `sys_log` VALUES ('39caf3d5d308001aeb0a18e15ae480b9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 10:31:07', null, null);
INSERT INTO `sys_log` VALUES ('772f238d46531a75fff31bae5841057c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 11:31:16', null, null);
INSERT INTO `sys_log` VALUES ('f79af48e6aeb150432640483f3bb7f2a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:09:11', null, null);
INSERT INTO `sys_log` VALUES ('20fc3263762c80ab9268ddd3d4b06500', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:36:44', null, null);
INSERT INTO `sys_log` VALUES ('e8b37ad67ef15925352a4ac3342cef07', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:38:10', null, null);
INSERT INTO `sys_log` VALUES ('d6aaf0f8e2428bf3c957becbf4bcedb4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:38:14', null, null);
INSERT INTO `sys_log` VALUES ('3bc73699a9fd3245b87336787422729b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:41:07', null, null);
INSERT INTO `sys_log` VALUES ('862aa0e6e101a794715174eef96f7847', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:41:09', null, null);
INSERT INTO `sys_log` VALUES ('a6209166e1e9b224cca09de1e9ea1ed7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:41:10', null, null);
INSERT INTO `sys_log` VALUES ('b954f7c34dfbe9f6a1fc12244e0a7d59', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:41:10', null, null);
INSERT INTO `sys_log` VALUES ('64711edfb8c4eb24517d86baca005c96', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:41:11', null, null);
INSERT INTO `sys_log` VALUES ('0efc9df0d52c65ec318e7b46db21655f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:42:47', null, null);
INSERT INTO `sys_log` VALUES ('c03985d6e038b5d8ebdeec27fce249ba', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:43:24', null, null);
INSERT INTO `sys_log` VALUES ('69e6fd7891d4b42b0cccdc0874a43752', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:45:58', null, null);
INSERT INTO `sys_log` VALUES ('9b23981621d5265a55681883ec19fa91', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:46:05', null, null);
INSERT INTO `sys_log` VALUES ('37ca8ff7098b9d118adb0a586bdc0d13', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:46:07', null, null);
INSERT INTO `sys_log` VALUES ('ea5f9191b0f593a1d6cb585538caa815', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:46:08', null, null);
INSERT INTO `sys_log` VALUES ('8e03def9e0283005161d062d4c0a5a80', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:46:09', null, null);
INSERT INTO `sys_log` VALUES ('f78e24f5e841acac2a720f46f6c554bc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:47:17', null, null);
INSERT INTO `sys_log` VALUES ('f93279c6899dc5e6cec975906f8bf811', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:47:20', null, null);
INSERT INTO `sys_log` VALUES ('cd5af66a87bb40026c72a748155b47e8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:47:26', null, null);
INSERT INTO `sys_log` VALUES ('5902fb4ba61ccf7ff4d2dd97072b7e5b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:48:30', null, null);
INSERT INTO `sys_log` VALUES ('e1d1fc464cf48ec26b7412585bdded1a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 13:49:15', null, null);
INSERT INTO `sys_log` VALUES ('5ea258e1f478d27e0879e2f4bcb89ecd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 14:01:59', null, null);
INSERT INTO `sys_log` VALUES ('2e44c368eda5a7f7a23305b61d82cddb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 18:14:23', null, null);
INSERT INTO `sys_log` VALUES ('34a6b86424857a63159f0e8254e238c2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 18:22:08', null, null);
INSERT INTO `sys_log` VALUES ('ffc6178ffa099bb90b9a4d0a64dae42b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 18:28:32', null, null);
INSERT INTO `sys_log` VALUES ('3612f8d40add5a7754ea3d54de0b5f20', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 19:59:59', null, null);
INSERT INTO `sys_log` VALUES ('7a511b225189342b778647db3db385cd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 20:50:10', null, null);
INSERT INTO `sys_log` VALUES ('b7085f003b4336af4d4ba18147f8e5ae', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 22:29:37', null, null);
INSERT INTO `sys_log` VALUES ('c66e22782dd3916d1361c76b0cc4ec8a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-21 22:44:06', null, null);
INSERT INTO `sys_log` VALUES ('c6cbe54fcb194d025a081e5f91a7e3f0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 10:26:38', null, null);
INSERT INTO `sys_log` VALUES ('1ab7c74d217152081f4fa59e4a56cc7b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 12:03:39', null, null);
INSERT INTO `sys_log` VALUES ('5f00b5514a11cd2fe240c131e9ddd136', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 16:30:52', null, null);
INSERT INTO `sys_log` VALUES ('82cee1c403025fc1db514c60fc7d8d29', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 16:41:50', null, null);
INSERT INTO `sys_log` VALUES ('af5869701738a6f4c2c58fe8dfe02726', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 16:42:40', null, null);
INSERT INTO `sys_log` VALUES ('233e39d8b7aa90459ebef23587c25448', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 17:38:36', null, null);
INSERT INTO `sys_log` VALUES ('b0cebd174565a88bb850a2475ce14625', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 18:19:39', null, null);
INSERT INTO `sys_log` VALUES ('26529d5753ceebbd0d774542ec83a43e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 20:17:41', null, null);
INSERT INTO `sys_log` VALUES ('f3e1f7fb81004ccd64df12d94ef1e695', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 21:30:37', null, null);
INSERT INTO `sys_log` VALUES ('a2e0435673b17f4fb848eecdf8ecacd6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 21:32:37', null, null);
INSERT INTO `sys_log` VALUES ('5323f848cddbb80ba4f0d19c0580eba9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 22:58:40', null, null);
INSERT INTO `sys_log` VALUES ('5858f2f8436460a94a517904c0bfcacb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-22 23:42:21', null, null);
INSERT INTO `sys_log` VALUES ('8d9ce65020320d46882be43b22b12a62', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 10:56:43', null, null);
INSERT INTO `sys_log` VALUES ('11802c7a3644af411bc4e085553cfd4f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 14:46:35', null, null);
INSERT INTO `sys_log` VALUES ('fc69a1640a4772c8edf2548d053fa6de', '1', '用户名: admin,登录成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-23 14:55:33', null, null);
INSERT INTO `sys_log` VALUES ('e3031f999984909f9048d8ec15543ad0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 18:43:02', null, null);
INSERT INTO `sys_log` VALUES ('f43e38800d779422c75075448af738d1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 18:47:11', null, null);
INSERT INTO `sys_log` VALUES ('dcfe23b155d5c6fa9a302c063b19451e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 18:47:21', null, null);
INSERT INTO `sys_log` VALUES ('802cec0efbe9d862b7cea29fefc5448b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 18:58:50', null, null);
INSERT INTO `sys_log` VALUES ('f58e160e97d13a851f59b70bf54e0d06', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 20:11:58', null, null);
INSERT INTO `sys_log` VALUES ('b8bd2a9de3fb917dfb6b435e58389901', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 20:13:31', null, null);
INSERT INTO `sys_log` VALUES ('e01ed1516e8ae3a2180acbd4e4508fa5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 20:28:12', null, null);
INSERT INTO `sys_log` VALUES ('b7f33b5a514045878447fc64636ac3e6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 22:00:34', null, null);
INSERT INTO `sys_log` VALUES ('d0ce9bfc790a573d48d49d3bbbf1a1cb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 22:09:06', null, null);
INSERT INTO `sys_log` VALUES ('74c991568d8bcb2049a0dbff53f72875', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 22:12:15', null, null);
INSERT INTO `sys_log` VALUES ('0251bbee51c28f83459f4a57eeb61777', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 22:14:27', null, null);
INSERT INTO `sys_log` VALUES ('a5848ab4e8d0fb6ecf71ee1d99165468', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 22:14:50', null, null);
INSERT INTO `sys_log` VALUES ('79a1737fcc199c8262f344e48afb000d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-23 23:25:25', null, null);
INSERT INTO `sys_log` VALUES ('6cfeaf6a6be5bb993b9578667999c354', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-24 11:43:34', null, null);
INSERT INTO `sys_log` VALUES ('c5d4597b38275dcb890c6568a7c113f2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-24 12:18:46', null, null);
INSERT INTO `sys_log` VALUES ('0f173ec7e8819358819aa14aafc724c0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:15:58', null, null);
INSERT INTO `sys_log` VALUES ('ce9893f4d0dd163e900fcd537f2c292d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:55:55', null, null);
INSERT INTO `sys_log` VALUES ('90711ddb861e28bd8774631c98f3edb9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:57:42', null, null);
INSERT INTO `sys_log` VALUES ('fb73d58bf6503270025972f99e50335d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:57:56', null, null);
INSERT INTO `sys_log` VALUES ('3a290289b4b30a1caaac2d03ad3161cd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:58:13', null, null);
INSERT INTO `sys_log` VALUES ('716f9f5f066a6f75a58b7b05f2f7f861', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:59:01', null, null);
INSERT INTO `sys_log` VALUES ('151a9f1b01e4e749124d274313cd138c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 11:59:17', null, null);
INSERT INTO `sys_log` VALUES ('0ef3e7ae8c073a7e3bdd736068f86c84', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:02:27', null, null);
INSERT INTO `sys_log` VALUES ('d7e7cb4c21372e48b8e0ec7e679466e3', '1', '用户名: null,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:02:34', null, null);
INSERT INTO `sys_log` VALUES ('15b9599cb02b49a62fb4a1a71ccebc18', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:02:50', null, null);
INSERT INTO `sys_log` VALUES ('ecfee5b948602a274093b8890e5e7f3f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:05:11', null, null);
INSERT INTO `sys_log` VALUES ('cbf83d11486a8d57814ae38c9822b022', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:05:39', null, null);
INSERT INTO `sys_log` VALUES ('f2ce8024e62740f63c134c3cfb3cae23', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:07:41', null, null);
INSERT INTO `sys_log` VALUES ('c665d704539483630cc9ed5715ed57a8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:10:12', null, null);
INSERT INTO `sys_log` VALUES ('e93f1a170e3cd33f90dd132540c7a39b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:12:43', null, null);
INSERT INTO `sys_log` VALUES ('fded8eb5d78d13791baec769019fee54', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:15:07', null, null);
INSERT INTO `sys_log` VALUES ('03ec66b6b6d17c007ec2f918efe5b898', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:16:03', null, null);
INSERT INTO `sys_log` VALUES ('5e8bac7831de49146d568c9a8477ddad', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:16:37', null, null);
INSERT INTO `sys_log` VALUES ('cd064a2f6cb6c640cb97a74aaa6041d7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:17:10', null, null);
INSERT INTO `sys_log` VALUES ('a521d9f2a0087daa37923fa704dea85b', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:45:52', null, null);
INSERT INTO `sys_log` VALUES ('4816854636129e31c2a5f9d38af842ef', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:45:54', null, null);
INSERT INTO `sys_log` VALUES ('90b4bad7939233a1e0d7935f079ea0fa', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:45:54', null, null);
INSERT INTO `sys_log` VALUES ('aec0817ecc0063bde76c1f6b6889d117', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:47:06', null, null);
INSERT INTO `sys_log` VALUES ('e169938510c9320cb1495ddb9aabb9d1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:47:40', null, null);
INSERT INTO `sys_log` VALUES ('b7478d917ab6f663e03d458f0bb022a3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:50:55', null, null);
INSERT INTO `sys_log` VALUES ('642e48f2e5ac8fe64f1bfacf4d234dc8', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:51:21', null, null);
INSERT INTO `sys_log` VALUES ('ed2740de487c684be9fa3cf72113ae30', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:51:43', null, null);
INSERT INTO `sys_log` VALUES ('b1e9797721dbfcc51bbe7182142cbdcd', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:52:25', null, null);
INSERT INTO `sys_log` VALUES ('6a67bf2ff924548dee04aa97e1d64d38', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:52:41', null, null);
INSERT INTO `sys_log` VALUES ('a56661bbc72b8586778513c71f4764f5', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:53:09', null, null);
INSERT INTO `sys_log` VALUES ('ae61be664d2f30d4f2248347c5998a45', '1', '用户名: jeecg,退出成功！', null, 'jeecg', 'jeecg', '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-03-25 12:53:17', null, null);
INSERT INTO `sys_log` VALUES ('4ab79469ba556fa890258a532623d1dc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:54:20', null, null);
INSERT INTO `sys_log` VALUES ('3a0330033a8d3b51ffbfb2e0a7db9bba', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:54:27', null, null);
INSERT INTO `sys_log` VALUES ('b972484d206b36420efac466fae1c53f', '1', '用户名: jeecg,退出成功！', null, 'jeecg', 'jeecg', '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-03-25 12:54:38', null, null);
INSERT INTO `sys_log` VALUES ('7e92abdc0c1f54596df499a5a2d11683', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 12:59:34', null, null);
INSERT INTO `sys_log` VALUES ('fa9b4d7d42bc9d1ba058455b4afedbfb', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 12:59:46', null, null);
INSERT INTO `sys_log` VALUES ('1d970c0e396ffc869e3a723d51f88b46', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 13:01:17', null, null);
INSERT INTO `sys_log` VALUES ('88d7136ed5c7630057451816dbaff183', '1', '用户名: jeecg,退出成功！', null, 'jeecg', 'jeecg', '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-03-25 13:01:24', null, null);
INSERT INTO `sys_log` VALUES ('48eac0dd1c11fe8f0cb49f1bd14529c2', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 13:01:31', null, null);
INSERT INTO `sys_log` VALUES ('a8c7ba2d11315b171940def2cbeb0e8f', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-25 13:01:40', null, null);
INSERT INTO `sys_log` VALUES ('20fea778f4e1ac5c01b5a5a58e3805be', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 19:01:49', null, null);
INSERT INTO `sys_log` VALUES ('48e5faf2d21ead650422dc2eaf1bb6c5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 22:08:09', null, null);
INSERT INTO `sys_log` VALUES ('f74f759b43afa639fd1c4f215c984ae0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-25 22:08:18', null, null);
INSERT INTO `sys_log` VALUES ('d82b170459d99fc05eb8aa1774e1a1c9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-26 18:45:14', null, null);
INSERT INTO `sys_log` VALUES ('e088a2607864d3e6aadf239874d51756', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-26 18:46:56', null, null);
INSERT INTO `sys_log` VALUES ('95d906e6f048c3e71ddbcc0c9448cf49', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-26 19:23:30', null, null);
INSERT INTO `sys_log` VALUES ('3767186b722b7fefd465e147d3170ad1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-26 21:57:19', null, null);
INSERT INTO `sys_log` VALUES ('f21e30d73c337ea913849ed65808525c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 10:23:22', null, null);
INSERT INTO `sys_log` VALUES ('189e3428e35e27dfe92ece2848b10ba8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 15:52:21', null, null);
INSERT INTO `sys_log` VALUES ('e2b6d0e751f130d35c0c3b8c6bd2a77e', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-27 16:18:40', null, null);
INSERT INTO `sys_log` VALUES ('586002e1fb4e60902735070bab48afe3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 16:18:52', null, null);
INSERT INTO `sys_log` VALUES ('611fa74c70bd5a7a8af376464a2133e8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 17:48:13', null, null);
INSERT INTO `sys_log` VALUES ('90555a39c0b02180df74752e4d33f253', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 18:26:25', null, null);
INSERT INTO `sys_log` VALUES ('217aa2f713b0903e6be699136e374012', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-27 20:07:32', null, null);
INSERT INTO `sys_log` VALUES ('5554869b3475770046602061775e0e57', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 14:38:05', null, null);
INSERT INTO `sys_log` VALUES ('bfec8c8c88868391041667d924e3af7f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 14:38:27', null, null);
INSERT INTO `sys_log` VALUES ('675153568c479d8b7c6fe63327066c9f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 15:29:42', null, null);
INSERT INTO `sys_log` VALUES ('4930e32672465979adbc592e116226a6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 16:53:28', null, null);
INSERT INTO `sys_log` VALUES ('9a1456ef58a2b1fb63cdc54b723f2539', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 17:26:39', null, null);
INSERT INTO `sys_log` VALUES ('484cdb8db40e3f76ef686552f57d8099', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 18:14:49', null, null);
INSERT INTO `sys_log` VALUES ('02d4447c9d97ac4fc1c3a9a4c789c2a8', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-28 18:24:18', null, null);
INSERT INTO `sys_log` VALUES ('59558082e1b1d754fa3def125ed4db3c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 18:24:19', null, null);
INSERT INTO `sys_log` VALUES ('c434dc5172dc993ee7cd96187ca58653', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-28 19:46:08', null, null);
INSERT INTO `sys_log` VALUES ('a6261bbbf8e964324935722ea1384a5d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 19:46:27', null, null);
INSERT INTO `sys_log` VALUES ('f0748a25728348591c7b73a66f273457', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-28 19:46:27', null, null);
INSERT INTO `sys_log` VALUES ('14f447d9b60725cc86b3100a5cb20b75', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 19:46:30', null, null);
INSERT INTO `sys_log` VALUES ('7c88e9cf6018a1b97b420b8cb6122815', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-28 19:46:30', null, null);
INSERT INTO `sys_log` VALUES ('cb7c6178101ef049d3f1820ee41df539', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 19:59:19', null, null);
INSERT INTO `sys_log` VALUES ('21fed0f2d080e04cf0901436721a77a6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-28 21:53:31', null, null);
INSERT INTO `sys_log` VALUES ('4ba055970859a6f1afcc01227cb82a2d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 09:43:56', null, null);
INSERT INTO `sys_log` VALUES ('3ba1e54aa9aa760b59dfe1d1259459bc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 09:44:07', null, null);
INSERT INTO `sys_log` VALUES ('7b44138c1b80b67da13b89db756a22b0', '2', '添加测试DEMO', null, null, null, '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"jeecg-boot\",\"createTime\":1553824172062,\"id\":\"5fce01cb7f0457746c97d8ca05628f81\",\"name\":\"1212\"}]', null, '25', 'jeecg-boot', '2019-03-29 09:49:32', null, null);
INSERT INTO `sys_log` VALUES ('d7e8a7f14967c70d68f5569cb4d11d0a', '2', '删除测试DEMO', null, null, null, '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.delete()', null, '[\"5fce01cb7f0457746c97d8ca05628f81\"]', null, '9', 'jeecg-boot', '2019-03-29 09:49:39', null, null);
INSERT INTO `sys_log` VALUES ('e7f2b0a7493e7858c5db1f1595fa54b1', '2', '添加测试DEMO', null, null, null, '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"jeecg-boot\",\"createTime\":1553824376817,\"id\":\"e771211b77cd3b326d3e61edfd9a5a19\",\"keyWord\":\"222\",\"name\":\"222\"}]', null, '7', 'jeecg-boot', '2019-03-29 09:52:56', null, null);
INSERT INTO `sys_log` VALUES ('997bb4cb1ad24439b6f7656222af0710', '2', '添加测试DEMO', null, null, null, '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"jeecg-boot\",\"createTime\":1553824768819,\"id\":\"ee84471f0dff5ae88c45e852bfa0280f\",\"keyWord\":\"22\",\"name\":\"222\"}]', null, '5', 'jeecg-boot', '2019-03-29 09:59:28', null, null);
INSERT INTO `sys_log` VALUES ('a6c3b28530416dace21371abe8cae00b', '2', '删除测试DEMO', null, null, null, '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.delete()', null, '[\"ee84471f0dff5ae88c45e852bfa0280f\"]', null, '9', 'jeecg-boot', '2019-03-29 09:59:48', null, null);
INSERT INTO `sys_log` VALUES ('d2fe98d661f1651b639bf74499f124db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 10:16:29', null, null);
INSERT INTO `sys_log` VALUES ('2186244ae450e83d1487aa01fbeae664', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 14:47:43', null, null);
INSERT INTO `sys_log` VALUES ('a5daa58b078cb8b3653af869aeecebd0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 17:14:31', null, null);
INSERT INTO `sys_log` VALUES ('f29f3b7b7e14b1389a0c53d263c0b26b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 17:44:25', null, null);
INSERT INTO `sys_log` VALUES ('2659c59136fb1a284ab0642361b10cdd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-29 18:40:22', null, null);
INSERT INTO `sys_log` VALUES ('a42e5cd05566ea226c2e2fc201860f2c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 11:15:50', null, null);
INSERT INTO `sys_log` VALUES ('f06048c147c5bcdbed672e32b2c86b1c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 14:07:28', null, null);
INSERT INTO `sys_log` VALUES ('13c83c56a0de8a702aeb2aa0c330e42c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 14:53:52', null, null);
INSERT INTO `sys_log` VALUES ('ab1d707bbfdf44aa17307d30ca872403', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 15:50:42', null, null);
INSERT INTO `sys_log` VALUES ('5d8ed15778aa7d99224ee62c606589fb', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-30 15:51:02', null, null);
INSERT INTO `sys_log` VALUES ('42aef93749cc6222d5debe3fb31ba41b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 15:51:04', null, null);
INSERT INTO `sys_log` VALUES ('5c04e3d9429e3bcff4d55f6205c4aa83', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 18:14:29', null, null);
INSERT INTO `sys_log` VALUES ('af8fe96a9f0b325e4833fc0d9c4721bf', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-30 18:14:56', null, null);
INSERT INTO `sys_log` VALUES ('21910e350c9083e107d39ff4278f51d6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 18:14:59', null, null);
INSERT INTO `sys_log` VALUES ('636309eec5e750bc94ce06fb98526fb2', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-03-30 18:15:03', null, null);
INSERT INTO `sys_log` VALUES ('9d0416e09fae7aeeeefc8511a61650c2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 18:15:05', null, null);
INSERT INTO `sys_log` VALUES ('2e63fd1b3b6a6145bc04b2a1df18d2f5', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 19:01:33', null, null);
INSERT INTO `sys_log` VALUES ('2966ed2bdf67c9f3306b058d13bef301', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 21:25:10', null, null);
INSERT INTO `sys_log` VALUES ('3683743d1936d06f3aaa03d6470e5178', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-30 22:40:12', null, null);
INSERT INTO `sys_log` VALUES ('0ba24c5f61ff53f93134cf932dd486db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-31 21:06:05', null, null);
INSERT INTO `sys_log` VALUES ('bbd3e1f27e025502a67cf54945b0b269', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-03-31 22:13:16', null, null);
INSERT INTO `sys_log` VALUES ('1f8f46118336b2cacf854c1abf8ae144', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 11:02:59', null, null);
INSERT INTO `sys_log` VALUES ('ac8cf22c2f10a38c7a631fc590551c40', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 12:04:16', null, null);
INSERT INTO `sys_log` VALUES ('7d11535270734de80bd52ec0daa4fc1f', '1', '用户名: admin,登录成功！', null, null, null, '192.168.1.105', null, null, null, null, null, 'jeecg-boot', '2019-04-01 12:20:14', null, null);
INSERT INTO `sys_log` VALUES ('6b4cdd499885ccba43b40f10abf64a78', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 13:04:43', null, null);
INSERT INTO `sys_log` VALUES ('77a329e5eb85754075165b06b7d877fd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 13:25:17', null, null);
INSERT INTO `sys_log` VALUES ('0e754ee377033067f7b2f10b56b8784c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 17:17:45', null, null);
INSERT INTO `sys_log` VALUES ('9b7a830914668881335da1b0ce2274b1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 17:19:02', null, null);
INSERT INTO `sys_log` VALUES ('a1b870eee811cfa4960f577b667b0973', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 18:23:44', null, null);
INSERT INTO `sys_log` VALUES ('85b3106d757d136b48172a9ab1f35bb6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 18:34:34', null, null);
INSERT INTO `sys_log` VALUES ('7f31435ca2f5a4ef998a4152b2433dec', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 18:36:40', null, null);
INSERT INTO `sys_log` VALUES ('f20cf3fe228ba6196a48015b98d0d354', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 19:25:22', null, null);
INSERT INTO `sys_log` VALUES ('65771bce3f5786dfb4d84570df61a47a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 22:07:57', null, null);
INSERT INTO `sys_log` VALUES ('c98a6367b152cf5311d0eec98fab390c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 22:13:34', null, null);
INSERT INTO `sys_log` VALUES ('93b4d26f60d7fb45a60524760bf053e4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 22:20:06', null, null);
INSERT INTO `sys_log` VALUES ('3087ac4988a961fa1ec0b4713615c719', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-01 22:54:24', null, null);
INSERT INTO `sys_log` VALUES ('a69f4ff4e48754de96ae6fa4fabc1579', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:18:48', null, null);
INSERT INTO `sys_log` VALUES ('a63147887c6ca54ce31f6c9e6279a714', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:19:07', null, null);
INSERT INTO `sys_log` VALUES ('a2950ae3b86f786a6a6c1ce996823b53', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 09:47:11', null, null);
INSERT INTO `sys_log` VALUES ('615625178b01fc20c60184cd28e64a70', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:47:13', null, null);
INSERT INTO `sys_log` VALUES ('89fbc93e77defb34c609c84a7fe83039', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 09:47:14', null, null);
INSERT INTO `sys_log` VALUES ('432067d777447423f1ce3db11a273f6f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:47:17', null, null);
INSERT INTO `sys_log` VALUES ('7d8539ff876aad698fba235a1c467fb8', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 09:47:18', null, null);
INSERT INTO `sys_log` VALUES ('689b8f2110f99c52e18268cbaf05bbb6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:58:44', null, null);
INSERT INTO `sys_log` VALUES ('2919d2f18db064978a619707bde4d613', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 09:58:45', null, null);
INSERT INTO `sys_log` VALUES ('0dc6d04b99e76ad400eef1ded2d3d97c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 09:59:45', null, null);
INSERT INTO `sys_log` VALUES ('45f0309632984f5f7c70b3d40dbafe8b', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 09:59:46', null, null);
INSERT INTO `sys_log` VALUES ('d869534109332e770c70fad65ef37998', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:02:30', null, null);
INSERT INTO `sys_log` VALUES ('c21422fa08f8480a53367fda7ddddf12', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 10:02:30', null, null);
INSERT INTO `sys_log` VALUES ('1e4533a02fb9c739a3555fa7be6e7899', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:04:56', null, null);
INSERT INTO `sys_log` VALUES ('03c0ab177bd7d840b778713b37daf86f', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 10:04:57', null, null);
INSERT INTO `sys_log` VALUES ('60886d5de8a18935824faf8b0bed489e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:11:35', null, null);
INSERT INTO `sys_log` VALUES ('4218b30015501ee966548c139c14f43f', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 10:11:35', null, null);
INSERT INTO `sys_log` VALUES ('de938485a45097d1bf3fa311d0216ed4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:15:22', null, null);
INSERT INTO `sys_log` VALUES ('e1d0b1fd3be59e465b740e32346e85b0', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 10:16:37', null, null);
INSERT INTO `sys_log` VALUES ('4234117751af62ac87343cbf8a6f1e0f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:17:17', null, null);
INSERT INTO `sys_log` VALUES ('845f732f6a0f0d575debc4103e92bea2', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 10:17:18', null, null);
INSERT INTO `sys_log` VALUES ('cc39057ae0a8a996fb0b3a8ad5b8f341', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 10:20:05', null, null);
INSERT INTO `sys_log` VALUES ('155d2991204d541388d837d1457e56ab', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 11:32:57', null, null);
INSERT INTO `sys_log` VALUES ('2312c2693d6b50ca06799fee0ad2554a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 12:11:32', null, null);
INSERT INTO `sys_log` VALUES ('d98115c02c0ac478a16d6c35de35053d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 12:50:09', null, null);
INSERT INTO `sys_log` VALUES ('55e906361eeabb6ec16d66c7196a06f0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 12:50:20', null, null);
INSERT INTO `sys_log` VALUES ('3836dc3f91d072e838092bc8d3143906', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 12:50:32', null, null);
INSERT INTO `sys_log` VALUES ('add13f513772a63f8ca8bf85634bb72c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 13:09:03', null, null);
INSERT INTO `sys_log` VALUES ('a6971e63e3d9158020e0186cda81467d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 14:59:23', null, null);
INSERT INTO `sys_log` VALUES ('2eb75cb6ca5bc60241e01fa7471c0ccf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 18:34:04', null, null);
INSERT INTO `sys_log` VALUES ('3e69108be63179550afe424330a8a9e4', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-02 18:38:05', null, null);
INSERT INTO `sys_log` VALUES ('6c558d70dc5794f9f473d8826485727a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 18:38:44', null, null);
INSERT INTO `sys_log` VALUES ('687810e7fea7e480962c58db515a5e1c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 18:42:30', null, null);
INSERT INTO `sys_log` VALUES ('d23e6766cecf911fb2e593eeee354e18', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 18:42:35', null, null);
INSERT INTO `sys_log` VALUES ('0819ea9729ddf70f64ace59370a62cf1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 18:59:43', null, null);
INSERT INTO `sys_log` VALUES ('939b3ff4733247a47efe1352157b1f27', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 19:01:06', null, null);
INSERT INTO `sys_log` VALUES ('6cf638853ef5384bf81ed84572a6445d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-02 19:25:24', null, null);
INSERT INTO `sys_log` VALUES ('bc28d4275c7c7fcd067e1aef40ec1dd4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 10:53:03', null, null);
INSERT INTO `sys_log` VALUES ('43536edd8aa99f9b120872e2c768206c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 10:53:26', null, null);
INSERT INTO `sys_log` VALUES ('7268539fbe77c5cc572fb46d71d838f1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 13:22:48', null, null);
INSERT INTO `sys_log` VALUES ('f0409312093beb563ac4016f2b2c6dfd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 13:24:59', null, null);
INSERT INTO `sys_log` VALUES ('a710ed2de7e31fd72b1efb1b54ba5a87', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 15:30:43', null, null);
INSERT INTO `sys_log` VALUES ('b01c3f89bcfd263de7cb1a9b0210a7af', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 17:53:55', null, null);
INSERT INTO `sys_log` VALUES ('e1fa52ecbcc0970622cc5a0c06de9317', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-03 18:33:04', null, null);
INSERT INTO `sys_log` VALUES ('790b722fa99a8f3a0bc38f61e13c1cf4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 18:34:07', null, null);
INSERT INTO `sys_log` VALUES ('20e5887d0c9c7981159fe91a51961141', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 20:12:07', null, null);
INSERT INTO `sys_log` VALUES ('ce6aa822166b97a78b0bbea62366f8e0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 20:14:29', null, null);
INSERT INTO `sys_log` VALUES ('3e6116220fa8d4808175738c6de51b12', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 21:04:46', null, null);
INSERT INTO `sys_log` VALUES ('10a434c326e39b1d046defddc8c57f4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 21:18:29', null, null);
INSERT INTO `sys_log` VALUES ('317e3ae1b6ccdfb5db6940789e12d300', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-03 21:44:31', null, null);
INSERT INTO `sys_log` VALUES ('2b801129457c05d23653ecaca88f1711', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-03 21:44:34', null, null);
INSERT INTO `sys_log` VALUES ('7a99cf653439ca82ac3b0d189ddaad4a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 10:41:34', null, null);
INSERT INTO `sys_log` VALUES ('68e90e08a866de748e9901e923406959', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 12:37:06', null, null);
INSERT INTO `sys_log` VALUES ('2942a12521ac8e3d441429e6c4b04207', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 14:12:10', null, null);
INSERT INTO `sys_log` VALUES ('dfacaa7c01ccf0bade680044cced3f11', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 15:25:10', null, null);
INSERT INTO `sys_log` VALUES ('f3cafb545e5693e446f641fa0b5ac8cd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 17:07:56', null, null);
INSERT INTO `sys_log` VALUES ('060d541a9571ca2b0d24790a98d170a6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 19:28:04', null, null);
INSERT INTO `sys_log` VALUES ('9df97c1b3213aa64eda81c6bf818b02b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 22:42:48', null, null);
INSERT INTO `sys_log` VALUES ('43079866b75ee6a031835795bb681e16', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-04 22:44:16', null, null);
INSERT INTO `sys_log` VALUES ('55d649432efa7eaecd750b4b6b883f83', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 22:44:19', null, null);
INSERT INTO `sys_log` VALUES ('ca737885d9034f71f70c4ae7986fafa8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 22:47:28', null, null);
INSERT INTO `sys_log` VALUES ('1534f0c50e67c5682e91af5160a67a80', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 22:47:28', null, null);
INSERT INTO `sys_log` VALUES ('93bb98ba996dacebfb4f61503067352e', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 22:47:28', null, null);
INSERT INTO `sys_log` VALUES ('5c48703e3a2d4f81ee5227f0e2245990', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-04 23:12:04', null, null);
INSERT INTO `sys_log` VALUES ('70849167f54fd50d8906647176d90fdf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 23:12:29', null, null);
INSERT INTO `sys_log` VALUES ('310bb368795f4985ed4eada030a435a0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-04 23:22:20', null, null);
INSERT INTO `sys_log` VALUES ('477592ab95cd219a2ccad79de2f69f51', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-05 10:38:24', null, null);
INSERT INTO `sys_log` VALUES ('e39f051ba6fdb7447f975421f3b090a7', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-05 12:49:18', null, null);
INSERT INTO `sys_log` VALUES ('4d1be4b4991a5c2d4d17d0275e4209cf', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-05 20:47:21', null, null);
INSERT INTO `sys_log` VALUES ('9eb3fb6d9d45e3847a88f65ed47da935', '1', '用户名: jeecg,登录成功！', null, null, null, '192.168.3.22', null, null, null, null, null, 'jeecg-boot', '2019-04-05 20:52:47', null, null);
INSERT INTO `sys_log` VALUES ('6664dc299f547f6702f93e2358810cc1', '1', '用户名: admin,登录成功！', null, null, null, '192.168.3.22', null, null, null, null, null, 'jeecg-boot', '2019-04-05 21:04:14', null, null);
INSERT INTO `sys_log` VALUES ('9c32ec437d8f8d407b1bd1165fc0305d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-08 15:01:25', null, null);
INSERT INTO `sys_log` VALUES ('68df65639e82cc6a889282fbef53afbb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-08 15:01:37', null, null);
INSERT INTO `sys_log` VALUES ('79e76353faffd0beb0544c0aede8564f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-08 17:28:14', null, null);
INSERT INTO `sys_log` VALUES ('da3fda67aea2e565574ec2bcfab5b750', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-08 22:36:20', null, null);
INSERT INTO `sys_log` VALUES ('de37620b6921abcfe642606a0358d30f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-09 15:42:46', null, null);
INSERT INTO `sys_log` VALUES ('b0e6b3a0ec5d8c73166fb8129d21a834', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-09 16:56:16', null, null);
INSERT INTO `sys_log` VALUES ('01075aa535274735e0df0a8bc44f62f9', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-09 16:56:46', null, null);
INSERT INTO `sys_log` VALUES ('01ebe1cbeae916a9228770f63130fdac', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-09 16:56:50', null, null);
INSERT INTO `sys_log` VALUES ('baa53d6a534e669f6150ea47565fa5b9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-09 17:27:24', null, null);
INSERT INTO `sys_log` VALUES ('e232f89df26cc9e5eced10476c4e4a2b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 10:05:11', null, null);
INSERT INTO `sys_log` VALUES ('335956cbad23d1974138752199bf1d84', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 10:05:36', null, null);
INSERT INTO `sys_log` VALUES ('bd6d7d720b9dd803f8ad26e2d40870f3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 11:04:06', null, null);
INSERT INTO `sys_log` VALUES ('ff3f7dbda20cd2734b1238fa5ba17fcf', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-10 11:26:43', null, null);
INSERT INTO `sys_log` VALUES ('672b527c49dc349689288ebf2c43ed4d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 11:26:47', null, null);
INSERT INTO `sys_log` VALUES ('21510ebaa4eca640852420ed6f6cbe01', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 11:41:26', null, null);
INSERT INTO `sys_log` VALUES ('3e2574b7b723fbc9c712b8e200ea0c84', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 14:24:34', null, null);
INSERT INTO `sys_log` VALUES ('0d4582c6b7719b0bfc0260939d97274f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 21:48:47', null, null);
INSERT INTO `sys_log` VALUES ('3e64011b4bea7cdb76953bfbf57135ce', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-10 23:09:32', null, null);
INSERT INTO `sys_log` VALUES ('a83e37b55a07fe48272b0005a193dee6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 09:17:59', null, null);
INSERT INTO `sys_log` VALUES ('30ec2dc50347240f131c1004ee9b3a40', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 10:19:05', null, null);
INSERT INTO `sys_log` VALUES ('7ce1934fb542a406e92867aec5b7254d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 14:53:23', null, null);
INSERT INTO `sys_log` VALUES ('5ee6d5fe1e6adcc4ad441b230fae802d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 15:56:33', null, null);
INSERT INTO `sys_log` VALUES ('d4ef00700436645680657f72445d38db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 18:05:29', null, null);
INSERT INTO `sys_log` VALUES ('aa49341b29865b45588ad2f9b89c47ea', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-11 19:42:42', null, null);
INSERT INTO `sys_log` VALUES ('d3b54be0510db6a6da27bf30becb5335', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 19:42:46', null, null);
INSERT INTO `sys_log` VALUES ('dd4e1ab492e59719173d8ae0f5dbc9a2', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-04-11 19:47:12', null, null);
INSERT INTO `sys_log` VALUES ('056dd4466f4ed51de26c535fd9864828', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-11 19:47:15', null, null);
INSERT INTO `sys_log` VALUES ('89bfd8b9d6fa57a8e7017a2345ec1534', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-12 09:27:32', null, null);
INSERT INTO `sys_log` VALUES ('51aeabed335ab4e238640a4d17dd51a3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg-boot', '2019-04-12 10:12:41', null, null);
INSERT INTO `sys_log` VALUES ('67181c36b55b06047a16a031fd1262c1', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-17 13:55:22', null, null);
INSERT INTO `sys_log` VALUES ('2d5af41d2df82b316ba31fcdf6168d6a', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-17 14:43:58', null, null);
INSERT INTO `sys_log` VALUES ('e2f703771f64b1bcd709204669ae3d93', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-05-17 14:48:39', null, null);
INSERT INTO `sys_log` VALUES ('8143ce0b35bfe6e7b8113e1ecc066acd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-17 14:48:48', null, null);
INSERT INTO `sys_log` VALUES ('2bca2d6666c1f6630225252c7b31326c', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-05-17 18:30:48', null, null);
INSERT INTO `sys_log` VALUES ('11695a9dcf44859cda97a4226bebe21b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-17 18:30:57', null, null);
INSERT INTO `sys_log` VALUES ('40209016cadff6b571a8150c6218cfa8', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-18 11:33:28', null, null);
INSERT INTO `sys_log` VALUES ('23a314588249752842447e4f98783be4', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-19 18:28:48', null, null);
INSERT INTO `sys_log` VALUES ('5f0a5e85efbe9c79645ffc0c15fcee1a', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1558261833637,\"id\":\"94f78b1c9753dfb1202d731f540666e1\",\"keyWord\":\"1\",\"name\":\"1\",\"sysOrgCode\":\"A01\"}]', null, '30', 'admin', '2019-05-19 18:30:33', null, null);
INSERT INTO `sys_log` VALUES ('57264fff74c4f857bddf5d766951f3c9', '2', '添加测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"createBy\":\"admin\",\"createTime\":1558262155067,\"id\":\"dcb45a2fc661e5cdc341b806e5914873\",\"name\":\"111\",\"sysOrgCode\":\"A01\"}]', null, '5', 'admin', '2019-05-19 18:35:55', null, null);
INSERT INTO `sys_log` VALUES ('eef5b90eea8e7394193443cfd7476529', '2', '删除测试DEMO', null, 'admin', '管理员', '127.0.0.1', 'org.jeecg.modules.demo.test.controller.JeecgDemoController.delete()', null, '[\"dcb45a2fc661e5cdc341b806e5914873\"]', null, '9', 'admin', '2019-05-19 18:36:02', null, null);
INSERT INTO `sys_log` VALUES ('488fc8f3d040fa75c6802898ea88f7d6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-20 11:38:38', null, null);
INSERT INTO `sys_log` VALUES ('6c99cfe2774c15ad030b83723f81d70d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-20 14:07:25', null, null);
INSERT INTO `sys_log` VALUES ('1bf5c5603b79f749d4ee75965b3698db', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-20 14:54:39', null, null);
INSERT INTO `sys_log` VALUES ('5bca377b50c362009738d612cac82006', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-20 14:54:38', null, null);
INSERT INTO `sys_log` VALUES ('2255d6f5e2a3d0839b8b9cfc67816c5c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-05-20 15:01:51', null, null);
INSERT INTO `sys_log` VALUES ('c7384ed6a6b09ff6704a6b1c1e378cee', '1', '用户名: 管理员,退出成功！', null, 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-07-05 14:45:30', null, null);
INSERT INTO `sys_log` VALUES ('63c998d68b4d0d1529d86b4c0628e072', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-07-05 14:45:40', null, null);
INSERT INTO `sys_log` VALUES ('b096a9e76395f1a52d8c260c4eb811fd', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 18:51:24', null, null);
INSERT INTO `sys_log` VALUES ('da7d2236f6f9e0f61897e5ea9b968d4d', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 20:02:07', null, null);
INSERT INTO `sys_log` VALUES ('6ee846271a3d4b9e576b5a0749c49d12', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 20:02:16', null, null);
INSERT INTO `sys_log` VALUES ('f6b0f562257bf02c983b9e3998ff864e', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 22:43:40', null, null);
INSERT INTO `sys_log` VALUES ('9e1da5d8758f2681543971ee43ee14e2', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 22:43:48', null, null);
INSERT INTO `sys_log` VALUES ('dd9b2cfffb798a22d90dada4fdbbbc61', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 23:39:56', null, null);
INSERT INTO `sys_log` VALUES ('b99f5b6975350d86db4c5dd91de9f608', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-08-23 23:40:05', null, null);
INSERT INTO `sys_log` VALUES ('e8eb946a1a2321a03b39e3fa5b172f83', '1', '用户登录失败，用户不存在！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:19:32', null, null);
INSERT INTO `sys_log` VALUES ('a1356d0a9d50e1b4ce05a149ba8a19d9', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:20:09', null, null);
INSERT INTO `sys_log` VALUES ('61aaf258a45f4f9f24ff479060188f48', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:22:18', null, null);
INSERT INTO `sys_log` VALUES ('143a9f82b9261239974ccbc6e8d74d00', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:22:34', null, null);
INSERT INTO `sys_log` VALUES ('55dede44b6c159e2675a20874a1e6e13', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:56:25', null, null);
INSERT INTO `sys_log` VALUES ('931f327eb112479389933a54ec2cc5d1', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:56:44', null, null);
INSERT INTO `sys_log` VALUES ('4ba8522eb527bb109a0d64f3b63f2c8a', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:57:06', null, null);
INSERT INTO `sys_log` VALUES ('503233e8df7378642c034edb9e67848b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:57:33', null, null);
INSERT INTO `sys_log` VALUES ('511bb541efac4f8f7cf9b36c0ad9b1d1', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:57:55', null, null);
INSERT INTO `sys_log` VALUES ('a90b137b6f0018dfdc7a6c3652872456', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:58:10', null, null);
INSERT INTO `sys_log` VALUES ('0f6ead986a51d222e18b63fbcdea654c', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 11:59:26', null, null);
INSERT INTO `sys_log` VALUES ('181cb01f33fdb546240abf8ec586bfe3', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 12:00:27', null, null);
INSERT INTO `sys_log` VALUES ('a1c17267f85542986ecc46f648cdd301', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 10:51:41', null, null);
INSERT INTO `sys_log` VALUES ('e115621dafcba841342db13060fe87cd', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 14:22:08', null, null);
INSERT INTO `sys_log` VALUES ('6c8df2f796e5dff69953fee4c6692787', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 14:22:28', null, null);
INSERT INTO `sys_log` VALUES ('7027112f73c63f5575d1b599264a0743', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 14:23:08', null, null);
INSERT INTO `sys_log` VALUES ('fe1ed76b5e6e715b0d781da57691d7be', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 14:23:21', null, null);
INSERT INTO `sys_log` VALUES ('e812b281f3e882306ba462344e84ca5e', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 15:13:52', null, null);
INSERT INTO `sys_log` VALUES ('ecee9cb956bafd49d624f58bf37df702', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 15:14:13', null, null);
INSERT INTO `sys_log` VALUES ('98fb4985378519edef9656ed1e905777', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 15:21:04', null, null);
INSERT INTO `sys_log` VALUES ('70092b911de6d16bc450daa4fbc18423', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 15:21:22', null, null);
INSERT INTO `sys_log` VALUES ('2bb26c5e0e38eb9c86add2ba1b0c16c9', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 17:04:05', null, null);
INSERT INTO `sys_log` VALUES ('acc03fd8988e646ec1580425a8b49218', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-04 17:04:25', null, null);
INSERT INTO `sys_log` VALUES ('12bd02f56eab640cf60b735ed350e68e', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 06:14:00', null, null);
INSERT INTO `sys_log` VALUES ('308845dd6fe3d02afd8b18a22a58827c', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 06:14:15', null, null);
INSERT INTO `sys_log` VALUES ('ad533788baa7744478a0023b6cf89363', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 09:24:07', null, null);
INSERT INTO `sys_log` VALUES ('0d9f09dd4459ae75f473341de3a59bbc', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 09:24:23', null, null);
INSERT INTO `sys_log` VALUES ('20a3b31dbb894a4f6a782fbc8eb55b1c', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:08:22', null, null);
INSERT INTO `sys_log` VALUES ('25f3bbee8d64daffb61ae478df830bba', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:11:17', null, null);
INSERT INTO `sys_log` VALUES ('2cca5dd10de9de7c9f78293774144fac', '2', '编辑用户，id： a75d45a015c44384a04449ee80dc3503', '2', 'jeecg', 'jeecg', '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:13:52', null, null);
INSERT INTO `sys_log` VALUES ('2351c1b9390e8f2a1533e96918b05453', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:20:46', null, null);
INSERT INTO `sys_log` VALUES ('37452c65264fafe2e382dac51e92af05', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:21:00', null, null);
INSERT INTO `sys_log` VALUES ('fef8316139e8da087fe0986062751a86', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:24:30', null, null);
INSERT INTO `sys_log` VALUES ('b4e42dba990c12c91b9766d8babc8c5a', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:24:47', null, null);
INSERT INTO `sys_log` VALUES ('8707cc4b275598c3cef6f13914d4f0f5', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:25:36', null, null);
INSERT INTO `sys_log` VALUES ('800ef83af019fada63f312fac4502d9d', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 11:25:52', null, null);
INSERT INTO `sys_log` VALUES ('747813d2161e37a4fae19b912ffa870d', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 12:48:50', null, null);
INSERT INTO `sys_log` VALUES ('594b3a5365f1da41574189eb58e017d0', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 12:49:09', null, null);
INSERT INTO `sys_log` VALUES ('da0dc0a02776b613618cd6098eee2fe0', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 12:52:29', null, null);
INSERT INTO `sys_log` VALUES ('d5128095eaad887f7c8e2ba295a62957', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 12:52:41', null, null);
INSERT INTO `sys_log` VALUES ('a177ec633bb871a506d4844fce60f5d0', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 13:05:27', null, null);
INSERT INTO `sys_log` VALUES ('c1c92b73c1c74134cf9e508b599ff5ab', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-05 13:05:44', null, null);
INSERT INTO `sys_log` VALUES ('1b9720ee489d3dacd6d7ffc7e17502f6', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 01:32:29', null, null);
INSERT INTO `sys_log` VALUES ('fc74ddb29ed47b5bf9cdd5c796bdbc82', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 01:32:41', null, null);
INSERT INTO `sys_log` VALUES ('6d80f644496b15a8f5a01dc2bb25f2bd', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 01:33:00', null, null);
INSERT INTO `sys_log` VALUES ('ea0461b7d5aaf55baef285ef3e4d6bc0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 01:33:09', null, null);
INSERT INTO `sys_log` VALUES ('0f2d40e84bcdba32ca24382ff4c780b9', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 02:19:21', null, null);
INSERT INTO `sys_log` VALUES ('1e52a140b80a9bb4f565e44f351e38f1', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 02:35:05', null, null);
INSERT INTO `sys_log` VALUES ('b9742dc9aa448eb721c1944a22be9dce', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 02:35:18', null, null);
INSERT INTO `sys_log` VALUES ('ff704506e05316ee405f050d2b7a8b1b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 06:37:38', null, null);
INSERT INTO `sys_log` VALUES ('10c3fb5802aa07b78092641a364ee20b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 08:40:54', null, null);
INSERT INTO `sys_log` VALUES ('08c22ddd705d05fa1eb9dc7009d3147b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-06 08:49:47', null, null);
INSERT INTO `sys_log` VALUES ('a526114901dfb6f4b2e3ca38538bc772', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 06:37:12', null, null);
INSERT INTO `sys_log` VALUES ('f5d46b8d662aff9483d2d5ce26b1a20b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 06:37:32', null, null);
INSERT INTO `sys_log` VALUES ('4b5f42921e93366244e52e23a36eebd1', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:27:40', null, null);
INSERT INTO `sys_log` VALUES ('6408e7da64eacc1195fbb921a995efa6', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:36:06', null, null);
INSERT INTO `sys_log` VALUES ('2ae05ba5abd3dc770645f50106cb15de', '1', '用户名: jeecg,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:36:31', null, null);
INSERT INTO `sys_log` VALUES ('54a7d404efceb295b6fd893a60efc752', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:36:41', null, null);
INSERT INTO `sys_log` VALUES ('9d7dbd3b507b166968f9e83baad1eeb1', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:37:26', null, null);
INSERT INTO `sys_log` VALUES ('49deb4f1927410a44516d1cf6665535b', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:37:38', null, null);
INSERT INTO `sys_log` VALUES ('e4865d2dd387bc82967e9dbf6806ac02', '1', '用户名: jeecg,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:39:37', null, null);
INSERT INTO `sys_log` VALUES ('86feac9e230c82e8b1b5e33a2e08561a', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:39:56', null, null);
INSERT INTO `sys_log` VALUES ('d890e52910dd766ee907317e7e993087', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:40:23', null, null);
INSERT INTO `sys_log` VALUES ('3ec693c7a87432ff17f418fd5bae2c43', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:40:35', null, null);
INSERT INTO `sys_log` VALUES ('e255000342f2896b49dd5eab188f6bb4', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.54.202', null, null, null, null, null, 'jeecg', '2019-09-07 13:40:47', null, null);
INSERT INTO `sys_log` VALUES ('dd720d139d2a19b5d7e62c73ae798333', '1', '用户名: jeecg,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:43:20', null, null);
INSERT INTO `sys_log` VALUES ('ff45d9cdfd8a23eca8588136cff26739', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 13:43:37', null, null);
INSERT INTO `sys_log` VALUES ('db8126b27ea2d93dd3031fc53ef12433', '1', '用户名: jeecg,退出成功！', null, null, null, '223.72.54.202', null, null, null, null, null, 'jeecg', '2019-09-07 14:29:43', null, null);
INSERT INTO `sys_log` VALUES ('7bc42755320012d3caedd782584b7b8e', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.54.202', null, null, null, null, null, 'jeecg', '2019-09-07 14:29:57', null, null);
INSERT INTO `sys_log` VALUES ('f1da4ecd550ae7d71959c289a1984ebe', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 14:53:21', null, null);
INSERT INTO `sys_log` VALUES ('eebff38e9118a9eb254cd69d7f1da755', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 15:33:13', null, null);
INSERT INTO `sys_log` VALUES ('3808d4485310ad69889685bbf11819d9', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 15:48:40', null, null);
INSERT INTO `sys_log` VALUES ('f4fbaca226b33ced4b635070e14645bc', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 16:31:10', null, null);
INSERT INTO `sys_log` VALUES ('b7b43febb6e72532be51368f116f4a77', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 16:32:03', null, null);
INSERT INTO `sys_log` VALUES ('a9a5fcae428f7db131257ce99d772b78', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 16:33:49', null, null);
INSERT INTO `sys_log` VALUES ('e357f9db90248e49b785654ba07f057f', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 17:14:17', null, null);
INSERT INTO `sys_log` VALUES ('3c878701727e014c32ec9bc01f3625cb', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 17:15:06', null, null);
INSERT INTO `sys_log` VALUES ('12271270cbf3aac0f4b7c6891366144e', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 19:33:08', null, null);
INSERT INTO `sys_log` VALUES ('e280f69846d25e18ecf770b8e00d1cd8', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 19:33:28', null, null);
INSERT INTO `sys_log` VALUES ('ea471f14b8d7bbbe659642db3adf5d00', '1', '用户名: jeecg,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 19:33:39', null, null);
INSERT INTO `sys_log` VALUES ('9a179d0ca8fcd44c08e07c14e59ff821', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:40:44', null, null);
INSERT INTO `sys_log` VALUES ('d975388275bfd3106ea095878ed515cd', '1', '用户名: jeecg,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:47:29', null, null);
INSERT INTO `sys_log` VALUES ('7ce906d9b4a3beb8f42677fecfd5c1d2', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:49:08', null, null);
INSERT INTO `sys_log` VALUES ('3d3db372f6576f506373a6bda78edd8f', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:49:30', null, null);
INSERT INTO `sys_log` VALUES ('7b05b97d8a6fa6a399caca8934138d14', '2', '编辑用户，id： 662d318c3f74b1714d8af5e11f270bb3', '2', 'admin', '管理员', '127.0.0.1', null, null, null, null, null, 'admin', '2019-09-07 19:51:19', null, null);
INSERT INTO `sys_log` VALUES ('86e3a78082b25277a4a35f35bc9a34d6', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:51:31', null, null);
INSERT INTO `sys_log` VALUES ('3b4ce01959de46ca771f6f7cc0da1b8c', '1', '用户名: test,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:51:54', null, null);
INSERT INTO `sys_log` VALUES ('2878d72602f4c47239f65a8c4654cf6c', '1', '用户名: 游客,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:52:10', null, null);
INSERT INTO `sys_log` VALUES ('4f6d543fc8bafdc8aa451dbdbc65e99a', '1', '用户名: test,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:55:01', null, null);
INSERT INTO `sys_log` VALUES ('2026162904ce719c680cfbf2afe5b8b0', '1', '用户名: 游客,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 19:58:01', null, null);
INSERT INTO `sys_log` VALUES ('3c4e810a95be4b65773d83d0d4223c95', '1', '用户名: jeecg,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 20:02:17', null, null);
INSERT INTO `sys_log` VALUES ('5b2f9d2fba35de3aca6d497e3b592f28', '1', '用户名: test,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 20:02:41', null, null);
INSERT INTO `sys_log` VALUES ('87f5cc4d1d46a73ba0542feee76aab83', '1', '用户名: 游客,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 20:03:00', null, null);
INSERT INTO `sys_log` VALUES ('4a2aa38f4ccabfcdaa2ae66fa4c4d6fa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:14:30', null, null);
INSERT INTO `sys_log` VALUES ('dc0f5059367caaf503b93f26a35a6cf0', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:14:46', null, null);
INSERT INTO `sys_log` VALUES ('299f8264e7e979a2a614e049f08210c3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:51:32', null, null);
INSERT INTO `sys_log` VALUES ('3f80a7534939727d9cf1ef2e44eeca96', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:51:37', null, null);
INSERT INTO `sys_log` VALUES ('2c650fd7961747acca4c6a2720dc4ec0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:54:05', null, null);
INSERT INTO `sys_log` VALUES ('0257045285792499fc09f5fb8b9d88fd', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:54:13', null, null);
INSERT INTO `sys_log` VALUES ('d66863e59b941a6f0950196039f9f782', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:54:42', null, null);
INSERT INTO `sys_log` VALUES ('2280bee331a607d2e597a79d6943e67d', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-07 20:54:47', null, null);
INSERT INTO `sys_log` VALUES ('bd1d9ffd38667607661ead1c9f1961e0', '1', '用户名: test,登录成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 21:00:10', null, null);
INSERT INTO `sys_log` VALUES ('37b2aedc1d8b6a1fc73623baa9776b1b', '1', '用户名: 游客,退出成功！', null, null, null, '223.72.44.15', null, null, null, null, null, 'jeecg', '2019-09-07 21:00:27', null, null);
INSERT INTO `sys_log` VALUES ('654a5eee7c655c3fa29e9b6b09d24e69', '1', '用户名: jeecg,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-08 05:57:25', null, null);
INSERT INTO `sys_log` VALUES ('a280b05617a209471ede0dffdb32951a', '1', '用户名: test,登录成功！', null, null, null, '1.203.65.106', null, null, null, null, null, 'jeecg', '2019-09-08 11:38:22', null, null);
INSERT INTO `sys_log` VALUES ('514bb278e13a6b57690faafad607edd4', '1', '用户名: 游客,退出成功！', null, null, null, '1.203.65.106', null, null, null, null, null, 'jeecg', '2019-09-08 11:40:50', null, null);
INSERT INTO `sys_log` VALUES ('708143734a164d76709f880819bde3d2', '1', '用户名: abc,登录成功！', null, null, null, '1.203.65.106', null, null, null, null, null, 'jeecg', '2019-09-08 11:41:01', null, null);
INSERT INTO `sys_log` VALUES ('d5227b7176c18805f68989ad0d140d1a', '1', '用户名: abc,退出成功！', null, null, null, '1.203.65.106', null, null, null, null, null, 'jeecg', '2019-09-08 11:43:48', null, null);
INSERT INTO `sys_log` VALUES ('35140a27e50b7118b816d5ea45747272', '1', '用户名: test,登录成功！', null, null, null, '1.203.65.106', null, null, null, null, null, 'jeecg', '2019-09-08 11:44:02', null, null);
INSERT INTO `sys_log` VALUES ('eba22e791478f30f49649e0120a0ca50', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 01:39:14', null, null);
INSERT INTO `sys_log` VALUES ('09ca161cff3288773c8bf6fb077f8ea2', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 01:41:54', null, null);
INSERT INTO `sys_log` VALUES ('089046237ed81b6b236c1637e78788a0', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 02:33:00', null, null);
INSERT INTO `sys_log` VALUES ('1e308aaf4cfe3f6467c70e55ea677072', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 02:44:00', null, null);
INSERT INTO `sys_log` VALUES ('dbd8dd0eab2996cad96e94e1e5aad320', '1', '用户名: admin,登录成功！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-09-09 03:03:19', null, null);
INSERT INTO `sys_log` VALUES ('e555ba5aaa096214673c620769a52b51', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 03:10:00', null, null);
INSERT INTO `sys_log` VALUES ('08d2cb275868d19eb1ec4f6537caecb6', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 03:10:13', null, null);
INSERT INTO `sys_log` VALUES ('ed0f1cf6a1e956970e234e567ff338b4', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 03:22:45', null, null);
INSERT INTO `sys_log` VALUES ('a544c4154c7ea274b6e44f18276d9c1f', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 03:23:00', null, null);
INSERT INTO `sys_log` VALUES ('3c4a0599fbeecc3a00282753e181376c', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 03:26:38', null, null);
INSERT INTO `sys_log` VALUES ('e0ee04cd220a21edf059f30a11f0a75b', '1', '用户名: 游客,退出成功！', null, null, null, '223.104.3.63', null, null, null, null, null, 'jeecg', '2019-09-09 03:56:17', null, null);
INSERT INTO `sys_log` VALUES ('56c402e54f941e02cf4cce64980fa86d', '1', '用户名: test,登录成功！', null, null, null, '223.104.3.63', null, null, null, null, null, 'jeecg', '2019-09-09 03:56:55', null, null);
INSERT INTO `sys_log` VALUES ('073f5b0bec261236c1d040bc1ad25d9c', '1', '用户名: test,登录成功！', null, null, null, '223.104.3.37', null, null, null, null, null, 'jeecg', '2019-09-09 04:12:50', null, null);
INSERT INTO `sys_log` VALUES ('f0835a23e1ae61006b7b267fd1c10674', '1', '用户名: 游客,退出成功！', null, null, null, '223.104.3.37', null, null, null, null, null, 'jeecg', '2019-09-09 04:13:49', null, null);
INSERT INTO `sys_log` VALUES ('6373641c976fa61aac693681c503708c', '1', '用户名: admin,登录成功！', null, null, null, '223.104.3.37', null, null, null, null, null, 'jeecg', '2019-09-09 04:14:48', null, null);
INSERT INTO `sys_log` VALUES ('aafda0631b0644424a446e4f433dd6fa', '1', '用户名: 管理员,退出成功！', null, null, null, '223.104.3.37', null, null, null, null, null, 'jeecg', '2019-09-09 04:15:26', null, null);
INSERT INTO `sys_log` VALUES ('bf43f49fa5effb63b2f47279d26bd657', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:19:22', null, null);
INSERT INTO `sys_log` VALUES ('4c501c2fff7d9e0a68561c798acc7172', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:25:26', null, null);
INSERT INTO `sys_log` VALUES ('a4aeb9adddc9756aba9b21386a8d64af', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:25:32', null, null);
INSERT INTO `sys_log` VALUES ('ae0565fed5c156029246ba95274838c3', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:26:48', null, null);
INSERT INTO `sys_log` VALUES ('25c11d6ae55dc0a4795ae28e62b7e071', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:27:06', null, null);
INSERT INTO `sys_log` VALUES ('6a0b30c62ec89c188a5a5cb272c96242', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 04:28:05', null, null);
INSERT INTO `sys_log` VALUES ('055116f7f976ad472865d9e1c9f5b784', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 05:23:18', null, null);
INSERT INTO `sys_log` VALUES ('f311973d8ab0909f486d595a69493b54', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 05:27:30', null, null);
INSERT INTO `sys_log` VALUES ('d660bcb540d2a71709cb1859517fe464', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 05:34:10', null, null);
INSERT INTO `sys_log` VALUES ('b7c70e8a9a22ad7ae624d333ea4578af', '1', '用户名: test,登录成功！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 06:05:33', null, null);
INSERT INTO `sys_log` VALUES ('165baf835134736025b0ae5828f1163a', '1', '用户名: 游客,退出成功！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 06:05:39', null, null);
INSERT INTO `sys_log` VALUES ('2608b33a3920285c6de391183f9655b6', '1', '用户名: abc,登录成功！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 06:08:27', null, null);
INSERT INTO `sys_log` VALUES ('8784673e6965506e64755de3930760c7', '1', '用户名: abc,退出成功！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 06:08:33', null, null);
INSERT INTO `sys_log` VALUES ('caad6859687141a5b3188e5d87d4123f', '1', '用户登录失败，用户不存在！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 06:26:55', null, null);
INSERT INTO `sys_log` VALUES ('5701bf2f508ffc9324aec9b94b75efec', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 07:33:08', null, null);
INSERT INTO `sys_log` VALUES ('7b1fd1b22eb1c1481957952cab0e75aa', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 09:03:29', null, null);
INSERT INTO `sys_log` VALUES ('7474a5c2dcd1bead344d44245abf8453', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 09:03:40', null, null);
INSERT INTO `sys_log` VALUES ('dabc535f1d39cf5aee23238410b03ed8', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 10:54:58', null, null);
INSERT INTO `sys_log` VALUES ('c32a401c39d4c88057782016a92bd1e0', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 10:55:09', null, null);
INSERT INTO `sys_log` VALUES ('a7e7a1a28084cec7a5d0691ad1fa835b', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 10:59:13', null, null);
INSERT INTO `sys_log` VALUES ('9dfe22536c57bbfeaa01f49a977d1698', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 11:33:20', null, null);
INSERT INTO `sys_log` VALUES ('519db0e59d806add9938616857f003ce', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 11:37:35', null, null);
INSERT INTO `sys_log` VALUES ('dde7bb4cbe01e4cc5ab7eed6bbb0c597', '1', '用户名: test,登录成功！', null, null, null, '106.121.7.135', null, null, null, null, null, 'jeecg', '2019-09-09 11:59:06', null, null);
INSERT INTO `sys_log` VALUES ('523070df0537193c41502e6567dabba7', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 12:07:16', null, null);
INSERT INTO `sys_log` VALUES ('27e9573a8ee011ded53fb96b6a3bcdb0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 12:07:31', null, null);
INSERT INTO `sys_log` VALUES ('3f428ac57ec615ff838c8ca76099d2aa', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 12:12:41', null, null);
INSERT INTO `sys_log` VALUES ('b788ae7ff1a464ed1c8cf317a09f9d41', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 12:32:48', null, null);
INSERT INTO `sys_log` VALUES ('5888d4ab9b65051516099f7a744b7bd4', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-09 12:32:55', null, null);
INSERT INTO `sys_log` VALUES ('2c2af4c18e6c528d0a5cccf0d2affbd6', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-09 12:42:18', null, null);
INSERT INTO `sys_log` VALUES ('093223b8c623893e61ae91fc550deea7', '1', '用户名: 管理员,退出成功！', null, null, null, '117.136.38.183', null, null, null, null, null, 'jeecg', '2019-09-09 14:24:08', null, null);
INSERT INTO `sys_log` VALUES ('c9fe6afb06fbd3141e83c11e965874f8', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-10 08:13:29', null, null);
INSERT INTO `sys_log` VALUES ('bd32439a21a8975ad058e70e9735a678', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-10 08:13:52', null, null);
INSERT INTO `sys_log` VALUES ('ad4f70701f803e414f913e6b2042eb25', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 08:35:11', null, null);
INSERT INTO `sys_log` VALUES ('27687b64d4a843b6386a01606cab7585', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 08:35:19', null, null);
INSERT INTO `sys_log` VALUES ('bf80a09b898d50789d63250b5b7322f3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-10 09:08:09', null, null);
INSERT INTO `sys_log` VALUES ('680d42dc7a9c75df312b6a064b1f43d8', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 10:01:56', null, null);
INSERT INTO `sys_log` VALUES ('d5b77dcbe159899d15eb09fbfa4789e2', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 10:10:46', null, null);
INSERT INTO `sys_log` VALUES ('460868c8d73f3c4ca68e29be39eb91a5', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 10:10:55', null, null);
INSERT INTO `sys_log` VALUES ('1a69c61a24570dfd0ba957a5564e4649', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 10:16:28', null, null);
INSERT INTO `sys_log` VALUES ('632f9f6e698a67309d072e40626ccb30', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-10 10:36:55', null, null);
INSERT INTO `sys_log` VALUES ('35af94e530424789484682c6528e152b', '1', '用户名: test,登录成功！', null, null, null, '106.121.9.242', null, null, null, null, null, 'jeecg', '2019-09-10 15:05:46', null, null);
INSERT INTO `sys_log` VALUES ('17262e44cfca2f5c763d06332f1b15eb', '2', '编辑用户，id： 662d318c3f74b1714d8af5e11f270bb3', '2', 'test', '游客', '106.121.9.242', null, null, null, null, null, 'test', '2019-09-10 15:06:18', null, null);
INSERT INTO `sys_log` VALUES ('2add768c5805b8adc0c5c512c6d341f6', '1', '用户名: jeecg,退出成功！', null, null, null, '106.121.9.242', null, null, null, null, null, 'jeecg', '2019-09-10 15:35:57', null, null);
INSERT INTO `sys_log` VALUES ('fa35a45d9e80c741cc319aa06db565be', '1', '用户名: 游客,退出成功！', null, null, null, '106.121.9.242', null, null, null, null, null, 'jeecg', '2019-09-10 15:41:51', null, null);
INSERT INTO `sys_log` VALUES ('38bb6efe8e7fb770e407c0ddec609d9d', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-11 01:07:52', null, null);
INSERT INTO `sys_log` VALUES ('8f4a9d669b5de974839237c999d525e6', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-11 02:39:20', null, null);
INSERT INTO `sys_log` VALUES ('75133076a4224245398e83735d53e4ae', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-11 08:19:27', null, null);
INSERT INTO `sys_log` VALUES ('64aeec3f1b7d01751ec8abc6305f9066', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-11 08:19:03', null, null);
INSERT INTO `sys_log` VALUES ('cf833df94268491f74be8136d8a9d2a3', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-11 08:23:47', null, null);
INSERT INTO `sys_log` VALUES ('d79db4c330408bc40f7c85c727934524', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '344', 'admin', '2019-09-11 08:44:11', null, null);
INSERT INTO `sys_log` VALUES ('37f2eb70e8a2970b6fb6a40ea5c44705', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '378', 'admin', '2019-09-11 09:36:08', null, null);
INSERT INTO `sys_log` VALUES ('037dd5495a3e7032d858a0e6a62e2b46', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '54', 'admin', '2019-09-11 09:37:29', null, null);
INSERT INTO `sys_log` VALUES ('18cd5eae4159edebf53e1d87102ba3b6', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '71', 'admin', '2019-09-11 09:37:33', null, null);
INSERT INTO `sys_log` VALUES ('a9eef22101416574ee760c12e19deb94', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '63', 'admin', '2019-09-11 09:37:48', null, null);
INSERT INTO `sys_log` VALUES ('1e4ed12db6a44173a388c2ada8429845', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-11 10:24:23', null, null);
INSERT INTO `sys_log` VALUES ('47c14af881164d3d7d100c35ec8bcd43', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '195', 'admin', '2019-09-11 10:24:37', null, null);
INSERT INTO `sys_log` VALUES ('4ae16b58d3592a331dcb656bafe75cbd', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '15', 'admin', '2019-09-11 10:24:46', null, null);
INSERT INTO `sys_log` VALUES ('246005c9cc43290e6a1eb90a7497fc7e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'admin', '2019-09-11 10:25:02', null, null);
INSERT INTO `sys_log` VALUES ('9fa6d3e7144897a256fbeb7c8648dd71', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '52', 'admin', '2019-09-11 10:25:33', null, null);
INSERT INTO `sys_log` VALUES ('a8662cdf82e4e5bc0c88a7312a9d8ebb', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '15', 'admin', '2019-09-11 10:25:59', null, null);
INSERT INTO `sys_log` VALUES ('eb7b63ba9a0c66a646a0b7f2a659f5c5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '15', 'admin', '2019-09-11 10:26:04', null, null);
INSERT INTO `sys_log` VALUES ('c64de7692c230c886ad5ccec20196c43', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '71', 'admin', '2019-09-11 10:26:39', null, null);
INSERT INTO `sys_log` VALUES ('5a210ec25b4a338ca4af7168f287e3c4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '45', 'admin', '2019-09-11 10:26:49', null, null);
INSERT INTO `sys_log` VALUES ('d64713fdbb50cfc91b90734c5235163b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '16', 'admin', '2019-09-11 10:27:00', null, null);
INSERT INTO `sys_log` VALUES ('cb4f5702e694a5907340ea623298f89b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '121', 'admin', '2019-09-11 10:32:23', null, null);
INSERT INTO `sys_log` VALUES ('c3a4b511e356d27ab5060ec7c3bea962', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '83', 'admin', '2019-09-11 10:32:31', null, null);
INSERT INTO `sys_log` VALUES ('059094fbc860fb7d873a195ceb190709', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '163', 'admin', '2019-09-11 10:38:56', null, null);
INSERT INTO `sys_log` VALUES ('b9a0103359b8ceb5e9ca5a49f6158a27', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '314', 'admin', '2019-09-11 10:39:06', null, null);
INSERT INTO `sys_log` VALUES ('ccd92ec2440caf54813e473dc92526b0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-11 10:43:45', null, null);
INSERT INTO `sys_log` VALUES ('3e32d857c0f693ae80607cfd15bbc70f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'admin', '2019-09-11 10:43:45', null, null);
INSERT INTO `sys_log` VALUES ('f07ad8b95660c7c803b9f2d56644c78f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '18', 'admin', '2019-09-11 10:43:52', null, null);
INSERT INTO `sys_log` VALUES ('3fefd1467d28cfdd87a6fab5bd918236', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-11 10:43:57', null, null);
INSERT INTO `sys_log` VALUES ('ba980c92b02e8a198b7cd28ac14e148e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '33', 'admin', '2019-09-11 10:44:09', null, null);
INSERT INTO `sys_log` VALUES ('ce915b65e5b3c941054417db3450d945', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-11 11:24:24', null, null);
INSERT INTO `sys_log` VALUES ('de4f8f318ef395e230c52274d51e652f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '88', 'admin', '2019-09-11 11:24:53', null, null);
INSERT INTO `sys_log` VALUES ('276ad918be2187da34daa6bc9e06c1ba', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '61', 'admin', '2019-09-11 11:24:59', null, null);
INSERT INTO `sys_log` VALUES ('cb52e7881ccfbe3b0c30171076a14648', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-11 11:32:56', null, null);
INSERT INTO `sys_log` VALUES ('bbe7d3ed4ea1431eeff659176d7ab327', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-11 11:33:03', null, null);
INSERT INTO `sys_log` VALUES ('542055afd7a461ebc5c015e66e82d31f', '1', '用户名: 管理员,退出成功！', null, null, null, '117.136.38.159', null, null, null, null, null, 'jeecg', '2019-09-11 12:03:17', null, null);
INSERT INTO `sys_log` VALUES ('1facd3e0088ff9fccf60b1ad5db64e89', '1', '用户名: test,登录成功！', null, null, null, '117.136.38.159', null, null, null, null, null, 'jeecg', '2019-09-11 12:04:06', null, null);
INSERT INTO `sys_log` VALUES ('65e6de54b31f725877a7b5c3138c9919', '1', '用户名: 游客,退出成功！', null, null, null, '117.136.38.159', null, null, null, null, null, 'jeecg', '2019-09-11 12:04:50', null, null);
INSERT INTO `sys_log` VALUES ('0ca75ff21bd81cde01bb18017bebf4ee', '1', '用户名: admin,登录成功！', null, null, null, '117.136.38.159', null, null, null, null, null, 'jeecg', '2019-09-11 12:05:55', null, null);
INSERT INTO `sys_log` VALUES ('277541a325727598a08244b3fc1f3048', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-12 01:25:35', null, null);
INSERT INTO `sys_log` VALUES ('5392f0cf7625ff74d971412006b66f27', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-12 01:25:46', null, null);
INSERT INTO `sys_log` VALUES ('19b152682cab3eabcaf3e3120fc9d5e6', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 01:33:07', null, null);
INSERT INTO `sys_log` VALUES ('0b4a443524acb85ff267d08f68cdb282', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '87', 'admin', '2019-09-12 02:49:03', null, null);
INSERT INTO `sys_log` VALUES ('2622ee9f7ed55b322e36fd0b12419380', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '61', 'admin', '2019-09-12 02:50:25', null, null);
INSERT INTO `sys_log` VALUES ('99f2cedec35e0f065b6fa43264cb969c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '51', 'admin', '2019-09-12 02:51:17', null, null);
INSERT INTO `sys_log` VALUES ('93fd0574c3d586aed2bf79c74e1ed37f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '44', 'admin', '2019-09-12 02:52:08', null, null);
INSERT INTO `sys_log` VALUES ('9dcf50df3b20e7da3135f04ad5430669', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '46', 'admin', '2019-09-12 02:52:14', null, null);
INSERT INTO `sys_log` VALUES ('3b26ba0a74ec426f769b482c840ec466', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 02:52:35', null, null);
INSERT INTO `sys_log` VALUES ('3e494da900007d1f89cac59afc401c58', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '44', 'admin', '2019-09-12 02:53:07', null, null);
INSERT INTO `sys_log` VALUES ('321ade1e2246fe854115baf3c5f7e1ef', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '53', 'admin', '2019-09-12 02:54:00', null, null);
INSERT INTO `sys_log` VALUES ('9df3fde5e9746c3c9a70fd1bcc3bcbf4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 02:55:39', null, null);
INSERT INTO `sys_log` VALUES ('e92f9ff8c1c8005231e142fa36c7c428', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '53', 'admin', '2019-09-12 02:56:49', null, null);
INSERT INTO `sys_log` VALUES ('f638936cb7199bf37dfdb5c4dc1d188c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '43', 'admin', '2019-09-12 02:57:21', null, null);
INSERT INTO `sys_log` VALUES ('615cec54e6f2e6c77989d55f8ce1f5c7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 02:58:11', null, null);
INSERT INTO `sys_log` VALUES ('d5069b8a78141ce3ae814114ddf99939', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 02:59:03', null, null);
INSERT INTO `sys_log` VALUES ('0e3a8b0ff921d28aad4d6591c89319a1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 03:00:29', null, null);
INSERT INTO `sys_log` VALUES ('e802d9a4c5992232152335a3a6d5353a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '46', 'admin', '2019-09-12 03:04:23', null, null);
INSERT INTO `sys_log` VALUES ('1d6a6287ba213b417cfe0b25ead66dc7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '55', 'admin', '2019-09-12 03:15:19', null, null);
INSERT INTO `sys_log` VALUES ('ecd1588062beab48a852be9b18bf93e9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 03:15:25', null, null);
INSERT INTO `sys_log` VALUES ('2713bf17c46f0824aa8aa978443ded41', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '43', 'admin', '2019-09-12 03:15:46', null, null);
INSERT INTO `sys_log` VALUES ('86ed052b53b9041d90e005a462dc2128', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '13', 'admin', '2019-09-12 03:16:14', null, null);
INSERT INTO `sys_log` VALUES ('4cc5c69066f7536b1a6ff6cc7b139ad2', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 03:16:18', null, null);
INSERT INTO `sys_log` VALUES ('e054866a92c33dcdb7aaa1d9d407a963', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '16', 'admin', '2019-09-12 03:16:24', null, null);
INSERT INTO `sys_log` VALUES ('f2c51510a7273f3e1dec525f54e71d67', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 03:16:27', null, null);
INSERT INTO `sys_log` VALUES ('6f17d3f5107f9d9edd61a5a5b8ccd9e9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 03:16:31', null, null);
INSERT INTO `sys_log` VALUES ('cd410429bae3be6fd1bc4d0f58a0c64d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '48', 'admin', '2019-09-12 03:16:35', null, null);
INSERT INTO `sys_log` VALUES ('5f76647682a043c817bc084d94d58e6b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '49', 'admin', '2019-09-12 03:17:51', null, null);
INSERT INTO `sys_log` VALUES ('0fdc28c1e088421c679980bb08db4b3f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '45', 'admin', '2019-09-12 03:18:08', null, null);
INSERT INTO `sys_log` VALUES ('df298a092761161053feeb37989d04fd', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '49', 'admin', '2019-09-12 03:18:22', null, null);
INSERT INTO `sys_log` VALUES ('b546af9b2d8194fdf73ee5e5917c2451', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '46', 'admin', '2019-09-12 03:18:26', null, null);
INSERT INTO `sys_log` VALUES ('01163bdd7192f582ee8e8a43ed6a1201', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '44', 'admin', '2019-09-12 03:21:13', null, null);
INSERT INTO `sys_log` VALUES ('a0cf049b2606b9eac5cbacad3f505a8d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 03:22:07', null, null);
INSERT INTO `sys_log` VALUES ('b293082c51dd051bae352c97bd194bee', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '87', 'admin', '2019-09-12 03:22:07', null, null);
INSERT INTO `sys_log` VALUES ('c2513e1132edc8f2fa636fe5999ed514', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '68', 'admin', '2019-09-12 03:22:08', null, null);
INSERT INTO `sys_log` VALUES ('0834b95192c626f5ddbe7436f80e6098', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '50', 'admin', '2019-09-12 03:22:24', null, null);
INSERT INTO `sys_log` VALUES ('847174463e44fba3a3b6fafebe86ffe6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '33', 'admin', '2019-09-12 03:22:24', null, null);
INSERT INTO `sys_log` VALUES ('bc3e96d54ba0fb338f0437ce9d04730a', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 03:22:24', null, null);
INSERT INTO `sys_log` VALUES ('9e574ea28ffda0e34ac953494067b0ba', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '45', 'admin', '2019-09-12 03:30:56', null, null);
INSERT INTO `sys_log` VALUES ('998bda4ee7ce65e3d33af1b035e0641b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '51', 'admin', '2019-09-12 03:30:56', null, null);
INSERT INTO `sys_log` VALUES ('bee07e579a9c9dc7dfa03b5cd3e8850b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '52', 'admin', '2019-09-12 03:30:56', null, null);
INSERT INTO `sys_log` VALUES ('aeade86a3e84c8ea120760a3b11864b7', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 03:31:25', null, null);
INSERT INTO `sys_log` VALUES ('21d717f1a6035924b599d104f1c9577e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '35', 'admin', '2019-09-12 03:31:25', null, null);
INSERT INTO `sys_log` VALUES ('118eb3ac4b1f0fee3b71cecf6482b6cd', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '41', 'admin', '2019-09-12 03:31:25', null, null);
INSERT INTO `sys_log` VALUES ('41b33d1d6394be99b20c4d6c4ed23be9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 03:33:55', null, null);
INSERT INTO `sys_log` VALUES ('196df9ac53da76d36f500cb1d5464794', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 03:33:55', null, null);
INSERT INTO `sys_log` VALUES ('4680056dbc1a93584c8c4d497bb23bd1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '29', 'admin', '2019-09-12 03:33:55', null, null);
INSERT INTO `sys_log` VALUES ('d56a50e8a001747b50397450ff59db8c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 03:35:03', null, null);
INSERT INTO `sys_log` VALUES ('b385a8659a0e497f0bd2a5ef962f8602', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 03:35:03', null, null);
INSERT INTO `sys_log` VALUES ('60c7635bb56fe90cf96519b42bbd08db', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 03:35:03', null, null);
INSERT INTO `sys_log` VALUES ('e9d6143f3018bde2d3b643b378b574bc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:35:51', null, null);
INSERT INTO `sys_log` VALUES ('2c3e669c23ff9fdb49d215625bddcf0c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'admin', '2019-09-12 03:35:51', null, null);
INSERT INTO `sys_log` VALUES ('8d46d9b7f0944055ebdcd5888537d637', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '14', 'admin', '2019-09-12 03:35:51', null, null);
INSERT INTO `sys_log` VALUES ('11590c16bea7e63fb05af24fae98acdd', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '11', 'admin', '2019-09-12 03:35:54', null, null);
INSERT INTO `sys_log` VALUES ('e48a113561f83d14e9492e6265193664', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:35:54', null, null);
INSERT INTO `sys_log` VALUES ('edb6897c7361da8317513f8aa859d839', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 03:35:54', null, null);
INSERT INTO `sys_log` VALUES ('70e82c4fc5f785f270cb3adffb52d562', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 03:36:14', null, null);
INSERT INTO `sys_log` VALUES ('4baf94bcd037b82821cb8a9d9d226183', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 03:36:14', null, null);
INSERT INTO `sys_log` VALUES ('996a0a13cd7f38222b4c9f2706b9d7cf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 03:36:14', null, null);
INSERT INTO `sys_log` VALUES ('e0815fae112c10cdc9d34ec389046f67', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '709', 'admin', '2019-09-12 03:36:19', null, null);
INSERT INTO `sys_log` VALUES ('88272e41c02456d0c7903aa2f7d1ba34', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '728', 'admin', '2019-09-12 03:36:19', null, null);
INSERT INTO `sys_log` VALUES ('df30b5b0cffecd6d378a2dcc135d2305', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '728', 'admin', '2019-09-12 03:36:19', null, null);
INSERT INTO `sys_log` VALUES ('eb372a0d91034baaa16ed1cf75700b68', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 03:36:24', null, null);
INSERT INTO `sys_log` VALUES ('adf8e40df49450057ea4dd83a9db41fb', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 03:36:24', null, null);
INSERT INTO `sys_log` VALUES ('63ae93ae5f86abaa515327f001743910', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 03:36:24', null, null);
INSERT INTO `sys_log` VALUES ('f1a7caafadf756cbd66c780afb7f1798', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '13', 'admin', '2019-09-12 03:36:26', null, null);
INSERT INTO `sys_log` VALUES ('965d752c96d59a7bad6e433e03762822', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '12', 'admin', '2019-09-12 03:36:26', null, null);
INSERT INTO `sys_log` VALUES ('de0f0bba95271addb7c36d0518d2a094', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 03:36:26', null, null);
INSERT INTO `sys_log` VALUES ('e95dcbb7bd85ccadd3d1c76bb22381d2', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 03:36:31', null, null);
INSERT INTO `sys_log` VALUES ('c53d4c3eee3b3070be3ca0ed53ec1506', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 03:36:31', null, null);
INSERT INTO `sys_log` VALUES ('cf6c94705699cc25782089237445fc09', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '51', 'admin', '2019-09-12 03:36:31', null, null);
INSERT INTO `sys_log` VALUES ('03cbf702f3f870760c38a2b591618d5d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:36:37', null, null);
INSERT INTO `sys_log` VALUES ('3fb6ec93f57c46980d13e9e1a16b67c6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:36:37', null, null);
INSERT INTO `sys_log` VALUES ('2a8b1970e334d933976847b3176be231', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:36:37', null, null);
INSERT INTO `sys_log` VALUES ('a6f6dd35b9dae6ea6a913bd9bf9e581e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '32', 'admin', '2019-09-12 03:38:46', null, null);
INSERT INTO `sys_log` VALUES ('143a1321e8db2b90cbe060f22449836f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 03:38:46', null, null);
INSERT INTO `sys_log` VALUES ('59facce99e73ee5d909bc5b5691fff68', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '43', 'admin', '2019-09-12 03:38:46', null, null);
INSERT INTO `sys_log` VALUES ('90cdbc5252cc36a757035227f746ad30', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 03:39:27', null, null);
INSERT INTO `sys_log` VALUES ('2b86493590fe55b6205d661fc5d88e86', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 03:39:27', null, null);
INSERT INTO `sys_log` VALUES ('95bb4eeace6c4ea61e8b05b2f2a570c1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 03:39:27', null, null);
INSERT INTO `sys_log` VALUES ('1cf073354fe6d0889db5712b9508fa0d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 03:39:43', null, null);
INSERT INTO `sys_log` VALUES ('c9903e40018321f9c2515089b5e1e708', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 03:39:43', null, null);
INSERT INTO `sys_log` VALUES ('8830ebba199901304188d7f645d0b85d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 03:39:43', null, null);
INSERT INTO `sys_log` VALUES ('72ea634457bada7830ee8c6f4e4bc246', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '141', 'admin', '2019-09-12 03:40:29', null, null);
INSERT INTO `sys_log` VALUES ('35b3c8f59d18e432e531f34ac0cecfbe', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '142', 'admin', '2019-09-12 03:40:29', null, null);
INSERT INTO `sys_log` VALUES ('3ebd15050ca681bdb4085deedf852f13', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '146', 'admin', '2019-09-12 03:40:29', null, null);
INSERT INTO `sys_log` VALUES ('54fdac36012a6b348827989b212d1d6f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '12', 'admin', '2019-09-12 03:41:06', null, null);
INSERT INTO `sys_log` VALUES ('b5c34f0e74df0009f5bb4c88dac484b8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '12', 'admin', '2019-09-12 03:41:06', null, null);
INSERT INTO `sys_log` VALUES ('372189c9876e4f0d536302211ed61409', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '15', 'admin', '2019-09-12 03:41:06', null, null);
INSERT INTO `sys_log` VALUES ('200f371507e88a2eedaef65423afbdee', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 03:41:20', null, null);
INSERT INTO `sys_log` VALUES ('abba1fe70397780c8a6905cd3a280a5e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 03:41:20', null, null);
INSERT INTO `sys_log` VALUES ('53cf77cd9791aec4e4ee844caad39ef0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 03:41:20', null, null);
INSERT INTO `sys_log` VALUES ('1cb997939e80d7ebd73def9f914dfde3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '10', 'admin', '2019-09-12 03:41:54', null, null);
INSERT INTO `sys_log` VALUES ('786bd4ccff09480ee3af64cc1070cd3c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '11', 'admin', '2019-09-12 03:41:54', null, null);
INSERT INTO `sys_log` VALUES ('368c9df7150386454ed7d31a28739d2a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '13', 'admin', '2019-09-12 03:41:54', null, null);
INSERT INTO `sys_log` VALUES ('d76d4d4fc9dfe045ea4e7d8bdffb3754', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 03:41:57', null, null);
INSERT INTO `sys_log` VALUES ('7adf1759d3026276fcb58813e451764e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 03:41:57', null, null);
INSERT INTO `sys_log` VALUES ('1c6e3a256808c23acc72daf715660e3e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 03:41:57', null, null);
INSERT INTO `sys_log` VALUES ('b6f91b52f32a3b2df3dbec9e8d69dbda', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 03:46:01', null, null);
INSERT INTO `sys_log` VALUES ('32854ac2fd9e5d01e9103887888b323c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 03:46:01', null, null);
INSERT INTO `sys_log` VALUES ('4719aca6f0dfde7580d5040e2673d408', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 03:46:01', null, null);
INSERT INTO `sys_log` VALUES ('9d6680e3944043247bbe77bdd24f24ee', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 03:46:11', null, null);
INSERT INTO `sys_log` VALUES ('74261e3eaf4ee8c5941663869a921d01', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 03:46:11', null, null);
INSERT INTO `sys_log` VALUES ('3bd14db4b3c7caeb7664ec10b95be8b5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 03:46:11', null, null);
INSERT INTO `sys_log` VALUES ('978a15b1974295f45d1dec2413b6fd7c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 04:00:01', null, null);
INSERT INTO `sys_log` VALUES ('e2c77b9eee1aa030d01522a3b20985c1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:00:01', null, null);
INSERT INTO `sys_log` VALUES ('d1b3871a9435b95ee2fadd40957d0f83', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:00:01', null, null);
INSERT INTO `sys_log` VALUES ('b4769a5fdbbce9f478a9f25df7eefcb5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:00:05', null, null);
INSERT INTO `sys_log` VALUES ('a2a113fe15883e51ba57ff6a7f93b5e0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:00:05', null, null);
INSERT INTO `sys_log` VALUES ('2ad01f7e21c60cc93c0f1858d7686bbe', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 04:00:05', null, null);
INSERT INTO `sys_log` VALUES ('ce55d52ad4ef50bcbf89090fc5474600', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:12:02', null, null);
INSERT INTO `sys_log` VALUES ('a2c0df217331a2e6f4a9ca3e8f365b72', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:12:02', null, null);
INSERT INTO `sys_log` VALUES ('a8930f5f554efc31244550b3332222c1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:12:02', null, null);
INSERT INTO `sys_log` VALUES ('aefac520fc07f89e910e9f9d69a7d98e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('7d883857a26ca89993d7e9136d662ff1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('daa4f3974bb6ea43a9c9bd13c6b1280d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('d3e86d6514d05803f854133900728825', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '15', 'admin', '2019-09-12 04:14:30', null, null);
INSERT INTO `sys_log` VALUES ('eebee23c2fc34dd7fea02e7992b33003', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '15', 'admin', '2019-09-12 04:14:30', null, null);
INSERT INTO `sys_log` VALUES ('c9d66a662efd9b4c9ec1222b91aa8822', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '15', 'admin', '2019-09-12 04:14:30', null, null);
INSERT INTO `sys_log` VALUES ('8ea715e4f7089460ad5a6f953d62f563', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:14:35', null, null);
INSERT INTO `sys_log` VALUES ('303cf0c66a93e6c57195b0bea19761d2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:14:35', null, null);
INSERT INTO `sys_log` VALUES ('3f086932117b3b96a0f56e3bbf569516', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 04:14:35', null, null);
INSERT INTO `sys_log` VALUES ('d774d078601a6898294ee84aa2dba963', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:14:41', null, null);
INSERT INTO `sys_log` VALUES ('1fb8aedacf6644ad57226410a24f358c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:14:41', null, null);
INSERT INTO `sys_log` VALUES ('dee585e76025e40b89da583418c6aca9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 04:14:41', null, null);
INSERT INTO `sys_log` VALUES ('ce092acc446d5b41d1809a119b44b967', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:14:43', null, null);
INSERT INTO `sys_log` VALUES ('d3b4eb09573286fecff38388cd7b703e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:14:43', null, null);
INSERT INTO `sys_log` VALUES ('744b3042185e2e3c4056e9ca73d70c64', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 04:14:43', null, null);
INSERT INTO `sys_log` VALUES ('57b04418c295b2c76170105c1726c83b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:15:01', null, null);
INSERT INTO `sys_log` VALUES ('08cc2f58a24a5bbd223c96bcb11994fe', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:15:01', null, null);
INSERT INTO `sys_log` VALUES ('4f1f8641e79c78148fa811f49acdce7c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 04:15:01', null, null);
INSERT INTO `sys_log` VALUES ('31e2d8a99f8388d3617985f7c1c4310b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:16:38', null, null);
INSERT INTO `sys_log` VALUES ('38b09bb0000499abe98426e094aa105e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 04:16:38', null, null);
INSERT INTO `sys_log` VALUES ('93474c8e4e8f5189ba371906ce06fc77', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:16:38', null, null);
INSERT INTO `sys_log` VALUES ('44583baf4f05ff1947955896845b3475', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:16:49', null, null);
INSERT INTO `sys_log` VALUES ('059044d85afa4f3da5ab5b7ac6eafd7e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 04:16:49', null, null);
INSERT INTO `sys_log` VALUES ('9e99299f13c07a635b07a8a4e2f40633', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:16:49', null, null);
INSERT INTO `sys_log` VALUES ('386c4a722ec79f43185420d4199ed02e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:16:52', null, null);
INSERT INTO `sys_log` VALUES ('ef6daadb26c563ace1486ba11ed981e6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:16:52', null, null);
INSERT INTO `sys_log` VALUES ('dadabbd9178d9a67bff8ff7050563aad', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:16:52', null, null);
INSERT INTO `sys_log` VALUES ('dd56394d7554f454657514070862b6a8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '13', 'admin', '2019-09-12 04:17:07', null, null);
INSERT INTO `sys_log` VALUES ('cbbce5cb9009cb0754ca2284ad32bf71', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '13', 'admin', '2019-09-12 04:17:07', null, null);
INSERT INTO `sys_log` VALUES ('9aeb53e27cc92f9c99ca98df7ffc0412', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '12', 'admin', '2019-09-12 04:17:07', null, null);
INSERT INTO `sys_log` VALUES ('46dd4760542b8ce5ecb3311793669385', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:17:14', null, null);
INSERT INTO `sys_log` VALUES ('1b276a8289845de4bc6310173474acb1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:17:14', null, null);
INSERT INTO `sys_log` VALUES ('6f941ca1dd8e3e8f3ceb0630073cc9c4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:17:14', null, null);
INSERT INTO `sys_log` VALUES ('fc562bd79bb4030555016aa61811ab28', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:17:17', null, null);
INSERT INTO `sys_log` VALUES ('89425dbe59a9b7a04541c8eb8d12d4fa', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:17:17', null, null);
INSERT INTO `sys_log` VALUES ('44c4d2a8f3aba34e2f17e2f1ecc46546', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:17:17', null, null);
INSERT INTO `sys_log` VALUES ('e6ac121496a5f9b4a6296b911bac4360', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:36:09', null, null);
INSERT INTO `sys_log` VALUES ('de90afbcd5412566ff5c03512c21d621', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:36:09', null, null);
INSERT INTO `sys_log` VALUES ('6c24bb095ef936aee77fdb4c0c5cb26a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:36:09', null, null);
INSERT INTO `sys_log` VALUES ('aa4502a476b57375acf50465ac8ebfaa', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 04:37:36', null, null);
INSERT INTO `sys_log` VALUES ('18af73cbac67b597366524ea891dad68', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:37:36', null, null);
INSERT INTO `sys_log` VALUES ('e7d637f21fdd230f88daceaac1687313', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 04:37:36', null, null);
INSERT INTO `sys_log` VALUES ('6e44bde6b58782a804d8cbbeb6a60b46', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 04:37:42', null, null);
INSERT INTO `sys_log` VALUES ('0c092bc9aeb98576918d09c9ae1e25db', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:37:42', null, null);
INSERT INTO `sys_log` VALUES ('d70167d29b738663b313083a41b4225b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:37:42', null, null);
INSERT INTO `sys_log` VALUES ('bbbd4d156472dd2197ffc85d207a86d5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:38:37', null, null);
INSERT INTO `sys_log` VALUES ('68f08999ccbf79aa90a1dd5786140f3f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:38:37', null, null);
INSERT INTO `sys_log` VALUES ('912fa0ceec495fc7ca436cb4f25c16cb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:38:37', null, null);
INSERT INTO `sys_log` VALUES ('178768a7033e002d1af3076329741b05', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:38:42', null, null);
INSERT INTO `sys_log` VALUES ('cc43cf645a1cc0ae0a1e0d57f99283a4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:38:42', null, null);
INSERT INTO `sys_log` VALUES ('d5fae67e95a8619713c6f5ca4e8b02d6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 04:38:42', null, null);
INSERT INTO `sys_log` VALUES ('3919f2c0af0ceb25ca44776436b6f3f9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:39:35', null, null);
INSERT INTO `sys_log` VALUES ('3517348ac0353199353a8ae71737336d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:39:35', null, null);
INSERT INTO `sys_log` VALUES ('595cac833fb5081c850c451ecbae6550', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 04:39:35', null, null);
INSERT INTO `sys_log` VALUES ('341fffb9b3172880d1775987d12dbc95', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:39:38', null, null);
INSERT INTO `sys_log` VALUES ('2b16f585e9f60f12b0ee293de9561ab7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:39:38', null, null);
INSERT INTO `sys_log` VALUES ('95032ea21a3ddb5f1e8625529126cd5c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:39:38', null, null);
INSERT INTO `sys_log` VALUES ('cdb28a33b311e956e55270a3fb0ff9a2', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:41:16', null, null);
INSERT INTO `sys_log` VALUES ('65ba93c5448a0e4ed2bce87902472a83', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 04:41:16', null, null);
INSERT INTO `sys_log` VALUES ('20921425916d4fab810203cf7cfbe613', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:41:16', null, null);
INSERT INTO `sys_log` VALUES ('8dc8953c1e91cc1a90351818b319d9bf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 04:41:19', null, null);
INSERT INTO `sys_log` VALUES ('744b302d21ec5e6cf670504c2ed6c76f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:41:19', null, null);
INSERT INTO `sys_log` VALUES ('5a9ee76f758525c7f6b39c4a4acb8cb9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 04:41:19', null, null);
INSERT INTO `sys_log` VALUES ('b2f6d4e2d1e708ea41ebe3bbe2837850', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:44:38', null, null);
INSERT INTO `sys_log` VALUES ('b3cbffaad7bcfeebb2cc6f7cc0b6748c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 04:44:38', null, null);
INSERT INTO `sys_log` VALUES ('c07931492ad29f1cecf712bf3283db64', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:44:38', null, null);
INSERT INTO `sys_log` VALUES ('ec59cf5a85521714b69b8b3e40d4f7bd', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:44:44', null, null);
INSERT INTO `sys_log` VALUES ('bc300ce023fc787e4912e3afcddb6f9e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 04:44:44', null, null);
INSERT INTO `sys_log` VALUES ('850f547f47a21652d44db1da9a4b4f2c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 04:44:44', null, null);
INSERT INTO `sys_log` VALUES ('a43df334496f6a40eeda6b33764324e9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 04:45:20', null, null);
INSERT INTO `sys_log` VALUES ('de1490485b1a1f8d51acfd063a07bdf0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:45:20', null, null);
INSERT INTO `sys_log` VALUES ('227d47992905c638df4b34605becdeb0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 04:45:20', null, null);
INSERT INTO `sys_log` VALUES ('0fadf79b4b87f3953f03041623e21086', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:45:25', null, null);
INSERT INTO `sys_log` VALUES ('6e9b1d58d9312fa6f2dd767c3767ba6c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:45:25', null, null);
INSERT INTO `sys_log` VALUES ('dab3415600b530f100549b6ee3b2fd87', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 04:45:25', null, null);
INSERT INTO `sys_log` VALUES ('2456710692ac58c0f8296146fff29efd', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:46:02', null, null);
INSERT INTO `sys_log` VALUES ('955c114faced3b9387fc235094abca40', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 04:46:02', null, null);
INSERT INTO `sys_log` VALUES ('2f4c12df8541bb4cc843e30c20db9082', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:46:02', null, null);
INSERT INTO `sys_log` VALUES ('f11ec8bbefbb35a31f74dd9e73171800', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:46:04', null, null);
INSERT INTO `sys_log` VALUES ('0c98cc3574fb3ce696901cb5e379699b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '29', 'admin', '2019-09-12 04:46:04', null, null);
INSERT INTO `sys_log` VALUES ('be746d6bb2d45a5721ad5b3b96554d89', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 04:46:04', null, null);
INSERT INTO `sys_log` VALUES ('1a0216a3896e10a19edf307a06b3ccb8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 04:56:04', null, null);
INSERT INTO `sys_log` VALUES ('665f258854a7b16f143a20dcd00f16e1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 04:56:04', null, null);
INSERT INTO `sys_log` VALUES ('45f3f19e454ab522b2b66e6631676b83', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 04:56:04', null, null);
INSERT INTO `sys_log` VALUES ('54641ac230c5604ff4e9194a214e915b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:56:06', null, null);
INSERT INTO `sys_log` VALUES ('3bc3b120c9c0c0094e01156f4f24232a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 04:56:06', null, null);
INSERT INTO `sys_log` VALUES ('2674df45232541b8c5d1495b9e6f6474', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 04:56:06', null, null);
INSERT INTO `sys_log` VALUES ('bb0b98b1ce97ce0a84aff2bb1b80fd96', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:57:46', null, null);
INSERT INTO `sys_log` VALUES ('06c1706ef833136fabb45811daac848d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 04:57:46', null, null);
INSERT INTO `sys_log` VALUES ('ba6710353a592f0199fd811c7252ee7f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '38', 'admin', '2019-09-12 04:57:46', null, null);
INSERT INTO `sys_log` VALUES ('d47a2e07198525d8e7be5821f106774c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 04:57:50', null, null);
INSERT INTO `sys_log` VALUES ('9e2026d9a60f8d9ae6ed767c31eabd6b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 04:57:50', null, null);
INSERT INTO `sys_log` VALUES ('0eff64bbf23b9149a75e0665b0d1e0fb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 04:57:50', null, null);
INSERT INTO `sys_log` VALUES ('981f0aef6b1a89b0a525022872ac0f36', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:01:04', null, null);
INSERT INTO `sys_log` VALUES ('cf1e41e0a0bb0304fec0f4da88685e0c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 05:01:04', null, null);
INSERT INTO `sys_log` VALUES ('706da558b8d43787560942f61ad99493', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:01:04', null, null);
INSERT INTO `sys_log` VALUES ('91f0773432dcc359f5e781902cf005a1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:01:11', null, null);
INSERT INTO `sys_log` VALUES ('0098543ff77d7816a8a2ab5d2ba2b265', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:01:11', null, null);
INSERT INTO `sys_log` VALUES ('61a74d3050f5987c086d2a00f38d7354', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:01:11', null, null);
INSERT INTO `sys_log` VALUES ('6d8978cbb6f276a2cdc341a06226920c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:04:39', null, null);
INSERT INTO `sys_log` VALUES ('94c1e9eb6837e84a1b12ea6b5a5c3acc', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:04:39', null, null);
INSERT INTO `sys_log` VALUES ('7eb16813ecb67d28d689f8ea9b37378d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:04:39', null, null);
INSERT INTO `sys_log` VALUES ('85d064f57c5668e4a341031324726f4d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:04:46', null, null);
INSERT INTO `sys_log` VALUES ('124255e5b0aefd387b1583436d8c829b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:04:46', null, null);
INSERT INTO `sys_log` VALUES ('efe64448feb4c56ccc18f526e9f6bc00', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:04:47', null, null);
INSERT INTO `sys_log` VALUES ('0142ec745d31bb98c76133c5060adab4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:06:09', null, null);
INSERT INTO `sys_log` VALUES ('9d87065bbe72f545a3186f3595117312', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 05:06:09', null, null);
INSERT INTO `sys_log` VALUES ('815908a1e470abee1dda2c48057fc0f6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:06:09', null, null);
INSERT INTO `sys_log` VALUES ('db4c8b1f8e81b11c3759031f792854e0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:06:17', null, null);
INSERT INTO `sys_log` VALUES ('6b987cfbca9151a4d4f51714d393ccf2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:06:17', null, null);
INSERT INTO `sys_log` VALUES ('0036f912afc88312daaf22eff5165e54', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 05:06:17', null, null);
INSERT INTO `sys_log` VALUES ('69ef714ea8a9f994ca5a9bd2a5c85d63', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:08:52', null, null);
INSERT INTO `sys_log` VALUES ('55734641926c9798fc4e013951a90f8a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 05:08:52', null, null);
INSERT INTO `sys_log` VALUES ('e22cd767c035e94e1f555770fb0d48a1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:08:52', null, null);
INSERT INTO `sys_log` VALUES ('7ddfaac91f33250d764e58a4dfbdd1cc', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:08:58', null, null);
INSERT INTO `sys_log` VALUES ('3fb5ec60989557a15ebc15e11de94900', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:08:58', null, null);
INSERT INTO `sys_log` VALUES ('17070a4beb3ffa66fdc55339496842f7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 05:08:58', null, null);
INSERT INTO `sys_log` VALUES ('b6a7f0986137418c1e30958fb01ee7ba', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '45', 'admin', '2019-09-12 05:09:50', null, null);
INSERT INTO `sys_log` VALUES ('c912e7b0a73382b6f784debe97dd6b04', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '45', 'admin', '2019-09-12 05:09:50', null, null);
INSERT INTO `sys_log` VALUES ('afc08da9c82f2e2ca86d8709d56fa1bb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '55', 'admin', '2019-09-12 05:09:50', null, null);
INSERT INTO `sys_log` VALUES ('37334c250efc42eb1d5c119ac12fc89e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:10:39', null, null);
INSERT INTO `sys_log` VALUES ('cdfe463f33463dd42e736f0a4149e6ae', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:10:39', null, null);
INSERT INTO `sys_log` VALUES ('c6dbe965a40fa952356def3d38d594a4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:10:39', null, null);
INSERT INTO `sys_log` VALUES ('c3afd5e65820385f0060fe95779ba3da', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:13:31', null, null);
INSERT INTO `sys_log` VALUES ('febb3d374eaccd5d4fdb947070dbf414', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:13:31', null, null);
INSERT INTO `sys_log` VALUES ('0c8092b98543d07fa86f2b4b33e699d8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:13:31', null, null);
INSERT INTO `sys_log` VALUES ('a9136e1eeebce03d45a6c430368bc590', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:14:29', null, null);
INSERT INTO `sys_log` VALUES ('eab46f0a051cf96a75ce18bc7c248fed', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:14:29', null, null);
INSERT INTO `sys_log` VALUES ('75b718864a6d9c0f783b63c3d2e34569', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '44', 'admin', '2019-09-12 05:14:29', null, null);
INSERT INTO `sys_log` VALUES ('cf152dae15c3a6efae18a7caef54ce52', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:17:41', null, null);
INSERT INTO `sys_log` VALUES ('8e69301726e76aae3958d3922077f0a6', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:17:41', null, null);
INSERT INTO `sys_log` VALUES ('3ba573ba7ab1ab464e7d31c28f7145a4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:17:41', null, null);
INSERT INTO `sys_log` VALUES ('ae3fb7d6bc3cef10f9dc4d1bae0f4272', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:17:45', null, null);
INSERT INTO `sys_log` VALUES ('065247bce1057ca8f64e39ed6e79f96c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:18:11', null, null);
INSERT INTO `sys_log` VALUES ('a45f1001d094e6997e3ad5a2ada72c66', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '21', 'admin', '2019-09-12 05:18:12', null, null);
INSERT INTO `sys_log` VALUES ('572f75b23a27a4b4844f23f56c3d6c51', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:21:49', null, null);
INSERT INTO `sys_log` VALUES ('ab19e1214c889da4b1bccc549443d2ba', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:21:49', null, null);
INSERT INTO `sys_log` VALUES ('638545e9e7ca530409e27a9d9f02e868', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '43', 'admin', '2019-09-12 05:21:49', null, null);
INSERT INTO `sys_log` VALUES ('adc984d9e5cad3aa3b4a7eb80e0dd06b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:27:37', null, null);
INSERT INTO `sys_log` VALUES ('03022a61f4a10f5a1d372ab42c793c7f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:27:37', null, null);
INSERT INTO `sys_log` VALUES ('550e9a725e192de80972eaf1f643097d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:27:37', null, null);
INSERT INTO `sys_log` VALUES ('f07f52efd5ed9b894945b1baa1a4cc44', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:27:41', null, null);
INSERT INTO `sys_log` VALUES ('6e3bc9578ca780e627c7f55086eaae37', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:27:47', null, null);
INSERT INTO `sys_log` VALUES ('0b45cd0c6f934d5eebd84c1593cd87cd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:27:47', null, null);
INSERT INTO `sys_log` VALUES ('eddaa244fbf3ed97ffeb83bc57612fc5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 05:27:47', null, null);
INSERT INTO `sys_log` VALUES ('c41d9eb676fc4f89c43f35d4f1ba6e3f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '20', 'admin', '2019-09-12 05:27:54', null, null);
INSERT INTO `sys_log` VALUES ('364c4ad04e8105bcc480454adbf32b38', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '20', 'admin', '2019-09-12 05:27:54', null, null);
INSERT INTO `sys_log` VALUES ('8c658fa22ffe84e174c934964f63075a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:27:54', null, null);
INSERT INTO `sys_log` VALUES ('6859f5323bc81a2a6271ef6708d41de7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:27:56', null, null);
INSERT INTO `sys_log` VALUES ('052e0a97734e0cc910aa5f83e0597f12', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:28:01', null, null);
INSERT INTO `sys_log` VALUES ('c7816e09f6fe6acab0700aef0d7d61d7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:28:04', null, null);
INSERT INTO `sys_log` VALUES ('d03e6d3ca01b9e91abe47c57e01805f0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:28:06', null, null);
INSERT INTO `sys_log` VALUES ('70a9a5d5ecdf36fc5d70f5e7ac45f158', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:28:08', null, null);
INSERT INTO `sys_log` VALUES ('ad2627adfa77b106eec7ab246a288d82', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:28:23', null, null);
INSERT INTO `sys_log` VALUES ('60a04127ceb3867e9f4b899b7fa7310b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:28:29', null, null);
INSERT INTO `sys_log` VALUES ('b4007a5532f8041562f8290a05dc8fad', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '71', 'admin', '2019-09-12 05:28:41', null, null);
INSERT INTO `sys_log` VALUES ('240e4816a00935f238638bf737623f1b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 05:29:56', null, null);
INSERT INTO `sys_log` VALUES ('616e44644627c8401af6115dca3b9533', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '35', 'admin', '2019-09-12 05:29:56', null, null);
INSERT INTO `sys_log` VALUES ('1b06e2f400440bff11eff33268cfd24a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:29:56', null, null);
INSERT INTO `sys_log` VALUES ('8bc4eb4a37ff52cba3ab21a06a717b40', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:30:47', null, null);
INSERT INTO `sys_log` VALUES ('2beb65f4421ec2e9e572cb1d2e9fa945', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:30:47', null, null);
INSERT INTO `sys_log` VALUES ('b8daec823151293368e995ba349586cf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:30:47', null, null);
INSERT INTO `sys_log` VALUES ('436975507d4360e02198d073a462acd3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:30:55', null, null);
INSERT INTO `sys_log` VALUES ('7a763bee246703951f49e017092f41b1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:30:55', null, null);
INSERT INTO `sys_log` VALUES ('0386ed81ce5a8bfbdff49583044b359c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:30:55', null, null);
INSERT INTO `sys_log` VALUES ('dce573ccb60661baeda69807fb0d4ae2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:31:54', null, null);
INSERT INTO `sys_log` VALUES ('6e38589b79faff386699709d02e7eaa1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '21', 'admin', '2019-09-12 05:31:54', null, null);
INSERT INTO `sys_log` VALUES ('fae52d92dead3d14cf1b8f501e2f2fdf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:31:54', null, null);
INSERT INTO `sys_log` VALUES ('31f73588d86439c8ff6d8e1b93df4ff5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:33:52', null, null);
INSERT INTO `sys_log` VALUES ('297968bf99389454ce7ba312b5f98b40', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:33:52', null, null);
INSERT INTO `sys_log` VALUES ('f3c30fe8a68380b77f00d17e5bb9d98d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:33:52', null, null);
INSERT INTO `sys_log` VALUES ('9931de246f9f12600bc3d187fc58e982', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:34:32', null, null);
INSERT INTO `sys_log` VALUES ('08f828abcb06bf004f6c004b98857451', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:34:32', null, null);
INSERT INTO `sys_log` VALUES ('b7180b6b0f8cfd99c7d464e38c8990c9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:34:32', null, null);
INSERT INTO `sys_log` VALUES ('1639f1f63e8568f035e90321787f1afb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '118', 'admin', '2019-09-12 05:35:02', null, null);
INSERT INTO `sys_log` VALUES ('55ab65d43c69aaa9db2aae3500f3556d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '90', 'admin', '2019-09-12 05:35:02', null, null);
INSERT INTO `sys_log` VALUES ('82ab86b003c8e2e926d25547bce5c20d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '123', 'admin', '2019-09-12 05:35:02', null, null);
INSERT INTO `sys_log` VALUES ('1e1abcc5f40481fbf8f57cbffcb47eb4', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:37:53', null, null);
INSERT INTO `sys_log` VALUES ('f7c21dfd79eb57862c636d2ebfa815d0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:37:53', null, null);
INSERT INTO `sys_log` VALUES ('27146b18973cc81906821300aed208fa', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:37:53', null, null);
INSERT INTO `sys_log` VALUES ('c7a2fb77476994fed2d268c00dde1149', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 05:37:56', null, null);
INSERT INTO `sys_log` VALUES ('987fe2ab55b7b026ab212329657f1214', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:38:12', null, null);
INSERT INTO `sys_log` VALUES ('740cd6598bd5c1d1d28018cf34671bdb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:38:12', null, null);
INSERT INTO `sys_log` VALUES ('085734972d9df35a8c5d9d8110957f5c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:38:12', null, null);
INSERT INTO `sys_log` VALUES ('39f250474cb5dd2dae95227e6e15d210', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:38:15', null, null);
INSERT INTO `sys_log` VALUES ('cacd227976467e3d0e6b8e2a09cfe5e5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 05:38:20', null, null);
INSERT INTO `sys_log` VALUES ('8f64e28b4ac470c2820abbbe281dd628', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:38:56', null, null);
INSERT INTO `sys_log` VALUES ('8d6584faa49165d53c532ac72cb35d9c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:38:56', null, null);
INSERT INTO `sys_log` VALUES ('2f619dcfe7ba0d0415a03c703eafa51b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:38:56', null, null);
INSERT INTO `sys_log` VALUES ('ee26f5063e7466a70b7e161bca2cdfbf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:40:47', null, null);
INSERT INTO `sys_log` VALUES ('d9c20e928aeb0c02ce651e8f1f1c33b4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:40:47', null, null);
INSERT INTO `sys_log` VALUES ('de4b68438b40cde6583c8be90f461a60', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:40:47', null, null);
INSERT INTO `sys_log` VALUES ('72566cc9429368f37317cdd2124fb540', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:41:05', null, null);
INSERT INTO `sys_log` VALUES ('ed6f4cb976ee632608bcd1b7b7160e79', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:42:19', null, null);
INSERT INTO `sys_log` VALUES ('c23880f69b441ab6279c8e744eadf338', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:42:19', null, null);
INSERT INTO `sys_log` VALUES ('febd6910d19452b01392d39d71a0808e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:42:19', null, null);
INSERT INTO `sys_log` VALUES ('d359921088cee88fe08860a54061a535', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:42:30', null, null);
INSERT INTO `sys_log` VALUES ('af703c475fa5c2f68ddfb4f696909277', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:42:30', null, null);
INSERT INTO `sys_log` VALUES ('2101115d2ad4717f85763ff6f9f975e3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:42:30', null, null);
INSERT INTO `sys_log` VALUES ('db361cb7c89b250ff7338a60c0ed12e7', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:43:28', null, null);
INSERT INTO `sys_log` VALUES ('e848dfeb63484e0c44c362788a035d4c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:43:28', null, null);
INSERT INTO `sys_log` VALUES ('3c62a856a43c4e3aa893df6e7ef7afc1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:43:28', null, null);
INSERT INTO `sys_log` VALUES ('cd372636e356f9b5dd64402957ba8e22', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:44:46', null, null);
INSERT INTO `sys_log` VALUES ('03914304470856c5dd7bc57451694950', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:44:46', null, null);
INSERT INTO `sys_log` VALUES ('5fc5587d5a08cd6613822cc0d8e0b9d0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:44:46', null, null);
INSERT INTO `sys_log` VALUES ('b2da69549e80b91aa91fe15fd38e2e88', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:44:49', null, null);
INSERT INTO `sys_log` VALUES ('7a8dce08ed76238f3d6496acaf8d3fc3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:44:55', null, null);
INSERT INTO `sys_log` VALUES ('7305ed984f8172e4fa06626fc46e4a36', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:44:57', null, null);
INSERT INTO `sys_log` VALUES ('867f54a52134898f5af14fc1e71f858c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:45:02', null, null);
INSERT INTO `sys_log` VALUES ('8ee12575d7bdb168e45505e169ac9316', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 05:45:06', null, null);
INSERT INTO `sys_log` VALUES ('6489eb3e246492d07bc101c459970d7a', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:46:31', null, null);
INSERT INTO `sys_log` VALUES ('0fede6c99e10eeeda8690ef1ef6f3f42', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:46:31', null, null);
INSERT INTO `sys_log` VALUES ('ba4697f77aa2d8664c14cc2aadd2ec66', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:46:31', null, null);
INSERT INTO `sys_log` VALUES ('4c3f4b3988034e5643099b0cc4ea23ce', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:48:14', null, null);
INSERT INTO `sys_log` VALUES ('82256bf397bf7bee01946e95799dc89b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 05:48:14', null, null);
INSERT INTO `sys_log` VALUES ('b626b4e409c546efb146ebce2f3efe22', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '20', 'admin', '2019-09-12 05:48:14', null, null);
INSERT INTO `sys_log` VALUES ('22f342ccf856a9614eab8a1434989771', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:51:28', null, null);
INSERT INTO `sys_log` VALUES ('8dd16bbdb99655c2bcea83427bd986f0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:51:28', null, null);
INSERT INTO `sys_log` VALUES ('1aeb04a6cfb16aa3fb1c0da5a7699763', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:51:28', null, null);
INSERT INTO `sys_log` VALUES ('1b8471119916056ca1d104c18b93d915', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:51:32', null, null);
INSERT INTO `sys_log` VALUES ('c17274075968078912387b29292f1e53', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:51:41', null, null);
INSERT INTO `sys_log` VALUES ('95e8740190f401136c1443cff3187b59', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:51:51', null, null);
INSERT INTO `sys_log` VALUES ('f959467a1e3f9781a38a370ce14c7e13', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:52:05', null, null);
INSERT INTO `sys_log` VALUES ('1f57726858d916d014c1863a863aaed4', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:52:05', null, null);
INSERT INTO `sys_log` VALUES ('3b74b76890b375434ee8afbd1e90de38', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:52:05', null, null);
INSERT INTO `sys_log` VALUES ('feea8fce7b8c4a20a9fb929e6f651b9d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:52:12', null, null);
INSERT INTO `sys_log` VALUES ('b36a11ae1cdcb2119134c83fddbe45cf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:52:12', null, null);
INSERT INTO `sys_log` VALUES ('513b2dbae4bb2bd6edd0126d3bbea16f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:52:12', null, null);
INSERT INTO `sys_log` VALUES ('d2af2004d04005ae0c65da50bda39d80', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:52:14', null, null);
INSERT INTO `sys_log` VALUES ('9f9f9d3f76e21ec91e6db6307d8c200d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:52:25', null, null);
INSERT INTO `sys_log` VALUES ('777e8844486e2bd361d2251fde645ff4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:52:28', null, null);
INSERT INTO `sys_log` VALUES ('edfab2e43760fa58c1191a3a803f0319', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 05:52:33', null, null);
INSERT INTO `sys_log` VALUES ('d1861809a33d959e5709facc1d6eae6e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '44', 'admin', '2019-09-12 05:52:37', null, null);
INSERT INTO `sys_log` VALUES ('9c6c02413bcef616c36fa92f35375b09', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 05:53:02', null, null);
INSERT INTO `sys_log` VALUES ('1d242c7c4025363bfba8e94ffa880380', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 05:53:45', null, null);
INSERT INTO `sys_log` VALUES ('1ff0e35e66ab1fa53586a31d03e152be', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '35', 'admin', '2019-09-12 05:53:45', null, null);
INSERT INTO `sys_log` VALUES ('77b6d81afc20283a06bda97a4344b0a9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:53:45', null, null);
INSERT INTO `sys_log` VALUES ('ea67033cdf1387ebe84821fa0da00eb0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:53:48', null, null);
INSERT INTO `sys_log` VALUES ('c1b0032ea5c433416d9f9cfafaacbbe9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:54:59', null, null);
INSERT INTO `sys_log` VALUES ('5dd9150ef4359293cc5b98aa824c2389', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:55:04', null, null);
INSERT INTO `sys_log` VALUES ('ab920380069ae31072ea1567ae906c06', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:55:11', null, null);
INSERT INTO `sys_log` VALUES ('bd4741a8cb606061e287e3a73117e323', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:55:15', null, null);
INSERT INTO `sys_log` VALUES ('efa28b378474f9be25fe64acc02da6f3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:55:19', null, null);
INSERT INTO `sys_log` VALUES ('1fdd122c4d10592959a63907c4f00c45', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:55:21', null, null);
INSERT INTO `sys_log` VALUES ('0244051532e4c71bec9c2077ff63fc65', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '48', 'admin', '2019-09-12 05:55:35', null, null);
INSERT INTO `sys_log` VALUES ('112e870d61c195783d3ca0ff7125edda', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:55:35', null, null);
INSERT INTO `sys_log` VALUES ('843129ead3cced777c5759684df99e6a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:55:35', null, null);
INSERT INTO `sys_log` VALUES ('d19c91fc7edbeef05d0fbed893f1b8ed', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:55:41', null, null);
INSERT INTO `sys_log` VALUES ('4bb248dda9a085cdc5aa258232fedacd', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 05:55:42', null, null);
INSERT INTO `sys_log` VALUES ('34050b9c6d870aef7ed6cd3eac969768', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 05:58:02', null, null);
INSERT INTO `sys_log` VALUES ('dea05c6b6e0b92e096e042e6b9be120e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 05:58:02', null, null);
INSERT INTO `sys_log` VALUES ('ad1f8ccd50afc0b3e8ac1c230bdb7fed', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:58:02', null, null);
INSERT INTO `sys_log` VALUES ('b324100b471eabfe79426196f89e15eb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '33', 'admin', '2019-09-12 05:58:05', null, null);
INSERT INTO `sys_log` VALUES ('316f4de302efb52e1b1e33c4bc187681', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:58:08', null, null);
INSERT INTO `sys_log` VALUES ('c3d84bc03543061a845c03c6d3e0b794', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:58:11', null, null);
INSERT INTO `sys_log` VALUES ('4fb5a28e5a64e963bd8e54bbf3adb143', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:58:13', null, null);
INSERT INTO `sys_log` VALUES ('487bfc6d755c5e8007804a740c9fbf9c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 05:58:18', null, null);
INSERT INTO `sys_log` VALUES ('b3b66fedc7e78748360a2395fc7cb016', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:58:20', null, null);
INSERT INTO `sys_log` VALUES ('6dbdec90a38441c13ac4210b1693820c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 05:58:22', null, null);
INSERT INTO `sys_log` VALUES ('87fe8bb5f1c4277cc1985c2022d2c77a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:58:25', null, null);
INSERT INTO `sys_log` VALUES ('4f6fdd0ec9c491b9858aaf43d565ec06', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:58:27', null, null);
INSERT INTO `sys_log` VALUES ('0fad946ffd6624ae8b7d7ac630e481cc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:58:29', null, null);
INSERT INTO `sys_log` VALUES ('8f5ba92c2140013a3d962a44c338cdb2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:58:30', null, null);
INSERT INTO `sys_log` VALUES ('6c6205999dc6d9d37cf33183313933f1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 05:58:32', null, null);
INSERT INTO `sys_log` VALUES ('6a22cd15bd4ad49cd83fdeb81dbf5e76', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 05:58:37', null, null);
INSERT INTO `sys_log` VALUES ('6efc07f35fab2bcaa41c289a8b7e6220', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 05:58:53', null, null);
INSERT INTO `sys_log` VALUES ('11d0125a469c17674d507707dc1a7822', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 05:58:57', null, null);
INSERT INTO `sys_log` VALUES ('9233707d0fe59408c92ea80c1f377a08', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:59:14', null, null);
INSERT INTO `sys_log` VALUES ('f4491cda8b797d9ac1d033994ddffac5', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 05:59:17', null, null);
INSERT INTO `sys_log` VALUES ('7da2bbc39d3cdd8465548ed015c8aa00', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:59:20', null, null);
INSERT INTO `sys_log` VALUES ('5e31562be1d6d97d65373e488eb515a0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:21', null, null);
INSERT INTO `sys_log` VALUES ('3479d86aff158ea2b29ff9ddc6173018', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:22', null, null);
INSERT INTO `sys_log` VALUES ('8813a77fe7e8625941c1744f57e1a184', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 05:59:23', null, null);
INSERT INTO `sys_log` VALUES ('7b576eebaadb34ac9610a5defa11d958', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:59:24', null, null);
INSERT INTO `sys_log` VALUES ('1aa6fc7fd563bdfeb534f733dcfb2303', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:59:28', null, null);
INSERT INTO `sys_log` VALUES ('22db27aab4b0291403a50a4b00624b1a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '57', 'admin', '2019-09-12 05:59:29', null, null);
INSERT INTO `sys_log` VALUES ('f56db1a1374daca8badcfca3510266ce', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 05:59:31', null, null);
INSERT INTO `sys_log` VALUES ('7b88bb0d9918cf54ca0249ded68356c3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 05:59:33', null, null);
INSERT INTO `sys_log` VALUES ('5057e429c9de1e9bc4162325da128580', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:59:34', null, null);
INSERT INTO `sys_log` VALUES ('a5d6b3d7dd02f28f2f848babe7256dfb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:41', null, null);
INSERT INTO `sys_log` VALUES ('82b4af7473d983181979658b0699b1a2', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:43', null, null);
INSERT INTO `sys_log` VALUES ('24e8025afc5407664c246344a0288c53', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:45', null, null);
INSERT INTO `sys_log` VALUES ('b24f011d32b06cdf915387cb35993c4e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 05:59:46', null, null);
INSERT INTO `sys_log` VALUES ('1ce1c54e6582042425e62821e10dbcb4', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 05:59:50', null, null);
INSERT INTO `sys_log` VALUES ('abf78e396bf77a8ef0868bce3b0cd8fb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 05:59:53', null, null);
INSERT INTO `sys_log` VALUES ('961b8a359cb48afcce8766b24bc66952', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 05:59:56', null, null);
INSERT INTO `sys_log` VALUES ('49bb9103bdbbb7d32e284773faadabd3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 05:59:57', null, null);
INSERT INTO `sys_log` VALUES ('d9b1793d7b4bead5d34ba8e765e72117', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:00:01', null, null);
INSERT INTO `sys_log` VALUES ('40b6515a954b5d1f04cc642dbd7afcba', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:02:22', null, null);
INSERT INTO `sys_log` VALUES ('f04aacfc886c057eb755ae0234e911a0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:02:22', null, null);
INSERT INTO `sys_log` VALUES ('4c468dc0d5528a84690dc37e088476de', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:02:22', null, null);
INSERT INTO `sys_log` VALUES ('d116281c521146d15e383a68c06a0028', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:02:25', null, null);
INSERT INTO `sys_log` VALUES ('9f9f62057689ab5ac40a1a38c888e620', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:02:50', null, null);
INSERT INTO `sys_log` VALUES ('7743c4d0ad883f8ff2abf4c3e82af88e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:02:50', null, null);
INSERT INTO `sys_log` VALUES ('8ae16a722d6632b41d4ac35d10605c67', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:02:50', null, null);
INSERT INTO `sys_log` VALUES ('e9d4119c210fd66f641eea0a4dde0034', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:02:52', null, null);
INSERT INTO `sys_log` VALUES ('39040dd80bbc06e701e8ca127d02a02e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:02:57', null, null);
INSERT INTO `sys_log` VALUES ('5552ef6814b8fc4eb7dd3477c84e73d0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:03:00', null, null);
INSERT INTO `sys_log` VALUES ('2777b6386ced08965e1bf0f5ffa4de19', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:03:02', null, null);
INSERT INTO `sys_log` VALUES ('fa11db05b508ba32377d23f8fba3c6a8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:03:06', null, null);
INSERT INTO `sys_log` VALUES ('fd8adce2dbee9f2c3bdec68ee2e53c03', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:03:10', null, null);
INSERT INTO `sys_log` VALUES ('2789f0c77c032124e06c94009c255209', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:03:12', null, null);
INSERT INTO `sys_log` VALUES ('6fcae346b1cb71186701f042c6722296', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:03:14', null, null);
INSERT INTO `sys_log` VALUES ('1fd1c46af14877648ad63a05022c0756', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 06:03:16', null, null);
INSERT INTO `sys_log` VALUES ('3b8853c949b21b86fd38d5d9f0d625e3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '20', 'admin', '2019-09-12 06:03:20', null, null);
INSERT INTO `sys_log` VALUES ('7dbcd4ec9dfa66fb175318b111df48ce', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:03:23', null, null);
INSERT INTO `sys_log` VALUES ('648018a78eca139e722c8c57219ccf52', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 06:04:42', null, null);
INSERT INTO `sys_log` VALUES ('448207cdbdaf8333e6de86a56e651c53', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:04:44', null, null);
INSERT INTO `sys_log` VALUES ('d4f9fb2f426dd3cd603d3f5e582a0acc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '20', 'admin', '2019-09-12 06:04:45', null, null);
INSERT INTO `sys_log` VALUES ('b3607af224c40f38d7bc643527650a03', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:04:46', null, null);
INSERT INTO `sys_log` VALUES ('79ec7316155848b0879c2984f6aaf40a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:04:48', null, null);
INSERT INTO `sys_log` VALUES ('8e4561a13a021bc1ee241df03881f848', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:04:49', null, null);
INSERT INTO `sys_log` VALUES ('6be1c90adada97374c5f564721f54d63', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:04:52', null, null);
INSERT INTO `sys_log` VALUES ('1bf8319955fc7b8ae1b89297ff11463f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:04:54', null, null);
INSERT INTO `sys_log` VALUES ('137857e84c7077dd63edb87d7b78697c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:04:55', null, null);
INSERT INTO `sys_log` VALUES ('3b5fcc5c511bd6642b7e3297eaa59aca', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:04:57', null, null);
INSERT INTO `sys_log` VALUES ('7566e07bd56357975ef523b88ef63af9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:04:58', null, null);
INSERT INTO `sys_log` VALUES ('e4af479520340403cc76ecd7d271595e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:04:59', null, null);
INSERT INTO `sys_log` VALUES ('7e8e50548fadd5adfd8a445a2c567ef5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:05:02', null, null);
INSERT INTO `sys_log` VALUES ('b6631a228294067f24daffd0b7cd3902', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 06:05:03', null, null);
INSERT INTO `sys_log` VALUES ('32d52c1c36306118cb864b09565b6806', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 06:05:06', null, null);
INSERT INTO `sys_log` VALUES ('926b0666870d62110da3758d1b290979', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:05:09', null, null);
INSERT INTO `sys_log` VALUES ('1eb842436d4369c3d34b09abcad72914', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:05:11', null, null);
INSERT INTO `sys_log` VALUES ('3b5b2d00e954b0eecfb35020c58dff68', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:05:12', null, null);
INSERT INTO `sys_log` VALUES ('d2a65aa07ed71a60a708acd7e6cff7dc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 06:05:14', null, null);
INSERT INTO `sys_log` VALUES ('7d4646e4f8dce16f684963366387d983', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:05:17', null, null);
INSERT INTO `sys_log` VALUES ('c081bd8cdeed7163867cb03ae13bf168', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('91b488889382042d2eda3a1edcb855ae', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('c0a413aeeddbc9fd3d23fd82904c3fa1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('a9606d1e6b3a725c5d370b186d7f50f8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('53e83c6ef27c7acec17d76e19fa8878a', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('67a91ddcce383d8e786b79260b3c881f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('f3bea8808eaf6edbd75035f581fd325c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('5584940021944ba42e1a1265c3e2acf1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('c4e29d24d4862f0a13a226262694aab8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('c2824e443a73352441eb74901dfc2502', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('9f4ee58b7265f18cc7b731a5a5d9222d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('828b67e40cdc67d2b667280e7b28bbca', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('5aaf75690ce3d6109a50fc997b445762', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('91d69ccb1beb7c536420b12a22a5b852', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '42', 'admin', '2019-09-12 06:14:59', null, null);
INSERT INTO `sys_log` VALUES ('fa177774667c051b15c61bedcf306cce', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '1053', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('ecde4f751c16ec30f15d86c18048f479', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('6bf524094987dd122c838d3a97ef339c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('269231139f34f42fc64b17893c35d28c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('ac196d32d69446202c6f35610c2501d0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('0197907d3a2c57c53b72e07202f35643', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('254abbd7e2feadb2c50e14909ab6ab9a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 06:15:00', null, null);
INSERT INTO `sys_log` VALUES ('d38c1edc5c6d8f77d54f73d8ef8d77d3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:15:16', null, null);
INSERT INTO `sys_log` VALUES ('7385961b007d612f2cfa0384da8191b9', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:15:16', null, null);
INSERT INTO `sys_log` VALUES ('df01db07b1c26d66e0c306c13f9bd57a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 06:15:16', null, null);
INSERT INTO `sys_log` VALUES ('99157fac331a9f271b2e1358210d3d67', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:16:00', null, null);
INSERT INTO `sys_log` VALUES ('e92a6871a7a4effb74d6473c9d9b50f8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:16:00', null, null);
INSERT INTO `sys_log` VALUES ('dfdcbb2a8135da6085bce82609731660', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:16:00', null, null);
INSERT INTO `sys_log` VALUES ('7ebfdf2fe94bb848d45536799fb7d170', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:16:16', null, null);
INSERT INTO `sys_log` VALUES ('57fbc987c8c58bae79e708d1f076707d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 06:16:18', null, null);
INSERT INTO `sys_log` VALUES ('1984355056b8ac4ee07e9c806742ce3e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '20', 'admin', '2019-09-12 06:17:53', null, null);
INSERT INTO `sys_log` VALUES ('9bbf3187fe5fab83070278f8b58d8045', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:17:53', null, null);
INSERT INTO `sys_log` VALUES ('f95065b7904a8cac41bb081f9e99e07a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:17:53', null, null);
INSERT INTO `sys_log` VALUES ('b84581e5be8f91c5506d9ee39ef16e82', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:17:58', null, null);
INSERT INTO `sys_log` VALUES ('e1633369868b69fe8c2a1a83f96d8147', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:18:02', null, null);
INSERT INTO `sys_log` VALUES ('8381721493a16a04c09450c1e005d8a8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:18:06', null, null);
INSERT INTO `sys_log` VALUES ('888ddbaeb697cd5bac5470f474d1a276', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:18:08', null, null);
INSERT INTO `sys_log` VALUES ('3f50583d0abb26337b9f6a2261e9d786', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '87', 'admin', '2019-09-12 06:18:09', null, null);
INSERT INTO `sys_log` VALUES ('542cbaab2dfb7f54e0d37b145a8de679', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:30:08', null, null);
INSERT INTO `sys_log` VALUES ('441139f8e985c94d56ade13d13abcf3e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 06:30:08', null, null);
INSERT INTO `sys_log` VALUES ('832eed0c6dcae18776149fd3727079d0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 06:30:08', null, null);
INSERT INTO `sys_log` VALUES ('e593c1b32d05d0b949074e3e297ecd60', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:30:11', null, null);
INSERT INTO `sys_log` VALUES ('1b45bf2505264ad1587f2bd31591f7e2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:30:14', null, null);
INSERT INTO `sys_log` VALUES ('515fc8f8bbfb8a93507ccdd6d1ab5054', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:30:15', null, null);
INSERT INTO `sys_log` VALUES ('c85f21daa56544f2a1bd32fb05f8fc95', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:30:16', null, null);
INSERT INTO `sys_log` VALUES ('d27832ddbdf81d3ed999c596c5dd6889', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:30:17', null, null);
INSERT INTO `sys_log` VALUES ('6820c8a63bd36b89cdf28b44dcbfa12d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:30:18', null, null);
INSERT INTO `sys_log` VALUES ('bdcad7ca74083e068eda0263ae30cfaa', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 06:30:20', null, null);
INSERT INTO `sys_log` VALUES ('53875bdc54c6bd52a27000a1993ae01f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:30:21', null, null);
INSERT INTO `sys_log` VALUES ('e406f1a892461f553727e297d9b2abf1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 06:30:22', null, null);
INSERT INTO `sys_log` VALUES ('c390ddb551a40cd8fef162750c6cf066', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 06:33:01', null, null);
INSERT INTO `sys_log` VALUES ('102f3cb18bbf1e4508068d0d35919748', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:33:01', null, null);
INSERT INTO `sys_log` VALUES ('2d45cae4b540aa6738b2a69136d4ad02', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 06:33:01', null, null);
INSERT INTO `sys_log` VALUES ('e2399021e3078289467f1b16cff31dd3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:34:21', null, null);
INSERT INTO `sys_log` VALUES ('17c0e03d8bb0103649c045ccbd534e22', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 06:34:21', null, null);
INSERT INTO `sys_log` VALUES ('e6d0d59b73fab5c1388b9ab59bfdf435', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:34:21', null, null);
INSERT INTO `sys_log` VALUES ('07632bd253df8d1214ab5ac2df3d3dae', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:34:32', null, null);
INSERT INTO `sys_log` VALUES ('a9b6afc076d12c68022fe64e92ab3fb8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:34:32', null, null);
INSERT INTO `sys_log` VALUES ('68716be8c43c2a39ce4df37c4d2375ae', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 06:34:32', null, null);
INSERT INTO `sys_log` VALUES ('a7757ede6520593ffa62557f1946c8b6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 06:34:35', null, null);
INSERT INTO `sys_log` VALUES ('99b586be5db57eb7f4138fd1069cbe2c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:34:36', null, null);
INSERT INTO `sys_log` VALUES ('4091852a5549b3a6f33c5eeb043c73a5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:34:38', null, null);
INSERT INTO `sys_log` VALUES ('e51651ef6ccf3f7996f54d62f23066b7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:34:40', null, null);
INSERT INTO `sys_log` VALUES ('664e75e2e63d3e33c88aad7e4d73b5b3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 06:34:43', null, null);
INSERT INTO `sys_log` VALUES ('bc2ede1a68f634c57889addaa82b241d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:34:45', null, null);
INSERT INTO `sys_log` VALUES ('3d6fa564e2664bd737c13232d3afecef', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 06:34:47', null, null);
INSERT INTO `sys_log` VALUES ('54d2c40afe44579b22587c5c3f710771', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:34:49', null, null);
INSERT INTO `sys_log` VALUES ('bcefc374bc915d62f2fff2961f6aab1b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:34:51', null, null);
INSERT INTO `sys_log` VALUES ('8e43f0dc2f2c6307ba55b06e99c668d1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:34:56', null, null);
INSERT INTO `sys_log` VALUES ('11670f9330da3443323c4772bddde050', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 06:34:59', null, null);
INSERT INTO `sys_log` VALUES ('7a62500c90b16aee5d4c7429dbd19e95', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:37:13', null, null);
INSERT INTO `sys_log` VALUES ('101ec4cd100ff696a8c1507b0b0a5b4b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 06:37:13', null, null);
INSERT INTO `sys_log` VALUES ('df9536dfee03d495d841fda2b7f02097', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 06:37:13', null, null);
INSERT INTO `sys_log` VALUES ('825945dcf264094c1411fbe3d1ece712', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:37:16', null, null);
INSERT INTO `sys_log` VALUES ('692fe99dad6b2e240848d384af0e52f5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:37:21', null, null);
INSERT INTO `sys_log` VALUES ('07e320ddf23b99c0a61924f9cfb46d09', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:37:23', null, null);
INSERT INTO `sys_log` VALUES ('44bc41299b44c129b2ed9ba4bb82c6cc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:37:24', null, null);
INSERT INTO `sys_log` VALUES ('bef832029b0300efd3d358c389b3b0a5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:45:19', null, null);
INSERT INTO `sys_log` VALUES ('a00ce39693a43030ecb9219919e18a41', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 06:45:19', null, null);
INSERT INTO `sys_log` VALUES ('01788a0e7d1487141412ff80ddeb284b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:45:19', null, null);
INSERT INTO `sys_log` VALUES ('dafedb42608f2266f0b05d9fcc8835a5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 06:46:25', null, null);
INSERT INTO `sys_log` VALUES ('51d2040d86f9c37eef0a745994a8727e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '35', 'admin', '2019-09-12 06:46:25', null, null);
INSERT INTO `sys_log` VALUES ('6889a344427ae6d9bf00c2c31e478129', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '33', 'admin', '2019-09-12 06:46:25', null, null);
INSERT INTO `sys_log` VALUES ('487ca9d87fc0e1ba5fbfcb5e0c1e8117', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:46:27', null, null);
INSERT INTO `sys_log` VALUES ('e8d3c78c4121941f0c66479de90dc4e6', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 06:55:57', null, null);
INSERT INTO `sys_log` VALUES ('44942f7831f19d8a1df58d665d349de8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 06:55:57', null, null);
INSERT INTO `sys_log` VALUES ('915b0050a6c516cb0f5cf44bf5e7cbb4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 06:55:57', null, null);
INSERT INTO `sys_log` VALUES ('9c88ee294cbe0e05cfc726cca1df552f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 06:55:59', null, null);
INSERT INTO `sys_log` VALUES ('3bfb7e56d80f7a2fbc1ffc9566e99df2', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 06:56:04', null, null);
INSERT INTO `sys_log` VALUES ('7c95c520cd548117301f1ce43c58e206', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:56:07', null, null);
INSERT INTO `sys_log` VALUES ('ae36f36e080ed7bcddd85463ccc17996', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 06:56:11', null, null);
INSERT INTO `sys_log` VALUES ('a7f786558801fe61581ec7d47b9b22ac', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:56:13', null, null);
INSERT INTO `sys_log` VALUES ('de5b7477bb5af1418d28126bf60fd1bc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:56:18', null, null);
INSERT INTO `sys_log` VALUES ('68c6be7c9c8090f0919500fe8d18017a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:56:19', null, null);
INSERT INTO `sys_log` VALUES ('8890d7798da93b508674dd6c9e298608', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 06:56:20', null, null);
INSERT INTO `sys_log` VALUES ('579c33232d6811279677bdfd8b6c9d04', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 06:56:22', null, null);
INSERT INTO `sys_log` VALUES ('bf789ddfa1915322c16dc4fd67c65dc0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 06:56:23', null, null);
INSERT INTO `sys_log` VALUES ('70546198065cd54e3b1e0493718b0421', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 06:56:24', null, null);
INSERT INTO `sys_log` VALUES ('38a5c9fd9dce534a72333e8cb4ac4cac', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 06:56:27', null, null);
INSERT INTO `sys_log` VALUES ('9cb1395d94d04e5e7b65f0a152b4a6a6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 06:56:30', null, null);
INSERT INTO `sys_log` VALUES ('ecfbe4682754f433298d5a78c22f936e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:04:35', null, null);
INSERT INTO `sys_log` VALUES ('e5336922b8f2833da27ee158d54e229c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:04:35', null, null);
INSERT INTO `sys_log` VALUES ('0ddfc82790cc9fe2d4745aff24aa8864', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:04:35', null, null);
INSERT INTO `sys_log` VALUES ('29626059cfcd391a2e1914478abbace8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:04:55', null, null);
INSERT INTO `sys_log` VALUES ('c81cd25178800d2a20269c84a61060f1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:04:55', null, null);
INSERT INTO `sys_log` VALUES ('88361e408dd988e86b745fe73e1376ee', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 07:04:55', null, null);
INSERT INTO `sys_log` VALUES ('d96732471c3fd5208725ea148ed09c05', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:06:20', null, null);
INSERT INTO `sys_log` VALUES ('8f58f14fab253ae66fc312da69feba32', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:06:20', null, null);
INSERT INTO `sys_log` VALUES ('3f8bbc87d643cfa686817f093cee80dd', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:06:20', null, null);
INSERT INTO `sys_log` VALUES ('878f9442f69ee463c421a641d167e68d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:06:22', null, null);
INSERT INTO `sys_log` VALUES ('3a2cd68da82766defc76b86f1a2a2747', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 07:06:27', null, null);
INSERT INTO `sys_log` VALUES ('5b9025bb4b1115cd9ac172fb56996264', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:18:12', null, null);
INSERT INTO `sys_log` VALUES ('b81b7e7f9348c42ee49f991e28c4aff6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:18:12', null, null);
INSERT INTO `sys_log` VALUES ('9295219920ef00a8b308eee473b301a8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:18:12', null, null);
INSERT INTO `sys_log` VALUES ('5bfdb1e6067114a75dd681bc7ea16a96', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:18:32', null, null);
INSERT INTO `sys_log` VALUES ('0ba4f14c44c8ded6f8bd9c3e26d32f11', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:18:32', null, null);
INSERT INTO `sys_log` VALUES ('25f15ece875e78f701e51a3f87d3e255', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 07:18:32', null, null);
INSERT INTO `sys_log` VALUES ('8ca9fe45ddca7e34988ba269c5711305', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:18:35', null, null);
INSERT INTO `sys_log` VALUES ('62b4dd86620b45e1edb072ad2aff9245', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:18:38', null, null);
INSERT INTO `sys_log` VALUES ('dcded9d9e4f289eb1eb913748eb142ac', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:18:44', null, null);
INSERT INTO `sys_log` VALUES ('c118e3240baed51ea9b44d21c8c104eb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:18:47', null, null);
INSERT INTO `sys_log` VALUES ('5b704b8a0d863323beed90e9d7041182', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:18:52', null, null);
INSERT INTO `sys_log` VALUES ('2b96fc83019e9f80770bd8a521463dbf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:19:02', null, null);
INSERT INTO `sys_log` VALUES ('161fe9342a9cf96ddab9dc018f7b119b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:19:05', null, null);
INSERT INTO `sys_log` VALUES ('8c8af1ecef1e6d76fed0b6baba40df94', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:29:10', null, null);
INSERT INTO `sys_log` VALUES ('78744ff067b6c66b15aa3807c9a032b5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:29:10', null, null);
INSERT INTO `sys_log` VALUES ('a870167101ed8aafc79986f405e7dad0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:29:10', null, null);
INSERT INTO `sys_log` VALUES ('e1e5d679f7736de9356b62371ff7d415', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:29:18', null, null);
INSERT INTO `sys_log` VALUES ('fe81cd9724ae4a93459b19a77786330a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:29:18', null, null);
INSERT INTO `sys_log` VALUES ('018c7311a4161759e61b9f40f53da3fb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '140', 'admin', '2019-09-12 07:29:19', null, null);
INSERT INTO `sys_log` VALUES ('c81ff3ef43afe3f0213475a397affcd3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:29:21', null, null);
INSERT INTO `sys_log` VALUES ('9da36ce000f565f0f505b66b9b58fd01', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:29:25', null, null);
INSERT INTO `sys_log` VALUES ('dfed381c2ff1a6f3a75f3dae9ee86b55', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:29:28', null, null);
INSERT INTO `sys_log` VALUES ('75958de57e381390d159bb2336be2fd7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:29:32', null, null);
INSERT INTO `sys_log` VALUES ('d202fd2ae6bf20694baea910c1d6b5d2', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:29:36', null, null);
INSERT INTO `sys_log` VALUES ('47c891bebda240f4c1e5769dd3ac4c76', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:29:41', null, null);
INSERT INTO `sys_log` VALUES ('b846ae4b27c0343331a8c5d672b18b3d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:29:46', null, null);
INSERT INTO `sys_log` VALUES ('7b7e581d79e37458883ecbd158bfedbc', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:30:26', null, null);
INSERT INTO `sys_log` VALUES ('e2d71db80b3c6707a4351ecf7498780b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:30:26', null, null);
INSERT INTO `sys_log` VALUES ('5b8b6d18265139fd3d8f36699382f2c8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:30:26', null, null);
INSERT INTO `sys_log` VALUES ('e711977afb1d482090e1299e92b86e31', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:30:29', null, null);
INSERT INTO `sys_log` VALUES ('f29384abd668d3a7dd9690495fa394da', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 07:30:34', null, null);
INSERT INTO `sys_log` VALUES ('d84259526e69cfbe07a64010397f3f76', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 07:30:38', null, null);
INSERT INTO `sys_log` VALUES ('aa13660de29526786005633ca9862224', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:30:43', null, null);
INSERT INTO `sys_log` VALUES ('393119264e3a3faaacfb9fef36a9a86a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:30:47', null, null);
INSERT INTO `sys_log` VALUES ('f4d232131114a7862e59fc44bc03ad8e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:30:50', null, null);
INSERT INTO `sys_log` VALUES ('c31ea9a04fe90d47feee3b54b105c1e7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:30:51', null, null);
INSERT INTO `sys_log` VALUES ('5416b548842047b4d92ff4f03bd08aa1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:30:52', null, null);
INSERT INTO `sys_log` VALUES ('204ef03af8833db749c4bb5f27e40a1f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:30:54', null, null);
INSERT INTO `sys_log` VALUES ('467d62c90b85aaf19263e1e944770a2e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:30:55', null, null);
INSERT INTO `sys_log` VALUES ('6f1342970b01d647eef657119c990ef1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:30:57', null, null);
INSERT INTO `sys_log` VALUES ('2bafab1b11a4585de898a9368d8c9768', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:30:58', null, null);
INSERT INTO `sys_log` VALUES ('0cc5933ea66f72cebc165cd997c23814', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 07:31:00', null, null);
INSERT INTO `sys_log` VALUES ('3899b29493661e06c8ada928f708a88d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:31:03', null, null);
INSERT INTO `sys_log` VALUES ('bdc85002dfdb18d17722aee983794586', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:31:06', null, null);
INSERT INTO `sys_log` VALUES ('fad30e9e78a36dff3053ee985c32d6ae', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:31:09', null, null);
INSERT INTO `sys_log` VALUES ('f0da8704464d022ad5e0d0a4e37d1004', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:31:13', null, null);
INSERT INTO `sys_log` VALUES ('12853ca6ac51c0983561f0bc873d7553', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:31:15', null, null);
INSERT INTO `sys_log` VALUES ('398c5b4aa00873ba02660a7461760265', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:31:18', null, null);
INSERT INTO `sys_log` VALUES ('1a0723adf4c47895b0af92aa9cefaacf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:31:19', null, null);
INSERT INTO `sys_log` VALUES ('42b6832885f8b74103d6ef4e5742f9f3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '169', 'admin', '2019-09-12 07:31:21', null, null);
INSERT INTO `sys_log` VALUES ('fbdcc2c01e5d85e0f0027ce11cf34f90', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:31:24', null, null);
INSERT INTO `sys_log` VALUES ('e958afc6054245b91f33013cbf660642', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:31:28', null, null);
INSERT INTO `sys_log` VALUES ('6b81d9def585059504126bb021949c8e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:31:35', null, null);
INSERT INTO `sys_log` VALUES ('23de98233d6307a52669229aee5471f6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 07:31:35', null, null);
INSERT INTO `sys_log` VALUES ('10a9157a34caeaa55476120272c1c844', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:31:35', null, null);
INSERT INTO `sys_log` VALUES ('ce2afc8c0b7a4b894b93301909d3cf12', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:31:37', null, null);
INSERT INTO `sys_log` VALUES ('c4f036a65ca36a50e5b69d8f60e5b7e2', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:31:41', null, null);
INSERT INTO `sys_log` VALUES ('234c646d01dbffd7ffd491ea4c462cae', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:31:44', null, null);
INSERT INTO `sys_log` VALUES ('e03abd886be4a0bf3f581f2aace5932f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:32:28', null, null);
INSERT INTO `sys_log` VALUES ('592dc7a1433d1405f64fe20f9a68a14b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:32:28', null, null);
INSERT INTO `sys_log` VALUES ('2d48a3b6d19e3ae261772ac6b063a4dc', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 07:32:28', null, null);
INSERT INTO `sys_log` VALUES ('a7da0a4e82b7d87e7ecbfe2f77a52618', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:32:31', null, null);
INSERT INTO `sys_log` VALUES ('86529b3cc72cead62a6d312bbf8037c4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:32:34', null, null);
INSERT INTO `sys_log` VALUES ('c6531f7515dcda51571f851de906eb28', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:32:37', null, null);
INSERT INTO `sys_log` VALUES ('611d1f955fcf5fd369cf4cf845eaf1bf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '19', 'admin', '2019-09-12 07:32:41', null, null);
INSERT INTO `sys_log` VALUES ('b2c4f129c69f0e3d1a4fed781d62fbe7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:32:42', null, null);
INSERT INTO `sys_log` VALUES ('fb9e7f41d5851b837dafd1078dd0f10c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:32:44', null, null);
INSERT INTO `sys_log` VALUES ('8db8e47f87dc363d143a499204e99854', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:32:46', null, null);
INSERT INTO `sys_log` VALUES ('9e79ed998e8b8b561301936bdbfde59f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:32:48', null, null);
INSERT INTO `sys_log` VALUES ('b0ff75ad9b74394f4ac9c4bf2a411d36', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '48', 'admin', '2019-09-12 07:32:50', null, null);
INSERT INTO `sys_log` VALUES ('9caf2813629a9b6451d7f8e58df82f2c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:32:52', null, null);
INSERT INTO `sys_log` VALUES ('d336d50bb13cf3a60dbd31a2e11f5774', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:32:53', null, null);
INSERT INTO `sys_log` VALUES ('27807de4adbd523e8e4816e11e640a7e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:32:55', null, null);
INSERT INTO `sys_log` VALUES ('6bd85dd5e83875e581a48013a1b83c32', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:32:58', null, null);
INSERT INTO `sys_log` VALUES ('f222ab7109537549aefa4d2287c7c7f0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:33:00', null, null);
INSERT INTO `sys_log` VALUES ('1a6db2c2a0f7cd72907cb1fd030b1292', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:33:07', null, null);
INSERT INTO `sys_log` VALUES ('c3f88148878361451130f47975afc616', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:33:07', null, null);
INSERT INTO `sys_log` VALUES ('da622e170bb160b16c4dc656265b6654', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 07:33:07', null, null);
INSERT INTO `sys_log` VALUES ('a028b92c853665abe002efb2a6e8ff44', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:33:09', null, null);
INSERT INTO `sys_log` VALUES ('132f817134240f07d8560d7aa7fb2ed4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 07:33:13', null, null);
INSERT INTO `sys_log` VALUES ('9bd0e7006bc1b9c4864201196bbdbba5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:33:14', null, null);
INSERT INTO `sys_log` VALUES ('268a5236ea47050969c54d554324c494', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:39:29', null, null);
INSERT INTO `sys_log` VALUES ('13b701c36594d51fd97c77769c1c8417', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:39:29', null, null);
INSERT INTO `sys_log` VALUES ('966a620fd3a4bfca950428b1e4c43d32', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:39:29', null, null);
INSERT INTO `sys_log` VALUES ('ef9a188513354b6e1b8c80f8cc26e485', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '38', 'admin', '2019-09-12 07:47:07', null, null);
INSERT INTO `sys_log` VALUES ('6d72feb523b14a7d8cfb1c6335cd0187', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '45', 'admin', '2019-09-12 07:47:07', null, null);
INSERT INTO `sys_log` VALUES ('3f0109f9d80487d82617d69444c6fb47', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:47:07', null, null);
INSERT INTO `sys_log` VALUES ('cf82808641cdbf5fee5832cb30b64ce4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 07:47:09', null, null);
INSERT INTO `sys_log` VALUES ('eee2e8cd0a2f7d6786e3a84b9a5d0be4', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:47:23', null, null);
INSERT INTO `sys_log` VALUES ('8fb134a58c260e2f66c9e23201c129fb', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:47:23', null, null);
INSERT INTO `sys_log` VALUES ('f1f9094a3ade037b679349ed6b249d2b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 07:47:23', null, null);
INSERT INTO `sys_log` VALUES ('a24c92d840d11d3de1f155d354a94f4e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:47:27', null, null);
INSERT INTO `sys_log` VALUES ('aac66c288c83d8dfc809eb6c57df9abe', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:47:29', null, null);
INSERT INTO `sys_log` VALUES ('67ddaa6cf618a25b283d5d98093ad9d5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 07:47:31', null, null);
INSERT INTO `sys_log` VALUES ('8759957248b808f7c378e597f72bde26', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:47:34', null, null);
INSERT INTO `sys_log` VALUES ('78a2d5e6c0c4d13acf59002f98230d88', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 07:47:39', null, null);
INSERT INTO `sys_log` VALUES ('c29da75e44cdd293299751ec510a448b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:47:42', null, null);
INSERT INTO `sys_log` VALUES ('7e3bd0b4c6b31049f012fcb2cce23483', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:48:36', null, null);
INSERT INTO `sys_log` VALUES ('97c871a3f7698f9c8a7a42f4cb7617e0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 07:48:36', null, null);
INSERT INTO `sys_log` VALUES ('845f6f0302016300f4d3594824995611', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:48:36', null, null);
INSERT INTO `sys_log` VALUES ('6babcc226160abdf9e38ed6146f34672', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:48:39', null, null);
INSERT INTO `sys_log` VALUES ('7ccb15f90172f9820b12e59337a3617d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:49:23', null, null);
INSERT INTO `sys_log` VALUES ('ab28ee08ffe3cd9b3a8bffbead36bfe5', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:49:23', null, null);
INSERT INTO `sys_log` VALUES ('226b9b54144b6238567c8efb36288ed0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 07:49:23', null, null);
INSERT INTO `sys_log` VALUES ('bac4b2e0710f9572fe3cf7feab86f412', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:51:02', null, null);
INSERT INTO `sys_log` VALUES ('2198fb430d9ee0d60010ca225b392b42', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:51:02', null, null);
INSERT INTO `sys_log` VALUES ('61d54caea9afc6fc775cd9cbabd84996', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 07:51:02', null, null);
INSERT INTO `sys_log` VALUES ('55c89ea821553e13852d90b54cec73ba', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:51:04', null, null);
INSERT INTO `sys_log` VALUES ('94928d9f9b2e136cb0111e67e08d8ebd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:51:08', null, null);
INSERT INTO `sys_log` VALUES ('a4ed3e29fe7f02284e708199094c63bc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:51:10', null, null);
INSERT INTO `sys_log` VALUES ('dae885b87bf1e454e444ea17b1697a9f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:51:13', null, null);
INSERT INTO `sys_log` VALUES ('4b713ba4eb7a1b6290c0196e9f4aa15d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:51:15', null, null);
INSERT INTO `sys_log` VALUES ('c12efa0b3215b485c1da18d21833ee81', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 07:52:22', null, null);
INSERT INTO `sys_log` VALUES ('d5d305da798fd729ae3ca4a643541152', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:52:22', null, null);
INSERT INTO `sys_log` VALUES ('48c026953388ecd6de1251f078bdb9fc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:52:22', null, null);
INSERT INTO `sys_log` VALUES ('e08a9fafcb1579dd342aea75fa9d8009', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:52:24', null, null);
INSERT INTO `sys_log` VALUES ('440f7b62f3f8a4d004fff6dc9a844b76', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:52:26', null, null);
INSERT INTO `sys_log` VALUES ('eabb705b134ad49910f7f88a9231cacc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:52:27', null, null);
INSERT INTO `sys_log` VALUES ('935a87768c1543fa3eb79ef3b57a63a1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:52:28', null, null);
INSERT INTO `sys_log` VALUES ('bfa45fa579613c53b147ad9219dc3c27', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:52:29', null, null);
INSERT INTO `sys_log` VALUES ('d9a80f4bc996f7ede7c55eb4320e3f45', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:52:32', null, null);
INSERT INTO `sys_log` VALUES ('0bc23d7e027b72b4705b6620a4f62f3b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:52:36', null, null);
INSERT INTO `sys_log` VALUES ('c852c519b64e4c3d2297096f26547ea8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:52:39', null, null);
INSERT INTO `sys_log` VALUES ('1750a7e30809746559bb0e7b7bb68115', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:52:41', null, null);
INSERT INTO `sys_log` VALUES ('8be0cb8dd5c7f24c401e2496ea9d6678', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:52:49', null, null);
INSERT INTO `sys_log` VALUES ('eda7e9a4e773f0c63867ace8fe590fac', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:52:49', null, null);
INSERT INTO `sys_log` VALUES ('988ded462739e0ab24af98384778fb25', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 07:52:49', null, null);
INSERT INTO `sys_log` VALUES ('196c299a610c00793384c53f6c5a42db', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:52:52', null, null);
INSERT INTO `sys_log` VALUES ('a00191e204e7d1a89ddd6e716e3255e7', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:54:13', null, null);
INSERT INTO `sys_log` VALUES ('4a49ac27b630f62844eea8f73fb78606', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:54:13', null, null);
INSERT INTO `sys_log` VALUES ('78a930bb0de3b806f667e4ea1986c995', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:54:13', null, null);
INSERT INTO `sys_log` VALUES ('68b5dcf2a345b7330887c84d05b326fe', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:54:24', null, null);
INSERT INTO `sys_log` VALUES ('eb5b0b04d93902bf3705cc28ebc50838', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:54:24', null, null);
INSERT INTO `sys_log` VALUES ('7cad5f4408923d35ec899afebc4e40b1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:54:24', null, null);
INSERT INTO `sys_log` VALUES ('40fc1ca582046694c3dcbdc176b4728f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:55:19', null, null);
INSERT INTO `sys_log` VALUES ('12721b92a42217690112eaf8362b647b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:55:19', null, null);
INSERT INTO `sys_log` VALUES ('567533a64833da214f7e30fcd96c4593', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:55:19', null, null);
INSERT INTO `sys_log` VALUES ('8b0e021716857663c652a4a636e32824', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 07:56:01', null, null);
INSERT INTO `sys_log` VALUES ('ea5ffd6b5101dc1e25aa58de62e940dd', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:56:01', null, null);
INSERT INTO `sys_log` VALUES ('6349833c790491dfa0af19c3db7565b9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 07:56:01', null, null);
INSERT INTO `sys_log` VALUES ('b19470dcca1ce8c4bae87320ef775a92', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:56:08', null, null);
INSERT INTO `sys_log` VALUES ('10cc54ba577da15c095a18f114e94d1a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 07:56:08', null, null);
INSERT INTO `sys_log` VALUES ('ce472430beb258876fba48216822276d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:56:08', null, null);
INSERT INTO `sys_log` VALUES ('3029f67518d38895587a4e7268f2d304', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 07:56:15', null, null);
INSERT INTO `sys_log` VALUES ('fd4bcd689ff83204a89abe09e6e2b3f7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:56:19', null, null);
INSERT INTO `sys_log` VALUES ('7706e6bbfd87f52daba766663d3316d0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '21', 'admin', '2019-09-12 07:56:23', null, null);
INSERT INTO `sys_log` VALUES ('d96577edc76e30113870f30557725ad5', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:56:24', null, null);
INSERT INTO `sys_log` VALUES ('17b61cc5c09374f42dd83b649ba44329', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:56:25', null, null);
INSERT INTO `sys_log` VALUES ('1ac2302fa7276cc6a62f0582f6268967', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:56:26', null, null);
INSERT INTO `sys_log` VALUES ('809a18d61636a6920f70bbad4bf97f37', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:57:25', null, null);
INSERT INTO `sys_log` VALUES ('517edce8dee8b75d8e51f7e675fb1283', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 07:57:25', null, null);
INSERT INTO `sys_log` VALUES ('db38a2e5f384ee8ad737ed1502c78ecf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:57:25', null, null);
INSERT INTO `sys_log` VALUES ('343cd57ac3c6086d149394ecdfd1f022', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '20', 'admin', '2019-09-12 07:57:33', null, null);
INSERT INTO `sys_log` VALUES ('c6951d44cd49219d021fde6e2fa7c01f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 07:58:03', null, null);
INSERT INTO `sys_log` VALUES ('e861fb9a730208bfc32c354c9adf65b1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 07:58:03', null, null);
INSERT INTO `sys_log` VALUES ('6d3faca6876056326d69e6cac9d6a814', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 07:58:03', null, null);
INSERT INTO `sys_log` VALUES ('689d9e62733b66c12a5eac60bfffae43', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 07:58:12', null, null);
INSERT INTO `sys_log` VALUES ('f5c7b1ff01349995daefa6e17519c09e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:58:19', null, null);
INSERT INTO `sys_log` VALUES ('c500e464b5e863eb44d56d157ee079ea', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 07:58:29', null, null);
INSERT INTO `sys_log` VALUES ('b00a0ea672d16b2c93fc50d56e61be5c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 07:58:35', null, null);
INSERT INTO `sys_log` VALUES ('e49a491c89c5724a367eb5bebcf555e6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 07:58:38', null, null);
INSERT INTO `sys_log` VALUES ('20338a8f5b68aa8c9c4b1673e3770c30', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '42', 'admin', '2019-09-12 08:00:29', null, null);
INSERT INTO `sys_log` VALUES ('0081263b2e6ef1ddf8da2ca3380c7f38', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '45', 'admin', '2019-09-12 08:00:29', null, null);
INSERT INTO `sys_log` VALUES ('267557e31d44d749afa5ddc2463afb7c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 08:00:29', null, null);
INSERT INTO `sys_log` VALUES ('7fe0dbda2039dc31ea7eea4568d131c7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:00:34', null, null);
INSERT INTO `sys_log` VALUES ('de736016eb5e65bc3c2f04db9914e075', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 08:00:36', null, null);
INSERT INTO `sys_log` VALUES ('4477dc7d0b90f411a09b0d374cc9572c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:00:37', null, null);
INSERT INTO `sys_log` VALUES ('4718f4f2a3ae3e94a0a4261838a01e69', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 08:00:38', null, null);
INSERT INTO `sys_log` VALUES ('1e6428bc1176828c33afb6ce61c1cc34', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:00:39', null, null);
INSERT INTO `sys_log` VALUES ('b24c33d204cc27c4780fed3407292c47', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '499', 'admin', '2019-09-12 08:00:42', null, null);
INSERT INTO `sys_log` VALUES ('1f5581ef76978b7266e7f6ea1c37e8c3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '265', 'admin', '2019-09-12 08:00:44', null, null);
INSERT INTO `sys_log` VALUES ('729205e054d81011d813b372c0c295f8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '5908', 'admin', '2019-09-12 08:00:54', null, null);
INSERT INTO `sys_log` VALUES ('ff754f47483fbf0b0a956b2ba66631d1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '44', 'admin', '2019-09-12 08:01:19', null, null);
INSERT INTO `sys_log` VALUES ('a206b1b2449333f0f3728b7bd9a74a4d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '42', 'admin', '2019-09-12 08:01:19', null, null);
INSERT INTO `sys_log` VALUES ('e2d060874bb997c460bb6db792f3404f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '310', 'admin', '2019-09-12 08:01:19', null, null);
INSERT INTO `sys_log` VALUES ('62fd0dfda069bb4734c0e0746f22f4fa', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '42', 'admin', '2019-09-12 08:01:23', null, null);
INSERT INTO `sys_log` VALUES ('25f9008f1c77ccfae81d90990b39094a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '711', 'admin', '2019-09-12 08:01:27', null, null);
INSERT INTO `sys_log` VALUES ('67ce68d380103a2c7c8bf23de7434b57', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '39', 'admin', '2019-09-12 08:01:29', null, null);
INSERT INTO `sys_log` VALUES ('6d8988566f06d94c87aaa96872cf5ff4', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '44', 'admin', '2019-09-12 08:01:30', null, null);
INSERT INTO `sys_log` VALUES ('b716e0bf3a81c0e8629276fe0aa7de13', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '39', 'admin', '2019-09-12 08:01:32', null, null);
INSERT INTO `sys_log` VALUES ('7af0fa4995e1f87c04e4e16182972d96', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 08:01:37', null, null);
INSERT INTO `sys_log` VALUES ('1148606dfc2d971b017bda39a88a83b2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:01:39', null, null);
INSERT INTO `sys_log` VALUES ('5d1ef5f87966e7a933951591e95177d8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:01:43', null, null);
INSERT INTO `sys_log` VALUES ('05a7b3a9708fb006cc677bb9bb52927e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 08:01:45', null, null);
INSERT INTO `sys_log` VALUES ('f711aeb8f318637cc201e7c8ed97b588', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:01:47', null, null);
INSERT INTO `sys_log` VALUES ('10c12c4b69913023f74fbe6a33bfdb9d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 08:01:49', null, null);
INSERT INTO `sys_log` VALUES ('c714eaafd275217f1db41389aa508e85', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '20', 'admin', '2019-09-12 08:01:52', null, null);
INSERT INTO `sys_log` VALUES ('59c3c2276655639927b18f209858ae47', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 08:01:53', null, null);
INSERT INTO `sys_log` VALUES ('c07952a661984f23eff8863600c51856', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 08:01:57', null, null);
INSERT INTO `sys_log` VALUES ('71746fa709b10e12a8f6e612668bf09f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 08:01:59', null, null);
INSERT INTO `sys_log` VALUES ('eaf7ed84d3efd23d3746d48d7b92a879', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 08:02:04', null, null);
INSERT INTO `sys_log` VALUES ('74a9e7ba7b1928dcd3d4dc460dd9c579', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '95', 'admin', '2019-09-12 08:02:05', null, null);
INSERT INTO `sys_log` VALUES ('2566270a552357d51e406e47975c5b0a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 08:02:07', null, null);
INSERT INTO `sys_log` VALUES ('58d3aaf95089e95670f2ce66f77c76a1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 08:02:12', null, null);
INSERT INTO `sys_log` VALUES ('bd8779538f9a7da663557a8e31a879a8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 08:02:14', null, null);
INSERT INTO `sys_log` VALUES ('055c33cb6e8eadb3430f871f47382341', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 08:02:20', null, null);
INSERT INTO `sys_log` VALUES ('8b9249c6e1e52617db758db771b64f81', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:02:20', null, null);
INSERT INTO `sys_log` VALUES ('1650d9b999cc74c2aba56e91db84f08b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 08:02:20', null, null);
INSERT INTO `sys_log` VALUES ('a89be4b47c0abb1b62e4ee21a0984596', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:02:24', null, null);
INSERT INTO `sys_log` VALUES ('9baba86f0daccf29d73258e0bc47afc6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:02:27', null, null);
INSERT INTO `sys_log` VALUES ('e7bff659dc82cc1c7efef9f15299620f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 08:02:28', null, null);
INSERT INTO `sys_log` VALUES ('3cf5ea6bcae748622edc65508a0096c1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 08:02:30', null, null);
INSERT INTO `sys_log` VALUES ('552f49be811ea3458b5a90228ee64ec3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 08:02:31', null, null);
INSERT INTO `sys_log` VALUES ('c958ba5d7b8c46ae30a2f49af032d9f9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:02:32', null, null);
INSERT INTO `sys_log` VALUES ('d7cfdb50ac7c41e53ee9c83d459fb85f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 08:02:33', null, null);
INSERT INTO `sys_log` VALUES ('73c541c76953bb4314cff146f83b9bdc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 08:02:35', null, null);
INSERT INTO `sys_log` VALUES ('cd991191c207f10497c2f37348e0bea7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:02:38', null, null);
INSERT INTO `sys_log` VALUES ('144905e3a3f151c325e15ce7e4f0457c', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 08:02:40', null, null);
INSERT INTO `sys_log` VALUES ('c42bd04750c22d9eaaa5425d6f245ba4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 08:02:41', null, null);
INSERT INTO `sys_log` VALUES ('29c1de04e18f81f39168a9806a2f901b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 08:02:47', null, null);
INSERT INTO `sys_log` VALUES ('3a88659f6ccac580a0a43d5aa39f6153', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:02:47', null, null);
INSERT INTO `sys_log` VALUES ('51e533ae7e528adc6fa5fa5f0f3f3141', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:02:47', null, null);
INSERT INTO `sys_log` VALUES ('cb87720a038dd090670d0b2d77f2837e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 08:02:50', null, null);
INSERT INTO `sys_log` VALUES ('38d520e1ac97aae1144472b3255b315c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '333', 'admin', '2019-09-12 08:11:10', null, null);
INSERT INTO `sys_log` VALUES ('798608ca960acbfaaa4570063043a80a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '366', 'admin', '2019-09-12 08:11:10', null, null);
INSERT INTO `sys_log` VALUES ('cf91191a4c5a5e23a7773edc5cf62a23', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '339', 'admin', '2019-09-12 08:11:10', null, null);
INSERT INTO `sys_log` VALUES ('467302279bccf8ec2344053ef0c25992', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '32', 'admin', '2019-09-12 08:11:21', null, null);
INSERT INTO `sys_log` VALUES ('bca051cbba1da3a250a8e06298559108', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '41', 'admin', '2019-09-12 08:11:21', null, null);
INSERT INTO `sys_log` VALUES ('e6a85b39d0732e58746a08fa610fdf02', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '75', 'admin', '2019-09-12 08:11:21', null, null);
INSERT INTO `sys_log` VALUES ('7d10f43ccd2a0edefef29cddff6213a8', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 08:18:16', null, null);
INSERT INTO `sys_log` VALUES ('f8f124dd806bc221d88c523ed3598944', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 08:18:16', null, null);
INSERT INTO `sys_log` VALUES ('1cc62b0e8c014336c11ff008b77c1388', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '52', 'admin', '2019-09-12 08:18:16', null, null);
INSERT INTO `sys_log` VALUES ('f001451d0975fa8cf139d01a63bfff83', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '72', 'admin', '2019-09-12 08:18:17', null, null);
INSERT INTO `sys_log` VALUES ('f2e89ccfbcff5b70716b3038f3073670', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 08:18:25', null, null);
INSERT INTO `sys_log` VALUES ('cd49fac6a79ff877ae00c421fcfc757d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:18:25', null, null);
INSERT INTO `sys_log` VALUES ('d25006a3482ff0e16de7b36615138b85', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '45', 'admin', '2019-09-12 08:18:25', null, null);
INSERT INTO `sys_log` VALUES ('bba0488b501482b841c8db8018803b88', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '53', 'admin', '2019-09-12 08:18:25', null, null);
INSERT INTO `sys_log` VALUES ('10c3a9942b0907a8e05690f943700196', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '49', 'admin', '2019-09-12 08:22:36', null, null);
INSERT INTO `sys_log` VALUES ('62197a5ba482602ab428cd8c91ad4669', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '54', 'admin', '2019-09-12 08:22:36', null, null);
INSERT INTO `sys_log` VALUES ('c6a05c9b13a196a24b46aac1fc6e75ce', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '64', 'admin', '2019-09-12 08:22:36', null, null);
INSERT INTO `sys_log` VALUES ('a43d97b1baed6efe0967ecba575fcdb5', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '80', 'admin', '2019-09-12 08:22:36', null, null);
INSERT INTO `sys_log` VALUES ('fb2f42c7866f5619d12482648c9e6c2c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '21', 'admin', '2019-09-12 08:22:41', null, null);
INSERT INTO `sys_log` VALUES ('472be2041ac5eb2814bb0d2c6e094d11', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 08:22:43', null, null);
INSERT INTO `sys_log` VALUES ('09a827fa1500bf2ed82b5c9c76968c29', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '1007', 'admin', '2019-09-12 08:22:49', null, null);
INSERT INTO `sys_log` VALUES ('06e86d8122636e26f182f454c7b45efb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '39', 'admin', '2019-09-12 08:23:50', null, null);
INSERT INTO `sys_log` VALUES ('faa75b1fc6fa77c2416a4e04091ab81b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '71', 'admin', '2019-09-12 08:23:50', null, null);
INSERT INTO `sys_log` VALUES ('8979da0f20364b7b47edd95634218fcc', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 08:23:50', null, null);
INSERT INTO `sys_log` VALUES ('6a2faa27db203ae4285689dd1aa265bd', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '60', 'admin', '2019-09-12 08:23:50', null, null);
INSERT INTO `sys_log` VALUES ('923910c5aa620f3c0a032d1a77847a34', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 08:23:56', null, null);
INSERT INTO `sys_log` VALUES ('1db95641e51597abccc12cf4d0d7d4bb', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 08:24:00', null, null);
INSERT INTO `sys_log` VALUES ('d8ce8b4c257dd319d08ab058c40fab1c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '42', 'admin', '2019-09-12 08:24:42', null, null);
INSERT INTO `sys_log` VALUES ('b87a96ccc738bf5039a6e402fc4e15fe', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '67', 'admin', '2019-09-12 08:24:42', null, null);
INSERT INTO `sys_log` VALUES ('b63e8d007de1927884f597bd042aeef1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 08:24:42', null, null);
INSERT INTO `sys_log` VALUES ('9ee7ce176e750405c191ced821342976', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '66', 'admin', '2019-09-12 08:24:42', null, null);
INSERT INTO `sys_log` VALUES ('974ac6d2cfca59cc20a9405eaa769a91', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:24:46', null, null);
INSERT INTO `sys_log` VALUES ('1781432aa97e8904936789f33c25f4f5', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '132', 'admin', '2019-09-12 08:26:02', null, null);
INSERT INTO `sys_log` VALUES ('305adfedfb58c406ef3d72c99e3d8a55', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '102', 'admin', '2019-09-12 08:26:02', null, null);
INSERT INTO `sys_log` VALUES ('ac47ba21925a9028b7dc1ebfc4c102e1', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '170', 'admin', '2019-09-12 08:26:02', null, null);
INSERT INTO `sys_log` VALUES ('8bb3e12bd6aa9f89dd937a23bb3f22f9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '171', 'admin', '2019-09-12 08:26:02', null, null);
INSERT INTO `sys_log` VALUES ('4444584cd09704f4d782fe648acd81bc', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 08:26:06', null, null);
INSERT INTO `sys_log` VALUES ('d66f232facd472673275ce71c3f0a2e3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 08:27:25', null, null);
INSERT INTO `sys_log` VALUES ('db84f2a15c5bc2c97b3e298c7143410a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '77', 'admin', '2019-09-12 08:27:25', null, null);
INSERT INTO `sys_log` VALUES ('beb8ea97f8829310d1e82a14227cdc99', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '44', 'admin', '2019-09-12 08:27:25', null, null);
INSERT INTO `sys_log` VALUES ('496056ae235edcf9e2644256899713f6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '41', 'admin', '2019-09-12 08:27:25', null, null);
INSERT INTO `sys_log` VALUES ('e994b368768247bb615b8ae9806abe7a', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 08:27:28', null, null);
INSERT INTO `sys_log` VALUES ('7f10a02de3ff432a49243b3715f6b084', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:27:33', null, null);
INSERT INTO `sys_log` VALUES ('c9e7c812f3a0df29966bea0bd05a1bfa', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '46', 'admin', '2019-09-12 08:27:36', null, null);
INSERT INTO `sys_log` VALUES ('14182a34a4fe58d4ceafa735922574b7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 08:27:39', null, null);
INSERT INTO `sys_log` VALUES ('92d29e1345d4261a13cc81f8bfa2f341', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 08:28:02', null, null);
INSERT INTO `sys_log` VALUES ('ec782463a30514a5720133cdc52c5b53', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '32', 'admin', '2019-09-12 08:28:02', null, null);
INSERT INTO `sys_log` VALUES ('d695fdd5d813bd55f0419afb0170ffcf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '46', 'admin', '2019-09-12 08:28:02', null, null);
INSERT INTO `sys_log` VALUES ('149f884e27beb14c53e6afff78055402', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '43', 'admin', '2019-09-12 08:28:02', null, null);
INSERT INTO `sys_log` VALUES ('48ea8280567f37aa8e5fe3322a08a476', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 08:28:06', null, null);
INSERT INTO `sys_log` VALUES ('b73eb375cbbe49944f0f1887edaf702d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '53', 'admin', '2019-09-12 08:28:10', null, null);
INSERT INTO `sys_log` VALUES ('59313503d1dffa25f8937ef42d0d0c29', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '56', 'admin', '2019-09-12 08:28:12', null, null);
INSERT INTO `sys_log` VALUES ('1efcd7cd0a892ffc633cf9a95308885a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '50', 'admin', '2019-09-12 08:28:49', null, null);
INSERT INTO `sys_log` VALUES ('c4ec49a440076549985124fd80cd9010', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '48', 'admin', '2019-09-12 08:28:49', null, null);
INSERT INTO `sys_log` VALUES ('2e66967273c4f09065f0d16bcf73a23b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '275', 'admin', '2019-09-12 08:28:49', null, null);
INSERT INTO `sys_log` VALUES ('6effa0bc55d275db4cc170b0f6aed81f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '522', 'admin', '2019-09-12 08:28:49', null, null);
INSERT INTO `sys_log` VALUES ('93bed8d721da2512df94803299a6d6c2', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '267', 'admin', '2019-09-12 08:28:51', null, null);
INSERT INTO `sys_log` VALUES ('de8e7e6780b4c5a755d1a33c639b162d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '728', 'admin', '2019-09-12 08:28:56', null, null);
INSERT INTO `sys_log` VALUES ('2fb9fab5ee4be94cdb70e1047619ac35', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '273', 'admin', '2019-09-12 08:29:00', null, null);
INSERT INTO `sys_log` VALUES ('b1b35bd7ab27d4bc7db9e8b258fe8403', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '40', 'admin', '2019-09-12 08:29:02', null, null);
INSERT INTO `sys_log` VALUES ('0520f208d00c0c4090b4b2f2069272d9', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '74', 'admin', '2019-09-12 08:32:57', null, null);
INSERT INTO `sys_log` VALUES ('b896e574228d2b480b66b7bd357debac', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '112', 'admin', '2019-09-12 08:32:57', null, null);
INSERT INTO `sys_log` VALUES ('3ac43f9c31e141bc6839a4ef5a2404cd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '90', 'admin', '2019-09-12 08:32:57', null, null);
INSERT INTO `sys_log` VALUES ('033005a1d10cd9cbc01fc7854db159ed', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '151', 'admin', '2019-09-12 08:35:20', null, null);
INSERT INTO `sys_log` VALUES ('a83a89ab903d80c0f8e4a980a30070f5', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '119', 'admin', '2019-09-12 08:35:20', null, null);
INSERT INTO `sys_log` VALUES ('bd1564722341abd32054a75eb06719cc', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '96', 'admin', '2019-09-12 08:35:20', null, null);
INSERT INTO `sys_log` VALUES ('3cb3a6b19eba2640f3d296474a345e0c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '101', 'admin', '2019-09-12 08:35:20', null, null);
INSERT INTO `sys_log` VALUES ('38e5ed7da0ee6d8cd6c00d902c22c9f3', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '728', 'admin', '2019-09-12 08:35:34', null, null);
INSERT INTO `sys_log` VALUES ('0de25b8a7e336e2c3e3f7bbfd3542baa', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '497', 'admin', '2019-09-12 08:35:37', null, null);
INSERT INTO `sys_log` VALUES ('96713fc2a93928361542ac99d8c03648', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '50', 'admin', '2019-09-12 08:35:48', null, null);
INSERT INTO `sys_log` VALUES ('a77985b37da23ed57792d39c38299e33', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '285', 'admin', '2019-09-12 08:35:52', null, null);
INSERT INTO `sys_log` VALUES ('6ef09db8d2a965d3d88d2f422cfd0e38', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '291', 'admin', '2019-09-12 08:37:22', null, null);
INSERT INTO `sys_log` VALUES ('fa95a5ff83d347311f03824091c6a63a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '299', 'admin', '2019-09-12 08:37:22', null, null);
INSERT INTO `sys_log` VALUES ('93c4970a88e1810b4ac90b8d39d5d91f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '312', 'admin', '2019-09-12 08:37:22', null, null);
INSERT INTO `sys_log` VALUES ('de89b833ab22f82190adb2d0647528fe', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '301', 'admin', '2019-09-12 08:37:22', null, null);
INSERT INTO `sys_log` VALUES ('75230e6d937320749c84ff9905bfab9f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 08:37:42', null, null);
INSERT INTO `sys_log` VALUES ('b4d24139ac4fdb9be90dee7edb7faa18', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '52', 'admin', '2019-09-12 08:37:42', null, null);
INSERT INTO `sys_log` VALUES ('d195cfa15735e5842fb0dc4446e015fe', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '48', 'admin', '2019-09-12 08:37:42', null, null);
INSERT INTO `sys_log` VALUES ('4ed9478d1c39a64a1ff80a44efc9e5f0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '73', 'admin', '2019-09-12 08:37:42', null, null);
INSERT INTO `sys_log` VALUES ('55d70572523faa4a5af2b1dabfa86de0', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-12 08:47:54', null, null);
INSERT INTO `sys_log` VALUES ('c6e8b178b8641c63e7fcda90961f12ce', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '260', 'admin', '2019-09-12 08:48:33', null, null);
INSERT INTO `sys_log` VALUES ('d32ff33b090400ac2a84ba5203b80a43', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '295', 'admin', '2019-09-12 08:58:32', null, null);
INSERT INTO `sys_log` VALUES ('3dc532182d09bda8c6dd77f5a4ac8097', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '55', 'admin', '2019-09-12 08:58:42', null, null);
INSERT INTO `sys_log` VALUES ('b031ed52e1366568bbaa0126e51db9c1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '244', 'admin', '2019-09-12 09:10:42', null, null);
INSERT INTO `sys_log` VALUES ('3a89d7e4532b376014c685dcf882c999', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '61', 'admin', '2019-09-12 09:11:28', null, null);
INSERT INTO `sys_log` VALUES ('757a0aaa8f19aaa21d2c8ff4e07c4a34', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '71', 'admin', '2019-09-12 09:11:44', null, null);
INSERT INTO `sys_log` VALUES ('ae7931b1d147156be5c4be47ccc09b9d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '43', 'admin', '2019-09-12 09:12:30', null, null);
INSERT INTO `sys_log` VALUES ('621e79278c572afc3cb5c5fcd3732e76', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '15', 'admin', '2019-09-12 09:12:36', null, null);
INSERT INTO `sys_log` VALUES ('72d41d66d36ff4a71ac8fdce80ead740', '1', '用户名: 管理员,退出成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-12 09:12:49', null, null);
INSERT INTO `sys_log` VALUES ('5277e928e4445172e358b448ef299ead', '1', '用户名: admin,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, 'jeecg', '2019-09-12 09:13:01', null, null);
INSERT INTO `sys_log` VALUES ('2b3530c56e33a98c06b6dd2a51a6765b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:13:19', null, null);
INSERT INTO `sys_log` VALUES ('0c75fbf66c91b46d5e24a954b68d08cf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '35', 'admin', '2019-09-12 09:13:19', null, null);
INSERT INTO `sys_log` VALUES ('7d58459fc5e202fc891902b8ef2c70d5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '63', 'admin', '2019-09-12 09:13:19', null, null);
INSERT INTO `sys_log` VALUES ('ed8e907a9ad80555d199564fc0b85501', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '50', 'admin', '2019-09-12 09:13:19', null, null);
INSERT INTO `sys_log` VALUES ('c6b140dd75069f8360bb8c111241c957', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:13:29', null, null);
INSERT INTO `sys_log` VALUES ('06ee2e64c45a4dfcc1b42e6fec330325', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '41', 'admin', '2019-09-12 09:13:31', null, null);
INSERT INTO `sys_log` VALUES ('4037bea503ed8101bd8faa40e69b3714', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 09:13:34', null, null);
INSERT INTO `sys_log` VALUES ('12408fde40281da35f963e6bf6d5638a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 09:13:39', null, null);
INSERT INTO `sys_log` VALUES ('14ab407866c877330e44f5a9080d45a8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:13:42', null, null);
INSERT INTO `sys_log` VALUES ('ac8a3be013ff1f0ea0207c75251a97a1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 09:13:46', null, null);
INSERT INTO `sys_log` VALUES ('982e58259ae7264bc338155a0acfde08', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '32', 'admin', '2019-09-12 09:13:49', null, null);
INSERT INTO `sys_log` VALUES ('29e19eb5159b7366092e092244ea52d2', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '34', 'admin', '2019-09-12 09:13:51', null, null);
INSERT INTO `sys_log` VALUES ('eced735812d56b9263f39eb82bad7371', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '35', 'admin', '2019-09-12 09:13:53', null, null);
INSERT INTO `sys_log` VALUES ('7a509d06e7e7ca587517141ff0fdb02d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '30', 'admin', '2019-09-12 09:13:55', null, null);
INSERT INTO `sys_log` VALUES ('bb94df27da5adf7dde6b1b7656760733', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '29', 'admin', '2019-09-12 09:13:59', null, null);
INSERT INTO `sys_log` VALUES ('d0fe9220f95305c5aa9848a9141bf7f4', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '27', 'admin', '2019-09-12 09:14:01', null, null);
INSERT INTO `sys_log` VALUES ('571b7ab8a15c81265a64d9e18cf3ce4f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:14:03', null, null);
INSERT INTO `sys_log` VALUES ('37a8ab651ccd96d1464cd1567c7c8bda', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:14:21', null, null);
INSERT INTO `sys_log` VALUES ('dcd62ac0f37ed826ee4a5cb9710ef915', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 09:14:27', null, null);
INSERT INTO `sys_log` VALUES ('41604484bf96429211426deb5af32ab3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:14:29', null, null);
INSERT INTO `sys_log` VALUES ('94700993d693a9b200f829aeed522555', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:14:32', null, null);
INSERT INTO `sys_log` VALUES ('48bd392ec80cdd132724065f03e107b8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:14:35', null, null);
INSERT INTO `sys_log` VALUES ('9b4b5690a1ae0f778b03d96e5126d72b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:16:21', null, null);
INSERT INTO `sys_log` VALUES ('ae163a22661bacae4fe71d4fac48e700', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 09:16:21', null, null);
INSERT INTO `sys_log` VALUES ('1bd44818eee1ad47d039444a66e3c0dd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '34', 'admin', '2019-09-12 09:16:21', null, null);
INSERT INTO `sys_log` VALUES ('5e35279deb23012e76179079baaeba81', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '38', 'admin', '2019-09-12 09:16:21', null, null);
INSERT INTO `sys_log` VALUES ('1bddda7d016ff7aef5cd33f108cfcba0', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:16:25', null, null);
INSERT INTO `sys_log` VALUES ('5dd9dab15fbcff80e63e05657204f8e7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:16:25', null, null);
INSERT INTO `sys_log` VALUES ('e24759e59dc98f664c9e131fb6e65948', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '32', 'admin', '2019-09-12 09:16:25', null, null);
INSERT INTO `sys_log` VALUES ('43bed547674270e0eeaaf10d97dd085c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '37', 'admin', '2019-09-12 09:16:25', null, null);
INSERT INTO `sys_log` VALUES ('da02666b7b3dfce1c63e226e1aa547b3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '31', 'admin', '2019-09-12 09:20:20', null, null);
INSERT INTO `sys_log` VALUES ('8d068e42d903290539a190fa4dbc3b86', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:20:20', null, null);
INSERT INTO `sys_log` VALUES ('ce0e5445fe2db822723ebed0595b5933', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 09:20:20', null, null);
INSERT INTO `sys_log` VALUES ('4b2a3cf14dfcbe6c5e4dd1ba4cc61da3', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:20:20', null, null);
INSERT INTO `sys_log` VALUES ('5dc52a2ea6f71b7a5d99d49bd0441b8f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '30', 'admin', '2019-09-12 09:20:22', null, null);
INSERT INTO `sys_log` VALUES ('bda54774f373d823a04d05b123e7c717', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '25', 'admin', '2019-09-12 09:20:24', null, null);
INSERT INTO `sys_log` VALUES ('42eee077d6623bbcdd86e20d6558079b', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '30', 'admin', '2019-09-12 09:20:26', null, null);
INSERT INTO `sys_log` VALUES ('d6c15563db149512b1d0c7dd7050ed66', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '30', 'admin', '2019-09-12 09:20:28', null, null);
INSERT INTO `sys_log` VALUES ('3c4d08ad20fc8fdcf6c102d7c394555d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:20:31', null, null);
INSERT INTO `sys_log` VALUES ('cee2452b5f6c6e0cac76b079b9544009', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 09:20:36', null, null);
INSERT INTO `sys_log` VALUES ('81b64054ffa1b8144ad83afdd59861ba', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:20:38', null, null);
INSERT INTO `sys_log` VALUES ('37503bf6e1bfe7e1dc3a242c627a0f12', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 09:20:42', null, null);
INSERT INTO `sys_log` VALUES ('8901305d0277e9b2f32dd96681c65521', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:20:44', null, null);
INSERT INTO `sys_log` VALUES ('d59f8ec89237080969d8a9c4a5d0d045', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '33', 'admin', '2019-09-12 09:20:46', null, null);
INSERT INTO `sys_log` VALUES ('7ea674da9d308f22600804a515943c73', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '25', 'admin', '2019-09-12 09:20:47', null, null);
INSERT INTO `sys_log` VALUES ('d14c06e3adba31ae5c0ffb27361f5f07', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:20:52', null, null);
INSERT INTO `sys_log` VALUES ('5027ec2ef0e3325fe214bc5a84132858', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '27', 'admin', '2019-09-12 09:20:53', null, null);
INSERT INTO `sys_log` VALUES ('5223102eff52ffc4db2c3f141171353e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '30', 'admin', '2019-09-12 09:20:54', null, null);
INSERT INTO `sys_log` VALUES ('93029a088ee80293567aa8419714934e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:20:58', null, null);
INSERT INTO `sys_log` VALUES ('136af155b8a1ab8a951e62ff7946ab6b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:21:02', null, null);
INSERT INTO `sys_log` VALUES ('cc290d0e68cff7bdefe73f3534175ea3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 09:21:04', null, null);
INSERT INTO `sys_log` VALUES ('ebc1959f60e0ff7c7e001dcee714314d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:21:05', null, null);
INSERT INTO `sys_log` VALUES ('a57e95243557bcec1751a948d3988bf5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 09:21:11', null, null);
INSERT INTO `sys_log` VALUES ('53e7de91ddc6fe12421f2bf2d92f432b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:21:13', null, null);
INSERT INTO `sys_log` VALUES ('4e37cd19d40d7e800fe136c508dc33fd', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:15', null, null);
INSERT INTO `sys_log` VALUES ('2b6b7f2216def70d3139c318f1161e39', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:16', null, null);
INSERT INTO `sys_log` VALUES ('99898ca007618bcf0e6479f302cb42ed', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:19', null, null);
INSERT INTO `sys_log` VALUES ('f0a34a7c267bb10efe0249012b47e5d7', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:22', null, null);
INSERT INTO `sys_log` VALUES ('f2114bf9df3216b1b69870527a7482d3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:21:24', null, null);
INSERT INTO `sys_log` VALUES ('096ef2b25bf40ed57dd6dcf30d1b5f37', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:21:25', null, null);
INSERT INTO `sys_log` VALUES ('23be8c13c0770ebdc134219032c38079', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:21:28', null, null);
INSERT INTO `sys_log` VALUES ('502f19abc7abba651cd5322166e5109a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:30', null, null);
INSERT INTO `sys_log` VALUES ('b6052864d3ff377522b8e4edb13d320e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '25', 'admin', '2019-09-12 09:21:40', null, null);
INSERT INTO `sys_log` VALUES ('d1c32b561ec39c3e4683f1262cd46a3e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '27', 'admin', '2019-09-12 09:21:42', null, null);
INSERT INTO `sys_log` VALUES ('0e295b8945b39d08cf287df59dfb830b', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '24', 'admin', '2019-09-12 09:21:43', null, null);
INSERT INTO `sys_log` VALUES ('7d563e7003d62e63f12ba165892fe274', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '24', 'admin', '2019-09-12 09:21:44', null, null);
INSERT INTO `sys_log` VALUES ('57841b32eb20fa7cc2c444157b9898b0', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:21:46', null, null);
INSERT INTO `sys_log` VALUES ('9d8329f9cd174a6ac236d7280e5cc92e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:21:49', null, null);
INSERT INTO `sys_log` VALUES ('3233993a3e562dec06e2df0c98891ce3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:21:54', null, null);
INSERT INTO `sys_log` VALUES ('28c5f62175f37fe594edf54b80e87820', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '22', 'admin', '2019-09-12 09:21:57', null, null);
INSERT INTO `sys_log` VALUES ('f76b351226f42c941bb9502cc871f4c1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:21:59', null, null);
INSERT INTO `sys_log` VALUES ('2dbeadacb443379230403415e566cc7d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:22:00', null, null);
INSERT INTO `sys_log` VALUES ('c0b5e7db3549fc61f1b37d7fc5edcb32', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '26', 'admin', '2019-09-12 09:22:02', null, null);
INSERT INTO `sys_log` VALUES ('ae5397c966312c645b8132401c0684ae', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:22:03', null, null);
INSERT INTO `sys_log` VALUES ('1dc56341b0e214b5d2807216cc83c3f7', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 09:30:09', null, null);
INSERT INTO `sys_log` VALUES ('134b4f9148fa3b17d2a4c9a5fa4bdac3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:30:09', null, null);
INSERT INTO `sys_log` VALUES ('96cfeea0c122690d266169b956d24876', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:30:09', null, null);
INSERT INTO `sys_log` VALUES ('ba3d2730029eed28da7909468a849aad', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '29', 'admin', '2019-09-12 09:30:09', null, null);
INSERT INTO `sys_log` VALUES ('33c28948745d701e7304436f13a19e80', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:30:12', null, null);
INSERT INTO `sys_log` VALUES ('4f819c32bd61ef67807e04fbc7511ca8', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '32', 'admin', '2019-09-12 09:30:13', null, null);
INSERT INTO `sys_log` VALUES ('f3fb0c5575dc76f72b51a9f1f42aa9df', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:30:14', null, null);
INSERT INTO `sys_log` VALUES ('f43e9189925851f7503a855d98c2afed', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '25', 'admin', '2019-09-12 09:30:17', null, null);
INSERT INTO `sys_log` VALUES ('e85455abd84cc4d75e2e354784a46e1d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '48', 'admin', '2019-09-12 09:30:19', null, null);
INSERT INTO `sys_log` VALUES ('8c73cd93ae312d04b58ca1e03863db6c', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '36', 'admin', '2019-09-12 09:30:21', null, null);
INSERT INTO `sys_log` VALUES ('5317edeabf6b6171caf271aed1a98866', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:30:22', null, null);
INSERT INTO `sys_log` VALUES ('683e89a4c6ac4f5e20e071d96746f825', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '23', 'admin', '2019-09-12 09:30:24', null, null);
INSERT INTO `sys_log` VALUES ('119993843c124942647ce9f447ef3a18', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '24', 'admin', '2019-09-12 09:30:26', null, null);
INSERT INTO `sys_log` VALUES ('7e574f1aebe1786d3c9b0ed73f1fb29f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '23', 'admin', '2019-09-12 09:30:28', null, null);
INSERT INTO `sys_log` VALUES ('c278606f21c6d20fcf03f30efefa90e4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:30:33', null, null);
INSERT INTO `sys_log` VALUES ('30b6fdf8281198255591bb8f5c7df28b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:30:35', null, null);
INSERT INTO `sys_log` VALUES ('084df25490dcc576894b9ffa4f1f73a5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '41', 'admin', '2019-09-12 09:30:37', null, null);
INSERT INTO `sys_log` VALUES ('330d7503b60c926de5260d507e14eb76', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '25', 'admin', '2019-09-12 09:30:39', null, null);
INSERT INTO `sys_log` VALUES ('619d6d761759533244b85929fd12643c', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:30:42', null, null);
INSERT INTO `sys_log` VALUES ('87f81373d638f19410ad72cb46beb412', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '29', 'admin', '2019-09-12 09:30:44', null, null);
INSERT INTO `sys_log` VALUES ('339a82365fd11a538b5d66a8f7812df1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:30:48', null, null);
INSERT INTO `sys_log` VALUES ('f4550f88ed2caa4119624771b63035ef', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '24', 'admin', '2019-09-12 09:30:52', null, null);
INSERT INTO `sys_log` VALUES ('81ef4700d2dac56cc8f3b467a006fa51', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '33', 'admin', '2019-09-12 09:30:54', null, null);
INSERT INTO `sys_log` VALUES ('eb8abc527d8637972fdd3dbffb66e61a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '35', 'admin', '2019-09-12 09:30:57', null, null);
INSERT INTO `sys_log` VALUES ('f366f653a2992e9d2822856a42c3001e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '23', 'admin', '2019-09-12 09:31:00', null, null);
INSERT INTO `sys_log` VALUES ('81bc15ca95c325b1dd8b303c194da09c', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '22', 'admin', '2019-09-12 09:31:01', null, null);
INSERT INTO `sys_log` VALUES ('dd8151a2ebc09e9f0f346236af7e94d8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:31:10', null, null);
INSERT INTO `sys_log` VALUES ('b6b45f08ca0312a3603cb99b8f560491', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '22', 'admin', '2019-09-12 09:31:12', null, null);
INSERT INTO `sys_log` VALUES ('236891ceafa21ad72679883c440d4a56', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '40', 'admin', '2019-09-12 09:31:15', null, null);
INSERT INTO `sys_log` VALUES ('27d026cf39a02db9acd4057750ff2069', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '20', 'admin', '2019-09-12 09:31:16', null, null);
INSERT INTO `sys_log` VALUES ('8169b8a723a00a15af6808965b852b50', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:31:18', null, null);
INSERT INTO `sys_log` VALUES ('09bc0c9b387f40bdb6d63701d0b9b124', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 09:31:21', null, null);
INSERT INTO `sys_log` VALUES ('0a4c873f3c0d5f7d4389adc448345b4a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:31:27', null, null);
INSERT INTO `sys_log` VALUES ('679691c1e8596dc38f2258f21beccc07', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 09:31:32', null, null);
INSERT INTO `sys_log` VALUES ('c5d398394cf8adb7a130aea2a09fd345', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 09:31:34', null, null);
INSERT INTO `sys_log` VALUES ('35e04eded0297d526aa0086bb52bc530', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:31:35', null, null);
INSERT INTO `sys_log` VALUES ('2afcc0025e1756fce8e0ff162f5c083d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:31:37', null, null);
INSERT INTO `sys_log` VALUES ('66dbcf7ed48008eb2bfc831af05adead', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:31:38', null, null);
INSERT INTO `sys_log` VALUES ('b3e68d8fb07aa14f6634bedcaf4f6973', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:31:39', null, null);
INSERT INTO `sys_log` VALUES ('dda33bd97d648f44e14a4474d502568c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:31:40', null, null);
INSERT INTO `sys_log` VALUES ('8b720410f9b0862efc7cadc86a0dcabe', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:31:40', null, null);
INSERT INTO `sys_log` VALUES ('a4394c3181921766dc65dba9b6f89018', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:31:41', null, null);
INSERT INTO `sys_log` VALUES ('d9d5e5959ffed42346e801861602abd1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 09:31:41', null, null);
INSERT INTO `sys_log` VALUES ('cbcf5b8fe66ae74ab1b6fad279b5bd2f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 09:31:42', null, null);
INSERT INTO `sys_log` VALUES ('7b3f9846d85d7dffdea0e8c92cb584f5', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:31:43', null, null);
INSERT INTO `sys_log` VALUES ('1b6bb23447eab3a139a2504568225598', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '35', 'admin', '2019-09-12 09:31:43', null, null);
INSERT INTO `sys_log` VALUES ('73ed796fb8fd60146efcca8eb87dfd2a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:31:48', null, null);
INSERT INTO `sys_log` VALUES ('a461bb53a4a0272bd2a58022b71027c4', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:31:51', null, null);
INSERT INTO `sys_log` VALUES ('db677ffc58abd869d2fb6ee97aefe4de', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:31:53', null, null);
INSERT INTO `sys_log` VALUES ('7f51fdd5b1cb8bb5f28df7b919fd8211', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '26', 'admin', '2019-09-12 09:31:55', null, null);
INSERT INTO `sys_log` VALUES ('1d4ca67af33deeac7f1e5d96b13cc45d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:32:34', null, null);
INSERT INTO `sys_log` VALUES ('c8bd096c8b2aeabc5a3d17aefd00ff44', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 09:33:53', null, null);
INSERT INTO `sys_log` VALUES ('980e9515ca4b535670cb56ac4b36bc13', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '27', 'admin', '2019-09-12 09:33:53', null, null);
INSERT INTO `sys_log` VALUES ('2910beaf61b6aadcacba28d47827b993', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 09:33:53', null, null);
INSERT INTO `sys_log` VALUES ('ba076b4c697609197a095ecd122fbc49', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '29', 'admin', '2019-09-12 09:33:53', null, null);
INSERT INTO `sys_log` VALUES ('567a57f20241862ff7bb0a6e7ae53c21', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 09:34:16', null, null);
INSERT INTO `sys_log` VALUES ('74b1d1c8743f0fac5fb61adc275975cf', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 09:34:16', null, null);
INSERT INTO `sys_log` VALUES ('b13652f616016cd332b3701e1524d51f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 09:34:16', null, null);
INSERT INTO `sys_log` VALUES ('c007991e6b5c13ccb172bf1d20b9d090', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 09:34:16', null, null);
INSERT INTO `sys_log` VALUES ('60b98ac074ef818da1f6fd8725a477be', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '37', 'admin', '2019-09-12 09:34:33', null, null);
INSERT INTO `sys_log` VALUES ('c13b7d0a4e53c03d19e4e8f264b62bf8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '29', 'admin', '2019-09-12 09:37:02', null, null);
INSERT INTO `sys_log` VALUES ('d47e9b4885fdd7cdba8b3c52c7b6bb6a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'admin', '2019-09-12 09:37:02', null, null);
INSERT INTO `sys_log` VALUES ('b8c876b405b2f8ad85175df58aff2ae6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 09:37:02', null, null);
INSERT INTO `sys_log` VALUES ('b58dae39e8396d68514d3468b32fe31f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:37:02', null, null);
INSERT INTO `sys_log` VALUES ('fb2553e55d69525f2cb44a2936b1b0dd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '34', 'admin', '2019-09-12 09:37:18', null, null);
INSERT INTO `sys_log` VALUES ('96807326c3f8c4c2c7223113a52110fc', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:37:18', null, null);
INSERT INTO `sys_log` VALUES ('ca30414613ca5961e3ed5441943cae23', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '34', 'admin', '2019-09-12 09:37:18', null, null);
INSERT INTO `sys_log` VALUES ('40b5aabd21b536e57dff2ef2d17cb82f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:37:18', null, null);
INSERT INTO `sys_log` VALUES ('10b092b5e430f6947c23b3f84ba8fa70', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '316', 'admin', '2019-09-12 09:39:52', null, null);
INSERT INTO `sys_log` VALUES ('2d0e99ecb6d32850b37d929b3a1d26c7', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '227', 'admin', '2019-09-12 09:39:52', null, null);
INSERT INTO `sys_log` VALUES ('ae9271894ba595cac8b6a8616306f402', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '192', 'admin', '2019-09-12 09:39:52', null, null);
INSERT INTO `sys_log` VALUES ('53fdfb52f1b8c340546d93020a55c9eb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '208', 'admin', '2019-09-12 09:39:52', null, null);
INSERT INTO `sys_log` VALUES ('b6f359e092c3135add6b2bc31f2ecff1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '17', 'admin', '2019-09-12 09:40:09', null, null);
INSERT INTO `sys_log` VALUES ('a087137a7a191c088c3551651e5ddc1b', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '17', 'admin', '2019-09-12 09:40:09', null, null);
INSERT INTO `sys_log` VALUES ('49222c4030c7707aa6a28ae3a0e32f82', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '18', 'admin', '2019-09-12 09:40:09', null, null);
INSERT INTO `sys_log` VALUES ('c8dba7efd09a455f769d72142140885f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '17', 'admin', '2019-09-12 09:40:09', null, null);
INSERT INTO `sys_log` VALUES ('d303339762286a35a2a7207b140f01a9', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'admin', '2019-09-12 09:40:13', null, null);
INSERT INTO `sys_log` VALUES ('04211b838229ee9b61c228fbaa5b6311', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '16', 'admin', '2019-09-12 09:40:13', null, null);
INSERT INTO `sys_log` VALUES ('4c7f76d051504b9f79262ea60335d921', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'admin', '2019-09-12 09:40:13', null, null);
INSERT INTO `sys_log` VALUES ('f1bca0c33fb9f5c379cab901eee246c1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:40:13', null, null);
INSERT INTO `sys_log` VALUES ('7ee41891dfe9412cb1e8b2c4ebddb479', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '14', 'admin', '2019-09-12 09:40:17', null, null);
INSERT INTO `sys_log` VALUES ('aa1bb8c0c915711fc177979405f6156a', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'admin', '2019-09-12 09:40:17', null, null);
INSERT INTO `sys_log` VALUES ('06d542db70f70bcf13f05112ef48400e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'admin', '2019-09-12 09:40:17', null, null);
INSERT INTO `sys_log` VALUES ('0f76dbbc834129ce4eb4ecc2ec05f80f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '24', 'admin', '2019-09-12 09:40:17', null, null);
INSERT INTO `sys_log` VALUES ('9bd3baaa338f81875abdc5ded2ccd15e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '13', 'admin', '2019-09-12 09:40:22', null, null);
INSERT INTO `sys_log` VALUES ('98b3f010a2f711c2b30183929d984f2b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '13', 'admin', '2019-09-12 09:40:22', null, null);
INSERT INTO `sys_log` VALUES ('9d3ff9a6829926ae52150d0034c83867', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '13', 'admin', '2019-09-12 09:40:22', null, null);
INSERT INTO `sys_log` VALUES ('63cf88a72ceb1d13d2a2ca5201cc83a3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '32', 'admin', '2019-09-12 09:40:22', null, null);
INSERT INTO `sys_log` VALUES ('ec9ab764f4fb0a1331f3fad1139085c6', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '33', 'admin', '2019-09-12 09:40:30', null, null);
INSERT INTO `sys_log` VALUES ('89999b31fcd18565bded917aa680e64c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '47', 'admin', '2019-09-12 09:40:30', null, null);
INSERT INTO `sys_log` VALUES ('c53008766ef72613542cc699fe51ed6f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '58', 'admin', '2019-09-12 09:40:30', null, null);
INSERT INTO `sys_log` VALUES ('c6c7b8d8f2cd24c86b4dc783e80e10f1', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '63', 'admin', '2019-09-12 09:40:30', null, null);
INSERT INTO `sys_log` VALUES ('b68ff2e0787b0eb5cea6148f2e895123', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '30', 'admin', '2019-09-12 09:41:04', null, null);
INSERT INTO `sys_log` VALUES ('ab0a57fc2fd73880cbbfa1298b008bec', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '50', 'admin', '2019-09-12 09:41:04', null, null);
INSERT INTO `sys_log` VALUES ('0d96c2b7762ede5b0489ae6bf7d61755', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '70', 'admin', '2019-09-12 09:41:04', null, null);
INSERT INTO `sys_log` VALUES ('9f5dddd33221ea44db3e6d4e2a5896b6', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '58', 'admin', '2019-09-12 09:41:04', null, null);
INSERT INTO `sys_log` VALUES ('3ac3df1a4851d608dd02db4ce98fb424', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '50', 'admin', '2019-09-12 09:42:14', null, null);
INSERT INTO `sys_log` VALUES ('83b888a1d85e90d71d923628b75f711f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '55', 'admin', '2019-09-12 09:42:14', null, null);
INSERT INTO `sys_log` VALUES ('20dad598d02eb58d188ed9f5b8295f4a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '74', 'admin', '2019-09-12 09:42:14', null, null);
INSERT INTO `sys_log` VALUES ('b960156e039db83c1048af13241b7a5c', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '74', 'admin', '2019-09-12 09:42:14', null, null);
INSERT INTO `sys_log` VALUES ('aa8d73c72649926d0b179b7399e10b4c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:42:52', null, null);
INSERT INTO `sys_log` VALUES ('1b54d620ab80060c5477125056cacd97', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 09:42:52', null, null);
INSERT INTO `sys_log` VALUES ('54d24df74379fc91480d06e6bfe6d5bc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '40', 'admin', '2019-09-12 09:42:52', null, null);
INSERT INTO `sys_log` VALUES ('c662bd645c3a593993b7c83c75b710bb', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '41', 'admin', '2019-09-12 09:42:52', null, null);
INSERT INTO `sys_log` VALUES ('5ca1b6c11015e37d87f5b46afd010431', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '37', 'admin', '2019-09-12 09:43:14', null, null);
INSERT INTO `sys_log` VALUES ('41d18271db5790512dceec16d9d16f47', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '56', 'admin', '2019-09-12 09:43:14', null, null);
INSERT INTO `sys_log` VALUES ('7067678b43e86e080e939fa7bb0a3c1d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '63', 'admin', '2019-09-12 09:43:14', null, null);
INSERT INTO `sys_log` VALUES ('83ec6871abec97dd5de1ed1959e6cd82', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '68', 'admin', '2019-09-12 09:43:14', null, null);
INSERT INTO `sys_log` VALUES ('690b44e91947864ba6460278a9319acf', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 09:44:47', null, null);
INSERT INTO `sys_log` VALUES ('047abba6e14034e4dcf873ff3b2544eb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '32', 'admin', '2019-09-12 09:44:47', null, null);
INSERT INTO `sys_log` VALUES ('e795ec8ca40115f018f1ffba466b8493', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '41', 'admin', '2019-09-12 09:44:47', null, null);
INSERT INTO `sys_log` VALUES ('372ffcd12270f4c3383cad326af66d89', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '42', 'admin', '2019-09-12 09:44:47', null, null);
INSERT INTO `sys_log` VALUES ('ff10a9d5e832ff4fc4ff253cbddf2fe2', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 09:44:54', null, null);
INSERT INTO `sys_log` VALUES ('72acab94db0526d8b55b474904161ff1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '41', 'admin', '2019-09-12 09:44:54', null, null);
INSERT INTO `sys_log` VALUES ('05516f2983df6b9387138b52fea4863b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '36', 'admin', '2019-09-12 09:44:54', null, null);
INSERT INTO `sys_log` VALUES ('ca24fcd77e407e5834d341db07966577', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '40', 'admin', '2019-09-12 09:44:54', null, null);
INSERT INTO `sys_log` VALUES ('f5a370086a8904c16a665d6934ebec86', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 10:01:35', null, null);
INSERT INTO `sys_log` VALUES ('33f62eab70145893fec98156c0721888', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '44', 'admin', '2019-09-12 10:01:35', null, null);
INSERT INTO `sys_log` VALUES ('2bebc0e89180f07e498a97a16adb55e1', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '41', 'admin', '2019-09-12 10:01:35', null, null);
INSERT INTO `sys_log` VALUES ('62579b5b0bdf283e563a64b3f1ffaee1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '39', 'admin', '2019-09-12 10:01:35', null, null);
INSERT INTO `sys_log` VALUES ('3386d4f8da4f646c7416ed3689578258', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 10:03:40', null, null);
INSERT INTO `sys_log` VALUES ('10fb469a48e17db3b6e8967491469367', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'admin', '2019-09-12 10:03:40', null, null);
INSERT INTO `sys_log` VALUES ('27140710f79d2455056a419d7b42fdaf', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '43', 'admin', '2019-09-12 10:03:40', null, null);
INSERT INTO `sys_log` VALUES ('c2609464e398f5b5f82556d4a778b381', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 10:03:40', null, null);
INSERT INTO `sys_log` VALUES ('533314be2f75669cd38f6ec26ee86dcb', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 10:03:49', null, null);
INSERT INTO `sys_log` VALUES ('c2f3dd1cd05f1ee43a299e7830479b9d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '31', 'admin', '2019-09-12 10:03:49', null, null);
INSERT INTO `sys_log` VALUES ('ba4f109c9b5d17918f4de1245ce27cd0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '34', 'admin', '2019-09-12 10:03:49', null, null);
INSERT INTO `sys_log` VALUES ('c48ab868eb423ae7246b1d6d35675fee', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '45', 'admin', '2019-09-12 10:03:49', null, null);
INSERT INTO `sys_log` VALUES ('7fb4049fde60db2692c4362c7a45e707', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 10:03:55', null, null);
INSERT INTO `sys_log` VALUES ('f42e1d8020b8be399d74887fd0d7840c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '37', 'admin', '2019-09-12 10:03:55', null, null);
INSERT INTO `sys_log` VALUES ('5471631753e0c8fd1585f5731b4387bb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 10:03:55', null, null);
INSERT INTO `sys_log` VALUES ('c8c01ec257223a14f4afe873ed0831bb', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '41', 'admin', '2019-09-12 10:03:55', null, null);
INSERT INTO `sys_log` VALUES ('04e48148eb96163214243fdca61592e4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 10:07:32', null, null);
INSERT INTO `sys_log` VALUES ('2aee8fa9ad12a26034b29b168eb9afd0', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 10:07:32', null, null);
INSERT INTO `sys_log` VALUES ('21001e0aa59e2f18dd3b39d7be3068f8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 10:07:32', null, null);
INSERT INTO `sys_log` VALUES ('3e3f1d0b08b1b6abf62250eb7432b717', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '49', 'admin', '2019-09-12 10:07:32', null, null);
INSERT INTO `sys_log` VALUES ('0fdd6e066d7b12b44a6d13dfae0d08ed', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '29', 'admin', '2019-09-12 10:07:42', null, null);
INSERT INTO `sys_log` VALUES ('278a3ba5eff11f8e11594213a10713b6', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '35', 'admin', '2019-09-12 10:07:42', null, null);
INSERT INTO `sys_log` VALUES ('a84ec4948cb22126936b586794e2d77e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 10:07:42', null, null);
INSERT INTO `sys_log` VALUES ('64aa224c72af8f573845d543d984c24a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '48', 'admin', '2019-09-12 10:07:42', null, null);
INSERT INTO `sys_log` VALUES ('d8cd2b4c561916373d4f0e7f5929362f', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 10:08:50', null, null);
INSERT INTO `sys_log` VALUES ('4efc2f17e09fb5d513d5e2084e4b5d0e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 10:08:50', null, null);
INSERT INTO `sys_log` VALUES ('3033a0855785b0e0fb88ba7179083106', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '31', 'admin', '2019-09-12 10:08:50', null, null);
INSERT INTO `sys_log` VALUES ('138cb226ada88b2bb62c89c5af511b93', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '40', 'admin', '2019-09-12 10:08:50', null, null);
INSERT INTO `sys_log` VALUES ('2f97ffdbf99be6b1a921658c33da5896', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 10:09:29', null, null);
INSERT INTO `sys_log` VALUES ('282b75e4711faa910d9fafba8a20f3c2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '35', 'admin', '2019-09-12 10:09:29', null, null);
INSERT INTO `sys_log` VALUES ('ec102e27fc8a271849b9ffb5a756b9bb', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '41', 'admin', '2019-09-12 10:09:29', null, null);
INSERT INTO `sys_log` VALUES ('d6f156dbbb294059dc0d308aa056f90a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 10:09:29', null, null);
INSERT INTO `sys_log` VALUES ('729cfa6540a4a90bde9e60f72e2fde13', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '36', 'admin', '2019-09-12 10:09:49', null, null);
INSERT INTO `sys_log` VALUES ('dc8fc9c2fd2c56e7d0425f66742b6ae3', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '27', 'admin', '2019-09-12 10:09:49', null, null);
INSERT INTO `sys_log` VALUES ('eb87696ff78c60dbcace489f36976e72', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '33', 'admin', '2019-09-12 10:09:49', null, null);
INSERT INTO `sys_log` VALUES ('0dcda71a4985ad0d3312569434472aa0', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '36', 'admin', '2019-09-12 10:09:49', null, null);
INSERT INTO `sys_log` VALUES ('abfd3a728159e00a9dc7268203488558', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 10:10:03', null, null);
INSERT INTO `sys_log` VALUES ('b12fe5d03e5743f39e0a1818789c244b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardInfolist()', null, null, null, '28', 'admin', '2019-09-12 10:10:03', null, null);
INSERT INTO `sys_log` VALUES ('794d1018ead60ceb1b4edb5465ded64a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '32', 'admin', '2019-09-12 10:10:03', null, null);
INSERT INTO `sys_log` VALUES ('6da83cb424b828821d7c19d8d4cbe87e', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 10:10:03', null, null);
INSERT INTO `sys_log` VALUES ('10a1121634e6de0ef1c75807dc8efb9b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '42', 'admin', '2019-09-12 10:15:39', null, null);
INSERT INTO `sys_log` VALUES ('d836bc9c1ca5e69c8c68dfaef2fac467', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '33', 'admin', '2019-09-12 10:15:39', null, null);
INSERT INTO `sys_log` VALUES ('c2705e0b09efde0f9b46abd52e979595', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '36', 'admin', '2019-09-12 10:15:39', null, null);
INSERT INTO `sys_log` VALUES ('3d42a57d5bc64a138e7260190b3ade97', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '70', 'admin', '2019-09-12 10:24:53', null, null);
INSERT INTO `sys_log` VALUES ('953f56b0575b849f3a2d831cd630262b', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '143', 'admin', '2019-09-12 10:24:53', null, null);
INSERT INTO `sys_log` VALUES ('7da04f78358efbdc5ae3316a8ba5488a', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '119', 'admin', '2019-09-12 10:24:53', null, null);
INSERT INTO `sys_log` VALUES ('32e00a1af55b419b52232b105ef346d9', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '315', 'admin', '2019-09-12 10:26:42', null, null);
INSERT INTO `sys_log` VALUES ('54b644d89775a2472978c64a5da00a92', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '362', 'admin', '2019-09-12 10:26:42', null, null);
INSERT INTO `sys_log` VALUES ('00b204bdfa8a5b1a9e4665b2adc98e28', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '348', 'admin', '2019-09-12 10:26:42', null, null);
INSERT INTO `sys_log` VALUES ('85be6c703cc975c3952b9d22667439e9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '47', 'admin', '2019-09-12 10:26:45', null, null);
INSERT INTO `sys_log` VALUES ('27cafe562a36942bb4de8dcbd6fcd77a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '32', 'admin', '2019-09-12 10:26:51', null, null);
INSERT INTO `sys_log` VALUES ('24ed7b35c9f0b7451bea4353df955287', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '12', 'admin', '2019-09-12 10:26:55', null, null);
INSERT INTO `sys_log` VALUES ('0f7e25dd81dfabc2b624c09da544c94d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '34', 'admin', '2019-09-12 10:26:58', null, null);
INSERT INTO `sys_log` VALUES ('8824cba6db9b8410b3523ca1f2177724', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '22', 'admin', '2019-09-12 10:27:16', null, null);
INSERT INTO `sys_log` VALUES ('7ec9bb97f7ca579897415aaa45f318ca', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '13', 'admin', '2019-09-12 10:27:21', null, null);
INSERT INTO `sys_log` VALUES ('060a85b42312b4564b9ede516cf25741', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '14', 'admin', '2019-09-12 10:27:24', null, null);
INSERT INTO `sys_log` VALUES ('6e18ff571f25b69f7697f11787c7e0f2', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '22', 'admin', '2019-09-12 10:27:26', null, null);
INSERT INTO `sys_log` VALUES ('4e2f3551c72f751386b66f4d4f3a281d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '33', 'admin', '2019-09-12 10:27:28', null, null);
INSERT INTO `sys_log` VALUES ('97679e792ce8a0caf62da77786c34d30', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:27:38', null, null);
INSERT INTO `sys_log` VALUES ('4cbc8323a8ce06a7e62f54d651a1a43d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:27:40', null, null);
INSERT INTO `sys_log` VALUES ('1eaabdde7b268cc15ccd37cfe93381bf', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '13', 'admin', '2019-09-12 10:27:42', null, null);
INSERT INTO `sys_log` VALUES ('6fc5818de4911920aec78c41222b8854', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'admin', '2019-09-12 10:27:45', null, null);
INSERT INTO `sys_log` VALUES ('6c1c2d1736bc8a39fd87d03e52ffb5de', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '110', 'admin', '2019-09-12 10:27:45', null, null);
INSERT INTO `sys_log` VALUES ('d585c33ce21ae27951ca311babdd80be', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '111', 'admin', '2019-09-12 10:27:45', null, null);
INSERT INTO `sys_log` VALUES ('6e85dee50777138a8fbb71264afd990c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 10:30:40', null, null);
INSERT INTO `sys_log` VALUES ('a73e996986562014f2b5171df87d36d9', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '40', 'admin', '2019-09-12 10:30:40', null, null);
INSERT INTO `sys_log` VALUES ('588f5bfdec5c62cb73070190eb12b93a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '46', 'admin', '2019-09-12 10:30:40', null, null);
INSERT INTO `sys_log` VALUES ('e703dc227df197b8fdc620d62502a4a1', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '29', 'admin', '2019-09-12 10:30:44', null, null);
INSERT INTO `sys_log` VALUES ('4bc2e1bdbd8b10bbaa26d28968a02182', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '22', 'admin', '2019-09-12 10:30:49', null, null);
INSERT INTO `sys_log` VALUES ('7c0546d5c6319ee123ce46564a54ade9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '12', 'admin', '2019-09-12 10:30:51', null, null);
INSERT INTO `sys_log` VALUES ('06c284b881d81a1171c4babd5f3a779d', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '24', 'admin', '2019-09-12 10:30:53', null, null);
INSERT INTO `sys_log` VALUES ('8f360bbdf174b52eb9efcd07a5d53a19', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '13', 'admin', '2019-09-12 10:30:56', null, null);
INSERT INTO `sys_log` VALUES ('be32e5e2fdd304f784abbfd7f63da108', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '12', 'admin', '2019-09-12 10:30:59', null, null);
INSERT INTO `sys_log` VALUES ('eb3450e4192b2a3958416db8697d5569', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:31:02', null, null);
INSERT INTO `sys_log` VALUES ('b13646dd923fdd1cb1d07f9276961810', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '23', 'admin', '2019-09-12 10:31:04', null, null);
INSERT INTO `sys_log` VALUES ('b0375d7739516efea515b44677aacb38', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '26', 'admin', '2019-09-12 10:31:09', null, null);
INSERT INTO `sys_log` VALUES ('6801faa64cea8a5a53f885e11b234c4a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '10', 'admin', '2019-09-12 10:31:11', null, null);
INSERT INTO `sys_log` VALUES ('d91e24b1ff104ef7329e557ac2d21013', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '9', 'admin', '2019-09-12 10:31:12', null, null);
INSERT INTO `sys_log` VALUES ('1d2b7adf9e51c72a4b64647e879214cd', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:31:13', null, null);
INSERT INTO `sys_log` VALUES ('1c0d1b3c72f348a4c95adb058c21fae9', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '27', 'admin', '2019-09-12 10:31:16', null, null);
INSERT INTO `sys_log` VALUES ('c7964b32407e1839677f7e32528d99bc', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '25', 'admin', '2019-09-12 10:31:18', null, null);
INSERT INTO `sys_log` VALUES ('acbeebb3d61cfdc36f15f6cad7da2e8a', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:31:23', null, null);
INSERT INTO `sys_log` VALUES ('838f998baf69b352104067c09f3a1091', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '12', 'admin', '2019-09-12 10:31:24', null, null);
INSERT INTO `sys_log` VALUES ('4d3301a8971558457ab3421c19739017', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '10', 'admin', '2019-09-12 10:31:27', null, null);
INSERT INTO `sys_log` VALUES ('5cce71b8f4aa528bc05570e62538d33c', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:31:28', null, null);
INSERT INTO `sys_log` VALUES ('6880e1b66d5ec8fc553ff6be21b21d65', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '25', 'admin', '2019-09-12 10:31:40', null, null);
INSERT INTO `sys_log` VALUES ('16fc5dbc587edf3e43079d79aa2ced26', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 10:33:03', null, null);
INSERT INTO `sys_log` VALUES ('84f48acd117830182b8f890547889b51', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '295', 'admin', '2019-09-12 10:33:03', null, null);
INSERT INTO `sys_log` VALUES ('6d9f2c76a2aeba0f6479ece27de3aa07', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '299', 'admin', '2019-09-12 10:33:03', null, null);
INSERT INTO `sys_log` VALUES ('0add5406ca23a88f04201dcd6e5fd53f', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '25', 'admin', '2019-09-12 10:33:05', null, null);
INSERT INTO `sys_log` VALUES ('e446e694719033f8fb053c177c737a03', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '21', 'admin', '2019-09-12 10:33:09', null, null);
INSERT INTO `sys_log` VALUES ('1100393655e2b7c0b7cfdd80e0d68e76', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 10:34:26', null, null);
INSERT INTO `sys_log` VALUES ('6d160cef12514206fa5a774e215bdcd0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '35', 'admin', '2019-09-12 10:34:26', null, null);
INSERT INTO `sys_log` VALUES ('b7319aeb6e70ef25af8d28c873c88fa3', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '51', 'admin', '2019-09-12 10:34:26', null, null);
INSERT INTO `sys_log` VALUES ('6449cddde085fa80073105f322c6cc69', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '29', 'admin', '2019-09-12 10:34:29', null, null);
INSERT INTO `sys_log` VALUES ('aa8afcaffcd9c585368ee529fed90172', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '23', 'admin', '2019-09-12 10:34:36', null, null);
INSERT INTO `sys_log` VALUES ('b39eece5a3251c5631d63a5ceb92f120', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:34:38', null, null);
INSERT INTO `sys_log` VALUES ('b5b1015799bff3b1a811967f58f50e0e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '12', 'admin', '2019-09-12 10:34:40', null, null);
INSERT INTO `sys_log` VALUES ('2f8fcc3436e811ba69c57e674d730079', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '22', 'admin', '2019-09-12 10:34:51', null, null);
INSERT INTO `sys_log` VALUES ('797378da62555606ea838fcb568a9606', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '37', 'admin', '2019-09-12 10:34:51', null, null);
INSERT INTO `sys_log` VALUES ('679009bdb34ff265c60afa9c5407748d', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'admin', '2019-09-12 10:34:51', null, null);
INSERT INTO `sys_log` VALUES ('6752554f7b78e6092b0e1350218c2747', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '14', 'admin', '2019-09-12 10:35:00', null, null);
INSERT INTO `sys_log` VALUES ('3c9f0268405d6c8ac7df20af24dc0a37', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '12', 'admin', '2019-09-12 10:35:00', null, null);
INSERT INTO `sys_log` VALUES ('dc10f2b276281689b3fdcea2842a15d1', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '12', 'admin', '2019-09-12 10:35:00', null, null);
INSERT INTO `sys_log` VALUES ('15b770c8b3e3f556906f1d13bb0b3f87', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '11', 'admin', '2019-09-12 10:35:04', null, null);
INSERT INTO `sys_log` VALUES ('ab9db41e23a8aed4e3b231e840e9be78', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '11', 'admin', '2019-09-12 10:35:04', null, null);
INSERT INTO `sys_log` VALUES ('aefc9879b99df2a611dc693913f458c0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 10:35:04', null, null);
INSERT INTO `sys_log` VALUES ('caefaaedeffa87fac645d4e69179b017', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'admin', '2019-09-12 10:35:10', null, null);
INSERT INTO `sys_log` VALUES ('44eda4a0fdccb666c11366b72687599d', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'admin', '2019-09-12 10:35:10', null, null);
INSERT INTO `sys_log` VALUES ('29cfc6083f368aaee0365c24ab5ae9e3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 10:35:10', null, null);
INSERT INTO `sys_log` VALUES ('13d24808c67a88097ebd9a19d69ca7ef', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:35:12', null, null);
INSERT INTO `sys_log` VALUES ('4196ba4aad7fac1dfe1800db901602eb', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '122', 'admin', '2019-09-12 10:35:33', null, null);
INSERT INTO `sys_log` VALUES ('118871af06a1e87163afe8a83ac34d3c', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '129', 'admin', '2019-09-12 10:35:33', null, null);
INSERT INTO `sys_log` VALUES ('8762cdb499d9c6c00053dabb9e0fec2e', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '141', 'admin', '2019-09-12 10:35:33', null, null);
INSERT INTO `sys_log` VALUES ('b33ce0903e8057c870455a50f029980b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '13', 'admin', '2019-09-12 10:36:58', null, null);
INSERT INTO `sys_log` VALUES ('d1e7e0048f25d06bd781b2a2c14a41d3', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '11', 'admin', '2019-09-12 10:36:58', null, null);
INSERT INTO `sys_log` VALUES ('d92f0fc9265bd90615b584bc401a2eab', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '29', 'admin', '2019-09-12 10:36:58', null, null);
INSERT INTO `sys_log` VALUES ('90a8dc389bc53a935cac60dd8f8f58c7', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'admin', '2019-09-12 10:37:04', null, null);
INSERT INTO `sys_log` VALUES ('5c706349a307e2e76f676214adfaf0a3', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 10:37:04', null, null);
INSERT INTO `sys_log` VALUES ('871b73ff7d652f8960fb2de74394bf22', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '34', 'admin', '2019-09-12 10:37:04', null, null);
INSERT INTO `sys_log` VALUES ('c7756961a60a555145c229af994bdaa0', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '25', 'admin', '2019-09-12 10:37:15', null, null);
INSERT INTO `sys_log` VALUES ('92a72e4286ee241938038d0bdc26a545', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 10:37:15', null, null);
INSERT INTO `sys_log` VALUES ('be95dc9ec2854f8f1bff1490687c9a0a', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '33', 'admin', '2019-09-12 10:37:15', null, null);
INSERT INTO `sys_log` VALUES ('522f97fab21fb80d4498bedebbd1de2b', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '23', 'admin', '2019-09-12 10:37:49', null, null);
INSERT INTO `sys_log` VALUES ('7d9153d8f31bb0268f41253fd2fd340d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '36', 'admin', '2019-09-12 10:37:49', null, null);
INSERT INTO `sys_log` VALUES ('f3bd7eed10b70b21527ec89631a805c8', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '34', 'admin', '2019-09-12 10:37:49', null, null);
INSERT INTO `sys_log` VALUES ('0f461527c587cc4a843142d578973cd6', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '24', 'admin', '2019-09-12 10:38:26', null, null);
INSERT INTO `sys_log` VALUES ('94e1d9dec14ff22bf25c14f88d7b52aa', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '38', 'admin', '2019-09-12 10:38:26', null, null);
INSERT INTO `sys_log` VALUES ('cb1dea6495109ecc95bbcb462ec3571b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'admin', '2019-09-12 10:38:26', null, null);
INSERT INTO `sys_log` VALUES ('63871c34885c5d5ccdc7a18fbc72b3f8', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '23', 'admin', '2019-09-12 10:38:33', null, null);
INSERT INTO `sys_log` VALUES ('38a70e80938a7ded987b303a3e9168f4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '21', 'admin', '2019-09-12 10:38:58', null, null);
INSERT INTO `sys_log` VALUES ('007e8476d3e81e3cf8128722b04f4640', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '34', 'admin', '2019-09-12 10:38:58', null, null);
INSERT INTO `sys_log` VALUES ('49fc7a8c396e5c71334eb3341a35660b', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '35', 'admin', '2019-09-12 10:38:58', null, null);
INSERT INTO `sys_log` VALUES ('dd09ca10e01b82b2708d9aa0bdbeac88', '2', '机框-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '20', 'admin', '2019-09-12 10:39:02', null, null);
INSERT INTO `sys_log` VALUES ('a440c5406199bd1f619f0cf1f4d41b8e', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '31', 'admin', '2019-09-12 10:39:02', null, null);
INSERT INTO `sys_log` VALUES ('ee3d81d20e3e0f285ef12074785e45bf', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '127.0.0.1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '30', 'admin', '2019-09-12 10:39:02', null, null);
INSERT INTO `sys_log` VALUES ('e715db162552164120632ec4b0cee4f6', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 10:47:33', null, null);
INSERT INTO `sys_log` VALUES ('e03a9775b227cb6d00cd5c5f1e2dbce4', '2', '机框-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '59', 'admin', '2019-09-12 10:47:40', null, null);
INSERT INTO `sys_log` VALUES ('d0fbaf58720eb0a64e2c0b5d47a78400', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '60', 'admin', '2019-09-12 10:47:40', null, null);
INSERT INTO `sys_log` VALUES ('4f7efd805132623d6d3ac2d49ec084d5', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '94', 'admin', '2019-09-12 10:47:40', null, null);
INSERT INTO `sys_log` VALUES ('9eb73c78a14717afee238710cbefe41b', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '28', 'admin', '2019-09-12 10:47:53', null, null);
INSERT INTO `sys_log` VALUES ('09d9c8a063411e7cdeb21c562a915828', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '5', 'admin', '2019-09-12 10:47:56', null, null);
INSERT INTO `sys_log` VALUES ('9eb7400782f5d967d4420ff79f12cfad', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '4', 'admin', '2019-09-12 10:47:59', null, null);
INSERT INTO `sys_log` VALUES ('de270506d31d3049fed830bd51403feb', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '5', 'admin', '2019-09-12 10:48:00', null, null);
INSERT INTO `sys_log` VALUES ('527e17842ff558a9cb477c5a513e786e', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '10', 'admin', '2019-09-12 10:48:03', null, null);
INSERT INTO `sys_log` VALUES ('e30e56db0197448f7465bba5cf366228', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '17', 'admin', '2019-09-12 10:48:06', null, null);
INSERT INTO `sys_log` VALUES ('b659b12b3882e2e00f0679a52c6e0c34', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '11', 'admin', '2019-09-12 10:48:09', null, null);
INSERT INTO `sys_log` VALUES ('a156d2a3009322534a033d449c89d9cb', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 10:48:56', null, null);
INSERT INTO `sys_log` VALUES ('0ab2709920e22abc03c3b41b11d24b21', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 10:59:05', null, null);
INSERT INTO `sys_log` VALUES ('813effaaa814658de28f34f2ea1b0c13', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 11:00:03', null, null);
INSERT INTO `sys_log` VALUES ('ed6c6499e76fe9fd05d345ec385d0bf2', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 11:05:55', null, null);
INSERT INTO `sys_log` VALUES ('c96409c737319a430f811ddb55000891', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 11:07:38', null, null);
INSERT INTO `sys_log` VALUES ('b02d313fca1eae276ec1bfbd961316cb', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 11:10:23', null, null);
INSERT INTO `sys_log` VALUES ('d0bfee3cf5cac6c1fb170c0e465ecac0', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '18', 'test', '2019-09-12 11:10:55', null, null);
INSERT INTO `sys_log` VALUES ('0d57159904e4c3c799074f670e2c26b8', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '6', 'test', '2019-09-12 11:10:56', null, null);
INSERT INTO `sys_log` VALUES ('db8dd8cc6f3cc5ab1562d087374dbfde', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '29', 'test', '2019-09-12 11:10:56', null, null);
INSERT INTO `sys_log` VALUES ('d5fd8a2c9c8b66ab4c59522ff4a3f74a', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '5', 'test', '2019-09-12 11:11:02', null, null);
INSERT INTO `sys_log` VALUES ('5982c85241a64c864adb34787cc78d82', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '10', 'test', '2019-09-12 11:11:02', null, null);
INSERT INTO `sys_log` VALUES ('2c04afda63b3e84c368bc39c1eb9d854', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'test', '2019-09-12 11:11:02', null, null);
INSERT INTO `sys_log` VALUES ('ada9952ae0b7af84738b4954758b6e26', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '3', 'test', '2019-09-12 11:11:44', null, null);
INSERT INTO `sys_log` VALUES ('42fddd5873e6cdd9f77521f0c7206e49', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '2', 'test', '2019-09-12 11:11:49', null, null);
INSERT INTO `sys_log` VALUES ('16b5a018957ccc64684d288f05eceb08', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-12 11:12:23', null, null);
INSERT INTO `sys_log` VALUES ('5614e54be5c27476185c2ed22c415a86', '1', '用户名: 游客,退出成功！', null, null, null, '60.8.244.242', null, null, null, null, null, 'jeecg', '2019-09-14 02:47:59', null, null);
INSERT INTO `sys_log` VALUES ('3be1a87b2b22c5170cfb5bbace704ec3', '1', '用户名: test,登录成功！', null, null, null, '60.8.244.242', null, null, null, null, null, 'jeecg', '2019-09-14 02:48:50', null, null);
INSERT INTO `sys_log` VALUES ('13b296c2b69d30bccad5ae7910a802c6', '1', '用户名: jeecg,登录成功！', null, null, null, '60.8.244.242', null, null, null, null, null, 'jeecg', '2019-09-14 03:25:05', null, null);
INSERT INTO `sys_log` VALUES ('2fe102719bb73fb2016e7f1792688def', '1', '用户名: 游客,退出成功！', null, null, null, '60.8.244.242', null, null, null, null, null, 'jeecg', '2019-09-14 03:26:01', null, null);
INSERT INTO `sys_log` VALUES ('d8c04f04c16bc4a577b0fdfc0ec1e258', '1', '用户名: test,登录成功！', null, null, null, '60.8.244.242', null, null, null, null, null, 'jeecg', '2019-09-14 03:26:59', null, null);
INSERT INTO `sys_log` VALUES ('db64632f6a7534be56ab2c2ab3197fd3', '1', '用户名: 游客,退出成功！', null, null, null, '106.121.160.210', null, null, null, null, null, 'jeecg', '2019-09-16 14:24:35', null, null);
INSERT INTO `sys_log` VALUES ('68fb8d5a8c826581ee6a5b12ebf610ba', '1', '用户名: test,登录成功！', null, null, null, '106.121.160.210', null, null, null, null, null, 'jeecg', '2019-09-16 14:24:44', null, null);
INSERT INTO `sys_log` VALUES ('372fb5ecb914461baf29eee0cdffd4c1', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-17 08:19:18', null, null);
INSERT INTO `sys_log` VALUES ('88779949f6db20b55ef3a653141563ee', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '18', 'admin', '2019-09-17 08:19:27', null, null);
INSERT INTO `sys_log` VALUES ('77a94b309493d5584aa12e59a2bf3b9c', '2', '机框-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '4', 'admin', '2019-09-17 08:19:27', null, null);
INSERT INTO `sys_log` VALUES ('0d0ae6e23e8e191e8a38b422f566d10f', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '34', 'admin', '2019-09-17 08:19:27', null, null);
INSERT INTO `sys_log` VALUES ('9ee2be0cc3a9c24851bdccc35dd218d3', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '3', 'admin', '2019-09-17 08:19:32', null, null);
INSERT INTO `sys_log` VALUES ('f98110b3e1c22bb057df571e8e0b0c44', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '2', 'admin', '2019-09-17 08:19:33', null, null);
INSERT INTO `sys_log` VALUES ('c8cd6e16b9ecb1ab98f8ca55b8599001', '2', '子卡-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '2', 'admin', '2019-09-17 08:19:35', null, null);
INSERT INTO `sys_log` VALUES ('eacca44e24746102274c77335c9957d7', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-20 08:43:35', null, null);
INSERT INTO `sys_log` VALUES ('6d5571a19a55b4f33b56c2574c327ff2', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-20 08:43:47', null, null);
INSERT INTO `sys_log` VALUES ('874374f9d96202f480f6fd4cbde32f0d', '2', '设备接口相关-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '9', 'admin', '2019-09-20 08:43:57', null, null);
INSERT INTO `sys_log` VALUES ('538ae45dd19d065fb47e3a13caef582f', '2', '机框-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '5', 'admin', '2019-09-20 08:43:57', null, null);
INSERT INTO `sys_log` VALUES ('38c345e209db2f2448040686daed5085', '2', '单板列表-分页列表查询', '1', 'admin', '管理员', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '5', 'admin', '2019-09-20 08:43:57', null, null);
INSERT INTO `sys_log` VALUES ('bdaded5d9404bce27a779d38c47e4eb1', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-23 08:45:03', null, null);
INSERT INTO `sys_log` VALUES ('69692671f430ba3d57b6a99903a91279', '1', '用户名: admin,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-27 10:13:00', null, null);
INSERT INTO `sys_log` VALUES ('23aa4c58bf021aaa153408f619ee3642', '1', '用户名: 管理员,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-09-29 01:38:50', null, null);
INSERT INTO `sys_log` VALUES ('4a5243a24f65ed8a67498f59c6ea4dc8', '1', '用户名: admin,登录成功！', null, null, null, '223.72.44.22', null, null, null, null, null, 'jeecg', '2019-10-09 09:00:04', null, null);
INSERT INTO `sys_log` VALUES ('5898ec03031df01b2e2e15dd28ee62ca', '1', '用户名: 管理员,退出成功！', null, null, null, '223.72.44.22', null, null, null, null, null, 'jeecg', '2019-10-09 14:21:56', null, null);
INSERT INTO `sys_log` VALUES ('5fb0ac6a181d831a8e5296a5702194ee', '1', '用户名: test,登录成功！', null, null, null, '223.72.44.22', null, null, null, null, null, 'jeecg', '2019-10-09 14:22:08', null, null);
INSERT INTO `sys_log` VALUES ('760565cbb7ec1e48c32659aac5cdf80a', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '223.72.44.22', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '7', 'test', '2019-10-09 14:22:41', null, null);
INSERT INTO `sys_log` VALUES ('5dbd6cf94e9e5fb2d0acf3c8052fcb18', '2', '机框-分页列表查询', '1', 'test', '游客', '223.72.44.22', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '15', 'test', '2019-10-09 14:22:41', null, null);
INSERT INTO `sys_log` VALUES ('f50ec7e5da07c692b90320cb10072051', '2', '单板列表-分页列表查询', '1', 'test', '游客', '223.72.44.22', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '25', 'test', '2019-10-09 14:22:41', null, null);
INSERT INTO `sys_log` VALUES ('948c31e700328b7f9b5987204b795d1d', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 09:45:37', null, null);
INSERT INTO `sys_log` VALUES ('e4b204341fc5a2e8b2b55eab84f4ba17', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 09:45:48', null, null);
INSERT INTO `sys_log` VALUES ('b8d3ce3f38b0d969d2e6947ed7d65fa1', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '33', 'test', '2019-10-10 09:46:10', null, null);
INSERT INTO `sys_log` VALUES ('4cbfe78b521a61db292ce93cff8e1e17', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'test', '2019-10-10 09:46:10', null, null);
INSERT INTO `sys_log` VALUES ('b21e9a5047a3fca718227e03f601d105', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '6', 'test', '2019-10-10 09:46:10', null, null);
INSERT INTO `sys_log` VALUES ('a2527263334758296ccf0ad8b7a74efe', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '5', 'test', '2019-10-10 09:47:34', null, null);
INSERT INTO `sys_log` VALUES ('f6c693f6c6f93347699fe619a5def079', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '9', 'test', '2019-10-10 09:47:34', null, null);
INSERT INTO `sys_log` VALUES ('1226d341a5c22a0abeb9ce5bc37fe242', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '12', 'test', '2019-10-10 09:47:34', null, null);
INSERT INTO `sys_log` VALUES ('9a1fd75923da18c67f8ad3daea4e5ea2', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 10:29:07', null, null);
INSERT INTO `sys_log` VALUES ('03d8d5f289b52d79eee92a3449c2a666', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 10:29:17', null, null);
INSERT INTO `sys_log` VALUES ('4185501790d0f6fdcf973010d1b30012', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '9', 'test', '2019-10-10 10:29:26', null, null);
INSERT INTO `sys_log` VALUES ('2c3c20914b4396bc1c71cb9983414846', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '7', 'test', '2019-10-10 10:29:26', null, null);
INSERT INTO `sys_log` VALUES ('5e20ba18b617a79b023baaebcbc82b31', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '23', 'test', '2019-10-10 10:29:26', null, null);
INSERT INTO `sys_log` VALUES ('16163d160c16a6d0ef579cc4a4978f0d', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 10:45:11', null, null);
INSERT INTO `sys_log` VALUES ('b77a1cafcf81cbeb14f8f71881d4a6dc', '1', '用户名: 游客,退出成功！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-10-10 10:46:44', null, null);
INSERT INTO `sys_log` VALUES ('6234de9d61520fcaad7c7bd7e34c8c79', '1', '用户名: test,登录成功！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-10-10 10:47:25', null, null);
INSERT INTO `sys_log` VALUES ('c4b62a2037de2e8954ce6c49d38de089', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '72', 'test', '2019-10-10 10:47:53', null, null);
INSERT INTO `sys_log` VALUES ('d91c4304335568f566424de1764197d8', '2', '单板列表-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '137', 'test', '2019-10-10 10:47:53', null, null);
INSERT INTO `sys_log` VALUES ('6384c10fd0d30c7f9273c2c926284f82', '2', '机框-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '97', 'test', '2019-10-10 10:47:53', null, null);
INSERT INTO `sys_log` VALUES ('2b603704b7b21f533eaf775f91c52e70', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '13', 'test', '2019-10-10 10:47:58', null, null);
INSERT INTO `sys_log` VALUES ('c7591ebf78eedb8b406fd94c1541316c', '2', '机框-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '34', 'test', '2019-10-10 10:47:58', null, null);
INSERT INTO `sys_log` VALUES ('7a27b5d1e57c688bbdd5dd987ae801fe', '2', '单板列表-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '67', 'test', '2019-10-10 10:47:58', null, null);
INSERT INTO `sys_log` VALUES ('e38387fef7f848ef21708993312a2fcf', '2', '机框-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '14', 'test', '2019-10-10 10:48:06', null, null);
INSERT INTO `sys_log` VALUES ('b885c25f08c0af994882195ba6a5f467', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '77', 'test', '2019-10-10 10:48:06', null, null);
INSERT INTO `sys_log` VALUES ('77a7e2c290efd3fc5ac96e48413335e5', '2', '单板列表-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '114', 'test', '2019-10-10 10:48:06', null, null);
INSERT INTO `sys_log` VALUES ('29effe52ca9dfa6a2c895019f36eda34', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '14', 'test', '2019-10-10 10:48:17', null, null);
INSERT INTO `sys_log` VALUES ('b433da89a252c9bb73ddb46c1444bd4a', '2', '机框-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '29', 'test', '2019-10-10 10:48:17', null, null);
INSERT INTO `sys_log` VALUES ('807cf16bddce59c3af3081c8cf6fbbe4', '2', '单板列表-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '50', 'test', '2019-10-10 10:48:17', null, null);
INSERT INTO `sys_log` VALUES ('5c8b1125b922e34facd0ffb8c786ae0c', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '15', 'test', '2019-10-10 10:48:25', null, null);
INSERT INTO `sys_log` VALUES ('d838836f5097bcc79cbb9ff58ac01f31', '2', '机框-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '26', 'test', '2019-10-10 10:48:25', null, null);
INSERT INTO `sys_log` VALUES ('2f227d6143ac31bc0ce3644844452fe5', '2', '单板列表-分页列表查询', '1', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '43', 'test', '2019-10-10 10:48:25', null, null);
INSERT INTO `sys_log` VALUES ('48df8c8b84ffab1cf20a9d92c8526e69', '1', '用户登录失败，用户不存在！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-10-10 10:52:58', null, null);
INSERT INTO `sys_log` VALUES ('eef8ba145877d1ea8351c23eb8376e1a', '1', '用户名: test,登录成功！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-10-10 11:11:56', null, null);
INSERT INTO `sys_log` VALUES ('092df763c1ae04a6262d1b6daef2c6ee', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 11:35:57', null, null);
INSERT INTO `sys_log` VALUES ('a6ed9a32fde6016595a74d149ee04eb4', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-10 11:36:05', null, null);
INSERT INTO `sys_log` VALUES ('5440fb5b4fc3ed14c2e0982f349e1eba', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '58', 'test', '2019-10-10 11:36:14', null, null);
INSERT INTO `sys_log` VALUES ('67d3e0b8fb787872c787298b340a31e5', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '47', 'test', '2019-10-10 11:36:14', null, null);
INSERT INTO `sys_log` VALUES ('6229aad6ae85e1b27cfe6189e3571f79', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '104', 'test', '2019-10-10 11:36:14', null, null);
INSERT INTO `sys_log` VALUES ('b5a6b7a51aa175d2b616d73f16f4e363', '2', '添加测试DEMO', '2', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"age\":0,\"bonusMoney\":0.0,\"content\":\"\",\"createBy\":\"test\",\"createTime\":1570708011322,\"email\":\"\",\"id\":\"1182261206883254273\",\"keyWord\":\"\",\"name\":\"\",\"salaryMoney\":0,\"sex\":\"\",\"sysOrgCode\":\"A01\",\"updateBy\":\"\"}]', null, '552', 'test', '2019-10-10 11:46:52', null, null);
INSERT INTO `sys_log` VALUES ('abc0761e982a6a18894db9da7bdf07c4', '2', '添加测试DEMO', '2', 'test', '游客', '0:0:0:0:0:0:0:1', 'org.smartbox.modules.demo.test.controller.JeecgDemoController.add()', null, '[{\"age\":0,\"bonusMoney\":0.0,\"content\":\"\",\"createBy\":\"test\",\"createTime\":1570708017238,\"email\":\"\",\"id\":\"1182261229989675009\",\"keyWord\":\"\",\"name\":\"\",\"salaryMoney\":0,\"sex\":\"\",\"sysOrgCode\":\"A01\",\"updateBy\":\"\"}]', null, '27', 'test', '2019-10-10 11:46:57', null, null);
INSERT INTO `sys_log` VALUES ('3500be5264166eb1c69f17893c077339', '1', '用户名: test,登录成功！', null, null, null, '0:0:0:0:0:0:0:1', null, null, null, null, null, 'jeecg', '2019-10-10 12:18:09', null, null);
INSERT INTO `sys_log` VALUES ('c5ae615744671aa4fa200aa64a77274a', '1', '用户名: test,登录成功！', null, null, null, '223.72.88.125', null, null, null, null, null, 'jeecg', '2019-10-19 07:56:13', null, null);
INSERT INTO `sys_log` VALUES ('792e0e70493e012b4a6b24b31d14c5e3', '1', '用户名: test,登录成功！', null, null, null, '58.212.25.59', null, null, null, null, null, 'jeecg', '2019-10-19 07:59:19', null, null);
INSERT INTO `sys_log` VALUES ('950d798a737271e04549cc466f1c6c01', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-23 10:31:19', null, null);
INSERT INTO `sys_log` VALUES ('59850723c99600a5810a106472df55f0', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '75', 'test', '2019-10-23 10:31:42', null, null);
INSERT INTO `sys_log` VALUES ('5a39c6e527f6f0245d9daadff715722b', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '103', 'test', '2019-10-23 10:31:42', null, null);
INSERT INTO `sys_log` VALUES ('b6b54a6914ca05229b634e5d6b90d896', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '68', 'test', '2019-10-23 10:31:42', null, null);
INSERT INTO `sys_log` VALUES ('5861fa5587dccbaa5baefec6e08a5b14', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '3', 'test', '2019-10-23 10:31:53', null, null);
INSERT INTO `sys_log` VALUES ('b4708ba24df42c25a0de6cfba4e95f76', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '36', 'test', '2019-10-23 10:31:54', null, null);
INSERT INTO `sys_log` VALUES ('931f73585e464eb9d98f939c33c32798', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '38', 'test', '2019-10-23 10:31:54', null, null);
INSERT INTO `sys_log` VALUES ('6b7931e3b788c246cd754b7a844d190c', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '26', 'test', '2019-10-23 10:32:06', null, null);
INSERT INTO `sys_log` VALUES ('77c9e12cb5165db625f3dfe594b4cfe6', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '10', 'test', '2019-10-23 10:32:09', null, null);
INSERT INTO `sys_log` VALUES ('80f9df4cb9c6b74b428107277680de12', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '3', 'test', '2019-10-23 10:32:11', null, null);
INSERT INTO `sys_log` VALUES ('9ed423e8f0d20ea3cb9305b22d2131fb', '2', '子卡-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.suboardlistByBid()', null, null, null, '10', 'test', '2019-10-23 10:32:13', null, null);
INSERT INTO `sys_log` VALUES ('5a8a741aed5bd5bb9fb1fcced090d506', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-23 10:32:21', null, null);
INSERT INTO `sys_log` VALUES ('413d56dfdc7212cc84fa91a6e1c93faa', '1', '用户名: test,登录成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-24 02:29:14', null, null);
INSERT INTO `sys_log` VALUES ('4df30da249bdd0e78c84971e327a0a55', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '15', 'test', '2019-10-24 02:30:26', null, null);
INSERT INTO `sys_log` VALUES ('28be465cb6a62505d40ab07e3330107b', '2', '单板列表-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'test', '2019-10-24 02:30:26', null, null);
INSERT INTO `sys_log` VALUES ('1bc0935365303e356e535e1a72a82152', '2', '机框-分页列表查询', '1', 'test', '游客', '59.108.205.146', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '37', 'test', '2019-10-24 02:30:26', null, null);
INSERT INTO `sys_log` VALUES ('eed19f64b9c33933a777ebce0db7b7e1', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.205.146', null, null, null, null, null, 'jeecg', '2019-10-24 03:24:41', null, null);
INSERT INTO `sys_log` VALUES ('51da641e928b542e0d522ce41e2e007f', '1', '用户名: test,登录成功！', null, null, null, '223.104.3.63', null, null, null, null, null, 'jeecg', '2019-11-04 11:24:28', null, null);
INSERT INTO `sys_log` VALUES ('8d8f5189f786c50bbaa78c71373031a4', '1', '用户名: 游客,退出成功！', null, null, null, '223.104.3.63', null, null, null, null, null, 'jeecg', '2019-11-04 11:26:51', null, null);
INSERT INTO `sys_log` VALUES ('b9d6d99f0d0f4d624907fd4f0019c18a', '1', '用户名: test,登录成功！', null, null, null, '59.108.92.235', null, null, null, null, null, 'jeecg', '2019-11-04 11:44:44', null, null);
INSERT INTO `sys_log` VALUES ('9704e27062ccd78d718e63828421612c', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.92.235', null, null, null, null, null, 'jeecg', '2019-11-04 11:44:44', null, null);
INSERT INTO `sys_log` VALUES ('2dbad59d3a68784fcaf7ccd040b6e160', '1', '用户名: test,登录成功！', null, null, null, '59.108.92.235', null, null, null, null, null, 'jeecg', '2019-11-04 11:45:03', null, null);
INSERT INTO `sys_log` VALUES ('cd5e1de3f3d2e6f309d8e50192f45144', '1', '用户名: 游客,退出成功！', null, null, null, '59.108.92.235', null, null, null, null, null, 'jeecg', '2019-11-04 11:45:03', null, null);
INSERT INTO `sys_log` VALUES ('380efe39e8bd6f8b3b71b384eced744d', '1', '用户名: test,登录成功！', null, null, null, '223.72.68.223', null, null, null, null, null, 'jeecg', '2019-11-14 08:02:04', null, null);
INSERT INTO `sys_log` VALUES ('2be17c910f19a297a2c2b3f6a91b5c59', '2', '单板列表-分页列表查询', '1', 'test', '游客', '223.72.68.223', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '28', 'test', '2019-11-14 08:02:26', null, null);
INSERT INTO `sys_log` VALUES ('e5c8a84ddeb3062f1e96fc98efb2d9fc', '2', '机框-分页列表查询', '1', 'test', '游客', '223.72.68.223', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '8', 'test', '2019-11-14 08:02:26', null, null);
INSERT INTO `sys_log` VALUES ('a7d0c8a8e0e67c26ae783b6cf732998b', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '223.72.68.223', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '18', 'test', '2019-11-14 08:02:26', null, null);
INSERT INTO `sys_log` VALUES ('918c6a246ad4b983a671a673fe9d13d8', '1', '用户名: test,登录成功！', null, null, null, '180.111.38.165', null, null, null, null, null, 'jeecg', '2019-11-16 09:07:12', null, null);
INSERT INTO `sys_log` VALUES ('ef4bb9251113624f19d4be75590f4a43', '1', '用户名: 游客,退出成功！', null, null, null, '49.94.171.45', null, null, null, null, null, 'jeecg', '2019-11-17 02:00:41', null, null);
INSERT INTO `sys_log` VALUES ('e9b66d566c38ffb7d427c6f43dc1a020', '1', '用户名: test,登录成功！', null, null, null, '49.94.171.45', null, null, null, null, null, 'jeecg', '2019-11-17 02:00:51', null, null);
INSERT INTO `sys_log` VALUES ('32b7b9c7948b1ce1f98ba89e337410d8', '2', '单板列表-分页列表查询', '1', 'test', '游客', '49.94.171.45', 'org.smartbox.modules.devices.controller.DevDevicesController.boardInfolist()', null, null, null, '39', 'test', '2019-11-17 02:01:26', null, null);
INSERT INTO `sys_log` VALUES ('dda7e5e823a243ab2804bc21ac359b10', '2', '设备接口相关-分页列表查询', '1', 'test', '游客', '49.94.171.45', 'org.smartbox.modules.devices.controller.DevDevicesController.queryPageList()', null, null, null, '28', 'test', '2019-11-17 02:01:26', null, null);
INSERT INTO `sys_log` VALUES ('5f1d53dab61ac8b5b0737400663482f9', '2', '机框-分页列表查询', '1', 'test', '游客', '49.94.171.45', 'org.smartbox.modules.devices.controller.DevDevicesController.chassisInfolist()', null, null, null, '5', 'test', '2019-11-17 02:01:26', null, null);
INSERT INTO `sys_log` VALUES ('9b98c19b171c54f09db82c56e17bede8', '1', '用户名: 游客,退出成功！', null, null, null, '49.94.171.45', null, null, null, null, null, 'jeecg', '2019-11-17 02:54:32', null, null);
INSERT INTO `sys_log` VALUES ('23a2679da5ef1e2200130c96fd18915e', '1', '用户名: 游客,退出成功！', null, null, null, '223.72.40.207', null, null, null, null, null, 'jeecg', '2019-11-18 03:24:47', null, null);

-- ----------------------------
-- Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父id',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) DEFAULT NULL COMMENT '菜单权限编码',
  `perms_type` varchar(10) DEFAULT '0' COMMENT '权限策略1显示2禁用',
  `sort_no` int(10) DEFAULT NULL COMMENT '菜单排序',
  `always_show` tinyint(1) DEFAULT NULL COMMENT '聚合子路由: 1是0否',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) DEFAULT '1' COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `is_leaf` tinyint(1) DEFAULT NULL COMMENT '是否叶子节点:    1:是   0:不是',
  `keep_alive` tinyint(1) DEFAULT NULL COMMENT '是否缓存该页面:    1:是   0:不是',
  `hidden` int(2) DEFAULT '0' COMMENT '是否隐藏路由: 0否,1是',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除状态 0正常 1已删除',
  `rule_flag` int(3) DEFAULT '0' COMMENT '是否添加数据权限1是0否',
  `status` varchar(2) DEFAULT NULL COMMENT '按钮权限状态(0无效1有效)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_prem_pid` (`parent_id`) USING BTREE,
  KEY `index_prem_is_route` (`is_route`) USING BTREE,
  KEY `index_prem_is_leaf` (`is_leaf`) USING BTREE,
  KEY `index_prem_sort_no` (`sort_no`) USING BTREE,
  KEY `index_prem_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('00a2a0ae65cdca5e93209cdbde97cbe6', '2e42e3835c2b44ec9f7bc26c146ee531', '成功', '/result/success', 'result/Success', null, null, '1', null, null, '1', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('020b06793e4de2eee0007f603000c769', 'f0675b52d89100ee88472b6800754a08', 'ViserChartDemo', '/report/ViserChartDemo', 'jeecg/report/ViserChartDemo', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-03 19:08:53', 'admin', '2019-04-03 19:08:53', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('024f1fd1283dc632458976463d8984e1', '700b7f95165c46cc7a78bf227aa8fed3', 'Tomcat信息', '/monitor/TomcatInfo', 'modules/monitor/TomcatInfo', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-02 09:44:29', 'admin', '2019-05-07 15:19:10', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('043780fa095ff1b2bec4dc406d76f023', '2a470fc0c3954d9dbb61de6d80846549', '表格合计', '/jeecg/tableTotal', 'jeecg/TableTotal', null, null, '1', null, '1', '3', '0', null, '1', '1', '0', '0', null, 'admin', '2019-08-14 10:28:46', null, null, '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('05b3c82ddb2536a4a5ee1a4c46b5abef', '540a2936940846cb98114ffb0d145cb8', '用户列表', '/list/user-list', 'list/UserList', null, null, '1', null, null, '3', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('0620e402857b8c5b605e1ad9f4b89350', '2a470fc0c3954d9dbb61de6d80846549', '异步树列表Demo', '/jeecg/JeecgTreeTable', 'jeecg/JeecgTreeTable', null, null, '1', null, '0', '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-05-13 17:30:30', 'admin', '2019-05-13 17:32:17', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('078f9558cdeab239aecb2bda1a8ed0d1', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（文章）', '/list/search/article', 'list/TableList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-12 14:00:34', 'admin', '2019-02-12 14:17:54', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('08e6b9dc3c04489c8e1ff2ce6f105aa4', '', '系统监控', '/dashboard3', 'layouts/RouteView', null, null, '0', null, null, '6', '0', 'dashboard', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:19:58', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('0ac2ad938963b6c6d1af25477d5b8b51', '8d4683aacaa997ab86b966b464360338', '代码生成按钮', null, null, null, null, '2', 'online:goGenerateCode', '1', '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-06-11 14:20:09', null, null, '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('13212d3416eb690c2e1d5033166ff47a', '2e42e3835c2b44ec9f7bc26c146ee531', '失败', '/result/fail', 'result/Error', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('1367a93f2c410b169faa7abcbad2f77c', '6e73eb3c26099c191bf03852ee1310a1', '基本设置', '/account/settings/base', 'account/settings/BaseSetting', null, null, '1', 'BaseSettings', null, null, '0', null, '1', '1', null, '1', null, null, '2018-12-26 18:58:35', 'admin', '2019-03-20 12:57:31', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('16f26e7c6f85876d8b227d3e32b3942f', '9ab9099e9a8f440826c637016df0e0c0', '设备列表', '/devices/listview', 'devices/DevicesList', null, null, '1', null, '1', '1', '0', null, '1', '1', '0', '0', null, 'admin', '2019-09-05 12:54:49', 'admin', '2019-09-06 03:24:07', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('190c2b43bec6a5f7a4194a85db67d96a', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '角色维护', '/isystem/roleUserList', 'system/RoleUserList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-17 15:13:56', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('1a0811914300741f4e11838ff37a1d3a', '3f915b2769fc80648e92d04e84ca059d', '手机号禁用', null, null, null, null, '2', 'user:form:phone', '2', '1', '0', null, '0', '1', null, '0', null, 'admin', '2019-05-11 17:19:30', 'admin', '2019-05-11 18:00:22', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('200006f0edf145a2b50eacca07585451', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（应用）', '/list/search/application', 'list/TableList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-12 14:02:51', 'admin', '2019-02-12 14:14:01', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('22d6a3d39a59dd7ea9a30acfa6bfb0a5', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO动态表单', '/online/df/:table/:id', 'modules/online/cgform/auto/OnlineDynamicForm', null, null, '1', null, null, '9', '0', null, '0', '1', null, '1', null, 'admin', '2019-04-22 15:15:43', 'admin', '2019-04-30 18:18:26', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('265de841c58907954b8877fb85212622', '2a470fc0c3954d9dbb61de6d80846549', '图片拖拽排序', '/jeecg/imgDragSort', 'jeecg/ImgDragSort', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-25 10:43:08', 'admin', '2019-04-25 10:46:26', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('277bfabef7d76e89b33062b16a9a5020', 'e3c13679c73a4f829bcff2aba8fd68b1', '基础表单', '/form/base-form', 'form/BasicForm', null, null, '1', null, null, '1', '0', null, '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-02-26 17:02:08', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('2a470fc0c3954d9dbb61de6d80846549', '', '常见案例', '/jeecg', 'layouts/RouteView', null, null, '0', null, null, '7', '0', 'qrcode', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:42', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('2aeddae571695cd6380f6d6d334d6e7d', 'f0675b52d89100ee88472b6800754a08', '布局统计报表', '/report/ArchivesStatisticst', 'jeecg/report/ArchivesStatisticst', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-03 18:32:48', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('2dbbafa22cda07fa5d169d741b81fe12', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '在线文档', '{{ window._CONFIG[\'domianURL\'] }}/doc.html', 'layouts/IframePageView', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-01-30 10:00:01', 'admin', '2019-03-23 19:44:43', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('2e42e3835c2b44ec9f7bc26c146ee531', '', '结果页', '/result', 'layouts/PageView', null, null, '0', null, null, '8', '0', 'check-circle-o', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:56', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('339329ed54cf255e1f9392e84f136901', '2a470fc0c3954d9dbb61de6d80846549', 'helloworld', '/jeecg/helloworld', 'jeecg/helloworld', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-02-15 16:24:56', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('3f915b2769fc80648e92d04e84ca059d', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '用户管理', '/isystem/user', 'system/UserList', null, null, '1', null, null, '1', '0', null, '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-16 11:20:33', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('3fac0d3c9cd40fa53ab70d4c583821f8', '2a470fc0c3954d9dbb61de6d80846549', '分屏', '/jeecg/splitPanel', 'jeecg/SplitPanel', null, null, '1', null, null, '6', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-25 16:27:06', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('4148ec82b6acd69f470bea75fe41c357', '2a470fc0c3954d9dbb61de6d80846549', '单表模型示例', '/jeecg/jeecgDemoList', 'jeecg/JeecgDemoList', 'DemoList', null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, null, '2018-12-28 15:57:30', 'admin', '2019-02-15 16:24:37', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('418964ba087b90a84897b62474496b93', '540a2936940846cb98114ffb0d145cb8', '查询表格', '/list/query-list', 'list/TableList', null, null, '1', null, null, '1', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('4356a1a67b564f0988a484f5531fd4d9', '2a470fc0c3954d9dbb61de6d80846549', '内嵌Table', '/jeecg/TableExpandeSub', 'jeecg/TableExpandeSub', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-04 22:48:13', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('45c966826eeff4c99b8f8ebfe74511fc', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '部门管理', '/isystem/depart', 'system/DepartList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-01-29 18:47:40', 'admin', '2019-03-07 19:23:16', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('4875ebe289344e14844d8e3ea1edd73f', '', '详情页', '/profile', 'layouts/RouteView', null, null, '0', null, null, '8', '0', 'profile', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:48', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('4f66409ef3bbd69c1d80469d6e2a885e', '6e73eb3c26099c191bf03852ee1310a1', '账户绑定', '/account/settings/binding', 'account/settings/Binding', null, null, '1', 'BindingSettings', null, null, null, null, '1', '1', null, null, null, null, '2018-12-26 19:01:20', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('4f84f9400e5e92c95f05b554724c2b58', '540a2936940846cb98114ffb0d145cb8', '角色列表', '/list/role-list', 'list/RoleList', null, null, '1', null, null, '4', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('53a9230444d33de28aa11cc108fb1dba', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '我的消息', '/isps/userAnnouncement', 'system/UserAnnouncementList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-19 10:16:00', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('54097c6a3cf50fad0793a34beff1efdf', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单', '/online/cgformList/:code', 'modules/online/cgform/auto/OnlCgformAutoList', null, null, '1', null, null, '9', '0', null, '1', '1', null, '1', null, 'admin', '2019-03-19 16:03:06', 'admin', '2019-04-30 18:19:03', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('540a2936940846cb98114ffb0d145cb8', '', '列表页', '/list', 'layouts/PageView', null, '/list/query-list', '0', null, null, '9', '0', 'table', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:20:20', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('54dd5457a3190740005c1bfec55b1c34', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '菜单管理', '/isystem/permission', 'system/PermissionList', null, null, '1', null, null, '3', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('58857ff846e61794c69208e9d3a85466', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '日志管理', '/isystem/log', 'system/LogList', null, null, '1', null, null, '1', '0', '', '1', '1', null, '0', null, null, '2018-12-26 10:11:18', 'admin', '2019-04-02 11:38:17', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('58b9204feaf07e47284ddb36cd2d8468', '2a470fc0c3954d9dbb61de6d80846549', '图片翻页', '/jeecg/imgTurnPage', 'jeecg/ImgTurnPage', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-25 11:36:42', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('5c2f42277948043026b7a14692456828', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '我的部门', '/isystem/departUserList', 'system/DepartUserList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-17 15:12:24', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('5c8042bd6c601270b2bbd9b20bccc68b', '', '消息中心', '/message', 'layouts/RouteView', null, null, '0', null, null, '6', '0', 'message', '1', '0', null, '0', null, 'admin', '2019-04-09 11:05:04', 'admin', '2019-04-11 19:47:54', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('6531cf3421b1265aeeeabaab5e176e6d', 'e3c13679c73a4f829bcff2aba8fd68b1', '分步表单', '/form/step-form', 'form/stepForm/StepForm', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('655563cd64b75dcf52ef7bcdd4836953', '2a470fc0c3954d9dbb61de6d80846549', '图片预览', '/jeecg/ImagPreview', 'jeecg/ImagPreview', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-17 11:18:45', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('65a8f489f25a345836b7f44b1181197a', 'c65321e57b7949b7a975313220de0422', '403', '/exception/403', 'exception/403', null, null, '1', null, null, '1', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('6ad53fd1b220989a8b71ff482d683a5a', '2a470fc0c3954d9dbb61de6d80846549', '一对多Tab示例', '/jeecg/tablist/JeecgOrderDMainList', 'jeecg/tablist/JeecgOrderDMainList', null, null, '1', null, null, '2', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-20 14:45:09', 'admin', '2019-02-21 16:26:21', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('6e73eb3c26099c191bf03852ee1310a1', '717f6bee46f44a3897eca9abd6e2ec44', '个人设置', '/account/settings/base', 'account/settings/Index', null, null, '1', null, null, '2', '1', null, '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-04-19 09:41:05', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('700b7f95165c46cc7a78bf227aa8fed3', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '性能监控', '/monitor', 'layouts/RouteView', null, null, '1', null, null, '0', '0', null, '1', '0', null, '0', null, 'admin', '2019-04-02 11:34:34', 'admin', '2019-05-05 17:49:47', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('717f6bee46f44a3897eca9abd6e2ec44', null, '个人页', '/account', 'layouts/RouteView', null, null, '0', null, null, '9', '0', 'user', '1', '0', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('73678f9daa45ed17a3674131b03432fb', '540a2936940846cb98114ffb0d145cb8', '权限列表', '/list/permission-list', 'list/PermissionList', null, null, '1', null, null, '5', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('7593c9e3523a17bca83b8d7fe8a34e58', '3f915b2769fc80648e92d04e84ca059d', '添加用户按钮', '', null, null, null, '2', 'user:add', '1', '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-16 11:20:33', 'admin', '2019-05-17 18:31:25', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('7960961b0063228937da5fa8dd73d371', '2a470fc0c3954d9dbb61de6d80846549', 'JEditableTable示例', '/jeecg/JEditableTable', 'jeecg/JeecgEditableTableExample', null, null, '1', null, null, '7', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-22 15:22:18', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('7ac9eb9ccbde2f7a033cd4944272bf1e', '540a2936940846cb98114ffb0d145cb8', '卡片列表', '/list/card', 'list/CardList', null, null, '1', null, null, '7', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('81b732362f9c77d11eea8c9541ef575c', '3f915b2769fc80648e92d04e84ca059d', '用户编辑按钮', null, null, null, null, '2', 'user:edit', '1', '1', '0', null, '1', '1', '0', '0', null, 'admin', '2019-09-05 12:42:48', null, null, '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('841057b8a1bef8f6b4b20f9a618a7fa6', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '数据日志', '/sys/dataLog-list', 'system/DataLogList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-11 19:26:49', 'admin', '2019-03-12 11:40:47', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('882a73768cfd7f78f3a37584f7299656', '6e73eb3c26099c191bf03852ee1310a1', '个性化设置', '/account/settings/custom', 'account/settings/Custom', null, null, '1', 'CustomSettings', null, null, null, null, '1', '1', null, null, null, null, '2018-12-26 19:00:46', null, '2018-12-26 21:13:25', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('8b3bff2eee6f1939147f5c68292a1642', '700b7f95165c46cc7a78bf227aa8fed3', '服务器信息', '/monitor/SystemInfo', 'modules/monitor/SystemInfo', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-02 11:39:19', 'admin', '2019-04-02 15:40:02', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('8d1ebd663688965f1fd86a2f0ead3416', '700b7f95165c46cc7a78bf227aa8fed3', 'Redis监控', '/monitor/redis/info', 'modules/monitor/RedisInfo', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-02 13:11:33', 'admin', '2019-05-07 15:18:54', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('8d4683aacaa997ab86b966b464360338', 'e41b69c57a941a3bbcce45032fe57605', 'Online表单开发', '/online/cgform', 'modules/online/cgform/OnlCgformHeadList', null, null, '1', null, null, '1', '0', null, '1', '0', null, '0', null, 'admin', '2019-03-12 15:48:14', 'admin', '2019-06-11 14:19:17', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('8fb8172747a78756c11916216b8b8066', '717f6bee46f44a3897eca9abd6e2ec44', '工作台', '/dashboard/workplace', 'dashboard/Workplace', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:45:02', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('944abf0a8fc22fe1f1154a389a574154', '5c8042bd6c601270b2bbd9b20bccc68b', '消息管理', '/modules/message/sysMessageList', 'modules/message/SysMessageList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-09 11:27:53', 'admin', '2019-04-09 19:31:23', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('9502685863ab87f0ad1134142788a385', '', '首页', '/dashboard/analysis', 'dashboard/Analysis', null, null, '0', null, null, '0', '0', 'home', '1', '1', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-29 11:04:13', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('97c8629abc7848eccdb6d77c24bb3ebb', '700b7f95165c46cc7a78bf227aa8fed3', '磁盘监控', '/monitor/Disk', 'modules/monitor/DiskMonitoring', null, null, '1', null, null, '6', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-25 14:30:06', 'admin', '2019-05-05 14:37:14', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('9a90363f216a6a08f32eecb3f0bf12a3', '2a470fc0c3954d9dbb61de6d80846549', '常用选择组件', '/jeecg/SelectDemo', 'jeecg/SelectDemo', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-19 11:19:05', 'admin', '2019-04-10 15:36:50', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('9ab9099e9a8f440826c637016df0e0c0', '', '设备管理', '/devices', 'layouts/RouteView', null, null, '0', null, '1', '1', '0', 'hdd', '1', '0', '0', '0', null, 'admin', '2019-09-05 11:21:47', 'admin', '2019-09-05 12:20:27', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('9cb91b8851db0cf7b19d7ecc2a8193dd', '1939e035e803a99ceecb6f5563570fb2', '我的任务表单', '/modules/bpm/task/form/FormModule', 'modules/bpm/task/form/FormModule', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-08 16:49:05', 'admin', '2019-03-08 18:37:56', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('a400e4f4d54f79bf5ce160ae432231af', '2a470fc0c3954d9dbb61de6d80846549', '百度', 'http://www.baidu.com', 'layouts/IframePageView', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-01-29 19:44:06', 'admin', '2019-02-15 16:25:02', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('ae4fed059f67086fd52a73d913cf473d', '540a2936940846cb98114ffb0d145cb8', '内联编辑表格', '/list/edit-table', 'list/TableInnerEditList', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('aedbf679b5773c1f25e9f7b10111da73', '08e6b9dc3c04489c8e1ff2ce6f105aa4', 'SQL监控', '{{ window._CONFIG[\'domianURL\'] }}/druid/', 'layouts/IframePageView', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-01-30 09:43:22', 'admin', '2019-03-23 19:00:46', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('b1cb0a3fedf7ed0e4653cb5a229837ee', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '定时任务', '/isystem/QuartzJobList', 'system/QuartzJobList', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, null, '2019-01-03 09:38:52', 'admin', '2019-04-02 10:24:13', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('b3c824fc22bd953e2eb16ae6914ac8f9', '4875ebe289344e14844d8e3ea1edd73f', '高级详情页', '/profile/advanced', 'profile/advanced/Advanced', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('b4dfc7d5dd9e8d5b6dd6d4579b1aa559', 'c65321e57b7949b7a975313220de0422', '500', '/exception/500', 'exception/500', null, null, '1', null, null, '3', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('b6bcee2ccc854052d3cc3e9c96d90197', '71102b3b87fb07e5527bbd2c530dd90a', '加班申请', '/modules/extbpm/joa/JoaOvertimeList', 'modules/extbpm/joa/JoaOvertimeList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-03 15:33:10', 'admin', '2019-04-03 15:34:48', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('c431130c0bc0ec71b0a5be37747bb36a', '2a470fc0c3954d9dbb61de6d80846549', '一对多JEditable', '/jeecg/JeecgOrderMainListForJEditableTable', 'jeecg/JeecgOrderMainListForJEditableTable', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-03-29 10:51:59', 'admin', '2019-04-04 20:09:39', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('c65321e57b7949b7a975313220de0422', null, '异常页', '/exception', 'layouts/RouteView', null, null, '0', null, null, '8', null, 'warning', '1', '0', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('c6cf95444d80435eb37b2f9db3971ae6', '2a470fc0c3954d9dbb61de6d80846549', '数据回执模拟', '/jeecg/InterfaceTest', 'jeecg/InterfaceTest', null, null, '1', null, null, '6', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-19 16:02:23', 'admin', '2019-02-21 16:25:45', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('cc50656cf9ca528e6f2150eba4714ad2', '4875ebe289344e14844d8e3ea1edd73f', '基础详情页', '/profile/basic', 'profile/basic/Index', null, null, '1', null, null, '1', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('d07a2c87a451434c99ab06296727ec4f', '700b7f95165c46cc7a78bf227aa8fed3', 'JVM信息', '/monitor/JvmInfo', 'modules/monitor/JvmInfo', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-01 23:07:48', 'admin', '2019-04-02 11:37:16', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('d2bbf9ebca5a8fa2e227af97d2da7548', 'c65321e57b7949b7a975313220de0422', '404', '/exception/404', 'exception/404', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('d7d6e2e4e2934f2c9385a623fd98c6f3', '', '系统管理', '/isystem', 'layouts/RouteView', null, null, '0', null, null, '4', '0', 'setting', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:19:52', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('d86f58e7ab516d3bc6bfb1fe10585f97', '717f6bee46f44a3897eca9abd6e2ec44', '个人中心', '/account/center', 'account/center/Index', null, null, '1', null, null, '1', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('de13e0f6328c069748de7399fcc1dbbd', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（项目）', '/list/search/project', 'list/TableList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-12 14:01:40', 'admin', '2019-02-12 14:14:18', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e08cb190ef230d5d4f03824198773950', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '系统通告', '/isystem/annountCement', 'system/SysAnnouncementList', null, null, '1', 'annountCement', null, '6', null, '', '1', '1', null, null, null, null, '2019-01-02 17:23:01', null, '2019-01-02 17:31:23', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e1979bb53e9ea51cecc74d86fd9d2f64', '2a470fc0c3954d9dbb61de6d80846549', 'PDF预览', '/jeecg/jeecgPdfView', 'jeecg/JeecgPdfView', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-25 10:39:35', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e3c13679c73a4f829bcff2aba8fd68b1', '', '表单页', '/form', 'layouts/PageView', null, null, '0', null, null, '9', '0', 'form', '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:20:14', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e41b69c57a941a3bbcce45032fe57605', '', '在线开发', '/online', 'layouts/RouteView', null, null, '0', null, null, '5', '0', 'cloud', '1', '0', null, '0', null, 'admin', '2019-03-08 10:43:10', 'admin', '2019-05-11 10:36:01', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e5973686ed495c379d829ea8b2881fc6', 'e3c13679c73a4f829bcff2aba8fd68b1', '高级表单', '/form/advanced-form', 'form/advancedForm/AdvancedForm', null, null, '1', null, null, '3', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e6bfd1fcabfd7942fdd05f076d1dad38', '2a470fc0c3954d9dbb61de6d80846549', '打印测试', '/jeecg/PrintDemo', 'jeecg/PrintDemo', null, null, '1', null, null, '3', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-19 15:58:48', 'admin', '2019-05-07 20:14:39', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('e8af452d8948ea49d37c934f5100ae6a', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '角色管理', '/isystem/role', 'system/RoleList', null, null, '1', null, null, '2', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('ebb9d82ea16ad864071158e0c449d186', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '分类字典', '/isys/category', 'system/SysCategoryList', null, null, '1', null, '1', '5', '0', null, '1', '1', null, '0', null, 'admin', '2019-05-29 18:48:07', 'admin', '2019-05-29 18:48:27', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('ec8d607d0156e198b11853760319c646', '6e73eb3c26099c191bf03852ee1310a1', '安全设置', '/account/settings/security', 'account/settings/Security', null, null, '1', 'SecuritySettings', null, null, null, null, '1', '1', null, null, null, null, '2018-12-26 18:59:52', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('f0675b52d89100ee88472b6800754a08', '', '统计报表', '/report', 'layouts/RouteView', null, null, '0', null, null, '1', '0', 'bar-chart', '1', '0', null, '0', null, 'admin', '2019-04-03 18:32:02', 'admin', '2019-05-19 18:34:13', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('f1cb187abf927c88b89470d08615f5ac', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '数据字典', '/isystem/dict', 'system/DictList', null, null, '1', null, null, '5', null, null, '1', '1', null, null, null, null, '2018-12-28 13:54:43', null, '2018-12-28 15:37:54', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('f23d9bfff4d9aa6b68569ba2cff38415', '540a2936940846cb98114ffb0d145cb8', '标准列表', '/list/basic-list', 'list/StandardList', null, null, '1', null, null, '6', null, null, '1', '1', null, null, null, null, '2018-12-25 20:34:38', null, null, '0', '0', null);
INSERT INTO `sys_permission` VALUES ('f780d0d3083d849ccbdb1b1baee4911d', '5c8042bd6c601270b2bbd9b20bccc68b', '模板管理', '/modules/message/sysMessageTemplateList', 'modules/message/SysMessageTemplateList', null, null, '1', null, null, '1', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-09 11:50:31', 'admin', '2019-04-12 10:16:34', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('fb07ca05a3e13674dbf6d3245956da2e', '540a2936940846cb98114ffb0d145cb8', '搜索列表', '/list/search', 'list/search/SearchLayout', null, '/list/search/article', '1', null, null, '8', '0', null, '1', '0', null, '0', null, null, '2018-12-25 20:34:38', 'admin', '2019-02-12 15:09:13', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('fb367426764077dcf94640c843733985', '2a470fc0c3954d9dbb61de6d80846549', '一对多示例', '/jeecg/JeecgOrderMainList', 'jeecg/JeecgOrderMainList', null, null, '1', null, null, '2', '0', null, '1', '1', null, '0', null, 'admin', '2019-02-15 16:24:11', 'admin', '2019-02-18 10:50:14', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('fba41089766888023411a978d13c0aa4', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO树表单列表', '/online/cgformTreeList/:code', 'modules/online/cgform/auto/OnlCgformTreeList', null, null, '1', null, '1', '9', '0', null, '1', '1', null, '1', null, 'admin', '2019-05-21 14:46:50', 'admin', '2019-06-11 13:52:52', '0', '0', '1');
INSERT INTO `sys_permission` VALUES ('fc810a2267dd183e4ef7c71cc60f4670', '700b7f95165c46cc7a78bf227aa8fed3', '请求追踪', '/monitor/HttpTrace', 'modules/monitor/HttpTrace', null, null, '1', null, null, '4', '0', null, '1', '1', null, '0', null, 'admin', '2019-04-02 09:46:19', 'admin', '2019-04-02 11:37:27', '0', '0', null);
INSERT INTO `sys_permission` VALUES ('fedfbf4420536cacc0218557d263dfea', '6e73eb3c26099c191bf03852ee1310a1', '新消息通知', '/account/settings/notification', 'account/settings/Notification', null, null, '1', 'NotificationSettings', null, null, null, '', '1', '1', null, null, null, null, '2018-12-26 19:02:05', null, null, '0', '0', null);

-- ----------------------------
-- Table structure for `sys_permission_data_rule`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_data_rule`;
CREATE TABLE `sys_permission_data_rule` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `permission_id` varchar(32) DEFAULT NULL COMMENT '菜单ID',
  `rule_name` varchar(50) DEFAULT NULL COMMENT '规则名称',
  `rule_column` varchar(50) DEFAULT NULL COMMENT '字段',
  `rule_conditions` varchar(50) DEFAULT NULL COMMENT '条件',
  `rule_value` varchar(300) DEFAULT NULL COMMENT '规则值',
  `status` varchar(3) DEFAULT NULL COMMENT '权限有效状态1有0否',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_fucntionid` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_permission_data_rule
-- ----------------------------
INSERT INTO `sys_permission_data_rule` VALUES ('32b62cb04d6c788d9d92e3ff5e66854e', '8d4683aacaa997ab86b966b464360338', '000', '00', '!=', '00', '1', '2019-04-02 18:36:08', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('40283181614231d401614234fe670003', '40283181614231d401614232cd1c0001', 'createBy', 'createBy', '=', '#{sys_user_code}', '1', '2018-01-29 21:57:04', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('4028318161424e730161424fca6f0004', '4028318161424e730161424f61510002', 'createBy', 'createBy', '=', '#{sys_user_code}', '1', '2018-01-29 22:26:20', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402880e6487e661a01487e732c020005', '402889fb486e848101486e93a7c80014', 'SYS_ORG_CODE', 'SYS_ORG_CODE', 'LIKE', '010201%', '1', '2014-09-16 20:32:30', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402880e6487e661a01487e8153ee0007', '402889fb486e848101486e93a7c80014', 'create_by', 'create_by', '', '#{SYS_USER_CODE}', '1', '2014-09-16 20:47:57', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402880ec5ddec439015ddf9225060038', '40288088481d019401481d2fcebf000d', '复杂关系', '', 'USE_SQL_RULES', 'name like \'%张%\' or age > 10', '1', null, null, '2017-08-14 15:10:25', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402880ec5ddfdd26015ddfe3e0570011', '4028ab775dca0d1b015dca3fccb60016', '复杂sql配置', '', 'USE_SQL_RULES', 'table_name like \'%test%\' or is_tree = \'Y\'', '1', null, null, '2017-08-14 16:38:55', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402880f25b1e2ac7015b1e5fdebc0012', '402880f25b1e2ac7015b1e5cdc340010', '只能看自己数据', 'create_by', '=', '#{sys_user_code}', '1', '2017-03-30 16:40:51', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881875b19f141015b19f8125e0014', '40288088481d019401481d2fcebf000d', '可看下属业务数据', 'sys_org_code', 'LIKE', '#{sys_org_code}', '1', null, null, '2017-08-14 15:04:32', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402881e45394d66901539500a4450001', '402881e54df73c73014df75ab670000f', 'sysCompanyCode', 'sysCompanyCode', '=', '#{SYS_COMPANY_CODE}', '1', '2016-03-21 01:09:21', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e45394d6690153950177cb0003', '402881e54df73c73014df75ab670000f', 'sysOrgCode', 'sysOrgCode', '=', '#{SYS_ORG_CODE}', '1', '2016-03-21 01:10:15', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f43101626727aff60067', '402881e56266f43101626724eb730065', '销售自己看自己的数据', 'createBy', '=', '#{sys_user_code}', '1', '2018-03-27 19:11:16', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f4310162672fb1a70082', '402881e56266f43101626724eb730065', '销售经理看所有下级数据', 'sysOrgCode', 'LIKE', '#{sys_org_code}', '1', '2018-03-27 19:20:01', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f431016267387c9f0088', '402881e56266f43101626724eb730065', '只看金额大于1000的数据', 'money', '>=', '1000', '1', '2018-03-27 19:29:37', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402881f3650de25101650dfb5a3a0010', '402881e56266f4310162671d62050044', '22', '', 'USE_SQL_RULES', '22', '1', '2018-08-06 14:45:01', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402889fb486e848101486e913cd6000b', '402889fb486e848101486e8e2e8b0007', 'userName', 'userName', '=', 'admin', '1', '2014-09-13 18:31:25', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('402889fb486e848101486e98d20d0016', '402889fb486e848101486e93a7c80014', 'title', 'title', '=', '12', '1', null, null, '2014-09-13 22:18:22', 'scott');
INSERT INTO `sys_permission_data_rule` VALUES ('402889fe47fcb29c0147fcb6b6220001', '8a8ab0b246dc81120146dc8180fe002b', '12', '12', '>', '12', '1', '2014-08-22 15:55:38', '8a8ab0b246dc81120146dc8181950052', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('4028ab775dca0d1b015dca4183530018', '4028ab775dca0d1b015dca3fccb60016', '表名限制', 'isDbSynch', '=', 'Y', '1', null, null, '2017-08-14 16:43:45', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('4028ef815595a881015595b0ccb60001', '40288088481d019401481d2fcebf000d', '限只能看自己', 'create_by', '=', '#{sys_user_code}', '1', null, null, '2017-08-14 15:03:56', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('4028ef81574ae99701574aed26530005', '4028ef81574ae99701574aeb97bd0003', '用户名', 'userName', '!=', 'admin', '1', '2016-09-21 12:07:18', 'admin', null, null);
INSERT INTO `sys_permission_data_rule` VALUES ('53609e1854f4a87eb23ed23a18a1042c', '4148ec82b6acd69f470bea75fe41c357', '只看当前部门数据', 'sysOrgCode', '=', '#{sys_org_code}', '1', '2019-05-11 19:40:39', 'admin', '2019-05-11 19:40:50', 'admin');
INSERT INTO `sys_permission_data_rule` VALUES ('a7d661ef5ac168b2b162420c6804dac5', '4148ec82b6acd69f470bea75fe41c357', '只看自己的数据', 'createBy', '=', '#{sys_user_code}', '1', '2019-05-11 19:19:05', 'admin', '2019-05-11 19:24:58', 'admin');
INSERT INTO `sys_permission_data_rule` VALUES ('f852d85d47f224990147f2284c0c0005', null, '小于', 'test', '<=', '11', '1', '2014-08-20 14:43:52', '8a8ab0b246dc81120146dc8181950052', null, null);

-- ----------------------------
-- Table structure for `sys_quartz_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_job`;
CREATE TABLE `sys_quartz_job` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除状态',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `job_class_name` varchar(255) DEFAULT NULL COMMENT '任务类名',
  `cron_expression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `parameter` varchar(255) DEFAULT NULL COMMENT '参数',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(1) DEFAULT NULL COMMENT '状态 0正常 -1停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_quartz_job
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_quartz_job_copy`
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_job_copy`;
CREATE TABLE `sys_quartz_job_copy` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除状态',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `job_class_name` varchar(255) DEFAULT NULL COMMENT '任务类名',
  `cron_expression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `parameter` varchar(255) DEFAULT NULL COMMENT '参数',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(1) DEFAULT NULL COMMENT '状态 0正常 -1停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_quartz_job_copy
-- ----------------------------
INSERT INTO `sys_quartz_job_copy` VALUES ('df26ecacf0f75d219d746750fe84bbee', null, null, '0', 'admin', '2019-01-19 15:09:41', 'org.jeecg.modules.quartz.job.SampleParamJob', '0/1 * * * * ?', 'scott', '带参测试 后台将每隔1秒执行输出日志', '-1');
INSERT INTO `sys_quartz_job_copy` VALUES ('a253cdfc811d69fa0efc70d052bc8128', 'admin', '2019-03-30 12:44:48', '0', 'admin', '2019-03-30 12:44:52', 'org.jeecg.modules.quartz.job.SampleJob', '0/1 * * * * ?', null, null, '-1');
INSERT INTO `sys_quartz_job_copy` VALUES ('5b3d2c087ad41aa755fc4f89697b01e7', 'admin', '2019-04-11 19:04:21', '0', 'admin', '2019-04-11 19:49:49', 'org.jeecg.modules.message.job.SendMsgJob', '0/60 * * * * ?', null, null, '-1');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `role_name` varchar(200) DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) NOT NULL COMMENT '角色编码',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `index_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('e51758fa916c881624b046d26bd09230', '人力资源部', 'hr', null, 'admin', '2019-01-21 18:07:24', 'admin', '2019-05-20 11:50:01');
INSERT INTO `sys_role` VALUES ('ee8626f80f7c2619917b6236f3a7f02b', '临时角色', 'test', '这是新建的临时角色123', null, '2018-12-20 10:59:04', 'admin', '2019-02-19 15:08:37');
INSERT INTO `sys_role` VALUES ('f6817f48af4fb3af11b9e8bf182f618b', '管理员', 'admin', '管理员', null, '2018-12-21 18:03:39', 'admin', '2019-05-20 11:40:26');

-- ----------------------------
-- Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` varchar(32) NOT NULL,
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(32) DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_group_role_per_id` (`role_id`,`permission_id`) USING BTREE,
  KEY `index_group_role_id` (`role_id`) USING BTREE,
  KEY `index_group_per_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色权限表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('00b0748f04d3ea52c8cfa179c1c9d384', '52b0cf022ac4187b2a70dfa4f8b2d940', 'd7d6e2e4e2934f2c9385a623fd98c6f3', null);
INSERT INTO `sys_role_permission` VALUES ('00b82058779cca5106fbb84783534c9b', 'f6817f48af4fb3af11b9e8bf182f618b', '4148ec82b6acd69f470bea75fe41c357', null);
INSERT INTO `sys_role_permission` VALUES ('0254c0b25694ad5479e6d6935bbc176e', 'f6817f48af4fb3af11b9e8bf182f618b', '944abf0a8fc22fe1f1154a389a574154', null);
INSERT INTO `sys_role_permission` VALUES ('0794548b4b6650ddcaff31e431b74801', 'ee8626f80f7c2619917b6236f3a7f02b', 'b1cb0a3fedf7ed0e4653cb5a229837ee', null);
INSERT INTO `sys_role_permission` VALUES ('09bd4fc30ffe88c4a44ed3868f442719', 'f6817f48af4fb3af11b9e8bf182f618b', 'e6bfd1fcabfd7942fdd05f076d1dad38', null);
INSERT INTO `sys_role_permission` VALUES ('0a2872c078f10c973d8bd98788429a40', 'ee8626f80f7c2619917b6236f3a7f02b', 'aedbf679b5773c1f25e9f7b10111da73', null);
INSERT INTO `sys_role_permission` VALUES ('0c154618ac16fa9314869380ee1279c5', 'f6817f48af4fb3af11b9e8bf182f618b', '190c2b43bec6a5f7a4194a85db67d96a', null);
INSERT INTO `sys_role_permission` VALUES ('0c2d2db76ee3aa81a4fe0925b0f31365', 'f6817f48af4fb3af11b9e8bf182f618b', '024f1fd1283dc632458976463d8984e1', null);
INSERT INTO `sys_role_permission` VALUES ('0c451b098cf504b07d613875017da52c', 'ee8626f80f7c2619917b6236f3a7f02b', 'd07a2c87a451434c99ab06296727ec4f', null);
INSERT INTO `sys_role_permission` VALUES ('0c6b8facbb1cc874964c87a8cf01e4b1', 'f6817f48af4fb3af11b9e8bf182f618b', '841057b8a1bef8f6b4b20f9a618a7fa6', null);
INSERT INTO `sys_role_permission` VALUES ('0c6e1075e422972083c3e854d9af7851', 'f6817f48af4fb3af11b9e8bf182f618b', '08e6b9dc3c04489c8e1ff2ce6f105aa4', null);
INSERT INTO `sys_role_permission` VALUES ('0dec36b68c234767cd35466efef3b941', 'ee8626f80f7c2619917b6236f3a7f02b', '54dd5457a3190740005c1bfec55b1c34', null);
INSERT INTO `sys_role_permission` VALUES ('0e139e6c1b5b73eee81381ddf0b5a9f3', 'f6817f48af4fb3af11b9e8bf182f618b', '277bfabef7d76e89b33062b16a9a5020', null);
INSERT INTO `sys_role_permission` VALUES ('0e1469997af2d3b97fff56a59ee29eeb', 'f6817f48af4fb3af11b9e8bf182f618b', 'e41b69c57a941a3bbcce45032fe57605', null);
INSERT INTO `sys_role_permission` VALUES ('0f861cb988fdc639bb1ab943471f3a72', 'f6817f48af4fb3af11b9e8bf182f618b', '97c8629abc7848eccdb6d77c24bb3ebb', null);
INSERT INTO `sys_role_permission` VALUES ('105c2ac10741e56a618a82cd58c461d7', 'e51758fa916c881624b046d26bd09230', '1663f3faba244d16c94552f849627d84', null);
INSERT INTO `sys_role_permission` VALUES ('115a6673ae6c0816d3f60de221520274', '21c5a3187763729408b40afb0d0fdfa8', '63b551e81c5956d5c861593d366d8c57', null);
INSERT INTO `sys_role_permission` VALUES ('126ea9faebeec2b914d6d9bef957afb6', 'f6817f48af4fb3af11b9e8bf182f618b', 'f1cb187abf927c88b89470d08615f5ac', null);
INSERT INTO `sys_role_permission` VALUES ('145eac8dd88eddbd4ce0a800ab40a92c', 'e51758fa916c881624b046d26bd09230', '08e6b9dc3c04489c8e1ff2ce6f105aa4', null);
INSERT INTO `sys_role_permission` VALUES ('14865b5bdf19d539375a0a03dff45f4f', 'ee8626f80f7c2619917b6236f3a7f02b', '08e6b9dc3c04489c8e1ff2ce6f105aa4', null);
INSERT INTO `sys_role_permission` VALUES ('154edd0599bd1dc2c7de220b489cd1e2', 'f6817f48af4fb3af11b9e8bf182f618b', '7ac9eb9ccbde2f7a033cd4944272bf1e', null);
INSERT INTO `sys_role_permission` VALUES ('165acd6046a0eaf975099f46a3c898ea', 'f6817f48af4fb3af11b9e8bf182f618b', '4f66409ef3bbd69c1d80469d6e2a885e', null);
INSERT INTO `sys_role_permission` VALUES ('1664b92dff13e1575e3a929caa2fa14d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd2bbf9ebca5a8fa2e227af97d2da7548', null);
INSERT INTO `sys_role_permission` VALUES ('16ef8ed3865ccc6f6306200760896c50', 'ee8626f80f7c2619917b6236f3a7f02b', 'e8af452d8948ea49d37c934f5100ae6a', null);
INSERT INTO `sys_role_permission` VALUES ('17ead5b7d97ed365398ab20009a69ea3', '52b0cf022ac4187b2a70dfa4f8b2d940', 'e08cb190ef230d5d4f03824198773950', null);
INSERT INTO `sys_role_permission` VALUES ('191c4e7c14149e0363e9e57c92fd1d26', 'ee8626f80f7c2619917b6236f3a7f02b', '190c2b43bec6a5f7a4194a85db67d96a', null);
INSERT INTO `sys_role_permission` VALUES ('1ac1688ef8456f384091a03d88a89ab1', '52b0cf022ac4187b2a70dfa4f8b2d940', '693ce69af3432bd00be13c3971a57961', null);
INSERT INTO `sys_role_permission` VALUES ('1af4babaa4227c3cbb830bc5eb513abb', 'ee8626f80f7c2619917b6236f3a7f02b', 'e08cb190ef230d5d4f03824198773950', null);
INSERT INTO `sys_role_permission` VALUES ('1c1dbba68ef1817e7fb19c822d2854e8', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb367426764077dcf94640c843733985', null);
INSERT INTO `sys_role_permission` VALUES ('1c55c4ced20765b8ebab383caa60f0b6', 'e51758fa916c881624b046d26bd09230', 'fb367426764077dcf94640c843733985', null);
INSERT INTO `sys_role_permission` VALUES ('1e099baeae01b747d67aca06bdfc34d1', 'e51758fa916c881624b046d26bd09230', '6ad53fd1b220989a8b71ff482d683a5a', null);
INSERT INTO `sys_role_permission` VALUES ('1e47db875601fd97723254046b5bba90', 'f6817f48af4fb3af11b9e8bf182f618b', 'baf16b7174bd821b6bab23fa9abb200d', null);
INSERT INTO `sys_role_permission` VALUES ('1e5170db3419b321b753e0af98d3eb9d', 'ee8626f80f7c2619917b6236f3a7f02b', '1a0811914300741f4e11838ff37a1d3a', null);
INSERT INTO `sys_role_permission` VALUES ('1f889a43ddacb3eb453dd060af212a5b', 'ee8626f80f7c2619917b6236f3a7f02b', '2dbbafa22cda07fa5d169d741b81fe12', null);
INSERT INTO `sys_role_permission` VALUES ('1fe4d408b85f19618c15bcb768f0ec22', '1750a8fb3e6d90cb7957c02de1dc8e59', '9502685863ab87f0ad1134142788a385', null);
INSERT INTO `sys_role_permission` VALUES ('20e53c87a785688bdc0a5bb6de394ef1', 'f6817f48af4fb3af11b9e8bf182f618b', '540a2936940846cb98114ffb0d145cb8', null);
INSERT INTO `sys_role_permission` VALUES ('248d288586c6ff3bd14381565df84163', '52b0cf022ac4187b2a70dfa4f8b2d940', '3f915b2769fc80648e92d04e84ca059d', null);
INSERT INTO `sys_role_permission` VALUES ('25491ecbd5a9b34f09c8bc447a10ede1', 'f6817f48af4fb3af11b9e8bf182f618b', 'd07a2c87a451434c99ab06296727ec4f', null);
INSERT INTO `sys_role_permission` VALUES ('25f5443f19c34d99718a016d5f54112e', 'ee8626f80f7c2619917b6236f3a7f02b', '6e73eb3c26099c191bf03852ee1310a1', null);
INSERT INTO `sys_role_permission` VALUES ('27489816708b18859768dfed5945c405', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '9502685863ab87f0ad1134142788a385', null);
INSERT INTO `sys_role_permission` VALUES ('2779cdea8367fff37db26a42c1a1f531', 'f6817f48af4fb3af11b9e8bf182f618b', 'fef097f3903caf3a3c3a6efa8de43fbb', null);
INSERT INTO `sys_role_permission` VALUES ('296f9c75ca0e172ae5ce4c1022c996df', '646c628b2b8295fbdab2d34044de0354', '732d48f8e0abe99fe6a23d18a3171cd1', null);
INSERT INTO `sys_role_permission` VALUES ('29fb6b0ad59a7e911c8d27e0bdc42d23', 'f6817f48af4fb3af11b9e8bf182f618b', '9a90363f216a6a08f32eecb3f0bf12a3', null);
INSERT INTO `sys_role_permission` VALUES ('2ad37346c1b83ddeebc008f6987b2227', 'f6817f48af4fb3af11b9e8bf182f618b', '8d1ebd663688965f1fd86a2f0ead3416', null);
INSERT INTO `sys_role_permission` VALUES ('2c462293cbb0eab7e8ae0a3600361b5f', '52b0cf022ac4187b2a70dfa4f8b2d940', '9502685863ab87f0ad1134142788a385', null);
INSERT INTO `sys_role_permission` VALUES ('2fdaed22dfa4c8d4629e44ef81688c6a', '52b0cf022ac4187b2a70dfa4f8b2d940', 'aedbf679b5773c1f25e9f7b10111da73', null);
INSERT INTO `sys_role_permission` VALUES ('300c462b7fec09e2ff32574ef8b3f0bd', '52b0cf022ac4187b2a70dfa4f8b2d940', '2a470fc0c3954d9dbb61de6d80846549', null);
INSERT INTO `sys_role_permission` VALUES ('303305d7301e40867cb6bbea4a651b8a', 'f6817f48af4fb3af11b9e8bf182f618b', '9ab9099e9a8f440826c637016df0e0c0', null);
INSERT INTO `sys_role_permission` VALUES ('326181da3248a62a05e872119a462be1', 'ee8626f80f7c2619917b6236f3a7f02b', '3f915b2769fc80648e92d04e84ca059d', null);
INSERT INTO `sys_role_permission` VALUES ('3369650f5072b330543f8caa556b1b33', 'e51758fa916c881624b046d26bd09230', 'a400e4f4d54f79bf5ce160ae432231af', null);
INSERT INTO `sys_role_permission` VALUES ('35a7e156c20e93aa7e825fe514bf9787', 'e51758fa916c881624b046d26bd09230', 'c6cf95444d80435eb37b2f9db3971ae6', null);
INSERT INTO `sys_role_permission` VALUES ('35ac7cae648de39eb56213ca1b649713', '52b0cf022ac4187b2a70dfa4f8b2d940', 'b1cb0a3fedf7ed0e4653cb5a229837ee', null);
INSERT INTO `sys_role_permission` VALUES ('38a2e55db0960262800576e34b3af44c', 'f6817f48af4fb3af11b9e8bf182f618b', '5c2f42277948043026b7a14692456828', null);
INSERT INTO `sys_role_permission` VALUES ('38dd7a19711e7ffe864232954c06fae9', 'e51758fa916c881624b046d26bd09230', 'd2bbf9ebca5a8fa2e227af97d2da7548', null);
INSERT INTO `sys_role_permission` VALUES ('3b15f3ef68c55b603a9c6eb3f50d534d', 'ee8626f80f7c2619917b6236f3a7f02b', '97c8629abc7848eccdb6d77c24bb3ebb', null);
INSERT INTO `sys_role_permission` VALUES ('3b1886f727ac503c93fecdd06dcb9622', 'f6817f48af4fb3af11b9e8bf182f618b', 'c431130c0bc0ec71b0a5be37747bb36a', null);
INSERT INTO `sys_role_permission` VALUES ('3de2a60c7e42a521fecf6fcc5cb54978', 'f6817f48af4fb3af11b9e8bf182f618b', '2d83d62bd2544b8994c8f38cf17b0ddf', null);
INSERT INTO `sys_role_permission` VALUES ('3e4e38f748b8d87178dd62082e5b7b60', 'f6817f48af4fb3af11b9e8bf182f618b', '7960961b0063228937da5fa8dd73d371', null);
INSERT INTO `sys_role_permission` VALUES ('3e563751942b0879c88ca4de19757b50', '1750a8fb3e6d90cb7957c02de1dc8e59', '58857ff846e61794c69208e9d3a85466', null);
INSERT INTO `sys_role_permission` VALUES ('3f1d04075e3c3254666a4138106a4e51', 'f6817f48af4fb3af11b9e8bf182f618b', '3fac0d3c9cd40fa53ab70d4c583821f8', null);
INSERT INTO `sys_role_permission` VALUES ('3fce2d8d888599c37c173197cc1bf886', 'ee8626f80f7c2619917b6236f3a7f02b', '5c2f42277948043026b7a14692456828', null);
INSERT INTO `sys_role_permission` VALUES ('412e2de37a35b3442d68db8dd2f3c190', '52b0cf022ac4187b2a70dfa4f8b2d940', 'f1cb187abf927c88b89470d08615f5ac', null);
INSERT INTO `sys_role_permission` VALUES ('4204f91fb61911ba8ce40afa7c02369f', 'f6817f48af4fb3af11b9e8bf182f618b', '3f915b2769fc80648e92d04e84ca059d', null);
INSERT INTO `sys_role_permission` VALUES ('439568ff7db6f329bf6dd45b3dfc9456', 'f6817f48af4fb3af11b9e8bf182f618b', '7593c9e3523a17bca83b8d7fe8a34e58', null);
INSERT INTO `sys_role_permission` VALUES ('444126230885d5d38b8fa6072c9f43f8', 'f6817f48af4fb3af11b9e8bf182f618b', 'f780d0d3083d849ccbdb1b1baee4911d', null);
INSERT INTO `sys_role_permission` VALUES ('445656dd187bd8a71605f4bbab1938a3', 'f6817f48af4fb3af11b9e8bf182f618b', '020b06793e4de2eee0007f603000c769', null);
INSERT INTO `sys_role_permission` VALUES ('455cdb482457f529b79b479a2ff74427', 'f6817f48af4fb3af11b9e8bf182f618b', 'e1979bb53e9ea51cecc74d86fd9d2f64', null);
INSERT INTO `sys_role_permission` VALUES ('459aa2e7021b435b4d65414cfbc71c66', 'e51758fa916c881624b046d26bd09230', '4148ec82b6acd69f470bea75fe41c357', null);
INSERT INTO `sys_role_permission` VALUES ('45a358bb738782d1a0edbf7485e81459', 'f6817f48af4fb3af11b9e8bf182f618b', '0ac2ad938963b6c6d1af25477d5b8b51', null);
INSERT INTO `sys_role_permission` VALUES ('4c0940badae3ef9231ee9d042338f2a4', 'e51758fa916c881624b046d26bd09230', '2a470fc0c3954d9dbb61de6d80846549', null);
INSERT INTO `sys_role_permission` VALUES ('4dab5a06acc8ef3297889872caa74747', 'f6817f48af4fb3af11b9e8bf182f618b', 'ffb423d25cc59dcd0532213c4a518261', null);
INSERT INTO `sys_role_permission` VALUES ('4e0a37ed49524df5f08fc6593aee875c', 'f6817f48af4fb3af11b9e8bf182f618b', 'f23d9bfff4d9aa6b68569ba2cff38415', null);
INSERT INTO `sys_role_permission` VALUES ('4ea403fc1d19feb871c8bdd9f94a4ecc', 'f6817f48af4fb3af11b9e8bf182f618b', '2e42e3835c2b44ec9f7bc26c146ee531', null);
INSERT INTO `sys_role_permission` VALUES ('4f254549d9498f06f4cc9b23f3e2c070', 'f6817f48af4fb3af11b9e8bf182f618b', '93d5cfb4448f11e9916698e7f462b4b6', null);
INSERT INTO `sys_role_permission` VALUES ('4f2fd4a190db856e21476de2704bbd99', 'f6817f48af4fb3af11b9e8bf182f618b', '1a0811914300741f4e11838ff37a1d3a', null);
INSERT INTO `sys_role_permission` VALUES ('4faad8ff93cb2b5607cd3d07c1b624ee', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '70b8f33da5f39de1981bf89cf6c99792', null);
INSERT INTO `sys_role_permission` VALUES ('504e326de3f03562cdd186748b48a8c7', 'f6817f48af4fb3af11b9e8bf182f618b', '027aee69baee98a0ed2e01806e89c891', null);
INSERT INTO `sys_role_permission` VALUES ('51b11ce979730f8ce8606da16e4d69bb', 'f6817f48af4fb3af11b9e8bf182f618b', 'e8af452d8948ea49d37c934f5100ae6a', null);
INSERT INTO `sys_role_permission` VALUES ('520b5989e6fe4a302a573d4fee12a40a', 'f6817f48af4fb3af11b9e8bf182f618b', '6531cf3421b1265aeeeabaab5e176e6d', null);
INSERT INTO `sys_role_permission` VALUES ('54fdf85e52807bdb32ce450814abc256', 'f6817f48af4fb3af11b9e8bf182f618b', 'cc50656cf9ca528e6f2150eba4714ad2', null);
INSERT INTO `sys_role_permission` VALUES ('55d731bfe997fad7993f6d2a83275185', 'ee8626f80f7c2619917b6236f3a7f02b', 'ebb9d82ea16ad864071158e0c449d186', null);
INSERT INTO `sys_role_permission` VALUES ('57c0b3a547b815ea3ec8e509b08948b3', '1750a8fb3e6d90cb7957c02de1dc8e59', '3f915b2769fc80648e92d04e84ca059d', null);
INSERT INTO `sys_role_permission` VALUES ('593ee05c4fe4645c7826b7d5e14f23ec', '52b0cf022ac4187b2a70dfa4f8b2d940', '8fb8172747a78756c11916216b8b8066', null);
INSERT INTO `sys_role_permission` VALUES ('5d230e6cd2935c4117f6cb9a7a749e39', 'f6817f48af4fb3af11b9e8bf182f618b', 'fc810a2267dd183e4ef7c71cc60f4670', null);
INSERT INTO `sys_role_permission` VALUES ('5de6871fadb4fe1cdd28989da0126b07', 'f6817f48af4fb3af11b9e8bf182f618b', 'a400e4f4d54f79bf5ce160a3432231af', null);
INSERT INTO `sys_role_permission` VALUES ('5e4015a9a641cbf3fb5d28d9f885d81a', 'f6817f48af4fb3af11b9e8bf182f618b', '2dbbafa22cda07fa5d169d741b81fe12', null);
INSERT INTO `sys_role_permission` VALUES ('5e634a89f75b7a421c02aecfd520325f', 'e51758fa916c881624b046d26bd09230', '339329ed54cf255e1f9392e84f136901', null);
INSERT INTO `sys_role_permission` VALUES ('5e74637c4bec048d1880ad0bd1b00552', 'e51758fa916c881624b046d26bd09230', 'a400e4f4d54f79bf5ce160a3432231af', null);
INSERT INTO `sys_role_permission` VALUES ('5fc194b709336d354640fe29fefd65a3', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '9ba60e626bf2882c31c488aba62b89f0', null);
INSERT INTO `sys_role_permission` VALUES ('60eda4b4db138bdb47edbe8e10e71675', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb07ca05a3e13674dbf6d3245956da2e', null);
INSERT INTO `sys_role_permission` VALUES ('61835e48f3e675f7d3f5c9dd3a10dcf3', 'f6817f48af4fb3af11b9e8bf182f618b', 'f0675b52d89100ee88472b6800754a08', null);
INSERT INTO `sys_role_permission` VALUES ('660fbc40bcb1044738f7cabdf1708c28', 'f6817f48af4fb3af11b9e8bf182f618b', 'b3c824fc22bd953e2eb16ae6914ac8f9', null);
INSERT INTO `sys_role_permission` VALUES ('66b202f8f84fe766176b3f51071836ef', 'f6817f48af4fb3af11b9e8bf182f618b', '1367a93f2c410b169faa7abcbad2f77c', null);
INSERT INTO `sys_role_permission` VALUES ('6b605c261ffbc8ac8a98ae33579c8c78', 'f6817f48af4fb3af11b9e8bf182f618b', 'fba41089766888023411a978d13c0aa4', null);
INSERT INTO `sys_role_permission` VALUES ('6c74518eb6bb9a353f6a6c459c77e64b', 'f6817f48af4fb3af11b9e8bf182f618b', 'b4dfc7d5dd9e8d5b6dd6d4579b1aa559', null);
INSERT INTO `sys_role_permission` VALUES ('6daddafacd7eccb91309530c17c5855d', 'f6817f48af4fb3af11b9e8bf182f618b', 'edfa74d66e8ea63ea432c2910837b150', null);
INSERT INTO `sys_role_permission` VALUES ('6fb4c2142498dd6d5b6c014ef985cb66', 'f6817f48af4fb3af11b9e8bf182f618b', '6e73eb3c26099c191bf03852ee1310a1', null);
INSERT INTO `sys_role_permission` VALUES ('71a5f54a90aa8c7a250a38b7dba39f6f', 'ee8626f80f7c2619917b6236f3a7f02b', '8fb8172747a78756c11916216b8b8066', null);
INSERT INTO `sys_role_permission` VALUES ('737d35f582036cd18bfd4c8e5748eaa4', 'e51758fa916c881624b046d26bd09230', '693ce69af3432bd00be13c3971a57961', null);
INSERT INTO `sys_role_permission` VALUES ('7413acf23b56c906aedb5a36fb75bd3a', 'f6817f48af4fb3af11b9e8bf182f618b', 'a4fc7b64b01a224da066bb16230f9c5a', null);
INSERT INTO `sys_role_permission` VALUES ('75002588591820806', '16457350655250432', '5129710648430592', null);
INSERT INTO `sys_role_permission` VALUES ('75002588604403712', '16457350655250432', '5129710648430593', null);
INSERT INTO `sys_role_permission` VALUES ('75002588612792320', '16457350655250432', '40238597734928384', null);
INSERT INTO `sys_role_permission` VALUES ('75002588625375232', '16457350655250432', '57009744761589760', null);
INSERT INTO `sys_role_permission` VALUES ('75002588633763840', '16457350655250432', '16392452747300864', null);
INSERT INTO `sys_role_permission` VALUES ('75002588637958144', '16457350655250432', '16392767785668608', null);
INSERT INTO `sys_role_permission` VALUES ('75002588650541056', '16457350655250432', '16439068543946752', null);
INSERT INTO `sys_role_permission` VALUES ('76a54a8cc609754360bf9f57e7dbb2db', 'f6817f48af4fb3af11b9e8bf182f618b', 'c65321e57b7949b7a975313220de0422', null);
INSERT INTO `sys_role_permission` VALUES ('77277779875336192', '496138616573952', '5129710648430592', null);
INSERT INTO `sys_role_permission` VALUES ('77277780043108352', '496138616573952', '5129710648430593', null);
INSERT INTO `sys_role_permission` VALUES ('77277780055691264', '496138616573952', '15701400130424832', null);
INSERT INTO `sys_role_permission` VALUES ('77277780064079872', '496138616573952', '16678126574637056', null);
INSERT INTO `sys_role_permission` VALUES ('77277780072468480', '496138616573952', '15701915807518720', null);
INSERT INTO `sys_role_permission` VALUES ('77277780076662784', '496138616573952', '15708892205944832', null);
INSERT INTO `sys_role_permission` VALUES ('77277780085051392', '496138616573952', '16678447719911424', null);
INSERT INTO `sys_role_permission` VALUES ('77277780089245696', '496138616573952', '25014528525733888', null);
INSERT INTO `sys_role_permission` VALUES ('77277780097634304', '496138616573952', '56898976661639168', null);
INSERT INTO `sys_role_permission` VALUES ('77277780135383040', '496138616573952', '40238597734928384', null);
INSERT INTO `sys_role_permission` VALUES ('77277780139577344', '496138616573952', '45235621697949696', null);
INSERT INTO `sys_role_permission` VALUES ('77277780147965952', '496138616573952', '45235787867885568', null);
INSERT INTO `sys_role_permission` VALUES ('77277780156354560', '496138616573952', '45235939278065664', null);
INSERT INTO `sys_role_permission` VALUES ('77277780164743168', '496138616573952', '43117268627886080', null);
INSERT INTO `sys_role_permission` VALUES ('77277780168937472', '496138616573952', '45236734832676864', null);
INSERT INTO `sys_role_permission` VALUES ('77277780181520384', '496138616573952', '45237010692050944', null);
INSERT INTO `sys_role_permission` VALUES ('77277780189908992', '496138616573952', '45237170029465600', null);
INSERT INTO `sys_role_permission` VALUES ('77277780198297600', '496138616573952', '57009544286441472', null);
INSERT INTO `sys_role_permission` VALUES ('77277780206686208', '496138616573952', '57009744761589760', null);
INSERT INTO `sys_role_permission` VALUES ('77277780215074816', '496138616573952', '57009981228060672', null);
INSERT INTO `sys_role_permission` VALUES ('77277780219269120', '496138616573952', '56309618086776832', null);
INSERT INTO `sys_role_permission` VALUES ('77277780227657728', '496138616573952', '57212882168844288', null);
INSERT INTO `sys_role_permission` VALUES ('77277780236046336', '496138616573952', '61560041605435392', null);
INSERT INTO `sys_role_permission` VALUES ('77277780244434944', '496138616573952', '61560275261722624', null);
INSERT INTO `sys_role_permission` VALUES ('77277780257017856', '496138616573952', '61560480518377472', null);
INSERT INTO `sys_role_permission` VALUES ('77277780265406464', '496138616573952', '44986029924421632', null);
INSERT INTO `sys_role_permission` VALUES ('77277780324126720', '496138616573952', '45235228800716800', null);
INSERT INTO `sys_role_permission` VALUES ('77277780332515328', '496138616573952', '45069342940860416', null);
INSERT INTO `sys_role_permission` VALUES ('77277780340903937', '496138616573952', '5129710648430594', null);
INSERT INTO `sys_role_permission` VALUES ('77277780349292544', '496138616573952', '16687383932047360', null);
INSERT INTO `sys_role_permission` VALUES ('77277780357681152', '496138616573952', '16689632049631232', null);
INSERT INTO `sys_role_permission` VALUES ('77277780366069760', '496138616573952', '16689745006432256', null);
INSERT INTO `sys_role_permission` VALUES ('77277780370264064', '496138616573952', '16689883993083904', null);
INSERT INTO `sys_role_permission` VALUES ('77277780374458369', '496138616573952', '16690313745666048', null);
INSERT INTO `sys_role_permission` VALUES ('77277780387041280', '496138616573952', '5129710648430595', null);
INSERT INTO `sys_role_permission` VALUES ('77277780395429888', '496138616573952', '16694861252005888', null);
INSERT INTO `sys_role_permission` VALUES ('77277780403818496', '496138616573952', '16695107491205120', null);
INSERT INTO `sys_role_permission` VALUES ('77277780412207104', '496138616573952', '16695243126607872', null);
INSERT INTO `sys_role_permission` VALUES ('77277780420595712', '496138616573952', '75002207560273920', null);
INSERT INTO `sys_role_permission` VALUES ('77277780428984320', '496138616573952', '76215889006956544', null);
INSERT INTO `sys_role_permission` VALUES ('77277780433178624', '496138616573952', '76216071333351424', null);
INSERT INTO `sys_role_permission` VALUES ('77277780441567232', '496138616573952', '76216264070008832', null);
INSERT INTO `sys_role_permission` VALUES ('77277780449955840', '496138616573952', '76216459709124608', null);
INSERT INTO `sys_role_permission` VALUES ('77277780458344448', '496138616573952', '76216594207870976', null);
INSERT INTO `sys_role_permission` VALUES ('77277780466733056', '496138616573952', '76216702639017984', null);
INSERT INTO `sys_role_permission` VALUES ('77277780475121664', '496138616573952', '58480609315524608', null);
INSERT INTO `sys_role_permission` VALUES ('77277780483510272', '496138616573952', '61394706252173312', null);
INSERT INTO `sys_role_permission` VALUES ('77277780491898880', '496138616573952', '61417744146370560', null);
INSERT INTO `sys_role_permission` VALUES ('77277780496093184', '496138616573952', '76606430504816640', null);
INSERT INTO `sys_role_permission` VALUES ('77277780504481792', '496138616573952', '76914082455752704', null);
INSERT INTO `sys_role_permission` VALUES ('77277780508676097', '496138616573952', '76607201262702592', null);
INSERT INTO `sys_role_permission` VALUES ('77277780517064704', '496138616573952', '39915540965232640', null);
INSERT INTO `sys_role_permission` VALUES ('77277780525453312', '496138616573952', '41370251991977984', null);
INSERT INTO `sys_role_permission` VALUES ('77277780538036224', '496138616573952', '45264987354042368', null);
INSERT INTO `sys_role_permission` VALUES ('77277780546424832', '496138616573952', '45265487029866496', null);
INSERT INTO `sys_role_permission` VALUES ('77277780554813440', '496138616573952', '45265762415284224', null);
INSERT INTO `sys_role_permission` VALUES ('77277780559007744', '496138616573952', '45265886315024384', null);
INSERT INTO `sys_role_permission` VALUES ('77277780567396352', '496138616573952', '45266070000373760', null);
INSERT INTO `sys_role_permission` VALUES ('77277780571590656', '496138616573952', '41363147411427328', null);
INSERT INTO `sys_role_permission` VALUES ('77277780579979264', '496138616573952', '41363537456533504', null);
INSERT INTO `sys_role_permission` VALUES ('77277780588367872', '496138616573952', '41364927394353152', null);
INSERT INTO `sys_role_permission` VALUES ('77277780596756480', '496138616573952', '41371711400054784', null);
INSERT INTO `sys_role_permission` VALUES ('77277780605145088', '496138616573952', '41469219249852416', null);
INSERT INTO `sys_role_permission` VALUES ('77277780613533696', '496138616573952', '39916171171991552', null);
INSERT INTO `sys_role_permission` VALUES ('77277780621922304', '496138616573952', '39918482854252544', null);
INSERT INTO `sys_role_permission` VALUES ('77277780630310912', '496138616573952', '41373430515240960', null);
INSERT INTO `sys_role_permission` VALUES ('77277780718391296', '496138616573952', '41375330996326400', null);
INSERT INTO `sys_role_permission` VALUES ('77277780722585600', '496138616573952', '63741744973352960', null);
INSERT INTO `sys_role_permission` VALUES ('77277780730974208', '496138616573952', '42082442672082944', null);
INSERT INTO `sys_role_permission` VALUES ('77277780739362816', '496138616573952', '41376192166629376', null);
INSERT INTO `sys_role_permission` VALUES ('77277780747751424', '496138616573952', '41377034236071936', null);
INSERT INTO `sys_role_permission` VALUES ('77277780756140032', '496138616573952', '56911328312299520', null);
INSERT INTO `sys_role_permission` VALUES ('77277780764528640', '496138616573952', '41378916912336896', null);
INSERT INTO `sys_role_permission` VALUES ('77277780768722944', '496138616573952', '63482475359244288', null);
INSERT INTO `sys_role_permission` VALUES ('77277780772917249', '496138616573952', '64290663792906240', null);
INSERT INTO `sys_role_permission` VALUES ('77277780785500160', '496138616573952', '66790433014943744', null);
INSERT INTO `sys_role_permission` VALUES ('77277780789694464', '496138616573952', '42087054753927168', null);
INSERT INTO `sys_role_permission` VALUES ('77277780798083072', '496138616573952', '67027338952445952', null);
INSERT INTO `sys_role_permission` VALUES ('77277780806471680', '496138616573952', '67027909637836800', null);
INSERT INTO `sys_role_permission` VALUES ('77277780810665985', '496138616573952', '67042515441684480', null);
INSERT INTO `sys_role_permission` VALUES ('77277780823248896', '496138616573952', '67082402312228864', null);
INSERT INTO `sys_role_permission` VALUES ('77277780827443200', '496138616573952', '16392452747300864', null);
INSERT INTO `sys_role_permission` VALUES ('77277780835831808', '496138616573952', '16392767785668608', null);
INSERT INTO `sys_role_permission` VALUES ('77277780840026112', '496138616573952', '16438800255291392', null);
INSERT INTO `sys_role_permission` VALUES ('77277780844220417', '496138616573952', '16438962738434048', null);
INSERT INTO `sys_role_permission` VALUES ('77277780852609024', '496138616573952', '16439068543946752', null);
INSERT INTO `sys_role_permission` VALUES ('77277860062040064', '496138616573953', '5129710648430592', null);
INSERT INTO `sys_role_permission` VALUES ('77277860070428672', '496138616573953', '5129710648430593', null);
INSERT INTO `sys_role_permission` VALUES ('77277860078817280', '496138616573953', '40238597734928384', null);
INSERT INTO `sys_role_permission` VALUES ('77277860091400192', '496138616573953', '43117268627886080', null);
INSERT INTO `sys_role_permission` VALUES ('77277860099788800', '496138616573953', '57009744761589760', null);
INSERT INTO `sys_role_permission` VALUES ('77277860112371712', '496138616573953', '56309618086776832', null);
INSERT INTO `sys_role_permission` VALUES ('77277860120760320', '496138616573953', '44986029924421632', null);
INSERT INTO `sys_role_permission` VALUES ('77277860129148928', '496138616573953', '5129710648430594', null);
INSERT INTO `sys_role_permission` VALUES ('77277860141731840', '496138616573953', '5129710648430595', null);
INSERT INTO `sys_role_permission` VALUES ('77277860150120448', '496138616573953', '75002207560273920', null);
INSERT INTO `sys_role_permission` VALUES ('77277860158509056', '496138616573953', '58480609315524608', null);
INSERT INTO `sys_role_permission` VALUES ('77277860162703360', '496138616573953', '76606430504816640', null);
INSERT INTO `sys_role_permission` VALUES ('77277860171091968', '496138616573953', '76914082455752704', null);
INSERT INTO `sys_role_permission` VALUES ('77277860179480576', '496138616573953', '76607201262702592', null);
INSERT INTO `sys_role_permission` VALUES ('77277860187869184', '496138616573953', '39915540965232640', null);
INSERT INTO `sys_role_permission` VALUES ('77277860196257792', '496138616573953', '41370251991977984', null);
INSERT INTO `sys_role_permission` VALUES ('77277860204646400', '496138616573953', '41363147411427328', null);
INSERT INTO `sys_role_permission` VALUES ('77277860208840704', '496138616573953', '41371711400054784', null);
INSERT INTO `sys_role_permission` VALUES ('77277860213035009', '496138616573953', '39916171171991552', null);
INSERT INTO `sys_role_permission` VALUES ('77277860221423616', '496138616573953', '39918482854252544', null);
INSERT INTO `sys_role_permission` VALUES ('77277860225617920', '496138616573953', '41373430515240960', null);
INSERT INTO `sys_role_permission` VALUES ('77277860234006528', '496138616573953', '41375330996326400', null);
INSERT INTO `sys_role_permission` VALUES ('77277860242395136', '496138616573953', '63741744973352960', null);
INSERT INTO `sys_role_permission` VALUES ('77277860250783744', '496138616573953', '42082442672082944', null);
INSERT INTO `sys_role_permission` VALUES ('77277860254978048', '496138616573953', '41376192166629376', null);
INSERT INTO `sys_role_permission` VALUES ('77277860263366656', '496138616573953', '41377034236071936', null);
INSERT INTO `sys_role_permission` VALUES ('77277860271755264', '496138616573953', '56911328312299520', null);
INSERT INTO `sys_role_permission` VALUES ('77277860313698304', '496138616573953', '41378916912336896', null);
INSERT INTO `sys_role_permission` VALUES ('77277860322086912', '496138616573953', '63482475359244288', null);
INSERT INTO `sys_role_permission` VALUES ('77277860326281216', '496138616573953', '64290663792906240', null);
INSERT INTO `sys_role_permission` VALUES ('77277860334669824', '496138616573953', '66790433014943744', null);
INSERT INTO `sys_role_permission` VALUES ('77277860343058432', '496138616573953', '42087054753927168', null);
INSERT INTO `sys_role_permission` VALUES ('77277860347252736', '496138616573953', '67027338952445952', null);
INSERT INTO `sys_role_permission` VALUES ('77277860351447041', '496138616573953', '67027909637836800', null);
INSERT INTO `sys_role_permission` VALUES ('77277860359835648', '496138616573953', '67042515441684480', null);
INSERT INTO `sys_role_permission` VALUES ('77277860364029952', '496138616573953', '67082402312228864', null);
INSERT INTO `sys_role_permission` VALUES ('77277860368224256', '496138616573953', '16392452747300864', null);
INSERT INTO `sys_role_permission` VALUES ('77277860372418560', '496138616573953', '16392767785668608', null);
INSERT INTO `sys_role_permission` VALUES ('77277860376612865', '496138616573953', '16438800255291392', null);
INSERT INTO `sys_role_permission` VALUES ('77277860385001472', '496138616573953', '16438962738434048', null);
INSERT INTO `sys_role_permission` VALUES ('77277860389195776', '496138616573953', '16439068543946752', null);
INSERT INTO `sys_role_permission` VALUES ('7750f9be48ee09cd561fce718219a3e2', 'ee8626f80f7c2619917b6236f3a7f02b', '882a73768cfd7f78f3a37584f7299656', null);
INSERT INTO `sys_role_permission` VALUES ('7a5d31ba48fe3fb1266bf186dc5f7ba7', '52b0cf022ac4187b2a70dfa4f8b2d940', '58857ff846e61794c69208e9d3a85466', null);
INSERT INTO `sys_role_permission` VALUES ('7a6bca9276c128309c80d21e795c66c6', 'f6817f48af4fb3af11b9e8bf182f618b', '54097c6a3cf50fad0793a34beff1efdf', null);
INSERT INTO `sys_role_permission` VALUES ('7ca833caa5eac837b7200d8b6de8b2e3', 'f6817f48af4fb3af11b9e8bf182f618b', 'fedfbf4420536cacc0218557d263dfea', null);
INSERT INTO `sys_role_permission` VALUES ('7de42bdc0b8c5446b7d428c66a7abc12', '52b0cf022ac4187b2a70dfa4f8b2d940', '54dd5457a3190740005c1bfec55b1c34', null);
INSERT INTO `sys_role_permission` VALUES ('7e19d90cec0dd87aaef351b9ff8f4902', '646c628b2b8295fbdab2d34044de0354', 'f9d3f4f27653a71c52faa9fb8070fbe7', null);
INSERT INTO `sys_role_permission` VALUES ('7f862c47003eb20e8bad05f506371f92', 'ee8626f80f7c2619917b6236f3a7f02b', 'd7d6e2e4e2934f2c9385a623fd98c6f3', null);
INSERT INTO `sys_role_permission` VALUES ('812ed54661b1a24b81b58974691a73f5', 'e51758fa916c881624b046d26bd09230', 'e6bfd1fcabfd7942fdd05f076d1dad38', null);
INSERT INTO `sys_role_permission` VALUES ('817ab01d5438e27c5273fbb1964d0ba3', 'ee8626f80f7c2619917b6236f3a7f02b', '700b7f95165c46cc7a78bf227aa8fed3', null);
INSERT INTO `sys_role_permission` VALUES ('82f2eba6032f151b7a248e21c6fff937', 'ee8626f80f7c2619917b6236f3a7f02b', '8d1ebd663688965f1fd86a2f0ead3416', null);
INSERT INTO `sys_role_permission` VALUES ('84d32474316a43b01256d6644e6e7751', 'ee8626f80f7c2619917b6236f3a7f02b', 'ec8d607d0156e198b11853760319c646', null);
INSERT INTO `sys_role_permission` VALUES ('84eac2f113c23737128fb099d1d1da89', 'f6817f48af4fb3af11b9e8bf182f618b', '03dc3d93261dda19fc86dd7ca486c6cf', null);
INSERT INTO `sys_role_permission` VALUES ('85755a6c0bdff78b3860b52d35310c7f', 'e51758fa916c881624b046d26bd09230', 'c65321e57b7949b7a975313220de0422', null);
INSERT INTO `sys_role_permission` VALUES ('86060e2867a5049d8a80d9fe5d8bc28b', 'f6817f48af4fb3af11b9e8bf182f618b', '765dd244f37b804e3d00f475fd56149b', null);
INSERT INTO `sys_role_permission` VALUES ('8703a2410cddb713c33232ce16ec04b9', 'ee8626f80f7c2619917b6236f3a7f02b', '1367a93f2c410b169faa7abcbad2f77c', null);
INSERT INTO `sys_role_permission` VALUES ('884f147c20e003cc80ed5b7efa598cbe', 'f6817f48af4fb3af11b9e8bf182f618b', 'e5973686ed495c379d829ea8b2881fc6', null);
INSERT INTO `sys_role_permission` VALUES ('88f7c1199b2d5eaf5abf3b860d77a113', 'ee8626f80f7c2619917b6236f3a7f02b', '7593c9e3523a17bca83b8d7fe8a34e58', null);
INSERT INTO `sys_role_permission` VALUES ('8a60df8d8b4c9ee5fa63f48aeee3ec00', '1750a8fb3e6d90cb7957c02de1dc8e59', 'd7d6e2e4e2934f2c9385a623fd98c6f3', null);
INSERT INTO `sys_role_permission` VALUES ('8b09925bdc194ab7f3559cd3a7ea0507', 'f6817f48af4fb3af11b9e8bf182f618b', 'ebb9d82ea16ad864071158e0c449d186', null);
INSERT INTO `sys_role_permission` VALUES ('8ce1022dac4e558ff9694600515cf510', '1750a8fb3e6d90cb7957c02de1dc8e59', '08e6b9dc3c04489c8e1ff2ce6f105aa4', null);
INSERT INTO `sys_role_permission` VALUES ('8d154c2382a8ae5c8d1b84bd38df2a93', 'f6817f48af4fb3af11b9e8bf182f618b', 'd86f58e7ab516d3bc6bfb1fe10585f97', null);
INSERT INTO `sys_role_permission` VALUES ('8d7cd842c7b8f2239e25a546bc608f5a', 'ee8626f80f7c2619917b6236f3a7f02b', '841057b8a1bef8f6b4b20f9a618a7fa6', null);
INSERT INTO `sys_role_permission` VALUES ('8d848ca7feec5b7ebb3ecb32b2c8857a', '52b0cf022ac4187b2a70dfa4f8b2d940', '4148ec82b6acd69f470bea75fe41c357', null);
INSERT INTO `sys_role_permission` VALUES ('8dd64f65a1014196078d0882f767cd85', 'f6817f48af4fb3af11b9e8bf182f618b', 'e3c13679c73a4f829bcff2aba8fd68b1', null);
INSERT INTO `sys_role_permission` VALUES ('8e3dc1671abad4f3c83883b194d2e05a', 'f6817f48af4fb3af11b9e8bf182f618b', 'b1cb0a3fedf7ed0e4653cb5a229837ee', null);
INSERT INTO `sys_role_permission` VALUES ('8f762ff80253f634b08cf59a77742ba4', 'ee8626f80f7c2619917b6236f3a7f02b', '9502685863ab87f0ad1134142788a385', null);
INSERT INTO `sys_role_permission` VALUES ('905bf419332ebcb83863603b3ebe30f0', 'f6817f48af4fb3af11b9e8bf182f618b', '8fb8172747a78756c11916216b8b8066', null);
INSERT INTO `sys_role_permission` VALUES ('90e1c607a0631364eec310f3cc4acebd', 'ee8626f80f7c2619917b6236f3a7f02b', '4f66409ef3bbd69c1d80469d6e2a885e', null);
INSERT INTO `sys_role_permission` VALUES ('9264104cee9b10c96241d527b2d0346d', '1750a8fb3e6d90cb7957c02de1dc8e59', '54dd5457a3190740005c1bfec55b1c34', null);
INSERT INTO `sys_role_permission` VALUES ('9380121ca9cfee4b372194630fce150e', 'f6817f48af4fb3af11b9e8bf182f618b', '65a8f489f25a345836b7f44b1181197a', null);
INSERT INTO `sys_role_permission` VALUES ('94911fef73a590f6824105ebf9b6cab3', 'f6817f48af4fb3af11b9e8bf182f618b', '8b3bff2eee6f1939147f5c68292a1642', null);
INSERT INTO `sys_role_permission` VALUES ('9700d20dbc1ae3cbf7de1c810b521fe6', 'f6817f48af4fb3af11b9e8bf182f618b', 'ec8d607d0156e198b11853760319c646', null);
INSERT INTO `sys_role_permission` VALUES ('980171fda43adfe24840959b1d048d4d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd7d6e2e4e2934f2c9385a623fd98c6f3', null);
INSERT INTO `sys_role_permission` VALUES ('987c23b70873bd1d6dca52f30aafd8c2', 'f6817f48af4fb3af11b9e8bf182f618b', '00a2a0ae65cdca5e93209cdbde97cbe6', null);
INSERT INTO `sys_role_permission` VALUES ('9b2ad767f9861e64a20b097538feafd3', 'f6817f48af4fb3af11b9e8bf182f618b', '73678f9daa45ed17a3674131b03432fb', null);
INSERT INTO `sys_role_permission` VALUES ('9ccd7a5b90b3defe21fd15e2cb7d345b', 'ee8626f80f7c2619917b6236f3a7f02b', '81b732362f9c77d11eea8c9541ef575c', null);
INSERT INTO `sys_role_permission` VALUES ('9d8772c310b675ae43eacdbc6c7fa04a', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '1663f3faba244d16c94552f849627d84', null);
INSERT INTO `sys_role_permission` VALUES ('9d980ec0489040e631a9c24a6af42934', 'f6817f48af4fb3af11b9e8bf182f618b', '05b3c82ddb2536a4a5ee1a4c46b5abef', null);
INSERT INTO `sys_role_permission` VALUES ('9f8311ecccd44e079723098cf2ffe1cc', '1750a8fb3e6d90cb7957c02de1dc8e59', '693ce69af3432bd00be13c3971a57961', null);
INSERT INTO `sys_role_permission` VALUES ('a034ed7c38c996b880d3e78f586fe0ae', 'f6817f48af4fb3af11b9e8bf182f618b', 'c89018ea6286e852b424466fd92a2ffc', null);
INSERT INTO `sys_role_permission` VALUES ('a307a9349ad64a2eff8ab69582fa9be4', 'f6817f48af4fb3af11b9e8bf182f618b', '0620e402857b8c5b605e1ad9f4b89350', null);
INSERT INTO `sys_role_permission` VALUES ('a5d25fdb3c62904a8474182706ce11a0', 'f6817f48af4fb3af11b9e8bf182f618b', '418964ba087b90a84897b62474496b93', null);
INSERT INTO `sys_role_permission` VALUES ('a72c31a3913c736d4eca11d13be99183', 'e51758fa916c881624b046d26bd09230', 'a44c30db536349e91106223957e684eb', null);
INSERT INTO `sys_role_permission` VALUES ('a7ab87eac0f8fafa2efa4b1f9351923f', 'ee8626f80f7c2619917b6236f3a7f02b', 'fedfbf4420536cacc0218557d263dfea', null);
INSERT INTO `sys_role_permission` VALUES ('a8d6138e99e825a9b2204941685580d3', 'ee8626f80f7c2619917b6236f3a7f02b', 'fc810a2267dd183e4ef7c71cc60f4670', null);
INSERT INTO `sys_role_permission` VALUES ('acacce4417e5d7f96a9c3be2ded5b4be', 'f6817f48af4fb3af11b9e8bf182f618b', 'f9d3f4f27653a71c52faa9fb8070fbe7', null);
INSERT INTO `sys_role_permission` VALUES ('ae1852fb349d8513eb3fdc173da3ee56', 'f6817f48af4fb3af11b9e8bf182f618b', '8d4683aacaa997ab86b966b464360338', null);
INSERT INTO `sys_role_permission` VALUES ('aefc8c22e061171806e59cd222f6b7e1', '52b0cf022ac4187b2a70dfa4f8b2d940', 'e8af452d8948ea49d37c934f5100ae6a', null);
INSERT INTO `sys_role_permission` VALUES ('af60ac8fafd807ed6b6b354613b9ccbc', 'f6817f48af4fb3af11b9e8bf182f618b', '58857ff846e61794c69208e9d3a85466', null);
INSERT INTO `sys_role_permission` VALUES ('b0c8a20800b8bf1ebdd7be473bceb44f', 'f6817f48af4fb3af11b9e8bf182f618b', '58b9204feaf07e47284ddb36cd2d8468', null);
INSERT INTO `sys_role_permission` VALUES ('b128ebe78fa5abb54a3a82c6689bdca3', 'f6817f48af4fb3af11b9e8bf182f618b', 'aedbf679b5773c1f25e9f7b10111da73', null);
INSERT INTO `sys_role_permission` VALUES ('b1e54050a8ba4be21ed1feee212ddc8e', 'f6817f48af4fb3af11b9e8bf182f618b', '81b732362f9c77d11eea8c9541ef575c', null);
INSERT INTO `sys_role_permission` VALUES ('b21b07951bb547b09cc85624a841aea0', 'f6817f48af4fb3af11b9e8bf182f618b', '4356a1a67b564f0988a484f5531fd4d9', null);
INSERT INTO `sys_role_permission` VALUES ('b3708f8d94c482f859964ef04ec0df52', 'ee8626f80f7c2619917b6236f3a7f02b', '9ab9099e9a8f440826c637016df0e0c0', null);
INSERT INTO `sys_role_permission` VALUES ('b64c4ab9cd9a2ea8ac1e9db5fb7cf522', 'f6817f48af4fb3af11b9e8bf182f618b', '2aeddae571695cd6380f6d6d334d6e7d', null);
INSERT INTO `sys_role_permission` VALUES ('bbec16ad016efec9ea2def38f4d3d9dc', 'f6817f48af4fb3af11b9e8bf182f618b', '13212d3416eb690c2e1d5033166ff47a', null);
INSERT INTO `sys_role_permission` VALUES ('bd30561f141f07827b836878137fddd8', 'e51758fa916c881624b046d26bd09230', '65a8f489f25a345836b7f44b1181197a', null);
INSERT INTO `sys_role_permission` VALUES ('be8e5a9080569e59863f20c4c57a8e22', 'f6817f48af4fb3af11b9e8bf182f618b', '22d6a3d39a59dd7ea9a30acfa6bfb0a5', null);
INSERT INTO `sys_role_permission` VALUES ('bea2986432079d89203da888d99b3f16', 'f6817f48af4fb3af11b9e8bf182f618b', '54dd5457a3190740005c1bfec55b1c34', null);
INSERT INTO `sys_role_permission` VALUES ('c09373ebfc73fb5740db5ff02cba4f91', 'f6817f48af4fb3af11b9e8bf182f618b', '339329ed54cf255e1f9392e84f136901', null);
INSERT INTO `sys_role_permission` VALUES ('c56fb1658ee5f7476380786bf5905399', 'f6817f48af4fb3af11b9e8bf182f618b', 'de13e0f6328c069748de7399fcc1dbbd', null);
INSERT INTO `sys_role_permission` VALUES ('c689539d20a445b0896270290c58d01f', 'e51758fa916c881624b046d26bd09230', '13212d3416eb690c2e1d5033166ff47a', null);
INSERT INTO `sys_role_permission` VALUES ('c6fee38d293b9d0596436a0cbd205070', 'f6817f48af4fb3af11b9e8bf182f618b', '4f84f9400e5e92c95f05b554724c2b58', null);
INSERT INTO `sys_role_permission` VALUES ('c8571839e6b14796e661f3e2843b80b6', 'ee8626f80f7c2619917b6236f3a7f02b', '45c966826eeff4c99b8f8ebfe74511fc', null);
INSERT INTO `sys_role_permission` VALUES ('c90b0b01c7ca454d2a1cb7408563e696', 'f6817f48af4fb3af11b9e8bf182f618b', '882a73768cfd7f78f3a37584f7299656', null);
INSERT INTO `sys_role_permission` VALUES ('c9d35261cccd67ab2932107a0967a7d7', 'e51758fa916c881624b046d26bd09230', 'b4dfc7d5dd9e8d5b6dd6d4579b1aa559', null);
INSERT INTO `sys_role_permission` VALUES ('ced80e43584ce15e97bb07298e93020d', 'e51758fa916c881624b046d26bd09230', '45c966826eeff4c99b8f8ebfe74511fc', null);
INSERT INTO `sys_role_permission` VALUES ('cf1feb1bf69eafc982295ad6c9c8d698', 'f6817f48af4fb3af11b9e8bf182f618b', 'a2b11669e98c5fe54a53c3e3c4f35d14', null);
INSERT INTO `sys_role_permission` VALUES ('cf2ef620217673e4042f695743294f01', 'f6817f48af4fb3af11b9e8bf182f618b', '717f6bee46f44a3897eca9abd6e2ec44', null);
INSERT INTO `sys_role_permission` VALUES ('cf43895aef7fc684669483ab00ef2257', 'f6817f48af4fb3af11b9e8bf182f618b', '700b7f95165c46cc7a78bf227aa8fed3', null);
INSERT INTO `sys_role_permission` VALUES ('d03d792b0f312e7b490afc5cec3dd6c5', 'e51758fa916c881624b046d26bd09230', '8fb8172747a78756c11916216b8b8066', null);
INSERT INTO `sys_role_permission` VALUES ('d09ae00904c5cea11f1433aec5e4631b', 'ee8626f80f7c2619917b6236f3a7f02b', '024f1fd1283dc632458976463d8984e1', null);
INSERT INTO `sys_role_permission` VALUES ('d281a95b8f293d0fa2a136f46c4e0b10', 'f6817f48af4fb3af11b9e8bf182f618b', '5c8042bd6c601270b2bbd9b20bccc68b', null);
INSERT INTO `sys_role_permission` VALUES ('d370a978d80c95aca784e09be4dd0f9c', 'ee8626f80f7c2619917b6236f3a7f02b', '53a9230444d33de28aa11cc108fb1dba', null);
INSERT INTO `sys_role_permission` VALUES ('d37ad568e26f46ed0feca227aa9c2ffa', 'f6817f48af4fb3af11b9e8bf182f618b', '9502685863ab87f0ad1134142788a385', null);
INSERT INTO `sys_role_permission` VALUES ('d3ddcacee1acdfaa0810618b74e38ef2', 'f6817f48af4fb3af11b9e8bf182f618b', 'c6cf95444d80435eb37b2f9db3971ae6', null);
INSERT INTO `sys_role_permission` VALUES ('d3fe195d59811531c05d31d8436f5c8b', '1750a8fb3e6d90cb7957c02de1dc8e59', 'e8af452d8948ea49d37c934f5100ae6a', null);
INSERT INTO `sys_role_permission` VALUES ('d5267597a4450f06d49d2fb63859641a', 'e51758fa916c881624b046d26bd09230', '2dbbafa22cda07fa5d169d741b81fe12', null);
INSERT INTO `sys_role_permission` VALUES ('d83282192a69514cfe6161b3087ff962', 'f6817f48af4fb3af11b9e8bf182f618b', '53a9230444d33de28aa11cc108fb1dba', null);
INSERT INTO `sys_role_permission` VALUES ('d8a5c9079df12090e108e21be94b4fd7', 'f6817f48af4fb3af11b9e8bf182f618b', '078f9558cdeab239aecb2bda1a8ed0d1', null);
INSERT INTO `sys_role_permission` VALUES ('db2384c21c2c2c39dc46fb89408648eb', 'ee8626f80f7c2619917b6236f3a7f02b', '8b3bff2eee6f1939147f5c68292a1642', null);
INSERT INTO `sys_role_permission` VALUES ('dbc5dd836d45c5bc7bc94b22596ab956', 'f6817f48af4fb3af11b9e8bf182f618b', '1939e035e803a99ceecb6f5563570fb2', null);
INSERT INTO `sys_role_permission` VALUES ('dc5470785c947f8ee15dd79efec0dc33', 'ee8626f80f7c2619917b6236f3a7f02b', '16f26e7c6f85876d8b227d3e32b3942f', null);
INSERT INTO `sys_role_permission` VALUES ('dc83bb13c0e8c930e79d28b2db26f01f', 'f6817f48af4fb3af11b9e8bf182f618b', '63b551e81c5956d5c861593d366d8c57', null);
INSERT INTO `sys_role_permission` VALUES ('dc8fd3f79bd85bd832608b42167a1c71', 'f6817f48af4fb3af11b9e8bf182f618b', '91c23960fab49335831cf43d820b0a61', null);
INSERT INTO `sys_role_permission` VALUES ('de82e89b8b60a3ea99be5348f565c240', 'f6817f48af4fb3af11b9e8bf182f618b', '56ca78fe0f22d815fabc793461af67b8', null);
INSERT INTO `sys_role_permission` VALUES ('de8f43229e351d34af3c95b1b9f0a15d', 'f6817f48af4fb3af11b9e8bf182f618b', 'a400e4f4d54f79bf5ce160ae432231af', null);
INSERT INTO `sys_role_permission` VALUES ('e10c35aa23121ed1afde45f646f35155', 'ee8626f80f7c2619917b6236f3a7f02b', '58857ff846e61794c69208e9d3a85466', null);
INSERT INTO `sys_role_permission` VALUES ('e18cbc19d75947bac931c864479e3c13', 'ee8626f80f7c2619917b6236f3a7f02b', 'f1cb187abf927c88b89470d08615f5ac', null);
INSERT INTO `sys_role_permission` VALUES ('e3e922673f4289b18366bb51b6200f17', '52b0cf022ac4187b2a70dfa4f8b2d940', '45c966826eeff4c99b8f8ebfe74511fc', null);
INSERT INTO `sys_role_permission` VALUES ('e7467726ee72235baaeb47df04a35e73', 'f6817f48af4fb3af11b9e8bf182f618b', 'e08cb190ef230d5d4f03824198773950', null);
INSERT INTO `sys_role_permission` VALUES ('eaef4486f1c9b0408580bbfa2037eb66', 'f6817f48af4fb3af11b9e8bf182f618b', '2a470fc0c3954d9dbb61de6d80846549', null);
INSERT INTO `sys_role_permission` VALUES ('ec4bc97829ab56afd83f428b6dc37ff6', 'f6817f48af4fb3af11b9e8bf182f618b', '200006f0edf145a2b50eacca07585451', null);
INSERT INTO `sys_role_permission` VALUES ('ec846a3f85fdb6813e515be71f11b331', 'f6817f48af4fb3af11b9e8bf182f618b', '732d48f8e0abe99fe6a23d18a3171cd1', null);
INSERT INTO `sys_role_permission` VALUES ('ec93bb06f5be4c1f19522ca78180e2ef', 'f6817f48af4fb3af11b9e8bf182f618b', '265de841c58907954b8877fb85212622', null);
INSERT INTO `sys_role_permission` VALUES ('ecdd72fe694e6bba9c1d9fc925ee79de', 'f6817f48af4fb3af11b9e8bf182f618b', '45c966826eeff4c99b8f8ebfe74511fc', null);
INSERT INTO `sys_role_permission` VALUES ('edefd8d468f5727db465cf1b860af474', 'f6817f48af4fb3af11b9e8bf182f618b', '6ad53fd1b220989a8b71ff482d683a5a', null);
INSERT INTO `sys_role_permission` VALUES ('ef8bdd20d29447681ec91d3603e80c7b', 'f6817f48af4fb3af11b9e8bf182f618b', 'ae4fed059f67086fd52a73d913cf473d', null);
INSERT INTO `sys_role_permission` VALUES ('f12b6c90e8913183d7ca547c66600891', 'e51758fa916c881624b046d26bd09230', 'aedbf679b5773c1f25e9f7b10111da73', null);
INSERT INTO `sys_role_permission` VALUES ('f177acac0276329dc66af0c9ad30558a', 'f6817f48af4fb3af11b9e8bf182f618b', 'c2c356bf4ddd29975347a7047a062440', null);
INSERT INTO `sys_role_permission` VALUES ('f17ab8ad1e71341140857ef4914ef297', '21c5a3187763729408b40afb0d0fdfa8', '732d48f8e0abe99fe6a23d18a3171cd1', null);
INSERT INTO `sys_role_permission` VALUES ('f421d5acd1420cf09ba45eb2608fa7db', 'f6817f48af4fb3af11b9e8bf182f618b', '16f26e7c6f85876d8b227d3e32b3942f', null);
INSERT INTO `sys_role_permission` VALUES ('f99f99cc3bc27220cdd4f5aced33b7d7', 'f6817f48af4fb3af11b9e8bf182f618b', '655563cd64b75dcf52ef7bcdd4836953', null);
INSERT INTO `sys_role_permission` VALUES ('fafe73c4448b977fe42880a6750c3ee8', 'f6817f48af4fb3af11b9e8bf182f618b', '9cb91b8851db0cf7b19d7ecc2a8193dd', null);
INSERT INTO `sys_role_permission` VALUES ('fced905c7598973b970d42d833f73474', 'f6817f48af4fb3af11b9e8bf182f618b', '4875ebe289344e14844d8e3ea1edd73f', null);
INSERT INTO `sys_role_permission` VALUES ('fd86f6b08eb683720ba499f9d9421726', 'ee8626f80f7c2619917b6236f3a7f02b', '693ce69af3432bd00be13c3971a57961', null);
INSERT INTO `sys_role_permission` VALUES ('fd97963dc5f144d3aecfc7045a883427', 'f6817f48af4fb3af11b9e8bf182f618b', '043780fa095ff1b2bec4dc406d76f023', null);
INSERT INTO `sys_role_permission` VALUES ('fed41a4671285efb266cd404f24dd378', '52b0cf022ac4187b2a70dfa4f8b2d940', '00a2a0ae65cdca5e93209cdbde97cbe6', null);

-- ----------------------------
-- Table structure for `sys_sms`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `es_title` varchar(100) DEFAULT NULL COMMENT '消息标题',
  `es_type` varchar(1) DEFAULT NULL COMMENT '发送方式：1短信 2邮件 3微信',
  `es_receiver` varchar(50) DEFAULT NULL COMMENT '接收人',
  `es_param` varchar(1000) DEFAULT NULL COMMENT '发送所需参数Json格式',
  `es_content` longtext COMMENT '推送内容',
  `es_send_time` datetime DEFAULT NULL COMMENT '推送时间',
  `es_send_status` varchar(1) DEFAULT NULL COMMENT '推送状态 0未推送 1推送成功 2推送失败 -1失败不再发送',
  `es_send_num` int(11) DEFAULT NULL COMMENT '发送次数 超过5次不再发送',
  `es_result` varchar(255) DEFAULT NULL COMMENT '推送失败原因',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_type` (`es_type`) USING BTREE,
  KEY `index_receiver` (`es_receiver`) USING BTREE,
  KEY `index_sendtime` (`es_send_time`) USING BTREE,
  KEY `index_status` (`es_send_status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_sms
-- ----------------------------
INSERT INTO `sys_sms` VALUES ('402880e74dc2f361014dc2f8411e0001', '消息推送测试333', '2', '411944058@qq.com', null, '张三你好，你的订单4028d881436d514601436d521ae80165已付款!', '2015-06-05 17:06:01', '3', null, null, '认证失败错误的用户名或者密码', 'admin', '2015-06-05 17:05:59', 'admin', '2015-11-19 22:30:39');
INSERT INTO `sys_sms` VALUES ('402880ea533647b00153364e74770001', '发个问候', '3', 'admin', null, '你好', '2016-03-02 00:00:00', '2', null, null, null, 'admin', '2016-03-02 15:50:24', 'admin', '2018-07-05 19:53:01');
INSERT INTO `sys_sms` VALUES ('402880ee5a17e711015a17f3188e013f', '消息推送测试333', '2', '411944058@qq.com', null, '张三你好，你的订单4028d881436d514601436d521ae80165已付款!', null, '2', null, null, null, 'admin', '2017-02-07 17:41:31', 'admin', '2017-03-10 11:37:05');
INSERT INTO `sys_sms` VALUES ('402880f05ab649b4015ab64b9cd80012', '消息推送测试333', '2', '411944058@qq.com', null, '张三你好，你的订单4028d881436d514601436d521ae80165已付款!', '2017-11-16 15:58:15', '3', null, null, null, 'admin', '2017-03-10 11:38:13', 'admin', '2017-07-31 17:24:54');
INSERT INTO `sys_sms` VALUES ('402880f05ab7b035015ab7c4462c0004', '消息推送测试333', '2', '411944058@qq.com', null, '张三你好，你的订单4028d881436d514601436d521ae80165已付款!', '2017-11-16 15:58:15', '3', null, null, null, 'admin', '2017-03-10 18:29:37', null, null);
INSERT INTO `sys_sms` VALUES ('402881f3646a472b01646a4a5af00001', '催办：HR审批', '3', 'admin', null, 'admin，您好！\r\n请前待办任务办理事项！HR审批\r\n\r\n\r\n===========================\r\n此消息由系统发出', '2018-07-05 19:53:35', '2', null, null, null, 'admin', '2018-07-05 19:53:35', 'admin', '2018-07-07 13:45:24');
INSERT INTO `sys_sms` VALUES ('402881f3647da06c01647da43a940014', '催办：HR审批', '3', 'admin', null, 'admin，您好！\r\n请前待办任务办理事项！HR审批\r\n\r\n\r\n===========================\r\n此消息由系统发出', '2018-07-09 14:04:32', '2', null, null, null, 'admin', '2018-07-09 14:04:32', 'admin', '2018-07-09 18:51:30');

-- ----------------------------
-- Table structure for `sys_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms_template`;
CREATE TABLE `sys_sms_template` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `template_name` varchar(50) DEFAULT NULL COMMENT '模板标题',
  `template_code` varchar(32) NOT NULL COMMENT '模板CODE',
  `template_type` varchar(1) NOT NULL COMMENT '模板类型：1短信 2邮件 3微信',
  `template_content` varchar(1000) NOT NULL COMMENT '模板内容',
  `template_test_json` varchar(1000) DEFAULT NULL COMMENT '模板测试json',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人登录名称',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniq_templatecode` (`template_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_sms_template
-- ----------------------------
INSERT INTO `sys_sms_template` VALUES ('4028608164691b000164693108140003', '催办：${taskName}', 'SYS001', '3', '${userName}，您好！\r\n请前待办任务办理事项！${taskName}\r\n\r\n\r\n===========================\r\n此消息由系统发出', '{\r\n\"taskName\":\"HR审批\",\r\n\"userName\":\"admin\"\r\n}', '2018-07-05 14:46:18', 'admin', '2018-07-05 18:31:34', 'admin');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `username` varchar(100) DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) DEFAULT '0' COMMENT '性别(0-默认未知,1-男,2-女)',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `org_code` varchar(64) DEFAULT NULL COMMENT '机构编码',
  `status` tinyint(1) DEFAULT NULL COMMENT '性别(1-正常,2-冻结)',
  `del_flag` tinyint(1) DEFAULT NULL COMMENT '删除状态(0-正常,1-已删除)',
  `activiti_sync` tinyint(1) DEFAULT NULL COMMENT '同步工作流引擎(1-同步,0-不同步)',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `index_user_name` (`username`) USING BTREE,
  KEY `index_user_status` (`status`) USING BTREE,
  KEY `index_user_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('2cb7884d588fe2452ce778cc26bac00c', 'dddd', '32323', '354b57884a83ed23', 'xpNTxzAs', null, null, '0', null, null, null, '1', '0', '1', 'jeecg', '2019-09-07 13:41:15', null, null);
INSERT INTO `sys_user` VALUES ('42d153bffeea74f72a9c1697874fa4a7', 'test22', '23232', 'ac52e15671a377cf', '5FMD48RM', 'user/20190314/ly-plate-e_1552531617500.png', '2019-02-09 00:00:00', '1', 'zhangdaiscott@163.com', '18611782222', null, '1', '0', '1', 'admin', '2019-01-26 18:01:10', 'admin', '2019-03-23 15:05:50');
INSERT INTO `sys_user` VALUES ('662d318c3f74b1714d8af5e11f270bb3', 'test', '游客', 'dc01d8ce88b1e063', 'He9KgmUM', null, null, '0', null, null, 'A01', '1', '0', '1', 'admin', '2019-09-07 19:51:05', 'admin', '2019-09-10 15:06:18');
INSERT INTO `sys_user` VALUES ('a75d45a015c44384a04449ee80dc3503', 'jeecg', 'jeecg', '3dd8371f3cf8240e', 'vDDkDzrK', 'user/20190220/e1fe9925bc315c60addea1b98eb1cb1349547719_1550656892940.jpg', null, '2', null, null, 'A02A01', '1', '0', '1', 'admin', '2019-02-13 16:02:36', 'admin', '2019-09-05 11:13:52');
INSERT INTO `sys_user` VALUES ('a83197a3d87a4365bdf87726c51ccf47', 'abc', 'abc', '47762a1fa5d914d5', 'UDxss7Mb', null, null, '0', null, null, 'A01', '1', '0', '1', 'test', '2019-09-08 11:40:40', null, null);
INSERT INTO `sys_user` VALUES ('e9ca23d68d884d4ebb19d07889727dae', 'admin', '管理员', '4c61ab26ec2900f9', '24fnobzl', 'user/20190119/logo-2_1547868176839.png', '2018-12-05 00:00:00', '1', '11@qq.com', '18566666661', 'A01', '1', '0', '1', null, '2038-06-21 17:54:10', 'admin', '2019-07-05 14:47:22');
INSERT INTO `sys_user` VALUES ('f0019fdebedb443c98dcb17d88222c38', 'zhagnxiao', '张小红', 'f898134e5e52ae11a2ffb2c3b57a4e90', 'go3jJ4zX', 'user/20190401/20180607175028Fn1Lq7zw_1554118444672.png', '2019-04-01 00:00:00', null, null, null, null, '1', '0', '1', 'admin', '2023-10-01 19:34:10', 'admin', '2019-04-10 22:00:22');

-- ----------------------------
-- Table structure for `sys_user_agent`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_agent`;
CREATE TABLE `sys_user_agent` (
  `id` varchar(32) NOT NULL COMMENT '序号',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `agent_user_name` varchar(100) DEFAULT NULL COMMENT '代理人用户名',
  `start_time` datetime DEFAULT NULL COMMENT '代理开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '代理结束时间',
  `status` varchar(2) DEFAULT NULL COMMENT '状态0无效1有效',
  `create_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_name` varchar(50) DEFAULT NULL COMMENT '更新人名称',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(50) DEFAULT NULL COMMENT '所属部门',
  `sys_company_code` varchar(50) DEFAULT NULL COMMENT '所属公司',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniq_username` (`user_name`) USING BTREE,
  KEY `statux_index` (`status`) USING BTREE,
  KEY `begintime_index` (`start_time`) USING BTREE,
  KEY `endtime_index` (`end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户代理人设置';

-- ----------------------------
-- Records of sys_user_agent
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_depart`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_depart`;
CREATE TABLE `sys_user_depart` (
  `ID` varchar(32) NOT NULL COMMENT 'id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `dep_id` varchar(32) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `index_depart_groupk_userid` (`user_id`) USING BTREE,
  KEY `index_depart_groupkorgid` (`dep_id`) USING BTREE,
  KEY `index_depart_groupk_uidanddid` (`user_id`,`dep_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_depart
-- ----------------------------
INSERT INTO `sys_user_depart` VALUES ('0c42ba309c2c4cad35836ec2336676fa', '42d153bffeea74f72a9c1697874fa4a7', '6d35e179cd814e3299bd588ea7daed3f');
INSERT INTO `sys_user_depart` VALUES ('c235f604c27fb74d2f68028813a6dcc7', '662d318c3f74b1714d8af5e11f270bb3', 'c6d7cb4deeac411cb3384b1b31278596');
INSERT INTO `sys_user_depart` VALUES ('1f0126668aab6fab9c418b2e89640fa0', 'a75d45a015c44384a04449ee80dc3503', 'a7d7e77e06c84325a40932163adcdaa6');
INSERT INTO `sys_user_depart` VALUES ('199dba7599aa3171c15fff080e0d5342', 'a83197a3d87a4365bdf87726c51ccf47', 'c6d7cb4deeac411cb3384b1b31278596');
INSERT INTO `sys_user_depart` VALUES ('1f3a0267811327b9eca86b0cc2b956f3', 'bcbe1290783a469a83ae3bd8effe15d4', '5159cde220114246b045e574adceafe9');
INSERT INTO `sys_user_depart` VALUES ('ff9c8c6e06514fcf26c108b1395cc876', 'e9ca23d68d884d4ebb19d07889727dae', 'c6d7cb4deeac411cb3384b1b31278596');
INSERT INTO `sys_user_depart` VALUES ('ac52f23ae625eb6560c9227170b88166', 'f0019fdebedb443c98dcb17d88222c38', '57197590443c44f083d42ae24ef26a2c');
INSERT INTO `sys_user_depart` VALUES ('179660a8b9a122f66b73603799a10924', 'f0019fdebedb443c98dcb17d88222c38', '67fc001af12a4f9b8458005d3f19934a');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index2_groupuu_user_id` (`user_id`) USING BTREE,
  KEY `index2_groupuu_ole_id` (`role_id`) USING BTREE,
  KEY `index2_groupuu_useridandroleid` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('b3ffd9311a1ca296c44e2409b547384f', '01b802058ea94b978a2c96f4807f6b48', '1');
INSERT INTO `sys_user_role` VALUES ('0ede6d23d53bc7dc990346ff14faabee', '3db4cf42353f4e868b7ccfeef90505d2', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('e78d210d24aaff48e0a736e2ddff4cdc', '3e177fede453430387a8279ced685679', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('f2de3ae7b5efd8345581aa802a6675d6', '41b1be8d4c52023b0798f51164ca682d', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('6f9da7310489bac1e5f95e0efe92b4ce', '42d153bffeea74f72a9c1697874fa4a7', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('f2922a38ba24fb53749e45a0c459adb3', '439ae3e9bcf7418583fcd429cadb1d72', '1');
INSERT INTO `sys_user_role` VALUES ('f72c6190b0722e798147e73c776c6ac9', '439ae3e9bcf7418583fcd429cadb1d72', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('ee45d0343ecec894b6886effc92cb0b7', '4d8fef4667574b24a9ccfedaf257810c', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('be2639167ede09379937daca7fc3bb73', '526f300ab35e44faaed54a9fb0742845', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('5d6847e4dc810c1cdf7036b0a5395fed', '662d318c3f74b1714d8af5e11f270bb3', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('31af310584bd5795f76b1fe8c38294a0', '70f5dcf03f36471dabba81381919291f', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('8d7846ec783e157174e4ce2949231a65', '7ee6630e89d17afbf6d12150197b578d', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('79d66ef7aa137cfa9957081a1483009d', '9a668858c4c74cf5a2b25ad9608ba095', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('f1f300f924a3d782330717ee6acd34cf', 'a75d45a015c44384a04449ee80dc3503', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('51bbd2b5fd5ee6247039874f9801c0a1', 'a83197a3d87a4365bdf87726c51ccf47', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('b694da35692bbfa1fff0e9d5b2dcf311', 'e9ca23d68d884d4ebb19d07889727dae', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('d2233e5be091d39da5abb0073c766224', 'f0019fdebedb443c98dcb17d88222c38', 'ee8626f80f7c2619917b6236f3a7f02b');

-- ----------------------------
-- Table structure for `test_demo`
-- ----------------------------
DROP TABLE IF EXISTS `test_demo`;
CREATE TABLE `test_demo` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `name` varchar(200) DEFAULT NULL COMMENT '用户名',
  `sex` varchar(32) DEFAULT NULL COMMENT '性别',
  `age` int(32) DEFAULT NULL COMMENT '年龄',
  `descc` varchar(500) DEFAULT NULL COMMENT '描述',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `user_code` varchar(32) DEFAULT NULL COMMENT '用户编码',
  `file_kk` varchar(500) DEFAULT NULL COMMENT '附件',
  `top_pic` varchar(500) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of test_demo
-- ----------------------------
INSERT INTO `test_demo` VALUES ('4028810c6aed99e1016aed9b31b40002', null, null, 'admin', '2019-05-29 17:09:25', 'jeecg', '2', '55', '5', '2019-05-15 00:00:00', null, null, null);
INSERT INTO `test_demo` VALUES ('4028810c6b02cba2016b02cba21f0000', 'admin', '2019-05-29 16:53:48', 'admin', '2019-08-23 23:45:21', '张小红', '1', '8222', '8', '2019-04-01 00:00:00', null, '', '');
INSERT INTO `test_demo` VALUES ('4028810c6b40244b016b4030a0e40001', 'admin', '2019-06-10 15:00:57', 'admin', '2019-08-23 23:42:49', '小芳', '2', '0', null, '2019-04-01 00:00:00', null, '', '');
INSERT INTO `test_demo` VALUES ('fa1d1c249461498d90f405b94f60aae0', '', null, 'admin', '2019-05-15 12:30:28', '战三', '2', '222', null, null, null, null, null);

-- ----------------------------
-- Table structure for `test_order_main`
-- ----------------------------
DROP TABLE IF EXISTS `test_order_main`;
CREATE TABLE `test_order_main` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `order_code` varchar(32) DEFAULT NULL COMMENT '订单编码',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  `descc` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of test_order_main
-- ----------------------------
INSERT INTO `test_order_main` VALUES ('402831816a38e7fd016a38e825c90003', 'admin', '2019-04-20 12:01:39', 'admin', '2019-08-23 23:43:17', '111333', '2019-04-20 00:00:00', '11');
INSERT INTO `test_order_main` VALUES ('4028810c6b40244b016b40686dfb0003', 'admin', '2019-06-10 16:01:54', 'admin', '2019-06-10 16:06:51', '1231', '2019-06-10 00:00:00', '123');
INSERT INTO `test_order_main` VALUES ('4028810c6b40244b016b4068ef890006', 'admin', '2019-06-10 16:02:27', 'admin', '2019-08-23 23:43:08', 'A001', '2019-06-10 00:00:00', '购买产品BOOT');

-- ----------------------------
-- Table structure for `test_order_product`
-- ----------------------------
DROP TABLE IF EXISTS `test_order_product`;
CREATE TABLE `test_order_product` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `product_name` varchar(32) DEFAULT NULL COMMENT '产品名字',
  `price` double(32,0) DEFAULT NULL COMMENT '价格',
  `num` int(32) DEFAULT NULL COMMENT '数量',
  `descc` varchar(32) DEFAULT NULL COMMENT '描述',
  `order_fk_id` varchar(32) NOT NULL COMMENT '订单外键ID',
  `pro_type` varchar(32) DEFAULT NULL COMMENT '产品类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of test_order_product
-- ----------------------------
INSERT INTO `test_order_product` VALUES ('15665749852471', 'admin', '2019-08-23 23:43:08', null, null, '222', '222', '22', '', '4028810c6b40244b016b4068ef890006', '2');
INSERT INTO `test_order_product` VALUES ('15665749948861', 'admin', '2019-08-23 23:43:17', null, null, '333', '33', null, '', '402831816a38e7fd016a38e825c90003', '');
INSERT INTO `test_order_product` VALUES ('402831816a38e7fd016a38e7fdeb0001', 'admin', '2019-04-20 12:01:29', null, null, '笔记本', '100', '10', null, '402831816a38e7fd016a38e7fddf0000', null);
INSERT INTO `test_order_product` VALUES ('402831816a38e7fd016a38e7fdf10002', 'admin', '2019-04-20 12:01:29', null, null, '显示器', '300', '1', null, '402831816a38e7fd016a38e7fddf0000', null);
INSERT INTO `test_order_product` VALUES ('4028810c6b40244b016b40686e050004', 'admin', '2019-06-10 16:06:51', null, null, '123', '222', '123', '123', '4028810c6b40244b016b40686dfb0003', null);
INSERT INTO `test_order_product` VALUES ('4028810c6b40244b016b406884080005', 'admin', '2019-08-23 23:43:17', null, null, '333', null, '33', '', '402831816a38e7fd016a38e825c90003', '');
INSERT INTO `test_order_product` VALUES ('4028810c6b40244b016b4068ef8f0007', 'admin', '2019-08-23 23:43:08', null, null, 'JEECG-BOOT开发平台', '10000', '1', '', '4028810c6b40244b016b4068ef890006', '1');

-- ----------------------------
-- Table structure for `test_person`
-- ----------------------------
DROP TABLE IF EXISTS `test_person`;
CREATE TABLE `test_person` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sex` varchar(32) DEFAULT NULL COMMENT '性别',
  `name` varchar(200) DEFAULT NULL COMMENT '用户名',
  `content` longtext COMMENT '请假原因',
  `be_date` datetime DEFAULT NULL COMMENT '请假时间',
  `qj_days` int(11) DEFAULT NULL COMMENT '请假天数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of test_person
-- ----------------------------
INSERT INTO `test_person` VALUES ('8ca668defdae47df8649a5477ae08b05', 'admin', '2019-04-12 09:51:37', null, null, '1', 'zhangdaiscott', 'dsdsd', '2019-04-12 00:00:00', '2');
