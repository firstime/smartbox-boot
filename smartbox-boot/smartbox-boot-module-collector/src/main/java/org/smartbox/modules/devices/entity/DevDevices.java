package org.smartbox.modules.devices.entity;

import java.util.Date;

public class DevDevices {
    private String id;

    private String devName;

    private String devType;

    private String devMac;

    private String devBox;

    private Integer devStatus;

    private Integer devUpdown;

    private String description;

    private Integer delFlag;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private String hasChild;

    private String productFamily;

    private String productName;

    private String product;

    private String transmitCapacity;

    private String ip;

    private String verision;

    private String versionStartTtime;

    private String patch;

    private String patchStartTime;

    private String region;

    private String country;

    private String siteName;

    private String groupName;

    private String networkType;

    private String networkName;

    private String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName == null ? null : devName.trim();
    }

    public String getDevType() {
        return devType;
    }

    public void setDevType(String devType) {
        this.devType = devType == null ? null : devType.trim();
    }

    public String getDevMac() {
        return devMac;
    }

    public void setDevMac(String devMac) {
        this.devMac = devMac == null ? null : devMac.trim();
    }

    public String getDevBox() {
        return devBox;
    }

    public void setDevBox(String devBox) {
        this.devBox = devBox == null ? null : devBox.trim();
    }

    public Integer getDevStatus() {
        return devStatus;
    }

    public void setDevStatus(Integer devStatus) {
        this.devStatus = devStatus;
    }

    public Integer getDevUpdown() {
        return devUpdown;
    }

    public void setDevUpdown(Integer devUpdown) {
        this.devUpdown = devUpdown;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getHasChild() {
        return hasChild;
    }

    public void setHasChild(String hasChild) {
        this.hasChild = hasChild == null ? null : hasChild.trim();
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily == null ? null : productFamily.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public String getTransmitCapacity() {
        return transmitCapacity;
    }

    public void setTransmitCapacity(String transmitCapacity) {
        this.transmitCapacity = transmitCapacity == null ? null : transmitCapacity.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getVerision() {
        return verision;
    }

    public void setVerision(String verision) {
        this.verision = verision == null ? null : verision.trim();
    }

    public String getVersionStartTtime() {
        return versionStartTtime;
    }

    public void setVersionStartTtime(String versionStartTtime) {
        this.versionStartTtime = versionStartTtime == null ? null : versionStartTtime.trim();
    }

    public String getPatch() {
        return patch;
    }

    public void setPatch(String patch) {
        this.patch = patch == null ? null : patch.trim();
    }

    public String getPatchStartTime() {
        return patchStartTime;
    }

    public void setPatchStartTime(String patchStartTime) {
        this.patchStartTime = patchStartTime == null ? null : patchStartTime.trim();
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName == null ? null : siteName.trim();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType == null ? null : networkType.trim();
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName == null ? null : networkName.trim();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }
}