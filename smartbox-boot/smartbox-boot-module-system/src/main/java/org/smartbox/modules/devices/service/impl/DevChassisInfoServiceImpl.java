package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevChassisInfo;
import org.smartbox.modules.devices.mapper.DevBoardInfoMapper;
import org.smartbox.modules.devices.mapper.DevChassisInfoMapper;
import org.smartbox.modules.devices.service.IDevChassisInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 机框表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Service
public class DevChassisInfoServiceImpl extends ServiceImpl<DevChassisInfoMapper, DevChassisInfo> implements IDevChassisInfoService {

	@Autowired
	DevChassisInfoMapper devChassisInfoMapper;
	
	@Override
	public IPage<DevChassisInfo> selectUserPage(Page<DevChassisInfo> page, String uid, String devid) {
		// TODO Auto-generated method stub
	    return devChassisInfoMapper.selectPageVo(page, uid,devid);
	}

}
