package org.smartbox.modules.demo.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.demo.test.entity.JeecgOrderMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 订单
 * @Author: jeecg-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface JeecgOrderMainMapper extends BaseMapper<JeecgOrderMain> {

}
