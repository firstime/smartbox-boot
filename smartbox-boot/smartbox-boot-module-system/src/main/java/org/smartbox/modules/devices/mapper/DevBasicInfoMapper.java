package org.smartbox.modules.devices.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.bean.DevCounts;
import org.smartbox.modules.devices.entity.DevBasicInfo;
import org.smartbox.modules.devices.entity.DevDevices;
import org.smartbox.modules.system.model.TreeSelectModel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DevBasicInfoMapper extends BaseMapper<DevBasicInfo>{
    int deleteByPrimaryKey(String id);

    int insert(DevBasicInfo record);

    int insertSelective(DevBasicInfo record);

    DevBasicInfo selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DevBasicInfo record);

    int updateByPrimaryKey(DevBasicInfo record);
    
	/**
	  *  根据父级ID查询树节点数据
	 * @param pid
	 * @return
	 */
	public List<TreeSelectModel> queryListByPid(@Param("pid")  String id);
	//根据uid获取设备总数
	public List<Map<String,Object>> getDevCountsByUid(@Param("uid")  String uid);
}