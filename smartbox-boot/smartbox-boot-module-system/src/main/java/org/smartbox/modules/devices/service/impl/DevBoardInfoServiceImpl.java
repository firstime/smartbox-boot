package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevBoardInfo;
import org.smartbox.modules.devices.mapper.DevBoardInfoMapper;
import org.smartbox.modules.devices.mapper.DevInterfaceInfoMapper;
import org.smartbox.modules.devices.service.IDevBoardInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 单板表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Service
public class DevBoardInfoServiceImpl extends ServiceImpl<DevBoardInfoMapper, DevBoardInfo> implements IDevBoardInfoService {

	
	@Autowired
	DevBoardInfoMapper devBoardInfoMapper;
	
	@Override
	public IPage<DevBoardInfo> selectUserPage(Page<DevBoardInfo> page, String uid, String devid) {
		// TODO Auto-generated method stub
	    return devBoardInfoMapper.selectPageVo(page, uid,devid);
	}

}
