package org.smartbox.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.smartbox.modules.system.entity.SysDepart;
import org.smartbox.modules.system.model.SysDepartTreeModel;
import org.smartbox.modules.system.model.TreeModel;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>
 * 部门 Mapper 接口
 * <p>
 * 
 * @Author: Steve
 * @Since：   2019-01-22
 */
public interface SysDepartMapper extends BaseMapper<SysDepart> {
	
	/**
	 * 根据用户ID查询部门集合
	 */
	public List<SysDepart> queryUserDeparts(@Param("userId") String userId);
}
