package org.smartbox.modules.devices.bean;

public class SubBoardInfo {
    //子卡序号
    private long id;
    //单板编码
    private long boardId;
    //子卡
    private String pic;
    //子卡槽位号
    private String slot;
    //子卡名称
    private String picName;
    //子卡版本
    private String picVersion;
    //barcode
    private String barCode;
    private String item;
    private String description;
}
