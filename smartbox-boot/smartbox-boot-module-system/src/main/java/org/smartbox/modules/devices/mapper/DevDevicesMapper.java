package org.smartbox.modules.devices.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.entity.DevDevices;
import org.smartbox.modules.system.model.TreeSelectModel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 设备基本表
 * @Author: 隋斌
 * @Date:   2019-09-06
 * @Version: V1.0
 */
public interface DevDevicesMapper extends BaseMapper<DevDevices> {
	
	/**
	  *  根据父级ID查询树节点数据
	 * @param pid
	 * @return
	 */
	public List<TreeSelectModel> queryListByPid(@Param("pid")  String id);
	
	

}
