package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevBoxInfo;
import org.smartbox.modules.devices.mapper.DevBoxInfoMapper;
import org.smartbox.modules.devices.service.IDevBoxInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 采集盒表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Service
public class DevBoxInfoServiceImpl extends ServiceImpl<DevBoxInfoMapper, DevBoxInfo> implements IDevBoxInfoService {

}
