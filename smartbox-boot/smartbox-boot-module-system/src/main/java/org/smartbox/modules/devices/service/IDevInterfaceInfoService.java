package org.smartbox.modules.devices.service;

import org.smartbox.modules.devices.entity.DevInterfaceInfo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 设备接口相关
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface IDevInterfaceInfoService extends IService<DevInterfaceInfo> {

	IPage<DevInterfaceInfo> selectUserPage(Page<DevInterfaceInfo> page, String uid, String devid);

}
