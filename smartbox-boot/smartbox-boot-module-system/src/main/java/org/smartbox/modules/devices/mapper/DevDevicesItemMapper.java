package org.smartbox.modules.devices.mapper;

import org.apache.ibatis.annotations.Select;
import org.smartbox.modules.devices.entity.DevDevicesItem;
import org.smartbox.modules.system.entity.SysDictItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-28
 */
public interface DevDevicesItemMapper extends BaseMapper<DevDevicesItem> {
    @Select("SELECT * FROM dev_devices_item WHERE dev_ID = #{mainId}")
    public List<DevDevicesItem> selectItemsByMainId(String mainId);
}
