package org.smartbox.modules.devices.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.entity.DevInterfaceInfo;
import org.smartbox.modules.devices.entity.DevSubboardInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Description: 子卡表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface DevSubboardInfoMapper extends BaseMapper<DevSubboardInfo> {
	//分页定义
	IPage<DevSubboardInfo> selectPageVo(Page<DevSubboardInfo> page, @Param("uid") String uid, @Param("devid") String devid);

	IPage<DevSubboardInfo> selectPageByBid(Page<DevSubboardInfo> page, @Param("uid") String uid, @Param("boardid") String boardid);

}
