package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 设备接口相关
 * @Author: jeecg-boot
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Data
@TableName("dev_interface_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_interface_info对象", description="设备接口相关")
public class DevInterfaceInfo {
    
	/**序号*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "序号")
	private java.lang.String id;
	/**设备ID*/
	@Excel(name = "设备ID", width = 15)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String basicinfoId;
	/**接口名称*/
	@Excel(name = "接口名称", width = 15)
    @ApiModelProperty(value = "接口名称")
	private java.lang.String interfaceName;
	/**接口类型*/
	@Excel(name = "接口类型", width = 15)
    @ApiModelProperty(value = "接口类型")
	private java.lang.String interfaceType;
	/**接口槽位号*/
	@Excel(name = "接口槽位号", width = 15)
    @ApiModelProperty(value = "接口槽位号")
	private java.lang.String interfaceSlot;
	/**入口利用率*/
	@Excel(name = "入口利用率", width = 15)
    @ApiModelProperty(value = "入口利用率")
	private java.lang.String inUti;
	/**出口利用率*/
	@Excel(name = "出口利用率", width = 15)
    @ApiModelProperty(value = "出口利用率")
	private java.lang.String outUti;
	/**物理口状态*/
	@Excel(name = "物理口状态", width = 15)
    @ApiModelProperty(value = "物理口状态")
	private java.lang.String phyStatus;
	/**协议口状态*/
	@Excel(name = "协议口状态", width = 15)
    @ApiModelProperty(value = "协议口状态")
	private java.lang.String protocalStatus;
	
	@Excel(name = "用户ID", width = 32)
    @ApiModelProperty(value = "用户ID")
	private java.lang.String uid;	
	
	@Excel(name = "设备ID", width = 32)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String devid;	
	
}
