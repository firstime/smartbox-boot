package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 子卡表
 * @Author: jeecg-boot
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Data
@TableName("dev_subboard_Info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_subboard_Info对象", description="子卡表")
public class DevSubboardInfo {
    
	/**序号*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "序号")
	private java.lang.String id;
	/**单板编号*/
	@Excel(name = "单板编号", width = 15)
    @ApiModelProperty(value = "单板编号")
	private java.lang.String boardId;
	/**子卡名称*/
	@Excel(name = "子卡名称", width = 15)
    @ApiModelProperty(value = "子卡名称")
	private java.lang.String picname;
	/**子卡槽位号*/
	@Excel(name = "子卡槽位号", width = 15)
    @ApiModelProperty(value = "子卡槽位号")
	private java.lang.String picSlot;
	/**子卡版本号*/
	@Excel(name = "子卡版本号", width = 15)
    @ApiModelProperty(value = "子卡版本号")
	private java.lang.String picVersion;
	
	@Excel(name = "用户ID", width = 32)
    @ApiModelProperty(value = "用户ID")
	private java.lang.String uid;	
	
	@Excel(name = "设备ID", width = 32)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String devid;	
}
