package org.smartbox.modules.devices.bean;

import com.alibaba.druid.VERSION;

public class BoardInfo {
    //单板序号
    private long id;
    //设备基本信息ID
    private long basicInfoId;
    //机框ID
    private long chassisId;
    //单板类型
    private String boardType;
    //单板槽位号
    private String slot;
    //单板名称
    private String boradName;
    //单板版本
    private String boardVersion;
    //CPU利用率
    private double cpuUsage;
    //内存使用率
    private double memUsage;
    //内存总大小
    private double memTotal;
    //内存使用大小
    private double memUsed;
    //SDRAM大小
    private double sdram;
    //flash大小
    private double flash;
    //nvram大小
    private double nvram;
    //CF Card大小
    private double cfCardMem;
    //启动时间
    private long startTime;
    //单板
    private String barCode;
    //单板编码
    private String item;
    //单板用户数
    private String userCount;
    //IPV4用户数
    private String ipv4UserCount;
    //IPV6用户数
    private String ipv6UserCount;
    //mac地址
    private String macInUsed;

}
