package org.smartbox.modules.devices.service;


import org.smartbox.modules.devices.entity.DevDevicesItem;
import org.smartbox.modules.system.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @Author suibin
 * @since 2019-09-06
 */
public interface IDevDevicesItemService extends IService<DevDevicesItem> {
    public List<DevDevicesItem> selectItemsByMainId(String mainId);
}

