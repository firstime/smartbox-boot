package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevDevicesItem;
import org.smartbox.modules.devices.mapper.DevDevicesItemMapper;
import org.smartbox.modules.devices.service.IDevDevicesItemService;
import org.smartbox.modules.system.entity.SysDictItem;
import org.smartbox.modules.system.mapper.SysDictItemMapper;
import org.smartbox.modules.system.service.ISysDictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @Author suibin
 * @since 2018-12-28
 */
@Service
public class DevDevicesItemServiceImpl extends ServiceImpl<DevDevicesItemMapper, DevDevicesItem> implements IDevDevicesItemService {

    @Autowired
    private DevDevicesItemMapper devDevicesItemMapper;

    @Override
    public List<DevDevicesItem> selectItemsByMainId(String mainId) {
        return devDevicesItemMapper.selectItemsByMainId(mainId);
    }
}
