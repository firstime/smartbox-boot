package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 设备基本信息表
 * @Author: jeecg-boot
 * @Date:   2019-09-12
 * @Version: V1.0
 */
@Data
@TableName("dev_basic_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_basic_info对象", description="设备基本信息表")
public class DevBasicInfo {
    
	/**序号*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "序号")
	private java.lang.String id;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
	private java.lang.String userId;
	/**文件类型*/
	@Excel(name = "文件类型", width = 15)
    @ApiModelProperty(value = "文件类型")
	private java.lang.String fileType;
	/**局点ID*/
	@Excel(name = "局点ID", width = 15)
    @ApiModelProperty(value = "局点ID")
	private java.lang.String siteId;
	/**文件名称*/
	@Excel(name = "文件名称", width = 15)
    @ApiModelProperty(value = "文件名称")
	private java.lang.String fileName;
	/**文件路径*/
	@Excel(name = "文件路径", width = 15)
    @ApiModelProperty(value = "文件路径")
	private java.lang.String filePath;
	/**文件大小*/
	@Excel(name = "文件大小", width = 15)
    @ApiModelProperty(value = "文件大小")
	private java.lang.Double fileSize;
	/**收集时间*/
	@Excel(name = "收集时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "收集时间")
	private java.util.Date collectTime;
	/**设备启动时间*/
	@Excel(name = "设备启动时间", width = 15)
    @ApiModelProperty(value = "设备启动时间")
	private java.lang.String deviceStartTime;
	/**设备运行时间*/
	@Excel(name = "设备运行时间", width = 15)
    @ApiModelProperty(value = "设备运行时间")
	private java.lang.String deviceRunTime;
	/**设备类型*/
	@Excel(name = "设备类型", width = 15)
    @ApiModelProperty(value = "设备类型")
	private java.lang.String deviceType;
	/**设备名称*/
	@Excel(name = "设备名称", width = 15)
    @ApiModelProperty(value = "设备名称")
	private java.lang.String deviceName;
	/**设备版本*/
	@Excel(name = "设备版本", width = 15)
    @ApiModelProperty(value = "设备版本")
	private java.lang.String deviceVerison;
	/**设备补丁*/
	@Excel(name = "设备补丁", width = 15)
    @ApiModelProperty(value = "设备补丁")
	private java.lang.String devicePatch;
	/**设备状态0异常1正常*/
	@Excel(name = "设备状态0异常1正常", width = 15)
    @ApiModelProperty(value = "设备状态0异常1正常")
	private java.lang.Integer deviceStatus;
	/**IP地址*/
	@Excel(name = "IP地址", width = 15)
    @ApiModelProperty(value = "IP地址")
	private java.lang.String ipAddress;
	/**是否备板*/
	@Excel(name = "是否备板", width = 15)
    @ApiModelProperty(value = "是否备板")
	private java.lang.String isbkp;
	/**备板类型*/
	@Excel(name = "备板类型", width = 15)
    @ApiModelProperty(value = "备板类型")
	private java.lang.String bkpBoardType;
	/**备板编码*/
	@Excel(name = "备板编码", width = 15)
    @ApiModelProperty(value = "备板编码")
	private java.lang.String bkpPcbVersion;
	/**设备描述信息*/
	@Excel(name = "设备描述信息", width = 15)
    @ApiModelProperty(value = "设备描述信息")
	private java.lang.String description;
	/**license状态*/
	@Excel(name = "license状态", width = 15)
    @ApiModelProperty(value = "license状态")
	private java.lang.String licenseState;
	/**license名称*/
	@Excel(name = "license名称", width = 15)
    @ApiModelProperty(value = "license名称")
	private java.lang.String productName;
	/**产品版本*/
	@Excel(name = "产品版本", width = 15)
    @ApiModelProperty(value = "产品版本")
	private java.lang.String productVersion;
	/**产品序列号*/
	@Excel(name = "产品序列号", width = 15)
    @ApiModelProperty(value = "产品序列号")
	private java.lang.String productSerialNo;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**过期时间*/
	@Excel(name = "过期时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "过期时间")
	private java.util.Date expiredTime;
}
