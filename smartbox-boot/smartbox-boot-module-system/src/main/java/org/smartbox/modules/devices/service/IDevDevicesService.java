package org.smartbox.modules.devices.service;

import java.util.List;

import org.smartbox.common.exception.JeecgBootException;
import org.smartbox.modules.devices.entity.DevDevices;
import org.smartbox.modules.system.model.TreeSelectModel;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 分类字典
 * @Author: jeecg-boot
 * @Date:   2019-05-29
 * @Version: V1.0
 */
public interface IDevDevicesService extends IService<DevDevices> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";

	
	/**
	  * 根据父级编码加载分类字典的数据
	 * @param pcode
	 * @return
	 */
	public List<TreeSelectModel> queryListByCode(String pcode) throws JeecgBootException;
	
	/**
	  * 根据pid查询子节点集合
	 * @param pid
	 * @return
	 */
	public List<TreeSelectModel> queryListByPid(String pid);
	
}
