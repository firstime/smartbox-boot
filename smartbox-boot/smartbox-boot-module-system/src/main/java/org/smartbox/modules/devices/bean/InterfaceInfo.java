package org.smartbox.modules.devices.bean;

public class InterfaceInfo {
    private long id;
    //端口类型
    private String interfaceType;
    //槽位号
    private String slot;
    //子卡号
    private String pic;
    //端口名称
    private String interfaceName;
    //端口物理状态
    private String phyState;
    //端口协议状态
    private String protocolState;
    //入口速率
    private double inUti;
    //出口速率
    private double outUti;


}
