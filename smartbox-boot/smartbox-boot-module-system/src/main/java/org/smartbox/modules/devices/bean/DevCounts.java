package org.smartbox.modules.devices.bean;

import java.util.Date;

public class DevCounts {
    private Long devErrCount;

    private Long devOkCount;

    private Long slotErrCount;

    private Long slotOkCount;

	public Long getDevErrCount() {
		return devErrCount;
	}

	public void setDevErrCount(Long devErrCount) {
		this.devErrCount = devErrCount;
	}

	public Long getDevOkCount() {
		return devOkCount;
	}

	public void setDevOkCount(Long devOkCount) {
		this.devOkCount = devOkCount;
	}

	public Long getSlotErrCount() {
		return slotErrCount;
	}

	public void setSlotErrCount(Long slotErrCount) {
		this.slotErrCount = slotErrCount;
	}

	public Long getSlotOkCount() {
		return slotOkCount;
	}

	public void setSlotOkCount(Long slotOkCount) {
		this.slotOkCount = slotOkCount;
	}

 

 
}