package org.smartbox.modules.devices.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.entity.DevInterfaceInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Description: 设备接口相关
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface DevInterfaceInfoMapper extends BaseMapper<DevInterfaceInfo> {
	//分页定义
	IPage<DevInterfaceInfo> selectPageVo(Page<DevInterfaceInfo> page, @Param("uid") String uid, @Param("devid") String devid);
}
