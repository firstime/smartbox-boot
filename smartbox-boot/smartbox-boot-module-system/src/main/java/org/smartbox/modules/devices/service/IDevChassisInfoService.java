package org.smartbox.modules.devices.service;

import org.smartbox.modules.devices.entity.DevBoardInfo;
import org.smartbox.modules.devices.entity.DevChassisInfo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 机框表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface IDevChassisInfoService extends IService<DevChassisInfo> {
	IPage<DevChassisInfo> selectUserPage(Page<DevChassisInfo> page, String uid, String devid);
}
