package org.smartbox.modules.devices.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.entity.DevChassisInfo;
import org.smartbox.modules.devices.entity.DevInterfaceInfo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Description: 机框表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface DevChassisInfoMapper extends BaseMapper<DevChassisInfo> {
	//分页定义
	IPage<DevChassisInfo> selectPageVo(Page<DevChassisInfo> page, @Param("uid") String uid, @Param("devid") String devid);

}
