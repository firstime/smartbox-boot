package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 采集盒表
 * @Author: jeecg-boot
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Data
@TableName("dev_box_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_box_info对象", description="采集盒表")
public class DevBoxInfo {
    
	/**盒子ID*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "盒子ID")
	private java.lang.String id;
	/**盒子编号*/
	@Excel(name = "盒子编号", width = 15)
    @ApiModelProperty(value = "盒子编号")
	private java.lang.String boxNo;
	/**盒子状态*/
	@Excel(name = "盒子状态", width = 15)
    @ApiModelProperty(value = "盒子状态")
	private java.lang.Integer boxStatus;
	/**盒子描述*/
	@Excel(name = "盒子描述", width = 15)
    @ApiModelProperty(value = "盒子描述")
	private java.lang.String boxDetail;
	/**设备ID*/
	@Excel(name = "设备ID", width = 15)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String basicInfoId;
}
