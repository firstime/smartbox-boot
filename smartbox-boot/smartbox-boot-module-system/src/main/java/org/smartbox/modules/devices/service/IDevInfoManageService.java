package org.smartbox.modules.devices.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.smartbox.common.exception.JeecgBootException;
import org.smartbox.modules.devices.bean.DevCounts;
import org.smartbox.modules.devices.entity.DevBasicInfo;
import org.smartbox.modules.devices.entity.DevBoardInfo;
import org.smartbox.modules.devices.entity.DevDevices;
import org.smartbox.modules.devices.entity.DevDevicesItem;
import org.smartbox.modules.system.model.TreeSelectModel;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 设备基本信息接口
 * @Author: suibin
 * @Date:   2019-05-29
 * @Version: V1.0
 */
public interface IDevInfoManageService  extends IService<DevBasicInfo>{
	/**根节点父ID的值*/
	String ROOT_PID_VALUE = "0";

	/**
	  * 根据父级编码加载分类字典的数据
	 * @param pcode
	 * @return
	 */
	List<TreeSelectModel> queryListByCode(String pcode) throws JeecgBootException;

	/**
	  * 根据pid查询子节点集合
	 * @param pid
	 * @return
	 */
	List<TreeSelectModel> queryListByPid(String pid);
	
	//根据uid获取设备总数
	public List<Map<String,Object>> getDevCountsByUid(String uid);



}
