package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 单板表
 * @Author: jeecg-boot
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Data
@TableName("dev_board_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_board_info对象", description="单板表")
public class DevBoardInfo {
    
	/**序号*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "序号")
	private java.lang.String id;
	/**基本信息表id*/
	@Excel(name = "基本信息表id", width = 15)
    @ApiModelProperty(value = "基本信息表id")
	private java.lang.String basicInfoId;
	/**机框id*/
	@Excel(name = "机框id", width = 15)
    @ApiModelProperty(value = "机框id")
	private java.lang.String chassisId;
	/**单板类型*/
	@Excel(name = "单板类型", width = 15)
    @ApiModelProperty(value = "单板类型")
	private java.lang.String boardType;
	/**槽位号*/
	@Excel(name = "槽位号", width = 15)
    @ApiModelProperty(value = "槽位号")
	private java.lang.String slot;
	/**槽位状态*/
	@Excel(name = "槽位状态", width = 15)
    @ApiModelProperty(value = "槽位状态")
	private java.lang.Integer slotStatus;
	/**单板名称*/
	@Excel(name = "单板名称", width = 15)
    @ApiModelProperty(value = "单板名称")
	private java.lang.String boardName;
	/**单板版本*/
	@Excel(name = "单板版本", width = 15)
    @ApiModelProperty(value = "单板版本")
	private java.lang.String boardPcbVersion;
	/**CPU使用率*/
	@Excel(name = "CPU使用率", width = 15)
    @ApiModelProperty(value = "CPU使用率")
	private java.lang.Double cpuUsageRate;
	/**内存使用率*/
	@Excel(name = "内存使用率", width = 15)
    @ApiModelProperty(value = "内存使用率")
	private java.lang.Double memUsageRate;
	/**内存使用大小*/
	@Excel(name = "内存使用大小", width = 15)
    @ApiModelProperty(value = "内存使用大小")
	private java.lang.String memUsed;
	/**内存总大小*/
	@Excel(name = "内存总大小", width = 15)
    @ApiModelProperty(value = "内存总大小")
	private java.lang.String memTotal;
	/**SDRAM大小*/
	@Excel(name = "SDRAM大小", width = 15)
    @ApiModelProperty(value = "SDRAM大小")
	private java.lang.String sdram;
	/**flash大小*/
	@Excel(name = "flash大小", width = 15)
    @ApiModelProperty(value = "flash大小")
	private java.lang.String flash;
	/**nvram大小*/
	@Excel(name = "nvram大小", width = 15)
    @ApiModelProperty(value = "nvram大小")
	private java.lang.String nvram;
	/**cfcard内存大小*/
	@Excel(name = "cfcard内存大小", width = 15)
    @ApiModelProperty(value = "cfcard内存大小")
	private java.lang.String cfcardMem;
	/**设备启动时间*/
	@Excel(name = "设备启动时间", width = 15)
    @ApiModelProperty(value = "设备启动时间")
	private java.lang.String boardUpTime;
	/**用户数*/
	@Excel(name = "用户数", width = 15)
    @ApiModelProperty(value = "用户数")
	private java.lang.String userCount;
	/**IPV4用户*/
	@Excel(name = "IPV4用户", width = 15)
    @ApiModelProperty(value = "IPV4用户")
	private java.lang.String ipv4UserCount;
	/**IPV6用户*/
	@Excel(name = "IPV6用户", width = 15)
    @ApiModelProperty(value = "IPV6用户")
	private java.lang.String ipv6UserCount;
	/**MAC*/
	@Excel(name = "MAC", width = 15)
    @ApiModelProperty(value = "MAC")
	private java.lang.String macInUsed;
	
	@Excel(name = "用户ID", width = 32)
    @ApiModelProperty(value = "用户ID")
	private java.lang.String uid;	
	
	@Excel(name = "设备ID", width = 32)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String devid;	
}
