package org.smartbox.modules.devices.bean;

public class ChassisInfo {
    //序号
    private long id;
    //设备基本信息ID
    private long basicInfoId;
    //机框ID
    private long ChassisID;
    //机框类型
    private String chassisType;
    //机框名称
    private String chassisName;
    //设备类型
    private String deviceType;
    //总电源大小
    private String totalPower;
    //需要电源大小
    private String needPower;
    //
    private String remainPower;

}
