package org.smartbox.modules.devices.service;

import org.smartbox.modules.devices.entity.DevInterfaceInfo;
import org.smartbox.modules.devices.entity.DevSubboardInfo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 子卡表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface IDevSubboardInfoService extends IService<DevSubboardInfo> {
	IPage<DevSubboardInfo> selectUserPage(Page<DevSubboardInfo> page, String uid, String devid);
	IPage<DevSubboardInfo> selectSubPageByBid(Page<DevSubboardInfo> page, String uid, String boardid);

}
