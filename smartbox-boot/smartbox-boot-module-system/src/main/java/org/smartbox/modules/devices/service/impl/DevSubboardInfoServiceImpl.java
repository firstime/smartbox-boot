package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevSubboardInfo;
import org.smartbox.modules.devices.mapper.DevInterfaceInfoMapper;
import org.smartbox.modules.devices.mapper.DevSubboardInfoMapper;
import org.smartbox.modules.devices.service.IDevSubboardInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 子卡表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Service
public class DevSubboardInfoServiceImpl extends ServiceImpl<DevSubboardInfoMapper, DevSubboardInfo> implements IDevSubboardInfoService {

	@Autowired
	DevSubboardInfoMapper devSubboardInfoMapper;
	
	@Override
	public IPage<DevSubboardInfo> selectUserPage(Page<DevSubboardInfo> page, String uid, String devid) {
		// TODO Auto-generated method stub
	    return devSubboardInfoMapper.selectPageVo(page, uid,devid);
	}

	@Override
	public IPage<DevSubboardInfo> selectSubPageByBid(Page<DevSubboardInfo> page, String uid, String boardid) {
		// TODO Auto-generated method stub
		return devSubboardInfoMapper.selectPageByBid(page, uid, boardid);
	}

}
