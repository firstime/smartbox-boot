package org.smartbox.modules.system.service.impl;

import org.smartbox.modules.system.entity.SysUserAgent;
import org.smartbox.modules.system.mapper.SysUserAgentMapper;
import org.smartbox.modules.system.service.ISysUserAgentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户代理人设置
 * @Author: jeecg-boot
 * @Date:  2019-04-17
 * @Version: V1.0
 */
@Service
public class SysUserAgentServiceImpl extends ServiceImpl<SysUserAgentMapper, SysUserAgent> implements ISysUserAgentService {

}
