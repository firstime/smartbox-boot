package org.smartbox.modules.devices.service;

import org.smartbox.modules.devices.entity.DevBoxInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 采集盒表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface IDevBoxInfoService extends IService<DevBoxInfo> {

}
