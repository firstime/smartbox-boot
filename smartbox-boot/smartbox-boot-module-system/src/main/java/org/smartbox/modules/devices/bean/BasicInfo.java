package org.smartbox.modules.devices.bean;

public class BasicInfo {
    //序号
    private long id;
    //文件类型
    private Integer fileType;
    //文件名称
    private String fileName;
    //文件保存路径
    private String filePath;
    //文件大小
    private long fileSize;
    //日志收集时间
    private long collectTime;
    //设备启动时间
    private long deviceStartTime;
    //设备运行时间
    private long deviceRunTime;
    //设备名称
    private String deviceName;
    //设备类型
    private String deviceType;
    //设备版本
    private String deviceVersion;
    //设备补丁
    private String devicePatch;
    //是否有备板
    private boolean isBackupModel;
    //备板类型
    private String backUpType;
    //备板编码
    private String backupCode;
    //用户数
    private long userCount;
    //IP地址
    private String ipAddress;
    //设备信息描述
    private String deviceDesc;
    //license 状态
    private String licenseState;
    //产品名称
    private String productName;
    //产品版本
    private String productVersion;
    //产品序列号
    private String productSerialNo;
    //license创建时间
    private String createdTime;
    //license 到期时间
    private String expiredTime;
    //备注
    private String remarks;






}
