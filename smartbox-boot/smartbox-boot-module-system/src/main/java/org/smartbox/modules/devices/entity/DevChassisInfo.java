package org.smartbox.modules.devices.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 机框表
 * @Author: jeecg-boot
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Data
@TableName("dev_chassis_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="dev_chassis_info对象", description="机框表")
public class DevChassisInfo {
    
	/**序号*/
	@TableId(type = IdType.UUID)
    @ApiModelProperty(value = "序号")
	private java.lang.String id;
	/**设备基本信息表id*/
	@Excel(name = "设备基本信息表id", width = 15)
    @ApiModelProperty(value = "设备基本信息表id")
	private java.lang.String basicinfoId;
	/**机框id*/
	@Excel(name = "机框id", width = 15)
    @ApiModelProperty(value = "机框id")
	private java.lang.String boardId;
	/**机框类型1-master;2-slave*/
	@Excel(name = "机框类型1-master;2-slave", width = 15)
    @ApiModelProperty(value = "机框类型1-master;2-slave")
	private java.lang.String chassisType;
	/**机框名称*/
	@Excel(name = "机框名称", width = 15)
    @ApiModelProperty(value = "机框名称")
	private java.lang.String chassisName;
	/**设备类型*/
	@Excel(name = "设备类型", width = 15)
    @ApiModelProperty(value = "设备类型")
	private java.lang.String deviceType;
	/**电源大小*/
	@Excel(name = "电源大小", width = 15)
    @ApiModelProperty(value = "电源大小")
	private java.lang.String totalPower;
	/**需要电源大小*/
	@Excel(name = "需要电源大小", width = 15)
    @ApiModelProperty(value = "需要电源大小")
	private java.lang.String needPower;
	/**当前电源大小*/
	@Excel(name = "当前电源大小", width = 15)
    @ApiModelProperty(value = "当前电源大小")
	private java.lang.String remainPower;
	
	@Excel(name = "用户ID", width = 32)
    @ApiModelProperty(value = "用户ID")
	private java.lang.String uid;	
	
	@Excel(name = "设备ID", width = 32)
    @ApiModelProperty(value = "设备ID")
	private java.lang.String devid;	
}
