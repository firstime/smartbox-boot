package org.smartbox.modules.devices.service.impl;

import java.util.List;
import java.util.Map;

import org.smartbox.common.exception.JeecgBootException;
import org.smartbox.common.util.oConvertUtils;
import org.smartbox.modules.devices.bean.DevCounts;
import org.smartbox.modules.devices.entity.DevBasicInfo;
import org.smartbox.modules.devices.entity.DevDevices;
import org.smartbox.modules.devices.mapper.DevBasicInfoMapper;
import org.smartbox.modules.devices.mapper.DevDevicesMapper;
import org.smartbox.modules.devices.service.IDevDevicesService;
import org.smartbox.modules.devices.service.IDevInfoManageService;
import org.smartbox.modules.system.entity.SysCategory;
import org.smartbox.modules.system.mapper.SysCategoryMapper;
import org.smartbox.modules.system.model.TreeSelectModel;
import org.smartbox.modules.system.service.ISysCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 分类字典
 * @Author: jeecg-boot
 * @Date:   2019-05-29
 * @Version: V1.0
 */
@Service
public class DevInfoManageServiceImpl extends ServiceImpl<DevBasicInfoMapper, DevBasicInfo> implements IDevInfoManageService {

	@Autowired
	DevBasicInfoMapper devBasicInfoMapper;
	
	
	@Override
	public List<TreeSelectModel> queryListByCode(String pcode) throws JeecgBootException{
		String pid = ROOT_PID_VALUE;
		return null;
	}

	@Override
	public List<TreeSelectModel> queryListByPid(String pid) {
		if(oConvertUtils.isEmpty(pid)) {
			pid = ROOT_PID_VALUE;
		}
		return baseMapper.queryListByPid(pid);
	}

	@Override
	public List<Map<String,Object>> getDevCountsByUid(String uid) {
		// TODO Auto-generated method stub
		return baseMapper.getDevCountsByUid(uid);
		 
	}

}
