package org.smartbox.modules.devices.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.smartbox.modules.devices.entity.DevBoxInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 采集盒表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface DevBoxInfoMapper extends BaseMapper<DevBoxInfo> {

}
