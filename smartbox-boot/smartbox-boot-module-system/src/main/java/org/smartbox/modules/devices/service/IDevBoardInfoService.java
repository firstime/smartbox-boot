package org.smartbox.modules.devices.service;

import org.smartbox.modules.devices.entity.DevBoardInfo;
import org.smartbox.modules.devices.entity.DevInterfaceInfo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 单板表
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
public interface IDevBoardInfoService extends IService<DevBoardInfo> {
	IPage<DevBoardInfo> selectUserPage(Page<DevBoardInfo> page, String uid, String devid);

}
