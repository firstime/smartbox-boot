package org.smartbox.modules.devices.service.impl;

import org.smartbox.modules.devices.entity.DevInterfaceInfo;
import org.smartbox.modules.devices.mapper.DevInterfaceInfoMapper;
import org.smartbox.modules.devices.service.IDevInterfaceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 设备接口相关
 * @Author: smartbox
 * @Date:   2019-09-11
 * @Version: V1.0
 */
@Service
public class DevInterfaceInfoServiceImpl extends ServiceImpl<DevInterfaceInfoMapper, DevInterfaceInfo> implements IDevInterfaceInfoService {

	@Autowired
	DevInterfaceInfoMapper devInterfaceInfoMapper;
	
	public IPage<DevInterfaceInfo> selectUserPage(Page<DevInterfaceInfo> page, String uid,String devid) {
	    // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题，这时候你需要自己查询 count 部分
	    // page.setOptimizeCountSql(false);
	    // 当 total 为小于 0 或者设置 setSearchCount(false) 分页插件不会进行 count 查询
	    // 要点!! 分页返回的对象与传入的对象是同一个
	    return devInterfaceInfoMapper.selectPageVo(page, uid,devid);
	}

	
}
